import React from "react";

import "./BathTub.css";
import HTMLReactParser from "html-react-parser";

const BathTubCard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  return (
    <section className="section ">
      <div className="container has-text-centered">
        <h2 className="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>
        <h6 class="subtitle is-5 is-size-6-mobile">{questionSubTitle}</h6>

        <div className="is-flex is-flex-wrap-wrap is-justify-content-center">
          {Options.map((item, index) => {
            return (
              <div
                onClick={() => handleSelect({ item })}
                className=" all-option3 m-2"
                key={index}
              >
                <h3 className="num-img1">{item.option_title}</h3>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default BathTubCard;
