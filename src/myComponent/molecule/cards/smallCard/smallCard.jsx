import React, { useState } from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./smallcardesign.css";
const QuoteCard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);
  const [bgColor, setBgcolor] = React.useState("");
  const [showDescription, setShowDescription] = useState(false);
  const [descState, setDescState] = useState({});
  const [openDescriptionIndex, setOpenDescriptionIndex] = useState(null);
  const isShowing = () => {
    if (showing == true) {
      setShowingData(false);
      setBgcolor("");
    } else {
      setShowingData(true);
      setBgcolor("#f2761a");
    }
  };

  const toggleDescription = (optionTitle) => {
    setDescState((prevState) => ({
      ...prevState,
      [optionTitle]: !prevState[optionTitle],
    }));
  };
  return (
    <section class="section">
      <div class="container has-text-centered">
        <h2 class="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>

        <h5 class="subtitle is-5 is-size-5-mobile detl-txt">
          {modalPresent == "Yes" ? (
            <a
              class="is-underlined js-modal-trigger"
              data-target="modal-js-1"
              onClick={() => handelModal()}
            >
              <i class="fa fa-info-circle" aria-hidden="true"></i>{" "}
              {questionSubTitle}
            </a>
          ) : (
            <></>
          )}
        </h5>
        <div class="is-flex is-flex-wrap-wrap is-justify-content-center">
          {Options.map((item, index) => {
            const isDescriptionOpen = openDescriptionIndex === index;
            return (
              <div class="RectangleCard" key={index}>
                <div class="all-option">
                  <i className="card-main-icon">
                    <Svg>
                      <use
                        className="icon"
                        xmlns="http://www.w3.org/2000/svg"
                        id="fuel-lpg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        xlinkHref={item.option_icon}
                      />
                    </Svg>
                  </i>
                  <p
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      handleSelect({ item });
                    }}
                  >
                    {item.option_title}
                  </p>
                  <p
                    class="show-info"
                    onClick={() => setOpenDescriptionIndex(index)}
                  >
                    <i class="fa-solid fa-info"></i>
                  </p>
                </div>
                <div
                  className={`card__content ${
                    isDescriptionOpen ? "description-open" : ""
                  }`}
                  onClick={(e) => {
                    e.stopPropagation();
                    handleSelect({ item });
                  }}
                >
                  <p class="card__title">Information</p>
                  <p class="card__description">{item.option_discription}</p>
                </div>
                {isDescriptionOpen && (
                  <>
                    <p className="card__description is-hidden-tablet is-hidden-desktop">
                      {item.option_discription}
                    </p>
                  </>
                )}
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default QuoteCard;
