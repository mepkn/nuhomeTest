import React from "react";
//import styles from "./quoteCardSmall.module.css";
import styles from "./quoteCard.module.css";

const QuoteCardSmall = ({ title, image, description, handleSelect, price }) => {
	return (
		<section
			className={`${styles.section11} ${styles.hasTextCentered}`}
			id="Section1"
		>
			<div
				onClick={() =>
					handleSelect({ title, image, description, handleSelect })
				}
				className={`${styles.container} ${styles.isMaxDesktop}`}
			>
				<div className={`${styles.stepAnswers} ${styles.columns}`}>
					<div className={styles.column}>
						<div
							className={`${styles.box} ${styles.cardSmall}`}
							data-target="modal-js-example"
						>
							{price && <div className={styles.cardPrice}>{price}</div>}
							<div>
								{image && (
									<i className={styles.cardMainIcon}>
										<img
											className={styles.iconsSmall}
											src={`http://trueq.draftforclients.com/${image}`}
										/>
									</i>
								)}
								{title && image && (
									<div className={styles.cardSmallTitle}>
										<p>{title}</p>
									</div>
								)}
							</div>
							{title && !image && (
								<div className={styles.cardSmallText}>
									<p>{title}</p>
								</div>
							)}
							<span
								className={styles.cardSelect}
								onClick={() =>
									handleSelect({ title, image, description, handleSelect })
								}
							>
								Select
							</span>
							<span className={styles.cardSelect}></span>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default QuoteCardSmall;
