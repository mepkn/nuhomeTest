import React, { useState } from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./quoteCarddesign.css";

const QuoteCard = ({
	title,
	Options,
	handleSelect,
	questionSubTitle,
	handelModal,
	modalPresent,
}) => {
	const [showing, setShowingData] = useState(false);
	const [bgColor, setBgcolor] = useState("");
	const [descState, setDescState] = useState({});
	const [openDescriptionIndex, setOpenDescriptionIndex] = useState(null);

	const isShowing = () => {
		if (showing === true) {
			setShowingData(false);
			setBgcolor("");
		} else {
			setShowingData(true);
			setBgcolor("#f2761a");
		}
	};

	return (
		<section className="section">
			<div className="container has-text-centered">
				<h2 className="subtitle is-2 is-size-3-mobile mb-3">
					{HTMLReactParser(`${title}`)}
				</h2>

				<h5 className="subtitle is-5 is-size-5-mobile detl-txt">
					{modalPresent === "Yes" ? (
						<a
							className="is-underlined js-modal-trigger"
							data-target="modal-js-1"
							onClick={() => handelModal()}
						>
							<i className="fa fa-info-circle" aria-hidden="true"></i>{" "}
							{questionSubTitle}
						</a>
					) : (
						<></>
					)}
				</h5>

				<div className="is-flex is-flex-wrap-wrap is-justify-content-center">
					{Options.map((item, index) => {
						const isDescriptionOpen = openDescriptionIndex === index;

						return (
							<div className="RectangleCard" key={index}>
								<div className="all-option">
									<i
										className="card-main-icon"
										onClick={(e) => {
											e.stopPropagation();
											handleSelect({ item });
										}}
									>
										<Svg>
											<use
												className="icon"
												xmlns="http://www.w3.org/2000/svg"
												id="fuel-lpg"
												xmlnsXlink="http://www.w3.org/1999/xlink"
												xlinkHref={item.option_icon}
											/>
										</Svg>
									</i>
									<p
										onClick={(e) => {
											e.stopPropagation();
											handleSelect({ item });
										}}
									>
										{item.option_title}
									</p>
									<p
										className="show-info"
										onClick={() => setOpenDescriptionIndex(index)}
									>
										<i className="fa-solid fa-info"></i>
									</p>
								</div>
								<div
									className={`card__content ${
										isDescriptionOpen ? "description-open" : ""
									}`}
									onClick={(e) => {
										e.stopPropagation();
										handleSelect({ item });
									}}
								>
									<p className="card__title"><i class="fa-solid fa-circle-info"></i></p>
									<p>{item.option_discription}</p>
								</div>

								{isDescriptionOpen && (
									<>
										<p className="card__description is-hidden-tablet is-hidden-desktop">
											{item.option_discription}
										</p>
									</>
								)}
							</div>
						);
					})}
				</div>
			</div>
		</section>
	);
};

export default QuoteCard;
