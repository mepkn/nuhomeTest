import React from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
const PropertyCard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);
  const [bgColor, setBgcolor] = React.useState("");
  const isShowing = () => {
    if (showing == true) {
      setShowingData(false);
      setBgcolor("");
    } else {
      setShowingData(true);
      setBgcolor("#f2761a");
    }
  };

  return (
    <section className="section has-text-centered" id="Section1">
      <div className="container">
        <div className="step-title pb-1">
          <h1 className="title is-1" style={{ fontWeight: "bold" }}>
            {HTMLReactParser(`${title}`)}
          </h1>
        </div>
        <div className="step-subtitle pb-2">
          {modalPresent == "Yes" ? (
            <p
              onClick={() => handelModal()}
              className="modalTitle is-1"
              style={{ textDecoration: "underline", fontWeight: "bold" }}
            >
              <span className="help_icon">i</span> {questionSubTitle}
            </p>
          ) : (
            <p
              className="subtitle is-4  has-text-left-mobile has-text-weight-medium "
              style={{ fontSize: "1.25rem" }}
            >
              {questionSubTitle}
            </p>
          )}
        </div>
        <div
          style={{
            margin: "auto",
            display: "grid",
            gridTemplateColumns: "auto auto auto",
          }}
        >
          {Options.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => handleSelect({ item })}
                style={{ margin: "20px" }}
              >
                <div className="card m-15">
                  {/* <div>
                  <span className="card-price">{item.option_price}</span>
                  </div> */}
                  <div>
                    <i className="card-main-icon">
                      <Svg>
                        <use
                          className="icon"
                          xmlns="http://www.w3.org/2000/svg"
                          id="fuel-lpg"
                          xmlnsXlink="http://www.w3.org/1999/xlink"
                          xlinkHref={item.option_icon}
                          style={{ color: "#fb6058" }}
                        />
                      </Svg>
                    </i>
                  </div>
                  <div className="card-title">
                    <h3>{item.option_title}</h3>
                  </div>
                  <i className="card-info-icon">
                    <span className="help_icon">i</span>
                  </i>
                  <span className="card-select">Select</span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default PropertyCard;
