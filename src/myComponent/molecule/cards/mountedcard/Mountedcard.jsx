import React from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./Mounted.css";
const Mountedcard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);

  return (
    <section class="section">
      <div class="container has-text-centered">
        <h2 class="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>

        <h5 className="subtitle is-5 is-size-5-mobile detl-txt">
          {modalPresent === "Yes" ? (
            <a
              className="is-underlined js-modal-trigger"
              data-target="modal-js-1"
              onClick={() => handelModal()}
            >
              <i className="fa fa-info-circle" aria-hidden="true"></i>{" "}
              {questionSubTitle}
            </a>
          ) : (
            <>
              {" "}
              <h6 class="subtitle is-5 is-size-6-mobile">{questionSubTitle}</h6>
            </>
          )}
        </h5>
        <div class="is-flex is-flex-wrap-wrap is-justify-content-center">
          {Options.map((item, index) => {
            return (
              <div
                onClick={() =>
                  handleSelect({
                    item,
                  })
                }
                key={index}
                className=" all-option2 m-2 "
              >
                <img src={`/image/${item.option_icon}.svg`} />
                <p>{item.option_title}</p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Mountedcard;
