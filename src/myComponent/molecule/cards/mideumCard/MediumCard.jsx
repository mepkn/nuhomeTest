import React from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./mediumCard.css";
const MediumCard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);
  const [bgColor, setBgcolor] = React.useState("");
  const isShowing = () => {
    if (showing == true) {
      setShowingData(false);
      setBgcolor("");
    } else {
      setShowingData(true);
      setBgcolor("#f2761a");
    }
  };

  return (
    <section class="section">
      <div class="container has-text-centered">
        <h2 class="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>

        <div className="is-flex is-flex-wrap-wrap is-justify-content-center styl5">
          {Options.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => handleSelect({ item })}
                className=" all-option2 m-2"
              >
                <i className="card-main-icon">
                  <Svg>
                    <use
                      className="img"
                      style={{ color: "#fb6058" }}
                      xmlns="http://www.w3.org/2000/svg"
                      id="fuel-lpg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                      xlinkHref={item.option_icon}
                    />
                  </Svg>
                </i>
                <p>{item.option_subtitle}</p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default MediumCard;
