import React from "react";
import "./numofradiator.css";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";

const NumOfRadiatorsCard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  return (
    <section className="section">
      <div className="container  has-text-centered">
        <h2 className="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>
        <h6 className="subtitle is-5 is-size-6-mobile">{questionSubTitle}</h6>

        <div className="is-flex is-flex-wrap-wrap is-justify-content-center styl6">
          {Options.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => handleSelect({ item })}
                className="column is-one-third all-option2 m-2"
              >
                <i className="card-main-icon">
                  <Svg>
                    <use
                      xmlns="http://www.w3.org/2000/svg"
                      id="fuel-lpg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                      xlinkHref={item.option_icon}
                    />
                  </Svg>
                </i>

                <p>{item.option_subtitle}</p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default NumOfRadiatorsCard;
