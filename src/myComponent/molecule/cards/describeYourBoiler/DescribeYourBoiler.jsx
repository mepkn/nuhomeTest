import React, { useState } from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./describeYourBoiler.css";
const DescribeYourBoiler = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);

  return (
    <section class="section">
      <div class="container has-text-centered">
        <h2 class="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>
        <h6 class="subtitle is-5 is-size-6-mobile">{questionSubTitle}</h6>

        <div class="is-flex is-flex-wrap-wrap is-justify-content-center">
          {Options.map((item, index) => {
            return (
              <div
                class=" all-option2 m-2"
                onClick={() =>
                  handleSelect({
                    item,
                  })
                }
                key={index}
              >
                <img src={`/image/${item.option_icon}.svg`} />
                <p>{item.option_title}</p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default DescribeYourBoiler;
