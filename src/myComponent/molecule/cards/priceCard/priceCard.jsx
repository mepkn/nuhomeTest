import React, { useEffect } from "react";

import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./pricecard.css";
import { useDispatch, useSelector } from "react-redux";
import { GET_CURRENCY } from "../../../../scenes/get-a-quote/redux/GetAquoteAction";
const PriceCard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);
  const [bgColor, setBgcolor] = React.useState("");
  const { currencyPhoneEmail, isLoadingdynamicCurrrencyandPhone } = useSelector(
    ({ getBoilerQuote }) => getBoilerQuote
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: GET_CURRENCY });
  }, []);

  const isShowing = () => {
    if (showing == true) {
      setShowingData(false);
      setBgcolor("");
    } else {
      setShowingData(true);
      setBgcolor("#f2761a");
    }
  };

  return (
    <section className="section">
      <div className="container has-text-centered">
        <h2 className="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>
        <h6 className="subtitle is-5 is-size-6-mobile">{questionSubTitle}</h6>

        <div className="is-flex is-flex-wrap-wrap is-justify-content-center mstyl1 styl4">
          {Options.map((item, index) => {
            // if (index <=3) {
            return (
              <div
                key={index}
                onClick={() => handleSelect({ item })}
                className=" all-option2 m-2"
              >
                <span>
                  {" "}
                  {item.option_price !== 0 &&
                    `+ ${
                      currencyPhoneEmail.company_currency
                        ? currencyPhoneEmail.company_currency
                        : "£"
                    } ${item.option_price}`}
                </span>
                <i className="card-main-icon">
                  <Svg>
                    <use
                      // className="icon"
                      xmlns="http://www.w3.org/2000/svg"
                      id="fuel-lpg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                      xlinkHref={item.option_icon}
                      style={{ color: "#fb6058" }}
                    />
                  </Svg>
                </i>
                <p>{item.option_title}</p>
              </div>
            );
          })}
        </div>
        {/* <div className="step-answers columns">
          {Options.map((item, index) => {
            if (index >= 4) {
              return (
                <div
                  key={index}
                  onClick={() =>
                    handleSelect({
                      title,
                      Options,

                      handleSelect,
                    })
                  }
                  className="column is-one-quarter"
                >
                  <div className=" card card-is-4">
                    <span className="card-price">{item.option_price}</span>
                    <i className="card-main-icon">
                      <img
                        className="icon"
                        src={`http://trueq.draftforclients.com/${item.option_icon}`}
                      />
                    </i>
                    <div className="card-title">
                      <h3>{item.option_title}</h3>
                    </div>
                    <i className="card-info-icon">
                      <span className="help_icon">i</span>
                    </i>
                    <span className="card-select">Select</span>
                  </div>
                </div>
              );
            }
          })}
        </div> */}
      </div>
    </section>
  );
};

export default PriceCard;
