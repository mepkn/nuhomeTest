import React from "react";
import Svg from "../../../atom/image/Svg";
import HTMLReactParser from "html-react-parser";
import "./radiatorscard.css";
const Radiatorscard = ({
  title,
  Options,
  handleSelect,
  questionSubTitle,
  handelModal,
  modalPresent,
}) => {
  const [showing, setShowingData] = React.useState(false);
  const [bgColor, setBgcolor] = React.useState("");
  const isShowing = () => {
    if (showing == true) {
      setShowingData(false);
      setBgcolor("");
    } else {
      setShowingData(true);
      setBgcolor("#f2761a");
    }
  };

  return (
    <section className="section">
      <div className="container  has-text-centered">
        <h2 className="subtitle is-2 is-size-3-mobile mb-3">
          {HTMLReactParser(`${title}`)}
        </h2>
        <h6 className="subtitle is-5 is-size-6-mobile">{questionSubTitle}</h6>

        <div className="is-flex is-flex-wrap-wrap is-justify-content-center">
          {Options.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => handleSelect({ item })}
                className=" all-option3 m-2"
              >
                <i className="card-main-icon">
                  <Svg>
                    <use
                      className="num-img1"
                      xmlns="http://www.w3.org/2000/svg"
                      id="fuel-lpg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                      xlinkHref={item.option_icon}
                    />
                  </Svg>
                </i>

                <div className="card-title">
                  <p className="optionTitleForType">{item.option_subtitle}</p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Radiatorscard;
