const data = [
  {
    id: 1,
    title: "Boiler & Pipeline work installation.",
    description:
      "We'll install your new boiler and won't leave until it's working and tested. We know every job is different so we include all pipework alterations to accommodate your new boiler within your fixed price. We will also install your condensate pipe, and upgrade the pipe from your gas meter to boiler, if required.",
  },
  {
    id: 2,
    title: "Removal and save disposal.",
    description:
      "Your price includes removal and recycling of your old boiler, as well as removal of any debris that occurs as part of the installation. Packaging is disposed of in your normal domestic recycling bin.",
  },
  {
    id: 3,
    title: "Removal of old tanks",
    description:
      "The engineers will remove your existing hot water cylinder and rework the pipework to allow a combi boiler to be installed. Any water storage tanks in the loft, which are decommissioned as part of the work, will be left in place unless the engineer is requested to remove them whilst on site.",
  },
  {
    id: 4,
    title: "Boiler & Pipeline work installation.",
    description:
      "We'll install a new magnetic filter to attract and retain particles from the water going round your heating system and boiler. Once your boiler has been changed and your system is refilled with fresh & clean water, we'll add a chemical inhibitor, which is formulated to prevent corrosion within your radiators and pipework.",
  },
  {
    id: 5,
    title: "New flue and any brickwork required.",
    description:
      "We will provide and install a brand new flue for your boiler up to 3 meters in length. If your flue is over 3m in length we suggest calling us on 0800 193 7777 prior to ordering.",
  },
  {
    id: 6,
    title: "New Thermostat.",
    description:
      "Your price includes a new thermostat & installation. You will be able to choose from a range of compatible thermostats once you have selected your boiler, including upgrading to any smart thermostats that we have available.",
  },
  {
    id: 7,
    title: "Chemical system flush.",
    description:
      "BOXT carries out a chemical flush as part of every boiler installation. This process involves adding a chemical solution to the system, which breaks down any built-up sludge and debris, and then using fresh water and natural flow pressure to flush it out. By cleaning built-up sludge from your radiators and pipes, the system will operate more efficiently and help keep your brand-new boiler in top condition for longer.",
  },
];

export default data;
