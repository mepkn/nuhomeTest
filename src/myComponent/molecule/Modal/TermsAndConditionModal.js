import React from "react";
import { useSelector } from "react-redux";
import DOMPurify from "dompurify";

const TermsAndConditionModal = ({ termAndCondition, setTermAndCondition }) => {
  const { getTermConditionData, isgettingTermAndCOndition } = useSelector(
    (state) => state.getTermAndCondition
  );
  if (isgettingTermAndCOndition) {
    return <div className="mainLoader"></div>;
  }
  return (
    <>
      {termAndCondition && (
        <div className="quotesContainer has-background-dark">
          <div className="saveQuoteModal">
            <h1 className="title is-4 is-underlined has-text-danger">
              {" "}
              Terms and Conditions
            </h1>
            {getTermConditionData &&
              getTermConditionData.map((item, index) => (
                <div key={index}>
                  <h2 className="subtitle is-6">{item.title}</h2>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: DOMPurify.sanitize(item.terms_page),
                    }}
                  />
                </div>
              ))}

            <button
              className="button is-danger "
              onClick={() => setTermAndCondition(false)}
            >
              close
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default TermsAndConditionModal;
