import React from "react";
import { useSelector } from "react-redux";

const PrivacyPolicyModal = ({ privacypolicydata, setPrivacypolicyData }) => {
  const { privacyPolicyData, isgettingPrivacyPolicyLoading } = useSelector(
    (state) => state.getTermAndCondition
  );

  if (isgettingPrivacyPolicyLoading) {
    return <div className="mainLoader"></div>;
  }
  return (
    <>
      {privacypolicydata && (
        <div className="quotesContainer has-background-dark">
          <div className="saveQuoteModal">
            <h1 className="title is-4 is-underlined has-text-danger">
              {" "}
              Privacy Policy
            </h1>
            <div className="column">
              {privacyPolicyData &&
                privacyPolicyData.map((item, index) => {
                  return (
                    <div>
                      <h1 className="subtitle is-6">{item.terms_page}</h1>
                    </div>
                  );
                })}
              <button
                className="button is-medium is-danger mt-4 "
                onClick={() => setPrivacypolicyData(false)}
              >
                close
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default PrivacyPolicyModal;
