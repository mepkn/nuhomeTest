import React, { useState } from "react";
import { format, isBefore, isSameDay } from "date-fns";
import { enGB } from "date-fns/locale";
import { DatePickerCalendar } from "react-nice-dates";
import "react-nice-dates/build/style.css";

function CalenderComponent({ date, setDate }) {
  // Function to determine if a date is disabled (i.e., in the past)
  const isDisabledDate = (day) => {
    return isBefore(day, new Date());
  };

  // Function to add the "disabled" modifier to past dates
  const disabledModifier = (day) => {
    return isDisabledDate(day) ? "disabled" : undefined;
  };

  return (
    <div className="columns is-flex is-flex-direction-column">
      <div className="column box">
        <DatePickerCalendar
          date={date}
          onDateChange={setDate}
          locale={enGB}
          modifiers={{ disabled: disabledModifier }} // Apply the disabled modifier to past dates
        />
      </div>
    </div>
  );
}

export default CalenderComponent;
