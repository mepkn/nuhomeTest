import React from "react";
import { useForm, Controller } from "react-hook-form";
import { useDispatch } from "react-redux";
import "./modal.css";
import { SAVE_BOOKING_MAIL } from "../../../scenes/products/redux/productActions";

const SaveQuoteModal = ({
  saveaQuoteModal,

  id,
  handleCancel,
}) => {
  const dispatch = useDispatch();
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      checkbox: false,
      lastname: "",
      FirstName: "",
      email: "",
      phoneNumber: "",
    },
  });

  const onSubmit = (data) => {
    reset();
    const payload = {
      first_name: data.FirstName,
      last_name: data.lastname,
      email: data.email,
      phone: data.phoneNumber,
    };

    if (id) {
      payload.product_id = id;
    }

    dispatch({
      type: SAVE_BOOKING_MAIL,
      payload,
    });
  };

  return (
    <>
      {saveaQuoteModal && (
        <div className="quotesContainer has-background-dark">
          <div className="saveQuoteModal">
            <div className="column">


              {/* Modal Title */}
              <div className="modal-text-box ">
                <h1 className="modal-heading"> Send my quotee </h1>
                <p className="modal-sub-heading"> Upgrade your heating system with our personalized quote. Fill
                  out the form Get a Free Boiler Quotes Now!{" "} </p>
              </div>




              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="columns">
                  <div className="column is-6 mx-1">
                    <h5 className="title is-6 has-text-left is-size-6-mobile">
                      First Name
                    </h5>
                    <Controller
                      name="FirstName"
                      control={control}
                      rules={{ required: "first Name is required" }}
                      render={({ field }) => (
                        <input
                          className="input formInput is-hovered is-size-6-mobile"
                          {...field}
                        />
                      )}
                    />
                    {errors.FirstName && (
                      <span className="is-size-7-mobile has-text-danger">
                        {errors.FirstName.message}*
                      </span>
                    )}
                  </div>
                  <div className="column is-6">
                    <h5 className="title is-6 has-text-left is-size-6-mobile">
                      Last Name
                    </h5>
                    <Controller
                      name="lastname"
                      control={control}
                      rules={{ required: "Last Name is required" }}
                      render={({ field }) => (
                        <input
                          className="input formInput is-hovered is-size-6-mobile"
                          {...field}
                        />
                      )}
                    />
                    {errors.lastname && (
                      <span className="is-size-7-mobile has-text-danger">
                        {errors.lastname.message}*
                      </span>
                    )}
                  </div>
                </div>
                <div className="columns">
                  <div className="column is-half">
                    <h5 className="title is-6 has-text-left is-size-6-mobile">
                      Email
                    </h5>
                    <Controller
                      name="email"
                      control={control}
                      rules={{
                        required: "Email is required",
                        pattern: {
                          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                          message: "Invalid email address",
                        },
                      }}
                      render={({ field }) => (
                        <input
                          className="input formInput is-hovered is-size-6-mobile"
                          {...field}
                        />
                      )}
                    />
                    {errors.email && (
                      <span className="is-size-7-mobile has-text-danger">
                        {errors.email.message}*
                      </span>
                    )}
                  </div>
                  <div className="column is-half">
                    <h5 className="title is-6 has-text-left is-size-6-mobile">
                      Phone
                    </h5>
                    <Controller
                      name="phoneNumber"
                      control={control}
                      rules={{
                        required: "Phone Number is required",
                        pattern: {
                          value: /^[6-9]\d{9}$/gi,
                          message: "Invalid Phone Number",
                        },
                      }}
                      render={({ field }) => (
                        <input
                          className="input formInput is-hovered is-size-6-mobile"
                          {...field}
                        />
                      )}
                    />
                    {errors.phoneNumber && (
                      <span className="is-size-7-mobile has-text-danger">
                        {errors.phoneNumber.message}*
                      </span>
                    )}
                  </div>
                </div>
                <div class="field">
                  <div class="control has-left is-flex">
                    <Controller
                      name="checkbox"
                      control={control}
                      rules={{ required: "please check the field" }}
                      render={({ field }) => (
                        <input
                          type="checkbox"
                          className="is-size-7-mobile"
                          {...field}
                        />
                      )}
                    />
                    <p className="is-size-7-mobile ml-2">
                      {" "}
                      I agree to the{" "}
                      <a href="#" className="has-text-left has-text-danger">
                        terms and conditions
                      </a>
                    </p>
                  </div>
                  {errors.checkbox && (
                    <span className="is-size-7-mobile has-text-danger">
                      {errors.checkbox.message}*
                    </span>
                  )}
                </div>

                <div className="field is-grouped is-flex-direction-row is-justify-content-space-between">
                  <div className="control">
                    <Controller
                      name="submit"
                      control={control}
                      render={({ field }) => (
                        <button className="button formInput is-success is-fullwidth">
                          Save My Quotes
                        </button>
                      )}
                    />
                  </div>

                  <div class="control">
                    <button
                      onClick={handleCancel}
                      class="button is-danger formInput"
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default SaveQuoteModal;
