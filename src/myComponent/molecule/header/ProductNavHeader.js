import React from "react";
import Products from "../../../scenes/products/Products";
import ChooseControls from "../../../scenes/choose-controls/ChooseControls";
import AddSmartDevice from "../../../scenes/addSmartDevice/AddSmartDevice";
import OrderSummary from "../../../scenes/order/OrderSummary";
import { ROUTES } from "../../../utils/constants";

const ProductNavHeader = ({
  handleNavPress,
  handleBackPress,
  router,
  setShowMenu,
  route,
  showMenu,
}) => {
  const { products, orderSummary, controls, addSmartDevice, infoForm } = ROUTES;
  return (
    <section>
      <nav className="navbar navbar-fix" role="navigation">
        <div className="navbar-menu is-active">
          <div className="navbar-center">
            <div className="navbar-item">
              <div className="buttons">
                <a
                  className="button is-primary"
                  onClick={() => handleBackPress()}
                >
                  <strong>Back</strong>
                </a>
              </div>
            </div>
            <div
              role="button"
              className="navbar-burger"
              aria-label="menu"
              // aria-expanded="false"
              data-target="topbarmenu"
              onClick={() => setShowMenu((val) => !val)}
              style={{
                height: "auto",
                width: "auto",
                position: "unset",
                backgroundColor: "purple",
              }}
            >
              {showMenu ? (
                <div className="navbar">
                  <button
                    id="nav01"
                    className="button is-white"
                    onClick={() => handleNavPress(products)}
                  >
                    Choose boiler
                  </button>
                  <button
                    id="nav02"
                    className="button is-white"
                    onClick={() => handleNavPress(controls)}
                  >
                    Choose controls
                  </button>
                  <button
                    className="button is-white"
                    id="nav04"
                    onClick={() => handleNavPress(AddARadiator)}
                  >
                    Add smart devices
                  </button>
                  <button
                    id="nav03"
                    className="button is-white"
                    onClick={() => handleNavPress(orderSummary)}
                  >
                    Select install date
                  </button>
                </div>
              ) : (
                <>
                  {" "}
                  <span
                    aria-hidden="true"
                    style={{ left: "calc(90% - 8px)" }}
                  ></span>
                  <span
                    aria-hidden="true"
                    style={{ left: "calc(90% - 8px)" }}
                  ></span>
                  <span
                    aria-hidden="true"
                    style={{ left: "calc(90% - 8px)" }}
                  ></span>
                </>
              )}
            </div>
          </div>

          <div id="topbarmenu" className="navbar-menu">
            <div className="navbar">
              <a
                id="nav01"
                className="navbar-item is-active "
                onClick={() => handleNavPress(products)}
              >
                1. Choose boiler
              </a>

              <a
                id="nav02"
                className="navbar-item "
                onClick={() => handleNavPress(controls)}
              >
                2. Choose controls{" "}
              </a>
              <a
                className="navbar-item"
                id="nav04"
                onClick={() => handleNavPress(addSmartDevice)}
              >
                3. Add smart devices
              </a>
              <a
                id="nav03"
                className="navbar-item "
                onClick={() => handleNavPress(orderSummary)}
              >
                4. Select install date
              </a>
            </div>
          </div>
        </div>
      </nav>
    </section>
  );
};

export default ProductNavHeader;
