import React from 'react'
// import SingleCard from './SingleCard'
import QuoteCard from '../molecule/cards/QuoteCard'

const Card = () => {
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-lg-4">
                        <QuoteCard />
                    </div>
                    <div className="col-lg-4">
                        <QuoteCard />
                    </div>
                    <div className="col-lg-4">
                        <QuoteCard />
                    </div>
                </div>
            </div>


        </div>
    )
}

export default Card