import React, { useEffect } from "react";
import SingleCard from "./SingleCard";
//import Services from "../Api/Serices";
import { useDispatch, useSelector } from "react-redux";
import { GET_BOILER_QUOTE } from "../../scenes/get-a-quote/redux/GetAquoteAction";

const Index = (props) => {
  const dispatch = useDispatch();
  const { getBoilerQuoteData } = useSelector(
    ({ getBoilerQuote }) => getBoilerQuote
  );
    
  useEffect(() => {
    dispatch({ type: GET_BOILER_QUOTE });
  }, []);

  return (
    <div>
      <section class="section has-text-centered" id="Section1">
        {getBoilerQuoteData.Question1.slice(0, 1).map(
          (boilerDataInfo, index) => {
            // const { id, Num_Option } = boilerDataInfo;
            return (
              <>
                <div class="container is-max-desktop" key={boilerDataInfo.id}>
                  <div class="step-title">
                    <h1 class="title" style={{marginTop:'50px'}}>{boilerDataInfo.QuestionTitle}</h1>
                  </div>
                  <div class="step-subtitle">
                    <p class="subtitle">
                      <span class="help_icon">i</span>{" "}
                      {boilerDataInfo.QuestionSubTitle}
                    </p>
                  </div>

                  <div
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    {boilerDataInfo.Options.map((option) => (
                      <div style={{ flexBasis: "25.3%" }}>
                        <SingleCard
                          title={option.option_title}
                          description={option.opstion_discription}
                          image={option.opstion_icon}
                        />
                      </div>
                    ))}
                  </div>
                </div>
              </>
            );
          }
        )}
      </section>
    </div>
  );
};

export default Index;
