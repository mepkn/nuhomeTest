import React from "react";

const SingleCard = ({ title, image, description, handleSelect }) => {
  return (
    <div>
      <section className="section11 has-text-centered" id="Section1">
        <div className="container is-max-desktop">
          <div className="step-answers columns">
            <div className="column">
              <div className="box card" data-target="modal-js-example">
                {image && (
                  <i className="card-main-icon">
                    <img
                      className={description ? "icon" : "icons"}
                      src={`http://trueq.draftforclients.com/${image}`}
                      alt="img"
                    />
                  </i>
                )}
                {title && (
                  <div className={ image ? "card-title" : "card-title-text"} >
                    <p>{title}</p>
                  </div>
                )}
				 
                <i className="card-info-icon">
                  <span className="help_icon">i</span>
                </i>
                <span
                  className="card-select"
                  onClick={() =>
                    handleSelect({ title, image, description, handleSelect })
                  }
                >
                  Select
                </span>
                {description && <p className="card-info">{description}</p>}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default SingleCard;
