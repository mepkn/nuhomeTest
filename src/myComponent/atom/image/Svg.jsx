import React from 'react'

function Svg({children}) {
  return (
    <svg className="icon"	 xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    <defs>
      <filter id="boxt-buyfilter0_d" x="27" y="4" width="54" height="54" filterUnits="userSpaceOnUse"
        color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="3" />
        <feGaussianBlur stdDeviation="3.5" />
        <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <path id="bubbles-1a" d="M0 0h90v119H0z" />
      <path id="bubbles-2a" d="M0 0h90v119H0z" />
      <path id="bubbles-3a" d="M0 0h90v119H0z" />
      <ellipse id="burstb" cx="28.983" cy="28.857" rx="21.784" ry="21.689" />
      <ellipse id="burstd" cx="28.983" cy="28.857" rx="14.959" ry="14.895" />
      <filter x="-13.8%" y="-13.8%" width="127.5%" height="127.7%" filterUnits="objectBoundingBox" id="bursta">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-20.1%" y="-20.1%" width="140.1%" height="140.3%" filterUnits="objectBoundingBox" id="burstc">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <path id="chevron-downa" d="M5.085 7L4 8.036l6.543 6.854 6.542-6.854L16 7l-5.457 5.717z" />
      <path id="epa-heat-aa" d="M75.342 46.436h75.341V0H0v46.436z" />
      <path id="epa-heat-ac" d="M0 46.436h150.683V0H0z" />
      <path id="epa-heat-ba" d="M75.342 46.436h75.341V0H0v46.436z" />
      <path id="epa-heat-bc" d="M0 46.436h150.683V0H0z" />
      <path id="epa-water-aa" d="M75.342 46.436h75.341V0H0v46.436z" />
      <path id="epa-water-ac" d="M0 46.436h150.683V0H0z" />
      <path id="epa-water-ba" d="M75.342 46.436h75.341V0H0v46.436z" />
      <path id="epa-water-bc" d="M0 46.436h150.683V0H0z" />
      <ellipse id="heatc" cx="237.402" cy="237.007" rx="178.403" ry="178.11" />
      <ellipse id="heatf" cx="237.402" cy="237.007" rx="122.513" ry="122.311" />
      <ellipse id="heath" cx="237.402" cy="237.007" rx="82.815" ry="82.679" />
      <ellipse id="heatj" cx="237.402" cy="237.007" rx="54.195" ry="54.106" />
      <ellipse id="heatl" cx="237.402" cy="237.007" rx="31.348" ry="31.297" />
      <ellipse id="heatn" cx="237.402" cy="237.007" rx="15.966" ry="15.94" />
      <filter x="-4.2%" y="-4.2%" width="108.4%" height="108.4%" filterUnits="objectBoundingBox" id="heatb">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-6.1%" y="-6.1%" width="112.2%" height="112.3%" filterUnits="objectBoundingBox" id="heate">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-9.1%" y="-9.1%" width="118.1%" height="118.1%" filterUnits="objectBoundingBox" id="heatg">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-13.8%" y="-13.9%" width="127.7%" height="127.7%" filterUnits="objectBoundingBox" id="heati">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-23.9%" y="-24%" width="147.8%" height="147.9%" filterUnits="objectBoundingBox" id="heatk">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-47%" y="-47.1%" width="193.9%" height="194.1%" filterUnits="objectBoundingBox" id="heatm">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <linearGradient x1="0%" y1="50%" x2="49.669%" y2="50%" id="heata">
        <stop stop-color="#F5AD8C" offset="0%" />
        <stop stop-color="#FB6058" offset="49.669%" />
        <stop stop-color="#F98671" offset="100%" />
      </linearGradient>
      <linearGradient x1="0%" y1="50%" x2="100%" y2="50%" id="heatd">
        <stop stop-color="#F5AD8C" offset="0%" />
        <stop stop-color="#F98671" offset="50.265%" />
        <stop stop-color="#FB6058" offset="100%" />
      </linearGradient>
      <clipPath id="heating_and_hot_waterclip0">
        <path fill="#fff" d="M0 0h164v164H0z" />
      </clipPath>
      <ellipse id="howitworks-1b" cx="79.953" cy="79.99" rx="60.094" ry="60.122" />
      <ellipse id="howitworks-1d" cx="79.953" cy="79.99" rx="41.267" ry="41.287" />
      <filter x="-12.5%" y="-12.5%" width="125%" height="124.9%" filterUnits="objectBoundingBox" id="howitworks-1a">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0" in="shadowBlurOuter1" />
      </filter>
      <filter x="-18.2%" y="-18.2%" width="136.3%" height="136.3%" filterUnits="objectBoundingBox" id="howitworks-1c">
        <feOffset in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="5" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.1 0" in="shadowBlurOuter1" />
      </filter>
      <path id="installera" d="M16.089.117v28.22H.247V.117H16.09z" />
      <path id="installerc" d="M.03.126h36.85v16.887H.03V.126z" />
      <path id="installere" d="M0 112.888h66.804V.167H0z" />
      <path id="logo-gassafe-blacka" d="M.06.667H284V308H.06z" />
      <path id="logo-mark-texta" d="M0 60.587h51.092V0H0z" />
      <pattern id="logo-refcompattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
        <use xlinkHref="#logo-refcomimage0" transform="matrix(.005 0 0 .01103 0 -.577)" />
      </pattern>
      <image id="logo-refcomimage0" width="200" height="200"
       xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAkFBMVEX///8qdJ+cscwtdqDf6O6YrsoebZr///7O2OVglrYkb5ukvM/F09/7/P2WrMmasMu60uAzeqPd5O3s8PX09vmywtepu9LE0OBTi67x8vWhtc4XaJfM1+Szw9e9ytza4etEgaeVtMqIrcVAfqWnxNV/p8F3n7vF2ONYj7Fvmriyx9ejwdNimLcMYZKNpcRNiKyA4+EEAAANpUlEQVR4nO1b6ZqiuhYNBlKIBhVwQgWVcvZc3//t7t4ZmAzVZXX3PVX3y/rRbRFJ9soeM0iIhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhcW/Au5+FXmeu27MCTd3vHTdCL/xOhLVQ2xq7OKR9J0JfR1OwCbn+24t++VJEsexGwHg/ySR1OL8eugXjDmUvjZEMZCy7YOnpolzMKuDjFfOi6AOyLY9vOcoLo+jdbaZzmZpmoaIXtqbzaabbJ272P7mjvcLh60C+soQ7CBku64MbwVGnSCRl4YAVaxofy9IxPl81uv1Rl7oeV6vAvyFj3q9WeYLNu7gvl0F+PInB6GrPcoWPb9AnckfIEJpwJzHIBd2s56lnlfKbAS2zTbzGL9+vaAiP40gB9niR2AQ4reJoDK2Bx+mmAujAi9P4igXltULQ2/UQcYbpRlq0N0tAvSXT43FjiDb8u8QYcHiBpMbR2gsOfr1UoeXJAY+aW/UwQaMb5bBa3wMavmcgQWPJfkrRGgQ9K+oDLIJsyWJp6EUebbJ5nkUizCV5PNp2mFooJjZGqYhOkw+ScX/C0Qo0hiCpGhP09BLYRC/54m5BjeAUDWbKr9OIggAZi6jMM1cpEI/QYWuxvDVwvDFrxMBq2ZnoBFnOfEjMvXSkbeJSTyriQtkvF46nedLEQemvbBDL1OwMPfg/NLtBZExM8j2ZSJAY3tFGl5GYg+JoEQpRJX5aNSWMwQyqJnYn42M/iKp+Hf2i8RC2YmQm0m2rxIBO9gtSTJPwylJZv9RRMCmMk7y9HneIUiBY2OQylKjWkJvCqJctx8qhToF8L0YXOSLREAdfUgbPqSMNCEbL9REQOKZiz5vdIZwNofhEn9m9BZvBOEi2TsfKSVYQESZGEX6ChHKJlD2RFOwEg8cxEvDiJeuMRqBz2cmJiJITX0IznkHlXRNyHrRrRTK7oQMTAr5GhF2jkiSjUCWMCMu2LwX85qPo3n5o468Dv6COb2DSjiLCP9AKQFMYN/k618gAjF3B+qYhcKOuGCQNojA45hEqVkpqJZeJqiYvuB5c0KGW6OsUtxTZ8trRCgrxoRncj5HLpmLD00iYCOgsllXrQUTj1T43BAUUCngZBezebELIUczyReJUMrOMI6aTJi9SAbTFhFw3DWw7dIJfqE3T0i8CQ2x2OuBk+0CU9ILrmTYYXavaoQduMrelWFJ02qJBN6jtNWllRnEPVOkxndh0UGfJQ4eCTl3WN2LRNA95jqlQRrUosZJ2p7bcAOUO2pf+ToWAsupMXyBk62LJyZsACGr23teIXIlyUaP7MmIJTSyjM3S5L0PmEgjWvcMVNDJ3EVz8mmw5bGpzHqVCA0mw3oplS7LLNhbLo3z6hLXJGYJqM4SiG8mJpCNls1AS9l7R+h9jQjwWBO34hH6YDla4sSgEZxx1yxmBUwcsTkQr0nSZ/XxF2TQyePzRASPmlTejJR+MZrxODXOeC9vlsPmqedmR5kTXo+1ge8bc/prRBSPyuJHladj+DITAdv5JZNeCCkw62DydtFMKDvExe8ToU6Lh7chlTV1E+lhMbb8FZOsi0mG1kXlRG6j84c1fscWnVuwoAbmDCEChSNPYxTDmlD9OQqByCj0TICS10edfIhwmkCZaXp5jkykCIPLKugGWyRmIiQfNDAk3F/7JdY5SeDP9Vr9xRO/C+s1pPB1Z7P8TtTsvmqAJacU4JrvBh8h7uDxbdCxg/xKD03IJx3fqP/x9K5+8Pzex8O2nrQFepbvFdbmwV8DN378TONv4a2Bj9pqXzA01N7l0fj9druezGbtnrBxnLec19RR+1kcdYQuTgbnfgOX/TDRbbdWm8B5D23J/en5/XxIxFsk2p+L1YqxVbDtD5LmcMTdnbcMG9lkcchrWrnWBsvlo7w96qEoJrqxTaT/T8CaCHATSOC5DbDCnf/8uSH450zw7Cc+OOJkRJ6lsMewkpWTZE91ozikOMZl62FV9TWUj46rxqi8v7oM9oUTmZgsF09nKo6z2nW1AdgNDzAcQ8MOhYJFrEPLDWvcqnyvRjvhXlDZhou4rauY8Eety6Fg7deeQDlJbmx/uV92k4tJIbkxhwZDdHpjG26gcXIw1XZDwbCVnEEtYz3r16CduanYs0a4tNGTUEhtEAoGddnmxTU47rfPmZGTd1WoNMsVdqy3NTluwYPfjMU2+OFQ90RLrWhZedVYVxk7SJrDek+Z8JD6qA8Y9XyJttutO9g+xxBOdloiUQaUfArsfGeSlp1xY9SwWw6LIhJv9ZQHDNxdybp6F7K65bZbvRHnmgtBal0N8IWaQsRuFznQ4eNWvPUfprDdVxJtLxCw+uWbk7jW1jiTFAcxJ82j1hCAO5YWxxa395ve+AykUZdlLjsP3m9H3blyyLqOBZFTzQwp28OTNTvv+ODAdgYepWVKq3T1TGPJnOjJ3TVOi+PK6IKt32gojSHYiUnTO4aTBA2rnPKBaLypkYKzCiwtjfQb+0Ui9u1YcVmsLgaFRBtttCrPvFdDE1+TXD+9txfTR1m/+VgrBOdMlBw6EkU1/TI8gsb64a6+PVni8efEaWiEDJvGK+U7Hc6X65M0hK/jq9AfLvpl71f5eoASXrV2nmdATh8qvNZb6TnBQr9x/GclAPp2tZFcpHPzqn+3Wh1NxIqk2jalW9nj9qNqJs5cNYeUqcN4FY3o6lYLsUW9lh6LN/XkXd7182tSHs9o5wa4Y4mYVM681o3aEIW3q8AyOTCpkXEg5ZJzo+XjxFCgRVOw9rMisuPL5dIdS/WDgjBg6o0yWlvdrESwXJc2UD7HrH5jynNMJZY0JFppq+GfbzIwBA+x+RDsYHAx9HXiqIntRD6D4dytUm+xeDwesGSU2qeY88q2BsZogAM9u1U2wKhylIPXPacswdXxpkr/Ar7uE3JdIkMz26OJ0dUhZ1IhV/XW+FkRupc0rofRWhYJ2NlHaU9dp0a8CqQVMOopfeoU19S/3HermR3Jan1G8tPq/SSIHFUOcQ/SdYrOGzV5z23k7ppgxyEXGcpwjkedQhS47d1ZKqIKL+RfbPBEBKZFKTE46dqqzLcYIdXMr04nMepEKIhd3sRIlJ07eYSyFj6UNlIWFI4u4w6GfX/c9AejK9rPA6wElHmovNwiIqMSdSZRSUSFX4iQKrBAaxzVez0tC6njY4dl5WEmO3uo7CnKcC2srMnKNjqpgHGRV4fHZZNzxDcUkcoNdCKNn4lwrFhUoBHZDz8HC/3Ukb6mRgrezUTiUSrrxzKMnmGddNZn4IHI8mVMuZKkgkzYqvtFzBvPlY+sLnrYfiCrGog5bdOqLAuHkxkIK6q8SozQsJPmTY1rKZKknlqa6DBKl7CmTPxzUDMNXY0a9vd0dLobn4vbJFyuJ5SKTjVnv6lkEGlDxO2qkyQFyonLWImBWlVrE/OO1kYblk5SgYqXORWDoa51m8z5zZUzVyVuLZJKqPBAVVWXnFVWeiS1WkolGR3cqZg2HViG1ffw7CqRzhgYVlIAH3fQJe46HUpeSsGrO6lVQkVzYc7LYkMXmhr1gL1zebJWCqZyYnTIDs7DhMdXfRgaCI/cq7/c8joNxeVC3piYFuI03KiPesp0bFfqFzOt2hwZCMqVM8bBuTa6dgLnC115MwgDjpJILAl4WQTSAIIrFAuqEead69oNtV+GdnQsHZSvJl/PwpF2HVcfDuXSpk81H6mcrrEYQY6q9A3a65uyRmksNykbtFg2GrEiILGif6isRGhKBWVRjLXhjnoqZJUVJ16aILUrkYKI8WAOb+7oUFkWcnU8n2PSlf6a31pPiz6OommoWKEJqbIVFaK6U/K1FeKNpvrzTr2jo48u+gb1JXBdKBEHddWk4k8DceseAHVWFd321gMN5Fzo2k1Ok9SIWOe7Kh22oyNiKU9dJC7lqkJGRBk75N/G+zlibnJtFSeTwvGaiSgTxD+MVn4KGp80G4t31aACQYE1xXFFdZYcl0H5eSAIWaGvPifqeEgmQKJvAmPUgnLDdBeP3evLXEOxDkFiV2B4CHADzyn2zSQU74tANwbbne4gkZds2DkRRLDzAkvaXRmUn5GFNSJDhaT1wOdVWxMYJlz1ed1RWCfDw73/ON/v+2HcXAahW2PjYtG/i41Z1cjXVe8krz5GLfnqg0w9PLf7+1DXUM1b+0nX0dML/c88PLb7y/jwKIOTDxo/DTzQHKV/+Fji34A4mfWMu9k/CzEeP4fTX3/xu0NeFft/UEkmr/f9ftj4t5GLmw3h9Mf7O5d3ZvAC2w9Hru74TX+8dW3Urcv0p3t8ou+PevMfrpTySpKX+j/bU5Lytj5QMS2+fgz4fKSuGOJvbaIX1cL5Mv42Ril+hqCoeLN59Fm98CX+BnT5Jy7e/Cnk0/K3kCPxc5x8+bF0PInyeebn3+/6V5TNyh/dAZdRL93M/ciNm8senizjKPezDf4q7PuYVBNJNBc/IcRfVYj6PgzDUYo/uJ1uMsR0Ok2hJeyBwuJvZE4G8BgnW/xqGK9KilCGfML/IIAV6CH6pnp4hvi96jwDRaQC080mm6+FmX1vNXTDcBHxp1KxsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsPhf4L+QMxuLpx9VPwAAAABJRU5ErkJggg==" />
      <linearGradient id="logo-vokeraa" x1="240.57" y1="88.33" x2="259.75" y2="69.15" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#6e6e6e" />
        <stop offset=".15" stop-color="#6e6e6e" />
        <stop offset=".53" stop-color="#c6c6c5" />
        <stop offset=".91" stop-color="#6e6e6e" />
        <stop offset="1" stop-color="#6e6e6e" />
      </linearGradient>
      <linearGradient id="logo-vokerab" x1="265.06" y1="53.11" x2="286.56" y2="53.11" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#6e6e6e" />
        <stop offset="0" stop-color="#707070" />
        <stop offset="0" stop-color="#8b8b8b" />
        <stop offset="0" stop-color="#a1a1a0" />
        <stop offset="0" stop-color="#b2b2b1" />
        <stop offset="0" stop-color="#bdbdbc" />
        <stop offset="0" stop-color="#c4c4c3" />
        <stop offset="0" stop-color="#c6c6c5" />
        <stop offset=".01" stop-color="#c6c6c5" />
        <stop offset="1" stop-color="#6e6e6e" />
        <stop offset="1" stop-color="#636362" />
      </linearGradient>
      <linearGradient id="logo-vokerac" x1="216.9" y1="67.29" x2="258.49" y2="25.7" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#6e6e6e" />
        <stop offset=".07" stop-color="#6e6e6e" />
        <stop offset=".32" stop-color="#f5f5f5" />
        <stop offset="1" stop-color="#c6c6c5" />
        <stop offset="1" stop-color="#ececec" />
        <stop offset="1" stop-color="#fefefe" />
        <stop offset="1" stop-color="#6e6e6e" />
      </linearGradient>
      <linearGradient id="logo-vokerad" x1="305.43" y1="43.84" x2="379.7" y2="43.84" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#c6c6c5" />
        <stop offset=".46" stop-color="#c6c6c5" />
        <stop offset=".85" stop-color="#6e6e6e" />
      </linearGradient>
      <linearGradient id="logo-vokerae" x1="316.75" y1="90.95" x2="356.37" y2="51.32" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#6e6e6e" />
        <stop offset=".6" stop-color="#f2f2f2" />
        <stop offset=".74" stop-color="#c6c6c5" />
        <stop offset="1" stop-color="#6e6e6e" />
      </linearGradient>
      <linearGradient id="logo-vokeraf" x1="339.38" y1="80.67" x2="364.81" y2="80.67" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#6e6e6e" />
        <stop offset=".16" stop-color="#6e6e6e" />
        <stop offset=".23" stop-color="#6e6e6e" />
        <stop offset=".5" stop-color="#fcfcfc" />
        <stop offset="1" stop-color="#6e6e6e" />
      </linearGradient>
      <linearGradient id="logo-vokerag" x1="71" y1="77.8" x2="122.58" y2="26.21" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#3c3c3c" />
        <stop offset="0" stop-color="#313131" />
        <stop offset="0" stop-color="#6e6e6e" />
        <stop offset=".52" stop-color="#eaeaea" />
        <stop offset="1" stop-color="#6e6e6e" />
        <stop offset="1" stop-color="#4c4c4c" />
        <stop offset="1" stop-color="#5a5a5a" />
        <stop offset="1" stop-color="#656565" />
        <stop offset="1" stop-color="#6c6c6c" />
        <stop offset="1" stop-color="#6e6e6e" />
      </linearGradient>
      <linearGradient id="logo-vokerah" x1="104.32" y1="93.12" x2="138.58" y2="58.86" gradientUnits="userSpaceOnUse">
        <stop offset="0" />
        <stop offset="0" stop-color="#757575" />
        <stop offset=".5" stop-color="#c6c6c5" />
        <stop offset="1" stop-color="#676767" />
        <stop offset="1" stop-color="#f5f5f5" />
      </linearGradient>
      <filter x="-10.3%" y="-5.4%" width="120.6%" height="118%" filterUnits="objectBoundingBox" id="pdfa">
        <feOffset dy="6" in="SourceAlpha" result="shadowOffsetOuter1" />
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter1" result="shadowBlurOuter1" />
        <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.136888587 0" in="shadowBlurOuter1" />
      </filter>
      <path
        d="M10.59 0h88.48l46.897 46.712v109.48c0 5.753-4.665 10.413-10.413 10.413H10.59c-5.749 0-10.414-4.66-10.414-10.413V10.412C.177 4.66 4.843 0 10.591 0z"
        id="pdfb" />
      <path id="phoneusa" d="M43.872.96v43.466H.5V.959h43.37z" />
      <clipPath id="ui-installerclip0">
        <path fill="#fff" d="M0 0h60v60H0z" />
      </clipPath>
      <clipPath id="floor-mountedclip0">
        <path fill="#fff" d="M0 0h188v188H0z" />
      </clipPath>
      <clipPath id="fusebox-25-plus-evclip0_8747_6730">
        <path fill="#fff" d="M0 0h170v170H0z" />
      </clipPath>
      <clipPath id="number-1-5clip0_1116_90">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-10-20clip0_2062_26985">
        <path fill="#fff" transform="translate(1)" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-10plusclip0_1021_117">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-11-15clip0_1116_92">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-15plusclip0_1116_94">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-2-5clip0_1021_8">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-20-25clip0_2062_26988">
        <path fill="#fff" transform="translate(1)" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-25plusclip0_2062_26991">
        <path fill="#fff" transform="translate(1)" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-5-10clip0_1021_90">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-6-10clip0_1116_5">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-up-to-10clip0_2062_26983">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="number-up-to-2clip0_1021_119">
        <path fill="#fff" d="M0 0h168.211v170H0z" />
      </clipPath>
      <clipPath id="wall-mounted-highclip0">
        <path fill="#fff" d="M0 0h188v188H0z" />
      </clipPath>
      <clipPath id="wall-mounted-lowclip0">
        <path fill="#fff" d="M0 0h188v188H0z" />
      </clipPath>
      <symbol id="aircon" viewBox="0 0 140 140">
        <path
          d="M50.4 64.4h-6.296a2.8 2.8 0 01-2.8-2.803l.04-33.89a2.8 2.8 0 012.8-2.796h51.8a2.8 2.8 0 012.8 2.803l-.04 33.89a2.8 2.8 0 01-2.8 2.796H89.6m-33.6 0h28"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M73.63 44.55c0 6.981-5.66 12.64-12.642 12.64-6.98 0-12.64-5.659-12.64-12.64 0-6.982 5.66-12.641 12.64-12.641 6.982 0 12.641 5.66 12.641 12.64z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M69.166 44.55a8.177 8.177 0 11-16.354 0 8.177 8.177 0 0116.354 0z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M64.703 44.55a3.714 3.714 0 11-7.428 0 3.714 3.714 0 017.428 0zM79.8 42h12.387M79.8 36.4h12.387M79.8 47.6h12.387M79.8 53.2h12.387"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <rect x="50.401" y="63.001" width="5.308" height="3.037" rx="1.518" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <rect x="84.001" y="63.001" width="5.308" height="3.037" rx="1.518" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M124.37 74.81l-.65.373.65-.374zm-2.178 31.309l.62.422-.62-.422zm-103.39.097l-.516.544.516-.544zm-2.624-31.587l.439.608-.439-.608zm108.304 23.754a.75.75 0 000-1.5v1.5zm-.714 4.056a.75.75 0 100-1.5v1.5zm-107.655-1.5a.75.75 0 000 1.5v-1.5zm7.836 6.029a.75.75 0 001.103-1.016l-1.103 1.016zM119.155 102a.749.749 0 10-1.364-.622l1.364.622zm-3.583 4.052a.748.748 0 00.221 1.037.748.748 0 001.037-.221l-1.258-.816zM16.573 75.274h107.199v-1.5H16.573v1.5zm107.147-.09c.158.274.333.873.481 1.83.142.927.245 2.1.308 3.45.126 2.698.089 6.054-.104 9.459-.193 3.404-.541 6.84-1.032 9.7-.245 1.431-.524 2.706-.834 3.756-.313 1.063-.643 1.842-.967 2.318l1.24.844c.456-.67.839-1.627 1.166-2.738.331-1.124.622-2.46.874-3.926.503-2.932.855-6.427 1.05-9.87.196-3.443.235-6.852.106-9.613-.065-1.38-.172-2.61-.325-3.607-.148-.967-.353-1.814-.663-2.352l-1.3.748zm-2.241 30.526H19.444v1.5h102.035v-1.5zm-102.16-.038c-.888-.842-1.758-2.42-2.415-4.079-.657-1.663-1.028-3.222-1.028-3.96h-1.5c0 1.03.458 2.804 1.134 4.512.677 1.712 1.644 3.542 2.776 4.615l1.032-1.088zm-3.464-8.217c-.037-.152-.096-.587-.16-1.302a78.072 78.072 0 01-.181-2.636 136.975 136.975 0 01-.195-7.367c.003-2.645.098-5.27.34-7.332.122-1.034.277-1.9.468-2.549a4.13 4.13 0 01.29-.755c.1-.188.175-.259.2-.277l-.878-1.216c-.283.204-.49.495-.646.789-.16.3-.292.653-.405 1.035-.225.765-.393 1.727-.519 2.798-.252 2.146-.347 4.837-.35 7.504-.005 2.672.082 5.34.197 7.452a78.72 78.72 0 00.185 2.688c.062.69.128 1.243.197 1.525l1.457-.357zm3.589 8.255c-.072 0-.112-.025-.126-.038l-1.032 1.088c.327.311.752.45 1.158.45v-1.5zm102.128-.013c.006-.009.011-.008-.001-.003a.227.227 0 01-.092.016v1.5c.466 0 1.003-.184 1.333-.669l-1.24-.844zm2.2-30.423c.019 0 .034.002.043.004.009.001.009.002.002 0a.248.248 0 01-.097-.094l1.3-.75c-.31-.537-.88-.66-1.248-.66v1.5zm-107.199-1.5c-.206 0-.535.031-.834.247l.878 1.216a.236.236 0 01-.078.039h.003a.293.293 0 01.031-.002v-1.5zm-1.447 24.61h109.356v-1.5H15.126v1.5zm108.642 2.555H21.541v1.5h102.227v-1.5zm-102.227 0h-5.428v1.5h5.428v-1.5zm-.711.989c.236.703.849 1.776 1.448 2.718.604.951 1.273 1.89 1.67 2.322l1.104-1.016c-.301-.327-.91-1.168-1.508-2.11-.604-.95-1.121-1.883-1.293-2.392l-1.421.478zm96.961-.55c-.935 2.048-.855 2.57-2.219 4.674l1.258.816c1.436-2.215 1.516-3.093 2.325-4.868l-1.364-.622z"
          fill="#2D3D4D" />
        <path d="M65.8 93.05a.75.75 0 000 1.5v-1.5zm7.402 1.5a.75.75 0 100-1.5v1.5zm-7.401 0h7.4v-1.5h-7.4v1.5z"
          fill="#FB6058" />
      </symbol>
      <symbol id="airing-cupboard" viewBox="0 0 100 100">
        <path
          d="M22.222 18.75h55.556v62.5H22.222v-62.5zm.347 12.153h54.514M57.292 18.75v22.569m0 39.931V52.083m19.791-10.764H57.292m0 0v10.764m19.791 0H57.292"
          fill="none" stroke="#fb6058" />
        <path
          d="M27.778 53.75c0-5.523 4.477-10 10-10h5c5.523 0 10 4.477 10 10v27.5h-25v-27.5zm12.153-10v-3.472c0-.463.555-1.389 2.777-1.389"
          fill="none" stroke="#fb6058" />
      </symbol>
      <symbol id="answer-house" viewBox="0 0 84 84">
        <path
          d="M2.8 64.4h79.8m-65.027-.539V33.78l17.94-11.013V18.2h3.702v4.566l19.07 11.013V63.86m-40.98-19.875H4.949v3.223h1.075m11.28 0H6.025m0 0v14.504m0 1.88v-1.88m11.28 0H6.025"
          stroke="#2D3D4D" stroke-width=".72" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M8.709 61.443V49.357h5.64v12.086M33.39 36.4H35m5.64 0h-1.343m0 0H35m4.297 0v-3.358M35 36.4v-3.358m0 0v-3.357h4.297v3.357m-4.297 0h4.297"
          stroke="#2D3D4D" stroke-width=".72" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M24.287 58.49h5.91v-9.67h-5.91v9.67z" stroke="#2D3D4D" stroke-width=".72"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M30.196 53.654h-5.909" stroke="#2D3D4D" stroke-width=".72" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M32.346 58.49h5.909v-9.67h-5.91v9.67z" stroke="#2D3D4D" stroke-width=".72"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M38.254 53.654h-5.372M22.138 58.49h18.264M21.333 31.361V25.99h.537m2.418 3.76v-3.76h-.538m-1.88 0v-3.76h1.88v3.76m-1.88 0h1.88m-2.686-3.761h3.492m-2.686 1.88h1.612"
          stroke="#2D3D4D" stroke-width=".72" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="#FB6058" stroke-width=".72" stroke-linecap="round" stroke-linejoin="round"
          d="M44.8 47.6h9.8v11.2h-9.8z" />
        <path
          d="M58.8 40.6h22.4v3.696h-1.125m-21.275 0h21.275m0 0V64.4m-15.082-3.915h10.014m-10.014 0h-2.462c-.788 0-.948-.678-.93-1.017m3.392 1.017v1.855c0 .574-.4.678-.602.658H62.37c-.482 0-.769-.414-.769-1.017v-2.513m13.406 1.017h2.462c.31 0 .93-.203.93-1.017m-3.392 1.017v1.496c0 .603.11 1.017.723 1.017h1.912c.154 0 .316-.004.44-.094.176-.126.318-.387.318-.923v-2.513m0 0v-3.976c0-.101-.006-.201-.03-.3-.104-.447-.412-1.116-1.035-1.26m-14.42-.027c-.438.16-1.314.67-1.314 1.436v4.127m1.313-5.563l1.405-4.153c.048-.142.07-.294.137-.429a.568.568 0 01.537-.323h9.765c.166 0 .332.016.482.085.252.117.568.34.697.693.175.478 1.005 2.97 1.398 4.155m-14.42-.028h8.316m-2.9 2.812h3.229m-3.229 1.256h3.229m5.775-4.04a1.113 1.113 0 00-.249-.028H75.39m-4.159 0c.073-.758.591-2.273 2.08-2.273 1.488 0 2.006 1.516 2.079 2.273m-4.159 0h4.159m1.204 3.41v0c0 .562-.456 1.017-1.017 1.017h-.702a1.017 1.017 0 01-1.017-1.017v0c0-.562.455-1.017 1.017-1.017h.702c.561 0 1.017.455 1.017 1.017zm-10.342 0v0c0 .562-.455 1.017-1.017 1.017h-.757a1.017 1.017 0 01-1.017-1.017v0c0-.562.456-1.017 1.017-1.017h.757c.562 0 1.017.455 1.017 1.017z"
          stroke="#2D3D4D" stroke-width=".72" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="arrow" viewBox="0 0 30 30">
        <path
          d="M29.923 2.715c-.573 1.796-1.456 3.502-2.206 5.207-.089.225-.442.225-.574.09-.75-.808-1.589-1.616-2.075-2.603h-.044c-5.384-.405-10.592 4.264-14.3 7.676-4.28 3.905-8.164 8.663-9.709 14.364-.088.719-.176 1.482-.176 2.245 0 .36-.486.36-.53 0v-.404c-.177 0-.309-.135-.309-.36.044-.493.177-.987.265-1.48.353-12.166 12.27-23.882 23.92-24.645-.396-.718-.75-1.481-1.19-2.2-.222-.314.132-.628.44-.583 2.207.314 4.193 1.391 6.224 2.2.176.044.353.224.264.493zM24.01.92c.265.583.442 1.167.75 1.75.089.225-.044.404-.176.45 0 .134-.088.224-.22.268C13.813 5.184 5.251 12.501 2.073 22.422c2.075-3.906 5.252-7.317 8.386-10.19 2.295-2.11 4.722-3.995 7.37-5.566 2.075-1.213 4.679-2.335 7.062-1.572-.132-.359.397-.538.574-.27A28.424 28.424 0 0027.23 6.98c.618-1.392 1.28-2.783 1.765-4.22-1.633-.628-3.266-1.436-4.987-1.84z"
          fill="#FFF" />
      </symbol>
      <symbol id="back" viewBox="0 0 40 40">
        <path d="M26 8L14 20l12 12" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="basket" viewBox="0 0 50 50">
        <circle cx="25" cy="25" r="24" fill="#fff" stroke="#EAE9E9" stroke-width="2" />
        <path
          d="M15.688 15h1.942a2 2 0 011.94 1.515l.636 2.542m0 0l1.474 7.066a5 5 0 004.895 3.978h4.635a5 5 0 004.83-3.71l1.288-4.818a2 2 0 00-1.932-2.516h-15.19zm-2.674 1.106h-3.504m4.61 4.241H12m8.206 5.697h-4.518"
          stroke="#2D3D4D" />
        <circle cx="33.206" cy="34.158" r="1.844" stroke="#2D3D4D" />
        <circle cx="23.986" cy="34.158" r="1.844" stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="bathroom" viewBox="0 0 100 100">
        <path
          d="M81.944 55.556a2.778 2.778 0 00-2.777-2.778H16.666a2.778 2.778 0 000 5.555h62.501a2.778 2.778 0 002.777-2.777z"
          fill="none" stroke="#fb6058" />
        <path d="M71.875 30.556H60.764c0-1.736.694-5.903 5.902-5.903 4.167 0 5.209 3.935 5.209 5.903z" fill="none"
          stroke="#fb6058" stroke-width="2" />
        <path
          d="M66.666 24.653c-.115-1.736.764-5.209 5.209-5.209 3.819 0 5.324 3.473 5.208 5.209-.116 9.49 0 27.778 0 27.778m-1.389 6.25l-9.722 15.972m0 0l3.472 5.903m-3.472-5.903H29.166m-9.375-15.972l9.375 15.972m0 0l-3.472 5.555m37.5-43.749v1.388m0 1.737v1.388m0 1.737v1.388m5.556-7.638v1.388m-2.778-3.125v1.389m0 2.083v1.389m0 2.084v1.388m0 2.084v1.389m2.778-6.944v1.388m0 1.737v1.388"
          fill="none" stroke="#fb6058" />
      </symbol>
      <symbol id="baths" viewBox="455 29 42 26">
        <g fill="none" fill-rule="evenodd" transform="translate(455 30)">
          <mask id="bathsa" fill="#fff">
            <path d="M20.85 24.942h20.851V5.92H.001v19.02h20.85z" />
          </mask>
          <path
            d="M38.752 7.942c-.23 2.296-1.517 7.002-4.884 10.078l-.002.002c-2.907 2.634-5.47 3.809-12.972 3.809-7.39 0-9.973-1.174-12.966-3.805C4.769 15.22 3.33 10.976 3.036 7.94l-.062-.638.627.144c1.027.236 2.136.505 3.412.959 3.092.94 6.966 2.118 13.881 2.118s10.79-1.178 13.902-2.125a87.6 87.6 0 001.076-.316 37.34 37.34 0 012.315-.636l.63-.144-.065.64zm2.95-1.315c0-.285-.26-.706-.57-.706-1.551 0-2.946.223-4.52.684l-2.653.793c-3.13.929-6.68 1.985-13.15 1.985-6.575 0-10.13-1.055-13.267-1.988l-.3-.087C5.116 6.655 3.11 6.04.572 6.04a.564.564 0 00-.57.543c0 .26.309.545.57.545h1.125l.026.46c.19 3.315 1.948 8.268 5.357 11.278.284.282.644.592 1.09.927l.362.273-.263.368c-.318.445-.482.959-.627 1.412-.027.085-.053.168-.08.247-.254 1.097-.429 1.624-1.48 1.624-.336 0-.57.3-.57.569 0 .274.33.656.658.656 1.855 0 2.306-1.19 2.636-2.517l.062-.24c.217-.8.326-1.21 1.004-1.21h.122l.105.068c2.604 1.46 5.53 2.006 10.796 2.006 5.27 0 8.196-.55 10.796-2.008l.106-.066h.121c.677 0 .826.554 1.05 1.38l.016.066c.331 1.326.782 2.519 2.637 2.519.336 0 .57-.3.57-.568 0-.26-.309-.568-.57-.568-1.045 0-1.22-.404-1.487-1.652-.08-.483-.226-1.079-.674-1.597l-.324-.375.398-.296c.298-.221.725-.557 1.069-.899 3.94-3.57 5.233-8.84 5.377-11.354l.026-.503h1.125c.337 0 .571-.232.571-.5z"
            fill="#6c6c6c" mask="url(#bathsa)" />
          <ellipse stroke="#7B7B7E" cx="18.375" cy="5.921" rx="1.969" ry="1.974" />
          <ellipse stroke="#7B7B7E" cx="24.609" cy="8.224" rx="1.641" ry="1.645" />
          <ellipse stroke="#7B7B7E" cx="13.781" cy="1.316" rx="1.313" ry="1.316" />
          <ellipse stroke="#7B7B7E" cx="27.891" cy="2.303" rx="1.641" ry="1.645" />
          <ellipse stroke="#7B7B7E" cx="33.141" cy="4.934" rx=".984" ry=".987" />
          <ellipse stroke="#7B7B7E" cx="9.516" cy="4.934" rx=".984" ry=".987" />
        </g>
      </symbol>
      <symbol id="bedrooms" viewBox="338 34 50 21">
        <g fill="#6c6c6c" fill-rule="evenodd">
          <path
            d="M339.672 49.04h46.287v-5.959h-46.287v5.96zm8.337-6.81h-4.025v-7.38h37.95v7.379H348.01zm16.963 0h-4.313 4.313zm22.137 7.256v-6.954c0-.245-.32-.302-.567-.302h-3.745v-7.713c0-.244-.203-.517-.45-.517h-38.92c-.249 0-.593.273-.593.517v7.713h-3.604c-.248 0-.42.057-.42.302v6.954c0 .244.172.406.42.406h3.03v4.62a.431.431 0 10.862 0v-4.62h39.675v4.62a.431.431 0 10.862 0v-4.62h2.883c.248 0 .567-.162.567-.406z" />
          <path
            d="M347.954 40.162c-.575 0-1.042.462-1.042 1.03 0 .566.467 1.028 1.042 1.028h12.644c.575 0 1.042-.462 1.042-1.029s-.467-1.029-1.042-1.029h-12.644zm12.644 2.91h-12.644c-1.05 0-1.905-.844-1.905-1.88 0-1.037.855-1.88 1.905-1.88h12.644c1.05 0 1.905.843 1.905 1.88 0 1.036-.854 1.88-1.905 1.88zm4.678-2.91c-.575 0-1.042.462-1.042 1.03 0 .566.467 1.028 1.042 1.028h12.644c.575 0 1.042-.462 1.042-1.029s-.467-1.029-1.042-1.029h-12.644zm12.644 2.91h-12.644c-1.05 0-1.905-.844-1.905-1.88 0-1.037.855-1.88 1.905-1.88h12.644c1.05 0 1.905.843 1.905 1.88 0 1.036-.854 1.88-1.905 1.88z" />
        </g>
      </symbol>
      <symbol id="boiler-white" viewBox="0 0 150 150">
        <path
          d="M35.408 94.155h79.099m-28.25 25.425v10.768M63.658 119.58v10.767M74.959 119v12m-40-107.631A3.369 3.369 0 0138.327 20h73.263a3.368 3.368 0 013.368 3.369v92.259a3.368 3.368 0 01-3.368 3.369H38.327a3.369 3.369 0 01-3.368-3.369v-92.26z"
          stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="101.478" cy="106.551" stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          r="5.333" />
        <circle cx="45.882" cy="106.043" stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          r="2" />
        <circle cx="55.77" cy="106.043" r="2" stroke="#fff" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M77.155 74.832c.315 2.064-1.166 2.696-1.166 2.696.801-4.153-3.253-7.865-3.253-7.865.316 2.84-3.109 6.043-3.109 9.419a4.832 4.832 0 004.833 4.832c2.668 0 4.83-2.065 4.83-4.784 0-3.302-2.135-4.298-2.135-4.298"
          stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="boxt-buy" viewBox="0 0 110 110">
        <path
          d="M24 22a2 2 0 012-2h58a2 2 0 012 2v60.247a4.369 4.369 0 01-2.951 4.132l-26.632 9.135a4.369 4.369 0 01-2.834 0l-26.631-9.135A4.369 4.369 0 0124 82.247V22z"
          fill="#3599A5" />
        <g filter="url(#boxt-buyfilter0_d)">
          <circle cx="54" cy="28" r="20" fill="#fff" />
        </g>
        <path
          d="M66 28.334c0-.766-.349-1.5-.888-1.978.38-.51.539-1.148.476-1.786-.127-1.34-1.333-2.425-2.729-2.425h-4.124l.254-1.977c.063-.607.063-1.117-.063-1.627A4.556 4.556 0 0054.483 15c-.476 0-.952.191-1.3.542a1.79 1.79 0 00-.54 1.276v2.616a6.12 6.12 0 01-.92 3.222l-1.11 1.786a1.57 1.57 0 01-.635.574c-.222-.701-.856-1.212-1.618-1.212h-3.648A1.71 1.71 0 0043 25.527v10.75A1.71 1.71 0 0044.713 38h3.712a1.71 1.71 0 001.713-1.723v-.255a4.621 4.621 0 003.077 1.18h8.058c.73 0 1.46-.319 1.967-.83.508-.51.761-1.18.73-1.881a1.99 1.99 0 00-.127-.734 2.656 2.656 0 001.427-2.36c0-.351-.063-.702-.19-1.021.571-.543.92-1.276.92-2.042zm-17.48 7.911c0 .064-.063.128-.127.128h-3.68c-.063 0-.127-.064-.127-.128v-10.75c0-.064.064-.128.127-.128h3.712c.063 0 .127.064.127.128v10.75h-.032zm15.291-6.954a.94.94 0 00-.508.638.905.905 0 00.19.798.979.979 0 01.223.637c0 .51-.38.957-.888 1.053-.318.064-.603.255-.73.574a.964.964 0 00.095.925c.127.16.19.383.19.607 0 .255-.095.542-.285.733a1.22 1.22 0 01-.857.351h-8.058a3.089 3.089 0 01-3.077-3.094v-5.87a3.085 3.085 0 001.904-1.371l1.11-1.787a7.623 7.623 0 001.174-4.051v-2.616a.25.25 0 01.063-.16.242.242 0 01.159-.063 2.96 2.96 0 012.887 2.297c.063.287.095.638.032 1.052l-.286 2.233c-.063.383.063.766.317 1.085.254.287.635.478 1.015.478h4.378c.54 0 1.079.447 1.142.957.032.383-.158.766-.476.99a.976.976 0 00-.412.892.89.89 0 00.603.766c.412.16.666.542.666.989.032.415-.19.766-.571.957z"
          fill="#3599A5" />
        <path
          d="M40.243 57.615c0-2.36-1.6-3.84-4.08-3.84h-5.52v13.78h5.92c2.74 0 4.4-1.94 4.4-4.22 0-1.12-.78-2.68-2.6-3.14 1.42-.54 1.88-1.68 1.88-2.58zm-6.66 1.6v-2.86h2.28c.86 0 1.3.58 1.3 1.44 0 .82-.52 1.42-1.28 1.42h-2.3zm2.76 2.42c1.04 0 1.54.86 1.54 1.64 0 .82-.5 1.7-1.48 1.7h-2.82v-3.34h2.76zm9.155-.94c0-2.3 1.78-4.18 4.06-4.18 2.28 0 4.08 1.88 4.08 4.18s-1.8 4.16-4.08 4.16c-2.28 0-4.06-1.86-4.06-4.16zm-2.92 0c0 3.88 3.08 7.06 6.98 7.06 3.92 0 7-3.18 7-7.06 0-3.92-3.08-7.08-7-7.08-3.9 0-6.98 3.16-6.98 7.08zm26.918-6.92h-3.5l-2.58 4.12-2.56-4.12h-3.52l4.34 6.44-4.76 7.34h3.44l3.06-5 3.06 5h3.46l-4.78-7.34 4.34-6.44zm1.064 2.64h3.72v11.14h2.96v-11.14h3.72v-2.64h-10.4v2.64zM36.18 73.533c0-1.298-.88-2.112-2.245-2.112H30.9V79h3.256c1.507 0 2.42-1.067 2.42-2.321 0-.616-.429-1.474-1.43-1.727.781-.297 1.034-.924 1.034-1.419zm-3.664.88V72.84h1.254c.473 0 .715.319.715.792 0 .451-.286.781-.704.781h-1.265zm1.518 1.331c.572 0 .847.473.847.902 0 .451-.275.935-.814.935h-1.55v-1.837h1.517zM37.904 79h4.894v-1.419H39.51v-1.617h2.794v-1.397H39.51V72.84h3.19v-1.419h-4.796V79zm5.844-2.222c0 1.364 1.1 2.343 2.75 2.343 1.342 0 2.706-.715 2.728-2.222.022-.726-.242-1.76-2.079-2.288l-.902-.286c-.737-.209-.814-.572-.814-.77 0-.451.418-.759.98-.759.637 0 .978.352.978.858h1.606c0-1.452-1.1-2.321-2.563-2.321-1.452 0-2.607.913-2.607 2.266 0 .682.286 1.661 1.958 2.178l.913.253c.77.22.924.517.913.858-.01.495-.462.803-1.11.803-.727 0-1.112-.429-1.134-.913h-1.617zm6.162-3.905h2.046V79h1.628v-6.127h2.046v-1.452h-5.72v1.452zm14.586.66c0-1.298-.88-2.112-2.244-2.112h-3.036V79h3.256c1.507 0 2.42-1.067 2.42-2.321 0-.616-.43-1.474-1.43-1.727.78-.297 1.034-.924 1.034-1.419zm-3.663.88V72.84h1.254c.473 0 .715.319.715.792 0 .451-.286.781-.704.781h-1.265zm1.518 1.331c.572 0 .847.473.847.902 0 .451-.275.935-.814.935h-1.551v-1.837h1.518zm6.74 1.848c-.682 0-1.31-.517-1.31-1.298v-4.873h-1.638v4.873c0 1.716 1.386 2.816 2.948 2.816 1.55 0 2.926-1.1 2.926-2.816v-4.873H70.4v4.873c0 .781-.638 1.298-1.31 1.298zM77.227 79v-3.41l2.937-4.169H78.25l-1.848 2.838-1.826-2.838h-1.914l2.926 4.147V79h1.639z"
          fill="#fff" />
      </symbol>
      <symbol id="boxt-logo-white" viewBox="0 0 66 16">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M8.595 7.446c1.961.475 2.919 2.286 2.919 3.644 0 2.534-1.824 4.684-4.88 4.684H0V.18h6.156c2.758 0 4.515 1.63 4.515 4.232 0 1.131-.502 2.445-2.076 3.033zM2.918 8.87v4.323h3.558c1.276 0 1.983-1.086 1.983-2.15 0-1.019-.707-2.173-2.052-2.173H2.918zm3.033-2.42c1.048 0 1.687-.838 1.687-1.879 0-1.109-.639-1.81-1.778-1.81H2.918V6.45h3.033zM29.14 8.01c0-2.85-2.235-5.159-5.063-5.159-2.85 0-5.06 2.308-5.06 5.16 0 2.852 2.21 5.138 5.06 5.138 2.828 0 5.063-2.286 5.063-5.138zm2.919 0A7.96 7.96 0 0124.077 16c-4.468 0-7.957-3.599-7.957-7.99 0-4.434 3.49-8.01 7.957-8.01 4.47 0 7.982 3.576 7.982 8.01zm12.016-.565l5.472 8.33h-3.466l-3.739-5.998-3.74 5.997H35.16l5.45-8.329L35.684.181h3.488l3.17 4.956L45.513.18H49l-4.925 7.264zM53.971.18H65.76v2.65h-4.423v12.945h-2.941V2.829H53.97V.18z"
          fill="#fff" />
      </symbol>
      <symbol id="bubbles-1" viewBox="0 0 86 55">
        <g transform="matrix(1 0 0 -1 -2 85)" fill="none" fill-rule="evenodd">
          <mask id="bubbles-1b" fill="#fff">
            <use xlinkHref="#bubbles-1a" />
          </mask>
          <path
            d="M76.656 50C70.78 50 66 54.822 66 60.746c0 5.927 4.78 10.743 10.656 10.743 5.878 0 10.658-4.816 10.658-10.743C87.311 54.822 82.531 50 76.656 50zm5.61 12.337A1.33 1.33 0 0180.943 61c0-2.523-2.037-4.573-4.54-4.573a1.335 1.335 0 01-1.327-1.34c0-.74.596-1.338 1.328-1.338 3.966 0 7.192 3.252 7.192 7.25 0 .74-.596 1.337-1.328 1.337zM23.5 38c-3.585 0-6.5 2.941-6.5 6.554 0 3.615 2.916 6.553 6.5 6.553 3.585 0 6.5-2.938 6.5-6.553C29.998 40.941 27.083 38 23.5 38zm3.422 7.524a.811.811 0 01-.809-.814c0-1.539-1.242-2.79-2.768-2.79a.814.814 0 01-.81-.816c0-.452.363-.817.81-.817 2.42 0 4.386 1.983 4.386 4.423 0 .45-.363.814-.81.814zM51.5 31c-3.585 0-6.5 2.941-6.5 6.554 0 3.615 2.916 6.553 6.5 6.553 3.585 0 6.5-2.938 6.5-6.553C57.998 33.941 55.083 31 51.5 31zm3.422 7.524a.811.811 0 01-.809-.814c0-1.539-1.242-2.79-2.768-2.79a.814.814 0 01-.81-.816c0-.452.363-.817.81-.817 2.42 0 4.386 1.983 4.386 4.423 0 .45-.363.814-.81.814zM51.5 71.489c-3.585 0-6.5 2.941-6.5 6.554 0 3.615 2.916 6.553 6.5 6.553 3.585 0 6.5-2.938 6.5-6.553-.002-3.613-2.917-6.554-6.5-6.554zm3.422 7.524a.811.811 0 01-.809-.814c0-1.539-1.242-2.79-2.768-2.79a.814.814 0 01-.81-.816c0-.452.363-.817.81-.817 2.42 0 4.386 1.983 4.386 4.423 0 .45-.363.814-.81.814zM19.5 58c-2.482 0-4.5 2.036-4.5 4.538 0 2.502 2.018 4.536 4.5 4.536S24 65.04 24 62.538C23.999 60.036 21.98 58 19.5 58zm2.369 5.21a.562.562 0 01-.56-.565c0-1.065-.86-1.93-1.916-1.93a.564.564 0 01-.561-.566c0-.313.251-.566.56-.566 1.675 0 3.037 1.373 3.037 3.062a.563.563 0 01-.56.564zM6 30c-2.206 0-4 1.81-4 4.033 0 2.225 1.794 4.033 4 4.033s4-1.808 4-4.033C9.999 31.81 8.205 30 6 30zm2.106 4.63a.5.5 0 01-.498-.501c0-.947-.764-1.717-1.703-1.717a.501.501 0 01-.499-.502.5.5 0 01.499-.503c1.488 0 2.699 1.221 2.699 2.722a.5.5 0 01-.498.501zM45 55c-2.206 0-4 1.81-4 4.033 0 2.225 1.794 4.033 4 4.033s4-1.808 4-4.033C48.999 56.81 47.205 55 45 55zm2.106 4.63a.5.5 0 01-.498-.501c0-.947-.764-1.717-1.703-1.717a.501.501 0 01-.499-.502.5.5 0 01.499-.503c1.488 0 2.699 1.221 2.699 2.722a.5.5 0 01-.498.501zM6 47c-2.206 0-4 1.81-4 4.033 0 2.225 1.794 4.033 4 4.033s4-1.808 4-4.033C9.999 48.81 8.205 47 6 47zm2.106 4.63a.5.5 0 01-.498-.501c0-.947-.764-1.717-1.703-1.717a.501.501 0 01-.499-.502.5.5 0 01.499-.503c1.488 0 2.699 1.221 2.699 2.722a.5.5 0 01-.498.501z"
            fill="#FFF" mask="url(#bubbles-1b)" />
        </g>
      </symbol>
      <symbol id="bubbles-2" viewBox="0 0 86 119">
        <g transform="matrix(1 0 0 -1 -1 119)" fill="none" fill-rule="evenodd">
          <mask id="bubbles-2b" fill="#fff">
            <use xlinkHref="#bubbles-2a" />
          </mask>
          <path
            d="M18.299 0C8.759 0 1 7.825 1 17.44c0 9.615 7.759 17.441 17.299 17.441 9.537 0 17.295-7.826 17.295-17.44C35.594 7.825 27.836 0 18.3 0zm10.036 18.78a1.336 1.336 0 01-1.328-1.34c0-4.842-3.905-8.78-8.708-8.78-.733 0-1.328-.6-1.328-1.339 0-.738.592-1.339 1.328-1.339 6.265 0 11.361 5.139 11.361 11.458a1.33 1.33 0 01-1.325 1.339zM29.299 73C19.759 73 12 80.826 12 90.44c0 9.615 7.759 17.441 17.299 17.441 9.537 0 17.295-7.826 17.295-17.44C46.594 80.826 38.836 73 29.3 73zm10.036 18.78a1.336 1.336 0 01-1.328-1.34c0-4.842-3.905-8.78-8.708-8.78-.733 0-1.328-.6-1.328-1.339 0-.738.592-1.339 1.328-1.339 6.265 0 11.361 5.139 11.361 11.458a1.33 1.33 0 01-1.325 1.339zM75.656 97C69.78 97 65 101.822 65 107.746c0 5.927 4.78 10.743 10.656 10.743 5.878 0 10.658-4.816 10.658-10.743C86.311 101.822 81.531 97 75.656 97zm5.61 12.337A1.33 1.33 0 0179.943 108c0-2.523-2.037-4.573-4.54-4.573a1.335 1.335 0 01-1.327-1.34c0-.74.596-1.338 1.328-1.338 3.966 0 7.192 3.252 7.192 7.25 0 .74-.596 1.337-1.328 1.337zM46.656 44C40.78 44 36 48.822 36 54.746c0 5.927 4.78 10.743 10.656 10.743 5.878 0 10.658-4.816 10.658-10.743C57.311 48.822 52.531 44 46.656 44zm5.61 12.337A1.33 1.33 0 0150.943 55c0-2.523-2.037-4.573-4.54-4.573a1.335 1.335 0 01-1.327-1.34c0-.74.596-1.338 1.328-1.338 3.966 0 7.192 3.252 7.192 7.25 0 .74-.596 1.337-1.328 1.337zM69.83 34.803c4.868 0 8.832-3.993 8.832-8.9 0-4.91-3.961-8.903-8.831-8.903C64.96 17 61 20.993 61 25.903c0 4.908 3.958 8.9 8.83 8.9zm-1.225-13.757c3.333 0 6.04 2.733 6.04 6.09a1.33 1.33 0 01-1.324 1.34 1.334 1.334 0 01-1.328-1.34c0-1.883-1.52-3.413-3.391-3.413-.733 0-1.325-.6-1.325-1.339 0-.74.595-1.339 1.328-1.339z"
            fill="#FFF" mask="url(#bubbles-2b)" />
        </g>
      </symbol>
      <symbol id="bubbles-3" viewBox="0 0 89 117">
        <g transform="matrix(1 0 0 -1 0 117)" fill="none" fill-rule="evenodd">
          <mask id="bubbles-3b" fill="#fff">
            <use xlinkHref="#bubbles-3a" />
          </mask>
          <path
            d="M24.369 56C10.929 56 0 67.024 0 80.568c0 13.545 10.93 24.569 24.369 24.569 13.434 0 24.364-11.024 24.364-24.569C48.733 67.024 37.803 56 24.369 56zm14.138 26.454a1.88 1.88 0 01-1.87-1.886c0-6.822-5.502-12.369-12.269-12.369a1.88 1.88 0 01-1.87-1.886c0-1.04.835-1.886 1.87-1.886 8.826 0 16.005 7.239 16.005 16.14a1.874 1.874 0 01-1.866 1.887zM57.5 103c-3.585 0-6.5 2.941-6.5 6.554 0 3.615 2.916 6.553 6.5 6.553 3.585 0 6.5-2.938 6.5-6.553-.002-3.613-2.917-6.554-6.5-6.554zm3.422 7.524a.811.811 0 01-.809-.814c0-1.539-1.242-2.79-2.768-2.79a.814.814 0 01-.81-.816c0-.452.363-.817.81-.817 2.42 0 4.386 1.983 4.386 4.423 0 .45-.363.814-.81.814zM82.5 74c-3.585 0-6.5 2.941-6.5 6.554 0 3.615 2.916 6.553 6.5 6.553 3.585 0 6.5-2.938 6.5-6.553C88.998 76.941 86.083 74 82.5 74zm3.422 7.524a.811.811 0 01-.809-.814c0-1.539-1.242-2.79-2.768-2.79a.814.814 0 01-.81-.816c0-.452.363-.817.81-.817 2.42 0 4.386 1.983 4.386 4.423 0 .45-.363.814-.81.814zM8.83 17.803c4.868 0 8.832-3.993 8.832-8.9C17.662 3.993 13.7 0 8.83 0 3.96 0 0 3.993 0 8.903c0 4.907 3.958 8.9 8.83 8.9zM7.606 4.046c3.333 0 6.04 2.734 6.04 6.091a1.33 1.33 0 01-1.324 1.34 1.334 1.334 0 01-1.328-1.34c0-1.883-1.52-3.413-3.391-3.413-.733 0-1.325-.6-1.325-1.339 0-.74.595-1.339 1.328-1.339zM71.83 45.803c4.868 0 8.832-3.993 8.832-8.9 0-4.91-3.961-8.903-8.831-8.903C66.96 28 63 31.993 63 36.903c0 4.907 3.958 8.9 8.83 8.9zm-1.225-13.758c3.333 0 6.04 2.734 6.04 6.091a1.33 1.33 0 01-1.324 1.34 1.334 1.334 0 01-1.328-1.34c0-1.883-1.52-3.413-3.391-3.413-.733 0-1.325-.6-1.325-1.339 0-.74.595-1.339 1.328-1.339z"
            fill="#FFF" mask="url(#bubbles-3b)" />
        </g>
      </symbol>
      <symbol id="burst" viewBox="0 0 58 58">
        <g fill="none" fill-rule="evenodd">
          <ellipse fill="#FB6058" opacity=".04" cx="28.983" cy="28.857" rx="28.983" ry="28.857" />
          <g opacity=".06">
            <use fill="#000" filter="url(#bursta)" xlinkHref="#burstb" />
            <use fill="#FB6058" xlinkHref="#burstb" />
          </g>
          <g opacity=".09">
            <use fill="#000" filter="url(#burstc)" xlinkHref="#burstd" />
            <use fill="#FB6058" xlinkHref="#burstd" />
          </g>
          <ellipse fill="#FFF9F9" cx="28.983" cy="28.857" rx="28.983" ry="28.857" />
          <ellipse fill="#FFEEEC" cx="28.983" cy="28.857" rx="21.784" ry="21.689" />
          <ellipse fill="#FFE3E1" cx="28.983" cy="28.857" rx="14.959" ry="14.895" />
        </g>
      </symbol>
      <symbol id="calculator-navy" viewBox="0 0 15 20">
        <path
          d="M14.995 1.541c0-.062.006-.122.004-.184-.002-.385-.15-.77-.438-1.022a1.33 1.33 0 00-.9-.335c-.088 0-.175.004-.263.004H1.422c-.058 0-.119-.006-.177-.002A1.341 1.341 0 00.001 1.314c-.004.139.004.275.004.412v16.379c0 .178-.006.356-.004.534.002.424.185.815.51 1.073.242.193.529.28.834.28h12.356a.414.414 0 00.4-.354.42.42 0 01-.376.362c.68-.023 1.214-.563 1.272-1.258.006-.058 0-.118 0-.178V1.541h-.002zM1.996 8.131v-.004l-.002-.016v-.004s.004 0 .004-.002L2 8.092l.002-.008.003-.008.002-.004.006-.03.002-.01.004-.016.002-.006.002-.007v-.002l.002-.004.002-.006.004-.01.004-.01.004-.011.002-.004.008-.017a.084.084 0 01.01-.018c.006-.013.014-.025.022-.038.006-.008.01-.016.018-.025l.018-.022.008-.009c.002-.002.004-.006.008-.008l.002-.002.002-.002.002-.002.002-.002.008-.008.004-.005.006-.006.014-.012.008-.006c.004-.002.006-.007.01-.009l.012-.01s.002 0 .002-.002a.135.135 0 01.023-.015.15.15 0 01.024-.014l.022-.012c.002-.002.006-.002.008-.005l.022-.01c.01-.004.018-.008.028-.01l.028-.01a.144.144 0 01.028-.009h.002a.058.058 0 01.018-.004l.01-.002c.01-.002.02-.004.03-.004l.03-.004c.015-.002.029-.002.043-.002.275-.005.55 0 .826 0 .233 0 .468-.01.703 0h.024c.004 0 .008 0 .012.002h.006l.012.002h.018l.004.002.012.002h.009l.008.002.018.004h.002l.008.002a.07.07 0 01.022.008l.01.005a.028.028 0 00.012.004c.008.002.014.006.022.01.004 0 .006.002.01.004l.01.006.012.007.012.006.004.002.01.006.01.006h.002a.035.035 0 01.01.009l.004.002.008.006c.004.004.01.006.015.01l.006.006.016.015.004.004.02.019.018.018.002.004.002.002.016.021s0 .002.002.002c0 .002.002.002.004.004a.05.05 0 01.01.015l.004.006.016.025c.016.029.03.058.042.089l.008.025.006.022.002.007.002.008.002.008v.006l.002.01.002.011v.002l.002.015v.002l.002.01v.043c0 .003 0 .005.002.005 0 .008 0 .016.002.024l-.002.025-.002.004v.052l.002.017v1.194c0 .089.006.18.004.269a.448.448 0 01-.008.074c0 .006-.002.01-.004.017a.075.075 0 01-.004.02v.002a.03.03 0 01-.004.013v.002l-.006.02-.002.007-.004.01c0 .004-.002.006-.004.01 0 .003 0 .003-.002.005v.004l-.002.008-.006.012a.494.494 0 01-.108.168l-.024.023-.023.018-.016.013c-.006.004-.01.008-.016.01-.006.004-.01.008-.016.01-.006.005-.012.007-.018.01-.006.005-.012.007-.018.011l-.018.008h-.002a.12.12 0 00-.016.007.345.345 0 01-.042.014l-.034.01h-.002a.642.642 0 01-.063.01c-.006.003-.012.003-.02.005l-.014.004-.004.002H2.635c-.124 0-.247 0-.361-.062-.322-.17-.276-.546-.276-.854V8.184c0-.011 0-.021.002-.03a.032.032 0 01-.004-.022zm2.554 9.274c0 .009 0 .019-.002.027l-.002.019c0 .004 0 .006-.002.008 0 .004 0 .006-.002.01 0 .002 0 .007-.002.009 0 .004 0 .006-.002.01-.002.006-.006.012-.006.019-.002.008-.008.016-.008.025 0 .006 0 .012-.002.02l-.002.006c0 .003 0 .005-.002.007l.002.006.002.002c0 .002-.004.002-.004.002a.004.004 0 01-.004.004.016.016 0 01-.004.008l-.002.003c0 .002-.006.004-.006.006h.002v.002l-.002.004-.002.004c0 .002.002.002 0 .004v.006h-.004.004s0 .003-.002.003l-.004.004-.002.002-.006.01c-.002.006-.006.01-.008.017a.346.346 0 01-.028.045c-.01.015-.022.029-.034.044-.002.004-.006.006-.01.01-.002.004-.006.006-.01.01l-.014.015-.014.012-.002.002-.002.002-.01.008-.003.003-.002.002s-.002 0-.002.002l-.008.006-.012.01-.002.002-.004.002-.018.013-.004.002-.022.014-.014.009-.012.006-.016.008h-.002l-.016.006-.014.007-.012.004c-.002 0-.004 0-.004.002l-.012.004c-.002 0-.004 0-.004.002h-.002a.06.06 0 01-.016.004c-.002 0-.004 0-.006.002l-.01.002h-.01l-.01.002-.012.002c-.004 0-.006.003-.01.003l-.02.002c-.012.002-.022 0-.036 0h-.006c-.002 0-.004 0-.004-.002H2.622c-.124 0-.247.006-.361-.054-.322-.17-.276-.538-.276-.847v-1.174c0-.011 0-.021.002-.032l-.002-.029v-.004l-.002-.017v-.004l.004-.002.002-.01v-.002l.002-.008.002-.009.002-.004.006-.029c0-.004.003-.008.003-.01l.004-.017.002-.006.002-.006v-.002l.002-.004.002-.006.004-.01.008-.022v-.002l.008-.018a.084.084 0 01.01-.019l.022-.037c.006-.008.01-.017.016-.023l.002-.004.016-.02.018-.02.002-.001.018-.019.002-.002.004-.002c.004-.004.008-.006.01-.01l.008-.007.02-.016.002-.002c.004-.002.006-.007.01-.009.004-.002.008-.004.012-.008l.004-.002.02-.012.023-.013.008-.004.022-.01.024-.01.01-.005.016-.006.01-.004.008-.002.016-.004h.002l.014-.002c.004-.002.01-.002.014-.005a.149.149 0 01.03-.004l.03-.004c.015-.002.029-.002.043-.002.275-.004.55 0 .826 0h.122c.193-.002.388-.01.581-.004l.002-.004h.002c.006 0 .012.004.016.006l.004.002.012.002c.002 0 .004 0 .006.002h.004l.004.002s0 .005.002.005h.023c.006.002.01 0 .016.002h.01l.009.002.008.002h.006l.008.002h.002c.008.002.016.004.024.008 0 0 .002 0 .002.002l.012.004c.008.002.014.006.022.01l.01.005.01.006a.521.521 0 01.066.041l.006.007.004.002.006.004.004.002.002.002a.129.129 0 01.023.02l.02.02.018.018.002.004.002.002.006.006.01.013s0 .002.002.002c0 .002.002.004.004.004l.01.014.004.006.016.025c.016.03.03.058.042.09l.008.024.006.023.002.004.004.014c0 .003 0 .005.002.005v.012-.004c.002.008.006.012.006.02v.025l.002.021v.013s0 .002-.002.002v1.326c.026.103.032.192.03.281zm0-3.823c0 .008 0 .016-.002.024l-.002.03v.006a.534.534 0 01-.056.182l-.016.03c-.004.009-.01.015-.014.024a.36.36 0 01-.048.062.187.187 0 01-.028.029.333.333 0 01-.03.026c-.007.005-.01.009-.017.013l-.016.012-.016.01a.337.337 0 01-.054.03l-.018.008a.028.028 0 01-.012.004.028.028 0 01-.012.004l-.008.002-.012.004-.012.004-.006.003-.014.004-.006.002-.008.002-.009.002-.042.006-.02.004h-.006l-.006.002-.004.002H2.631c-.124 0-.247-.002-.361-.064-.322-.17-.276-.546-.276-.856v-1.175c0-.011 0-.021.002-.03v-.031h-.002v-.029l.002-.002s.002 0 .002-.002a.015.015 0 01.01-.004l-.001-.01v-.002h-.002l.004-.03c0-.001 0-.005.002-.008v-.002c.002-.004.002-.008.002-.01l.002-.006c0-.004.002-.008.004-.013v-.002l.002-.006c0-.002 0-.004.002-.006l.004-.01.008-.021v-.002l.008-.019a.361.361 0 01.032-.056l.012-.016c.002-.002 0-.004.002-.006 0 0-.002 0-.002-.002.02-.009.02-.017.02-.023v-.002s0-.002.002-.002l.002-.002s-.002-.002 0-.002h-.002c0-.005.012-.007.016-.01l.004-.003v-.012.002s0-.002.002-.002v.002l.002.002h.002l.002-.002.002-.002h.008v-.009l.002-.002.004-.006v-.004c.002 0 .004 0 .006-.002a.004.004 0 00.004-.004v.006-.004l.002-.004c.002-.002.004-.007.006-.007V11.6l.012-.01s.002 0 .002-.002h.004l.008-.006.014-.01c.009-.007.017-.01.025-.015l.012-.006.012-.006.006-.003.02-.01.006-.002.018-.008.01-.004.016-.007.01-.002h.004l.008-.002.004-.002c.002 0 .004 0 .004-.002.006-.002.01-.004.016-.004a.028.028 0 00.012-.004h.002c.004 0 .006 0 .008-.002.006-.002.014-.002.02-.002l.03-.004c.015-.003.029-.003.043-.003.275-.004.55 0 .826 0 .233 0 .468-.01.703 0h.024c.004 0 .008 0 .012.003h.008l.006.002h.002s0 .002.002.002h.02l.016.002h.006c.002 0 .004 0 .006.002l.019.004.006.002h.006l.006.002h.002c.004 0 .008.002.012.004l.004.002.008.002c.004.002.008.002.012.005l.006.002.016.008c.002 0 .004.002.006.004h.002l.004.002.006.002h.002l.004.002.006.002.036.021a.06.06 0 01.014.01l.008.007.02.016.002.002c.006.004.012.01.019.017.002.002.004.002.004.004.006.006.014.012.02.02.006.007.012.01.016.017l.002.002s0 .002.002.002l.002.002v-.006l.004.006.002.005v.002a.358.358 0 01.006.006v.002l.004.002h.002c0 .002 0 .004.002.006.002.006.01.013.012.019l.004.006.002.004.008.015c.016.028.03.057.042.088l.008.025.006.023v.004a.075.075 0 01.004.021v.006l.002.006.006.025v.006l.002.021c0 .006 0 .015.002.02v.024c.002.033 0 .066 0 .097v1.216a.881.881 0 01.032.276zm4.218 2.309v.014l-.002.004v.005l.002.008v.01l-.002.01v1.392l.002.077v.033c0 .004 0 .008-.002.012v.039s.002 0 .002.002v.018l-.002.01h.002v.007-.003h-.002c-.002 0-.004 0-.004.003H8.76s-.002 0-.002.002l-.002.004c0 .002-.004.002-.004.004v.014c0 .002 0 .005-.002.007s0 .006-.002.008c0 .002 0 .004-.002.004l-.002.004-.008.023s.002 0 .002.002v.002l-.002.006-.002.007v.012c0 .002-.002.004-.004.006l-.002.004-.002.002s-.002 0-.002.002-.002.002-.004.005l-.002.002-.002.002v.012l-.002.008h.002c0 .002-.006.006-.008.009-.002.002-.004.004-.004.006l-.002.004-.002.002-.01.014a.054.054 0 01-.01.013l-.01.012-.018.023s0 .002-.002.002c-.002.004-.006.006-.008.008l-.03.032-.015.012c-.004.004-.008.008-.014.01l-.004.002-.016.013-.026.018a.497.497 0 01-.221.077c-.012.002-.022 0-.036 0-.008 0-.014-.004-.02-.004H6.863c-.124 0-.239.006-.353-.054-.322-.17-.268-.538-.268-.846v-1.024c0-.027 0-.052-.002-.08l-.002-.094v-.079h.002v-.01l.002-.019.004-.02a.462.462 0 01.014-.05l.006-.019.01-.025.009-.016.002-.004.01-.017v-.002l.012-.019.012-.018a.228.228 0 01.026-.036c.006-.006.012-.014.02-.02l.01-.01.01-.01a.026.026 0 01.008-.007l.02-.019c.002-.002.004-.002.004-.004l.002-.004c.006-.004.01-.014.016-.014v.004l.002-.002v-.002l.004-.002c.002-.003.002-.003.004-.003h.002l.008-.006h.002l.006-.004h.002s.002 0 .002-.002l.004-.002.008-.004h.002a.097.097 0 01.015-.006l.01-.007h.002s.002 0 .002-.002h.006v.002h.012l-.002-.002s-.002-.002 0-.002l.002-.002h.012l.024-.01.016-.006.018-.007.022-.006.052-.012c.002 0 .006 0 .008-.002l.025-.004h.002c.01-.002.018-.002.028-.002h.004c.275 0 .548.004.824.004h.701c.006 0 .014 0 .02.002.004 0 .008 0 .012.002l.02.002.022.004.008.002h.002c.004 0 .01.002.014.004l.01.002h.004l.018.007a.095.095 0 01.023.008l.024.008.022.008a.507.507 0 01.249.249l.006.014.002.004v.002l.006.015a.34.34 0 01.018.06v.002a.492.492 0 01.01.058v.006a.69.69 0 01.004.066v.004c0 .007 0 .015.002.021l.002.017v.022l-.002.002c.004-.024.006-.022.006-.02zm.002-2.31c0 .009 0 .017-.002.026l-.002.024-.002.021v.004a.296.296 0 01-.012.058.469.469 0 01-.036.101.348.348 0 01-.02.038l-.01.018a.12.12 0 01-.012.017l-.024.033-.027.031a1.954 1.954 0 00-.04.04l-.02.016a.063.063 0 01-.014.01.033.033 0 01-.014.008.11.11 0 01-.026.015l-.014.008-.018.008c-.008.005-.016.007-.024.01l-.016.007a1.245 1.245 0 00-.018.006c-.009.002-.017.006-.025.006a.591.591 0 01-.08.015l-.02.004-.004.002-.004.002-.006.002-.004.002H6.855c-.124 0-.239-.002-.353-.064-.322-.17-.268-.546-.268-.856V12.09c-.002-.035-.002-.07-.002-.104 0-.006 0-.01.002-.016v-.038l.002-.018v-.01c.002-.013.004-.027.008-.04a.513.513 0 01.5-.393h.1c.242-.002.483-.004.727-.004.096 0 .192-.006.287-.006h.078c.01.004.018.002.026.01 0 0 0 .002.002.002l.002.004v.002h.004l.004-.002h.061l.016.002h.052c.002 0 .004-.004.008-.004h.036l.024.002H8.3c.006 0 .01 0 .016.002h.01c.006 0 .01.003.016.003h.006l.012.002c.002 0 .002.004.004.004.006 0 .012.008.016.008.002 0 .004-.004.008-.002.006.002.01 0 .016.002h.004l.007.002h.004c.002 0 .004 0 .006.002.004 0 .008.002.01.004h.002l.008.004c.004.002.008.005.012.005a.69.69 0 00.012.006l.014.006c.002 0 .004.002.006.004l.004.002.01.006h.002l.008.004s.002.003.004.003c0 0 .002 0 .002.002l.004.002s.002 0 .002.002l.006.004h.002c.004.002.008.004.01.008l.012.008c.004.002.008.007.012.01l.022.02.012.01s.002 0 .002.002l.01.006.004.002c.002 0 .004.006.004.008l.019.021c.006.006.01.015.016.02 0 0 0 .003.002.003l.014.02a.045.045 0 01.006.01l.006.011a.087.087 0 01.01.019 1.45 1.45 0 00.016.033v.002l.006.012.004.01.006.017.002.005v.006c.002.004.004.01.004.014l.002.008v.002l.004.017a.075.075 0 01.004.02c.002 0 .004 0 .004.003h.004l-.002.002v.004l.002.017v.084c0 .031.004.056.002.085v.02l-.002.036V13.3c.016.102.024.193.02.282zm0-3.823a.448.448 0 01-.008.074l-.004.025c0 .006-.002.01-.004.017l-.002.004v.002l-.002.01-.002.01-.002.009-.006.017v.002l-.002.004a.501.501 0 01-.092.167l-.012.015-.023.023-.038.035-.012.008-.014.01c-.006.005-.01.009-.016.01l-.016.011-.016.01a.073.073 0 01-.018.009l-.016.008h-.002l-.016.006-.014.006-.016.007c-.005.002-.009.002-.01.004a.06.06 0 01-.017.004.069.069 0 01-.02.004l-.02.004-.04.006c-.006.002-.014.002-.02.005-.004.002-.008.002-.014.002l-.004.002H6.853c-.124 0-.239 0-.353-.063-.322-.17-.268-.546-.268-.854V8.305c-.002-.047-.004-.095-.002-.142 0-.007 0-.01.002-.017v-.062l.002-.014.004-.025a.513.513 0 01.5-.393c.033 0 .067.004.1.004.02 0 .041.004.061.004h.006c.113-.02.223 0 .336 0 .109 0 .215-.004.324-.004.08 0 .16-.002.24-.004h.113c.034 0 .066 0 .1.002l.077.002h.008c.008-.002.014-.004.02-.004h.002c.01-.003.018 0 .028.006 0 0 .002 0 .002.002l.002.002h.012s.002 0 .002-.002v.002-.002l.004-.002h.008l.006-.002h.025c.002 0 .004 0 .006.002l.006.002c.008 0 .018.002.026.004h.046c.008 0 .016 0 .024.002h.006a.1.1 0 01.024.004c.006 0 .012.002.016.004.006 0 .012.002.016.004.004 0 .008.003.012.005H8.4l.006.002c.006.002.01 0 .017.002h.006v.006l.004.002.006.002c.004.002.006.004.01.004l.004.002.004.002.02.009.046.024.022.015.01.008.01.008c.006.007.014.01.02.017l.018.017.018.018c.006.006.013.013.017.02l.016.022s0 .002.002.002c.004.006.01.012.014.02.004.009.01.015.014.023a.098.098 0 01.01.02c.002.007.006.011.008.017l.006.013.006.012.002.004v.002l.004.01.006.017.002.004c0 .003 0 .003.002.005v.002h.004v.008a.18.18 0 01.008.035v.002l.004.019.004.02.002.017c.002.019.004.04.004.06v.004c0 .031.004.056.002.087v.02l-.002.034v1.17c.01.087.018.178.014.267zm1.682-1.492v-.035a.72.72 0 01.014-.186l.004-.019.004-.012.002-.004v-.005l.002-.008v-.002l.006-.017.004-.01.002-.006v-.004l.004-.008c.002-.005.004-.007.004-.01l.004-.01c.002-.003.004-.01.008-.014a.906.906 0 01.046-.072l.019-.02.002-.003.006-.008.002-.002.002-.002.002-.002s0-.002.002-.002l.002-.002.006-.007.006-.006.004-.004.004-.004c.006-.004.01-.01.016-.015l.01-.008.01-.008h.002l.02-.015c.01-.006.02-.014.032-.018 0 0 .002 0 .002-.002l.008-.004.002.002c.008-.019.016-.021.024-.021h.01v-.004c0-.002.008-.002.012-.004l.007-.002.006-.002.002-.005.002-.004.004.002c.004-.002.006 0 .01-.002h.006c.004-.002.006-.002.01-.002h.006l.022-.006.008-.002.008-.002h.002l.018-.004.012-.002.012-.002c.006-.003.014-.003.022-.003.014-.002.028-.002.042-.002.276-.004.553 0 .828 0h.087c.032 0 .064 0 .094-.002h.078c.037 0 .073 0 .107-.002h.221a2.194 2.194 0 01.14.006h.015c.002 0 .002 0 .004.003h.002l.004.002h.002s.002-.002.002 0h.006c.004 0 .008-.002.012 0h.02v.002h.01v-.002.004c.004 0 .006.004.01.004l.01.002c.01.002.02.006.03.01.004.002.008.002.01.004.008.003.016.007.025.009a6.845 6.845 0 00.078.041l.004.002.006.002.004.002h.002l.012.009.002.002h.002l.002.002.002.002.002.002.002.002h.002l.004.002.004.002.008.006.002.002.018.017c.006.006.012.008.018.012.002.002.002 0 .002 0 .004 0 .006.01.01.013a.016.016 0 01.004.008l.012.014c0 .003.002.003.002.005l.017.02c.004.007.011.013.016.019l.002.002h.002l.002.002c.002.002.004.002.004.004 0 .004 0 .006.002.01 0 0 0 .003.002.005v.002c.002.002.002.004.002.006l.006.012a.098.098 0 01.01.021l.002.006c.002.007.006.01.008.017l.018.047.006.02.004.016v.002l.004.018.004.019a.06.06 0 01.002.019v.006c0 .004.004.006.004.01l.004.002-.002.008c0 .003-.002.007 0 .01l.002.024c0 .008 0 .016.002.022v1.289c0 .09 0 .18-.004.27 0 .008 0 .016-.002.026l-.006.05a92.603 92.603 0 00-.016.072c-.002.007-.002.01-.006.017-.002.004-.002.01-.006.014a.075.075 0 01-.008.019l-.018.04-.01.02-.012.02a.328.328 0 01-.027.038l-.01.019h.004l-.008.004h-.004v.004c0 .002 0 .006-.002.008h-.004l-.002.004-.002.002c-.002.002-.004.002-.006.005l-.008.006-.046.043-.026.02-.012.01c-.006.003-.01.003-.016.005l-.006.002-.004.002-.004.002c-.004.003-.006.007-.008.01h-.012c-.002 0-.004 0-.006.003a.101.101 0 00-.03.014.175.175 0 01-.035.017l-.016.004-.05.012c-.016.003-.03.007-.046.007-.002 0-.004 0-.006.002-.002 0-.006 0-.008.002l-.016.002h-.006c-.002 0-.004-.002-.004 0h-.006l-.012.002h-1.395c-.125 0-.24 0-.354-.062-.321-.17-.267-.546-.267-.855V8.328c-.002-.01-.002-.035-.004-.062zm2.554 4.147v5.011a.263.263 0 01-.004.041v.002a.517.517 0 01-.5.466c-.25.008-.503 0-.752 0-.261 0-.524.008-.788 0a.515.515 0 01-.504-.462c-.004-.04-.004-.08-.004-.122v-5.19c0-.208.006-.402.177-.553.209-.184.559-.12.816-.12h.956c.13 0 .261 0 .376.08a.535.535 0 01.227.443c.002.135 0 .27 0 .404zm0-9.527V5.45a2.323 2.323 0 01-.008.17.306.306 0 01-.008.045l-.004.02a.8.8 0 01-.012.04l-.006.01s-.002 0-.002.003c0 0 .002 0 .002.002-.004.006-.004.012-.008.02a.044.044 0 00-.004.013l-.002.008a.604.604 0 01-.045.079l-.006.008-.005.006-.006.006a.06.06 0 00-.01.015l-.007.008-.012.015-.012.012-.026.025-.014.012c-.004.004-.01.013-.014.013-.014.02-.03.02-.046.03a.058.058 0 01-.016.009c-.004.002-.006.004-.01.004-.004.002-.006.004-.01.004l-.014.007c-.007.002-.013.006-.017.008l-.018.006-.018.006-.018.006-.018.007-.018.004-.014.002-.014.002-.014.002-.014.002c-.008.002-.018.002-.028.002H3.053c-.18 0-.367.006-.548 0-.316-.01-.509-.265-.509-.575V2.998c0-.138-.002-.277.004-.416 0-.01 0-.018.002-.029a.517.517 0 01.509-.486h.026c.693-.006 1.386.004 2.08.004h7.826c.215 0 .42.087.515.3.066.157.048.348.048.515z"
          fill="#2D3D4D" />
      </symbol>
      <symbol id="calculator-white" viewBox="0 0 15 20">
        <path
          d="M14.995 1.541c0-.062.006-.122.004-.184-.002-.385-.15-.77-.438-1.022a1.33 1.33 0 00-.9-.335c-.088 0-.175.004-.263.004H1.422c-.058 0-.119-.006-.177-.002A1.341 1.341 0 00.001 1.314c-.004.139.004.275.004.412v16.379c0 .178-.006.356-.004.534.002.424.185.815.51 1.073.242.193.529.28.834.28h12.356a.414.414 0 00.4-.354.42.42 0 01-.376.362c.68-.023 1.214-.563 1.272-1.258.006-.058 0-.118 0-.178V1.541h-.002zM1.996 8.131v-.004l-.002-.016v-.004s.004 0 .004-.002L2 8.092l.002-.008.003-.008.002-.004.006-.03.002-.01.004-.016.002-.006.002-.007v-.002l.002-.004.002-.006.004-.01.004-.01.004-.011.002-.004.008-.017a.084.084 0 01.01-.018c.006-.013.014-.025.022-.038.006-.008.01-.016.018-.025l.018-.022.008-.009c.002-.002.004-.006.008-.008l.002-.002.002-.002.002-.002.002-.002.008-.008.004-.005.006-.006.014-.012.008-.006c.004-.002.006-.007.01-.009l.012-.01s.002 0 .002-.002a.135.135 0 01.023-.015.15.15 0 01.024-.014l.022-.012c.002-.002.006-.002.008-.005l.022-.01c.01-.004.018-.008.028-.01l.028-.01a.144.144 0 01.028-.009h.002a.058.058 0 01.018-.004l.01-.002c.01-.002.02-.004.03-.004l.03-.004c.015-.002.029-.002.043-.002.275-.005.55 0 .826 0 .233 0 .468-.01.703 0h.024c.004 0 .008 0 .012.002h.006l.012.002h.018l.004.002.012.002h.009l.008.002.018.004h.002l.008.002a.07.07 0 01.022.008l.01.005a.028.028 0 00.012.004c.008.002.014.006.022.01.004 0 .006.002.01.004l.01.006.012.007.012.006.004.002.01.006.01.006h.002a.035.035 0 01.01.009l.004.002.008.006c.004.004.01.006.015.01l.006.006.016.015.004.004.02.019.018.018.002.004.002.002.016.021s0 .002.002.002c0 .002.002.002.004.004a.05.05 0 01.01.015l.004.006.016.025c.016.029.03.058.042.089l.008.025.006.022.002.007.002.008.002.008v.006l.002.01.002.011v.002l.002.015v.002l.002.01v.043c0 .003 0 .005.002.005 0 .008 0 .016.002.024l-.002.025-.002.004v.052l.002.017v1.194c0 .089.006.18.004.269a.448.448 0 01-.008.074c0 .006-.002.01-.004.017a.075.075 0 01-.004.02v.002a.03.03 0 01-.004.013v.002l-.006.02-.002.007-.004.01c0 .004-.002.006-.004.01 0 .003 0 .003-.002.005v.004l-.002.008-.006.012a.494.494 0 01-.108.168l-.024.023-.023.018-.016.013c-.006.004-.01.008-.016.01-.006.004-.01.008-.016.01-.006.005-.012.007-.018.01-.006.005-.012.007-.018.011l-.018.008h-.002a.12.12 0 00-.016.007.345.345 0 01-.042.014l-.034.01h-.002a.642.642 0 01-.063.01c-.006.003-.012.003-.02.005l-.014.004-.004.002H2.635c-.124 0-.247 0-.361-.062-.322-.17-.276-.546-.276-.854V8.184c0-.011 0-.021.002-.03a.032.032 0 01-.004-.022zm2.554 9.274c0 .009 0 .019-.002.027l-.002.019c0 .004 0 .006-.002.008 0 .004 0 .006-.002.01 0 .002 0 .007-.002.009 0 .004 0 .006-.002.01-.002.006-.006.012-.006.019-.002.008-.008.016-.008.025 0 .006 0 .012-.002.02l-.002.006c0 .003 0 .005-.002.007l.002.006.002.002c0 .002-.004.002-.004.002a.004.004 0 01-.004.004.016.016 0 01-.004.008l-.002.003c0 .002-.006.004-.006.006h.002v.002l-.002.004-.002.004c0 .002.002.002 0 .004v.006h-.004.004s0 .003-.002.003l-.004.004-.002.002-.006.01c-.002.006-.006.01-.008.017a.346.346 0 01-.028.045c-.01.015-.022.029-.034.044-.002.004-.006.006-.01.01-.002.004-.006.006-.01.01l-.014.015-.014.012-.002.002-.002.002-.01.008-.003.003-.002.002s-.002 0-.002.002l-.008.006-.012.01-.002.002-.004.002-.018.013-.004.002-.022.014-.014.009-.012.006-.016.008h-.002l-.016.006-.014.007-.012.004c-.002 0-.004 0-.004.002l-.012.004c-.002 0-.004 0-.004.002h-.002a.06.06 0 01-.016.004c-.002 0-.004 0-.006.002l-.01.002h-.01l-.01.002-.012.002c-.004 0-.006.003-.01.003l-.02.002c-.012.002-.022 0-.036 0h-.006c-.002 0-.004 0-.004-.002H2.622c-.124 0-.247.006-.361-.054-.322-.17-.276-.538-.276-.847v-1.174c0-.011 0-.021.002-.032l-.002-.029v-.004l-.002-.017v-.004l.004-.002.002-.01v-.002l.002-.008.002-.009.002-.004.006-.029c0-.004.003-.008.003-.01l.004-.017.002-.006.002-.006v-.002l.002-.004.002-.006.004-.01.008-.022v-.002l.008-.018a.084.084 0 01.01-.019l.022-.037c.006-.008.01-.017.016-.023l.002-.004.016-.02.018-.02.002-.001.018-.019.002-.002.004-.002c.004-.004.008-.006.01-.01l.008-.007.02-.016.002-.002c.004-.002.006-.007.01-.009.004-.002.008-.004.012-.008l.004-.002.02-.012.023-.013.008-.004.022-.01.024-.01.01-.005.016-.006.01-.004.008-.002.016-.004h.002l.014-.002c.004-.002.01-.002.014-.005a.149.149 0 01.03-.004l.03-.004c.015-.002.029-.002.043-.002.275-.004.55 0 .826 0h.122c.193-.002.388-.01.581-.004l.002-.004h.002c.006 0 .012.004.016.006l.004.002.012.002c.002 0 .004 0 .006.002h.004l.004.002s0 .005.002.005h.023c.006.002.01 0 .016.002h.01l.009.002.008.002h.006l.008.002h.002c.008.002.016.004.024.008 0 0 .002 0 .002.002l.012.004c.008.002.014.006.022.01l.01.005.01.006a.521.521 0 01.066.041l.006.007.004.002.006.004.004.002.002.002a.129.129 0 01.023.02l.02.02.018.018.002.004.002.002.006.006.01.013s0 .002.002.002c0 .002.002.004.004.004l.01.014.004.006.016.025c.016.03.03.058.042.09l.008.024.006.023.002.004.004.014c0 .003 0 .005.002.005v.012-.004c.002.008.006.012.006.02v.025l.002.021v.013s0 .002-.002.002v1.326c.026.103.032.192.03.281zm0-3.823c0 .008 0 .016-.002.024l-.002.03v.006a.534.534 0 01-.056.182l-.016.03c-.004.009-.01.015-.014.024a.36.36 0 01-.048.062.187.187 0 01-.028.029.333.333 0 01-.03.026c-.007.005-.01.009-.017.013l-.016.012-.016.01a.337.337 0 01-.054.03l-.018.008a.028.028 0 01-.012.004.028.028 0 01-.012.004l-.008.002-.012.004-.012.004-.006.003-.014.004-.006.002-.008.002-.009.002-.042.006-.02.004h-.006l-.006.002-.004.002H2.631c-.124 0-.247-.002-.361-.064-.322-.17-.276-.546-.276-.856v-1.175c0-.011 0-.021.002-.03v-.031h-.002v-.029l.002-.002s.002 0 .002-.002a.015.015 0 01.01-.004l-.001-.01v-.002h-.002l.004-.03c0-.001 0-.005.002-.008v-.002c.002-.004.002-.008.002-.01l.002-.006c0-.004.002-.008.004-.013v-.002l.002-.006c0-.002 0-.004.002-.006l.004-.01.008-.021v-.002l.008-.019a.361.361 0 01.032-.056l.012-.016c.002-.002 0-.004.002-.006 0 0-.002 0-.002-.002.02-.009.02-.017.02-.023v-.002s0-.002.002-.002l.002-.002s-.002-.002 0-.002h-.002c0-.005.012-.007.016-.01l.004-.003v-.012.002s0-.002.002-.002v.002l.002.002h.002l.002-.002.002-.002h.008v-.009l.002-.002.004-.006v-.004c.002 0 .004 0 .006-.002a.004.004 0 00.004-.004v.006-.004l.002-.004c.002-.002.004-.007.006-.007V11.6l.012-.01s.002 0 .002-.002h.004l.008-.006.014-.01c.009-.007.017-.01.025-.015l.012-.006.012-.006.006-.003.02-.01.006-.002.018-.008.01-.004.016-.007.01-.002h.004l.008-.002.004-.002c.002 0 .004 0 .004-.002.006-.002.01-.004.016-.004a.028.028 0 00.012-.004h.002c.004 0 .006 0 .008-.002.006-.002.014-.002.02-.002l.03-.004c.015-.003.029-.003.043-.003.275-.004.55 0 .826 0 .233 0 .468-.01.703 0h.024c.004 0 .008 0 .012.003h.008l.006.002h.002s0 .002.002.002h.02l.016.002h.006c.002 0 .004 0 .006.002l.019.004.006.002h.006l.006.002h.002c.004 0 .008.002.012.004l.004.002.008.002c.004.002.008.002.012.005l.006.002.016.008c.002 0 .004.002.006.004h.002l.004.002.006.002h.002l.004.002.006.002.036.021a.06.06 0 01.014.01l.008.007.02.016.002.002c.006.004.012.01.019.017.002.002.004.002.004.004.006.006.014.012.02.02.006.007.012.01.016.017l.002.002s0 .002.002.002l.002.002v-.006l.004.006.002.005v.002a.358.358 0 01.006.006v.002l.004.002h.002c0 .002 0 .004.002.006.002.006.01.013.012.019l.004.006.002.004.008.015c.016.028.03.057.042.088l.008.025.006.023v.004a.075.075 0 01.004.021v.006l.002.006.006.025v.006l.002.021c0 .006 0 .015.002.02v.024c.002.033 0 .066 0 .097v1.216a.881.881 0 01.032.276zm4.218 2.309v.014l-.002.004v.005l.002.008v.01l-.002.01v1.392l.002.077v.033c0 .004 0 .008-.002.012v.039s.002 0 .002.002v.018l-.002.01h.002v.007-.003h-.002c-.002 0-.004 0-.004.003H8.76s-.002 0-.002.002l-.002.004c0 .002-.004.002-.004.004v.014c0 .002 0 .005-.002.007s0 .006-.002.008c0 .002 0 .004-.002.004l-.002.004-.008.023s.002 0 .002.002v.002l-.002.006-.002.007v.012c0 .002-.002.004-.004.006l-.002.004-.002.002s-.002 0-.002.002-.002.002-.004.005l-.002.002-.002.002v.012l-.002.008h.002c0 .002-.006.006-.008.009-.002.002-.004.004-.004.006l-.002.004-.002.002-.01.014a.054.054 0 01-.01.013l-.01.012-.018.023s0 .002-.002.002c-.002.004-.006.006-.008.008l-.03.032-.015.012c-.004.004-.008.008-.014.01l-.004.002-.016.013-.026.018a.497.497 0 01-.221.077c-.012.002-.022 0-.036 0-.008 0-.014-.004-.02-.004H6.863c-.124 0-.239.006-.353-.054-.322-.17-.268-.538-.268-.846v-1.024c0-.027 0-.052-.002-.08l-.002-.094v-.079h.002v-.01l.002-.019.004-.02a.462.462 0 01.014-.05l.006-.019.01-.025.009-.016.002-.004.01-.017v-.002l.012-.019.012-.018a.228.228 0 01.026-.036c.006-.006.012-.014.02-.02l.01-.01.01-.01a.026.026 0 01.008-.007l.02-.019c.002-.002.004-.002.004-.004l.002-.004c.006-.004.01-.014.016-.014v.004l.002-.002v-.002l.004-.002c.002-.003.002-.003.004-.003h.002l.008-.006h.002l.006-.004h.002s.002 0 .002-.002l.004-.002.008-.004h.002a.097.097 0 01.015-.006l.01-.007h.002s.002 0 .002-.002h.006v.002h.012l-.002-.002s-.002-.002 0-.002l.002-.002h.012l.024-.01.016-.006.018-.007.022-.006.052-.012c.002 0 .006 0 .008-.002l.025-.004h.002c.01-.002.018-.002.028-.002h.004c.275 0 .548.004.824.004h.701c.006 0 .014 0 .02.002.004 0 .008 0 .012.002l.02.002.022.004.008.002h.002c.004 0 .01.002.014.004l.01.002h.004l.018.007a.095.095 0 01.023.008l.024.008.022.008a.507.507 0 01.249.249l.006.014.002.004v.002l.006.015a.34.34 0 01.018.06v.002a.492.492 0 01.01.058v.006a.69.69 0 01.004.066v.004c0 .007 0 .015.002.021l.002.017v.022l-.002.002c.004-.024.006-.022.006-.02zm.002-2.31c0 .009 0 .017-.002.026l-.002.024-.002.021v.004a.296.296 0 01-.012.058.469.469 0 01-.036.101.348.348 0 01-.02.038l-.01.018a.12.12 0 01-.012.017l-.024.033-.027.031a1.954 1.954 0 00-.04.04l-.02.016a.063.063 0 01-.014.01.033.033 0 01-.014.008.11.11 0 01-.026.015l-.014.008-.018.008c-.008.005-.016.007-.024.01l-.016.007a1.245 1.245 0 00-.018.006c-.009.002-.017.006-.025.006a.591.591 0 01-.08.015l-.02.004-.004.002-.004.002-.006.002-.004.002H6.855c-.124 0-.239-.002-.353-.064-.322-.17-.268-.546-.268-.856V12.09c-.002-.035-.002-.07-.002-.104 0-.006 0-.01.002-.016v-.038l.002-.018v-.01c.002-.013.004-.027.008-.04a.513.513 0 01.5-.393h.1c.242-.002.483-.004.727-.004.096 0 .192-.006.287-.006h.078c.01.004.018.002.026.01 0 0 0 .002.002.002l.002.004v.002h.004l.004-.002h.061l.016.002h.052c.002 0 .004-.004.008-.004h.036l.024.002H8.3c.006 0 .01 0 .016.002h.01c.006 0 .01.003.016.003h.006l.012.002c.002 0 .002.004.004.004.006 0 .012.008.016.008.002 0 .004-.004.008-.002.006.002.01 0 .016.002h.004l.007.002h.004c.002 0 .004 0 .006.002.004 0 .008.002.01.004h.002l.008.004c.004.002.008.005.012.005a.69.69 0 00.012.006l.014.006c.002 0 .004.002.006.004l.004.002.01.006h.002l.008.004s.002.003.004.003c0 0 .002 0 .002.002l.004.002s.002 0 .002.002l.006.004h.002c.004.002.008.004.01.008l.012.008c.004.002.008.007.012.01l.022.02.012.01s.002 0 .002.002l.01.006.004.002c.002 0 .004.006.004.008l.019.021c.006.006.01.015.016.02 0 0 0 .003.002.003l.014.02a.045.045 0 01.006.01l.006.011a.087.087 0 01.01.019 1.45 1.45 0 00.016.033v.002l.006.012.004.01.006.017.002.005v.006c.002.004.004.01.004.014l.002.008v.002l.004.017a.075.075 0 01.004.02c.002 0 .004 0 .004.003h.004l-.002.002v.004l.002.017v.084c0 .031.004.056.002.085v.02l-.002.036V13.3c.016.102.024.193.02.282zm0-3.823a.448.448 0 01-.008.074l-.004.025c0 .006-.002.01-.004.017l-.002.004v.002l-.002.01-.002.01-.002.009-.006.017v.002l-.002.004a.501.501 0 01-.092.167l-.012.015-.023.023-.038.035-.012.008-.014.01c-.006.005-.01.009-.016.01l-.016.011-.016.01a.073.073 0 01-.018.009l-.016.008h-.002l-.016.006-.014.006-.016.007c-.005.002-.009.002-.01.004a.06.06 0 01-.017.004.069.069 0 01-.02.004l-.02.004-.04.006c-.006.002-.014.002-.02.005-.004.002-.008.002-.014.002l-.004.002H6.853c-.124 0-.239 0-.353-.063-.322-.17-.268-.546-.268-.854V8.305c-.002-.047-.004-.095-.002-.142 0-.007 0-.01.002-.017v-.062l.002-.014.004-.025a.513.513 0 01.5-.393c.033 0 .067.004.1.004.02 0 .041.004.061.004h.006c.113-.02.223 0 .336 0 .109 0 .215-.004.324-.004.08 0 .16-.002.24-.004h.113c.034 0 .066 0 .1.002l.077.002h.008c.008-.002.014-.004.02-.004h.002c.01-.003.018 0 .028.006 0 0 .002 0 .002.002l.002.002h.012s.002 0 .002-.002v.002-.002l.004-.002h.008l.006-.002h.025c.002 0 .004 0 .006.002l.006.002c.008 0 .018.002.026.004h.046c.008 0 .016 0 .024.002h.006a.1.1 0 01.024.004c.006 0 .012.002.016.004.006 0 .012.002.016.004.004 0 .008.003.012.005H8.4l.006.002c.006.002.01 0 .017.002h.006v.006l.004.002.006.002c.004.002.006.004.01.004l.004.002.004.002.02.009.046.024.022.015.01.008.01.008c.006.007.014.01.02.017l.018.017.018.018c.006.006.013.013.017.02l.016.022s0 .002.002.002c.004.006.01.012.014.02.004.009.01.015.014.023a.098.098 0 01.01.02c.002.007.006.011.008.017l.006.013.006.012.002.004v.002l.004.01.006.017.002.004c0 .003 0 .003.002.005v.002h.004v.008a.18.18 0 01.008.035v.002l.004.019.004.02.002.017c.002.019.004.04.004.06v.004c0 .031.004.056.002.087v.02l-.002.034v1.17c.01.087.018.178.014.267zm1.682-1.492v-.035a.72.72 0 01.014-.186l.004-.019.004-.012.002-.004v-.005l.002-.008v-.002l.006-.017.004-.01.002-.006v-.004l.004-.008c.002-.005.004-.007.004-.01l.004-.01c.002-.003.004-.01.008-.014a.906.906 0 01.046-.072l.019-.02.002-.003.006-.008.002-.002.002-.002.002-.002s0-.002.002-.002l.002-.002.006-.007.006-.006.004-.004.004-.004c.006-.004.01-.01.016-.015l.01-.008.01-.008h.002l.02-.015c.01-.006.02-.014.032-.018 0 0 .002 0 .002-.002l.008-.004.002.002c.008-.019.016-.021.024-.021h.01v-.004c0-.002.008-.002.012-.004l.007-.002.006-.002.002-.005.002-.004.004.002c.004-.002.006 0 .01-.002h.006c.004-.002.006-.002.01-.002h.006l.022-.006.008-.002.008-.002h.002l.018-.004.012-.002.012-.002c.006-.003.014-.003.022-.003.014-.002.028-.002.042-.002.276-.004.553 0 .828 0h.087c.032 0 .064 0 .094-.002h.078c.037 0 .073 0 .107-.002h.221a2.194 2.194 0 01.14.006h.015c.002 0 .002 0 .004.003h.002l.004.002h.002s.002-.002.002 0h.006c.004 0 .008-.002.012 0h.02v.002h.01v-.002.004c.004 0 .006.004.01.004l.01.002c.01.002.02.006.03.01.004.002.008.002.01.004.008.003.016.007.025.009a6.845 6.845 0 00.078.041l.004.002.006.002.004.002h.002l.012.009.002.002h.002l.002.002.002.002.002.002.002.002h.002l.004.002.004.002.008.006.002.002.018.017c.006.006.012.008.018.012.002.002.002 0 .002 0 .004 0 .006.01.01.013a.016.016 0 01.004.008l.012.014c0 .003.002.003.002.005l.017.02c.004.007.011.013.016.019l.002.002h.002l.002.002c.002.002.004.002.004.004 0 .004 0 .006.002.01 0 0 0 .003.002.005v.002c.002.002.002.004.002.006l.006.012a.098.098 0 01.01.021l.002.006c.002.007.006.01.008.017l.018.047.006.02.004.016v.002l.004.018.004.019a.06.06 0 01.002.019v.006c0 .004.004.006.004.01l.004.002-.002.008c0 .003-.002.007 0 .01l.002.024c0 .008 0 .016.002.022v1.289c0 .09 0 .18-.004.27 0 .008 0 .016-.002.026l-.006.05a92.603 92.603 0 00-.016.072c-.002.007-.002.01-.006.017-.002.004-.002.01-.006.014a.075.075 0 01-.008.019l-.018.04-.01.02-.012.02a.328.328 0 01-.027.038l-.01.019h.004l-.008.004h-.004v.004c0 .002 0 .006-.002.008h-.004l-.002.004-.002.002c-.002.002-.004.002-.006.005l-.008.006-.046.043-.026.02-.012.01c-.006.003-.01.003-.016.005l-.006.002-.004.002-.004.002c-.004.003-.006.007-.008.01h-.012c-.002 0-.004 0-.006.003a.101.101 0 00-.03.014.175.175 0 01-.035.017l-.016.004-.05.012c-.016.003-.03.007-.046.007-.002 0-.004 0-.006.002-.002 0-.006 0-.008.002l-.016.002h-.006c-.002 0-.004-.002-.004 0h-.006l-.012.002h-1.395c-.125 0-.24 0-.354-.062-.321-.17-.267-.546-.267-.855V8.328c-.002-.01-.002-.035-.004-.062zm2.554 4.147v5.011a.263.263 0 01-.004.041v.002a.517.517 0 01-.5.466c-.25.008-.503 0-.752 0-.261 0-.524.008-.788 0a.515.515 0 01-.504-.462c-.004-.04-.004-.08-.004-.122v-5.19c0-.208.006-.402.177-.553.209-.184.559-.12.816-.12h.956c.13 0 .261 0 .376.08a.535.535 0 01.227.443c.002.135 0 .27 0 .404zm0-9.527V5.45a2.323 2.323 0 01-.008.17.306.306 0 01-.008.045l-.004.02a.8.8 0 01-.012.04l-.006.01s-.002 0-.002.003c0 0 .002 0 .002.002-.004.006-.004.012-.008.02a.044.044 0 00-.004.013l-.002.008a.604.604 0 01-.045.079l-.006.008-.005.006-.006.006a.06.06 0 00-.01.015l-.007.008-.012.015-.012.012-.026.025-.014.012c-.004.004-.01.013-.014.013-.014.02-.03.02-.046.03a.058.058 0 01-.016.009c-.004.002-.006.004-.01.004-.004.002-.006.004-.01.004l-.014.007c-.007.002-.013.006-.017.008l-.018.006-.018.006-.018.006-.018.007-.018.004-.014.002-.014.002-.014.002-.014.002c-.008.002-.018.002-.028.002H3.053c-.18 0-.367.006-.548 0-.316-.01-.509-.265-.509-.575V2.998c0-.138-.002-.277.004-.416 0-.01 0-.018.002-.029a.517.517 0 01.509-.486h.026c.693-.006 1.386.004 2.08.004h7.826c.215 0 .42.087.515.3.066.157.048.348.048.515z"
          fill="#fff" />
      </symbol>
      <symbol id="calendar-check" viewBox="0 0 45 48">
        <g fill="none" fill-rule="evenodd">
          <path
            d="M1.514 12.94h41.888V7.348a1.996 1.996 0 00-2.01-1.984h-3.196V4.15h3.197c1.788 0 3.236 1.43 3.236 3.197V44.75c0 1.767-1.448 3.197-3.236 3.197H3.523c-1.789 0-3.236-1.43-3.236-3.197V7.348c0-1.767 1.447-3.197 3.236-3.197H6.72v1.213H3.523c-1.11 0-2.01.887-2.01 1.984v5.592zm0 1.212V44.75c0 1.097.898 1.985 2.01 1.985h37.869c1.11 0 2.009-.888 2.009-1.985V14.152H1.514zm18.977-10v1.212h-9.837V4.15h9.837zm13.77 0v1.212h-9.836V4.15h9.836zm1.865-2.94c-.747 0-1.353.599-1.353 1.337v4.21c0 .738.606 1.337 1.353 1.337.748 0 1.354-.599 1.354-1.337v-4.21c0-.738-.606-1.337-1.354-1.337zm0-1.212c1.426 0 2.581 1.141 2.581 2.549v4.21c0 1.408-1.155 2.549-2.58 2.549-1.426 0-2.582-1.141-2.582-2.55V2.55c0-1.409 1.156-2.55 2.581-2.55zm-27 1.212c-.747 0-1.353.599-1.353 1.337v4.21c0 .738.606 1.337 1.353 1.337.748 0 1.354-.599 1.354-1.337v-4.21c0-.738-.606-1.337-1.354-1.337zm0-1.212c1.426 0 2.581 1.141 2.581 2.549v4.21c0 1.408-1.155 2.549-2.58 2.549-1.426 0-2.582-1.141-2.582-2.55V2.55C6.545 1.141 7.701 0 9.126 0zm13.091 1.212c-.747 0-1.353.599-1.353 1.337v4.21c0 .738.606 1.337 1.353 1.337.748 0 1.354-.599 1.354-1.337v-4.21c0-.738-.606-1.337-1.354-1.337zm0-1.212c1.426 0 2.581 1.141 2.581 2.549v4.21c0 1.408-1.155 2.549-2.58 2.549-1.426 0-2.582-1.141-2.582-2.55V2.55c0-1.409 1.156-2.55 2.581-2.55z"
            fill="#344557" fill-rule="nonzero" />
          <path fill="#F06B63"
            d="M17.369 35.196l1.455 1.407 13.833-13.377-1.457-1.408zm-5.096-4.929l5.096 4.929 1.455-1.409-5.095-4.929z" />
        </g>
      </symbol>
      <symbol id="call-icon" viewBox="0 0 30 30">
        <path
          d="M17.604 19.745c-1.255-.364-5.844-5.36-6.802-6.787-.19-.283-.324-.444 0-.767.405-.404 1.822-2.141 2.024-3.313.162-.937-1.62-2.868-2.55-3.514-.527-.324-1.053-.801-3.645 1.05-3.563 2.545-.81 8.2 3.645 13.008 4.454 4.807 9.474 6.422 11.296 6.019 2.39-.53 3.928-3.313 3.928-4.484-.081-1.414-2.915-3.474-3.928-3.474-.85-.122-2.712 2.625-3.968 2.262zm-1.295-8.928c.89.014 2.81.655 3.36 3.11m-3.36-6.059c1.876.08 5.75 1.406 6.235 6.06M16.511 5c2.591.04 8.017 1.882 8.989 8.928"
          stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="call-transparent" viewBox="0 0 40 40">
        <path
          d="M23.472 26.326c-1.673-.485-7.792-7.146-9.07-9.049-.253-.377-.431-.592 0-1.023.54-.539 2.43-2.855 2.7-4.417.216-1.25-2.16-3.824-3.401-4.686-.702-.43-1.404-1.067-4.86 1.4-4.75 3.394-1.079 10.935 4.86 17.344 5.938 6.41 12.633 8.564 15.062 8.026C31.948 33.215 34 29.504 34 27.942c-.108-1.885-3.887-4.632-5.237-4.632-1.134-.162-3.617 3.5-5.29 3.016z"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M21.745 14.423c1.188.018 3.746.872 4.48 4.147m-4.48-8.08c2.501.109 7.666 1.875 8.314 8.08M22.015 6.667C25.47 6.72 32.705 9.177 34 18.57"
          stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="cart" viewBox="0 0 33 26">
        <path
          d="M29.434 5.123H6.892l-.595-1.529A5.274 5.274 0 001.387.236C.698.236.142.793.142 1.48c0 .689.556 1.246 1.243 1.246 1.15 0 2.18.706 2.596 1.776l4.484 11.494a4.561 4.561 0 004.244 2.902h12.893a4.55 4.55 0 004.244-2.902l2.547-6.527c.15-.378.22-.775.22-1.161-.002-1.696-1.36-3.18-3.18-3.186zm.645 3.439l-2.547 6.523a2.073 2.073 0 01-1.929 1.32H12.71a2.07 2.07 0 01-1.929-1.32L7.865 7.608h21.569c.398 0 .693.334.696.694l-.051.26zM28.446 23.4c0 3.249-4.863 3.249-4.863 0 0-3.245 4.863-3.245 4.863 0m-13.696.105c0 3.245-4.862 3.245-4.862 0s4.863-3.245 4.863 0"
          fill="currentColor" fill-rule="evenodd" />
      </symbol>
      <symbol id="chat-icon" viewBox="0 0 60 60">
        <path fill="#F4F4F4"
          d="M26.869 36.888c-2.701 4.941-7.286 7.388-9.24 7.993 1.243-1.453 1.421-7.993 0-7.993-3.2 0-12.262-3.997-12.44-13.08.143-11.918 7.553-15.806 14.75-15.806 3.968-.06 4.62 0 10.573 0 9.684 0 13.333 9.81 12.705 15.806-1.257 11.99-11.728 12.898-16.348 13.08zm19.321-6.292c2.604.179 7.81 2.275 7.81 9.226 0 4.389-5.206 8.061-7.81 7.882-.119.926-.143 3.064.719 4.21-1.167.538-4.398-.895-6.014-4.21-1.706 0-7.18.717-9.515-6.001"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path fill="#F4F4F4" d="M15.904 17.5H32.57m-16.667 4.762H32.57m-16.667 4.762H26.7" stroke="#FB6058"
          stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="chevron-down-v2" viewBox="0 0 11 6">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 .93L1.009 0l4.492 4.14L9.99 0 11 .93 5.5 6 0 .93z"
          fill="#2D3D4D" />
      </symbol>
      <symbol id="chevron-down" viewBox="0 0 21 21">
        <g fill="none" fill-rule="evenodd">
          <mask id="chevron-downb" fill="#fff">
            <use xlinkHref="#chevron-downa" />
          </mask>
          <use fill="#000" fill-rule="nonzero" xlinkHref="#chevron-downa" />
          <g fill="#040C49" mask="url(#chevron-downb)">
            <path d="M0 0h21v21H0z" />
          </g>
        </g>
      </symbol>
      <symbol id="circle-inactive" viewBox="0 0 20 20">
        <circle cx="10" cy="10" r="10" fill="#fff" />
        <circle cx="10" cy="10" r="9.5" stroke="currentColor" />
      </symbol>
      <symbol id="clock" viewBox="0 0 72 73">
        <path
          d="M66.42 36.286a30.695 30.695 0 01-18.944 28.359A30.703 30.703 0 0114.022 58 30.698 30.698 0 1135.71 5.59v30.695l26.602-15.351a30.477 30.477 0 014.109 15.35zm-4.108-15.351L35.71 36.285V5.592c-13.887.012-26.04 9.347-29.625 22.765C2.499 41.774 8.37 55.926 20.4 62.864c12.032 6.942 27.223 4.934 37.039-4.887 9.82-9.824 11.817-25.015 4.872-37.042zM54.21 66.833c-13.668 8.28-31.176 6.469-42.86-4.434C-.336 51.5-3.351 34.16 3.96 19.949c12.8-24.846 48.75-25.401 62.676-1.5 9.625 16.614 3.94 38.462-12.422 48.388l-.004-.004zm8.102-45.898L35.71 36.285V5.592C18.827 5.759 5.187 19.407 5.026 36.286c.157 16.883 13.8 30.53 30.684 30.699 23.168 0 38.25-25.898 26.602-46.05zm0 0L35.71 36.285V5.592c-13.887.012-26.04 9.347-29.625 22.765C2.499 41.774 8.37 55.926 20.4 62.864c12.032 6.942 27.223 4.934 37.039-4.887 9.82-9.824 11.817-25.015 4.872-37.042zm4.109 15.35a30.695 30.695 0 01-18.945 28.36A30.703 30.703 0 0114.022 58 30.698 30.698 0 1135.71 5.59v30.695l26.602-15.351a30.477 30.477 0 014.109 15.35zm-56.781.88h4.273V35.43H9.64v1.734zm7.629 10.77l-.856-1.5-3.703 2.136.855 1.5 3.704-2.136zm0-23.29l-3.704-2.136-.855 1.5 3.703 2.136.856-1.5zm8.293 30.954l-1.5-.856-2.137 3.703 1.5.856 2.137-3.703zm0-38.625l-2.137-3.704-1.5.856 2.137 3.703 1.5-.855zM36.58 58.09h-1.71v4.273h1.71V58.09zm12.93.35l-2.137-3.702-1.5.856 2.137 3.703 1.5-.856zm9.23-9.854l-3.703-2.137-.855 1.5 3.703 2.137.855-1.5zm3.067-13.156h-4.274v1.734h4.274V35.43z"
          fill="currentColor" fill-rule="evenodd" />
      </symbol>
      <symbol id="coockie-icon" viewBox="0 0 22 22">
        <g fill="#334557">
          <path
            d="M18.333 0c-.918 0-1.778.334-2.444.936A3.625 3.625 0 0013.444 0c-.918 0-1.778.334-2.444.936A3.625 3.625 0 008.556 0C7.637 0 6.777.334 6.11.936A3.625 3.625 0 003.667 0 3.67 3.67 0 000 3.667c0 .918.334 1.778.936 2.444A3.625 3.625 0 000 8.556C0 9.474.334 10.334.936 11A3.625 3.625 0 000 13.444c0 .919.334 1.779.936 2.445A3.625 3.625 0 000 18.333 3.67 3.67 0 003.667 22c.918 0 1.778-.334 2.444-.936A3.625 3.625 0 008.556 22c.918 0 1.778-.334 2.444-.936a3.625 3.625 0 002.444.936c.919 0 1.779-.334 2.445-.936a3.625 3.625 0 002.444.936A3.67 3.67 0 0022 18.333c0-.918-.334-1.778-.936-2.444A3.625 3.625 0 0022 13.444c0-.918-.334-1.778-.936-2.444A3.625 3.625 0 0022 8.556c0-.919-.334-1.779-.936-2.445A3.625 3.625 0 0022 3.667 3.67 3.67 0 0018.333 0zm1.462 6.6c.624.468.983 1.18.983 1.956 0 .774-.359 1.487-.983 1.955a.611.611 0 000 .978c.624.468.983 1.18.983 1.955 0 .775-.359 1.488-.983 1.956a.611.611 0 000 .978c.624.468.983 1.18.983 1.955a2.447 2.447 0 01-2.445 2.445 2.425 2.425 0 01-1.955-.983.611.611 0 00-.978 0 2.425 2.425 0 01-1.956.983 2.425 2.425 0 01-1.955-.983.611.611 0 00-.978 0 2.425 2.425 0 01-1.955.983 2.425 2.425 0 01-1.956-.983.611.611 0 00-.978 0 2.425 2.425 0 01-1.955.983 2.447 2.447 0 01-2.445-2.445c0-.774.359-1.487.983-1.955a.611.611 0 000-.978 2.425 2.425 0 01-.983-1.956c0-.774.359-1.487.983-1.955a.611.611 0 000-.978 2.425 2.425 0 01-.983-1.955c0-.775.359-1.488.983-1.956a.611.611 0 000-.978 2.425 2.425 0 01-.983-1.955 2.447 2.447 0 012.445-2.445c.774 0 1.487.359 1.955.983a.611.611 0 00.978 0 2.425 2.425 0 011.956-.983c.774 0 1.487.359 1.955.983a.611.611 0 00.978 0 2.425 2.425 0 011.955-.983c.775 0 1.488.359 1.956.983a.611.611 0 00.978 0 2.425 2.425 0 011.955-.983 2.447 2.447 0 012.445 2.445c0 .774-.359 1.487-.983 1.955a.611.611 0 000 .978z" />
          <circle cx="5.958" cy="5.958" r="1" />
          <circle cx="11.092" cy="5.958" r="1" />
          <circle cx="16.042" cy="5.958" r="1" />
          <circle cx="5.958" cy="10.542" r="1" />
          <circle cx="11.092" cy="10.542" r="1" />
          <circle cx="16.042" cy="10.542" r="1" />
          <circle cx="5.958" cy="16.042" r="1" />
          <circle cx="11.092" cy="16.042" r="1" />
          <circle cx="16.042" cy="16.042" r="1" />
        </g>
      </symbol>
      <symbol id="cross" viewBox="119 101 32 32">
        <path d="M121.5 130.5l27-27m0 27l-27-27" fill="none" stroke="currentColor" stroke-width="4" />
      </symbol>
      <symbol id="discount" viewBox="0 0 22 21">
        <path d="M12.516 7.246l-5.167 4.77" stroke="#00A56F" stroke-width=".667" stroke-linecap="round"
          stroke-linejoin="round" />
        <circle cx="8.541" cy="8.041" r=".795" stroke="#00A56F" stroke-width=".667" stroke-linecap="round"
          stroke-linejoin="round" />
        <circle cx="11.323" cy="11.222" r=".795" stroke="#00A56F" stroke-width=".667" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M16.442 11.631l-5.63 3.252-2.503 1.444-1.154.667a1.333 1.333 0 01-1.822-.488l-3.666-6.351a1.333 1.333 0 01.488-1.822l5.196-3 1.347-.777 2.744-1.585a1.333 1.333 0 011.103-.105l4.585 1.586c.627.217 1 .862.874 1.513l-.92 4.764c-.072.38-.307.709-.642.902z"
          stroke="#00A56F" stroke-width=".667" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M18 6l.758 4.328c.07.398-.045.806-.312 1.11l-4.057 4.615-1.82 2.07-.81.92a1.333 1.333 0 01-1.87.13L7.363 17"
          stroke="#00A56F" stroke-width=".667" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="16.111" cy="5.996" r=".75" stroke="#00A56F" stroke-width=".667" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="double-chevron-up" viewBox="0 0 11 12">
        <g fill="currentColor">
          <path d="M5.491 0L.035 5.414l1.548 1.535L5.49 3.072 9.4 6.949l1.548-1.535z" />
          <path d="M5.491 5.033L.035 10.447l1.548 1.536L5.49 8.105l3.91 3.878 1.548-1.536z" />
        </g>
      </symbol>
      <symbol id="dropdown-chevron" viewBox="0 0 12 7">
        <path d="M11 1L6 6 1 1" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="engineer" viewBox="2 0 50 65">
        <g fill="currentColor" fill-rule="evenodd">
          <path
            d="M34.094 37.277l.013 8.072-15.502.024-.013-8.071c5.043-1.811 10.538-1.822 15.502-.025m1.024-1.259c-5.51-2.133-11.875-2.124-17.48.035-.28.14-.447.398-.447.686l.015 9.339c0 .361.34.703.7.703l16.991-.027c.25 0 .385-.135.476-.226.105-.108.223-.33.223-.479l-.015-9.339c0-.266-.144-.584-.463-.692" />
          <path
            d="M1.262.418L1.243.407a.743.743 0 00-.682 0C.396.492.2.678.2 1.095l.01 6.777c0 .362.341.704.7.703.361 0 .7-.343.7-.705l-.008-5.452C7.276 6.853 10.66 13.64 10.67 20.644c0 .362.342.704.702.703.36 0 .7-.343.699-.705A24.374 24.374 0 001.262.418"
            mask="url(#engineermask-2)" transform="translate(38.765 38.28)" />
          <path
            d="M11.545.36c-.22-.109-.572-.122-.699.012C4.042 4.912-.013 12.52 0 20.72c0 .361.342.703.701.702.36 0 .7-.343.7-.704C1.39 13.487 4.67 6.82 10.412 2.369l.008 5.455c.001.362.342.704.702.704.36 0 .7-.344.699-.706L11.81.996a.464.464 0 00.014-.327.537.537 0 00-.279-.308"
            mask="url(#engineermask-4)" transform="translate(2 38.28)" />
          <path
            d="M39.539 10.9a2.446 2.446 0 01-1.391 1.326c-1.153.925-5.21 2.15-11.759 2.16-6.476.01-10.517-1.174-11.723-2.092a2.443 2.443 0 01-1.462-1.314c-.14.247-.216.509-.216.789.005 2.89 8.018 3.926 13.403 3.917 5.386-.008 13.396-1.07 13.392-3.959 0-.295-.088-.57-.244-.827" />
          <path
            d="M38.441 8.728l.002 1.208c0 .297-.115.564-.3.768a13.457 13.457 0 011.639 6.433c.012 7.44-6.002 13.502-13.406 13.514-7.403.012-13.435-6.032-13.447-13.472a13.47 13.47 0 011.632-6.461 1.14 1.14 0 01-.283-.744l-.001-1.254a14.759 14.759 0 00-2.64 8.46c.012 8.157 6.625 14.783 14.741 14.77 8.117-.012 14.71-6.659 14.698-14.815a14.763 14.763 0 00-2.635-8.407" />
          <path
            d="M18.893 1.323l14.92-.023c2.546-.004 4.62 2.074 4.624 4.632l.006 4.004a1.141 1.141 0 01-1.135 1.145l-21.89.034a1.142 1.142 0 01-1.14-1.14l-.006-3.993c-.004-2.565 2.07-4.655 4.62-4.66m-4.226 10.972c.237.078.49.121.753.12l21.89-.034a2.446 2.446 0 002.426-2.446L39.73 5.93C39.725 2.655 37.07-.005 33.812 0L18.89.024c-3.265.004-5.917 2.678-5.912 5.96l.006 3.992c.001.359.08.698.22 1.005.28.619.81 1.1 1.46 1.313m7.756 12.822a5.63 5.63 0 004.035 1.683 5.626 5.626 0 004.03-1.695l-8.066.012zm4.037 2.982a6.928 6.928 0 01-5.915-3.284l-.621-.994 13.059-.02-.618.995a6.927 6.927 0 01-5.905 3.303zM42.971 56.14l.063.098-2.746.004-.004-2.014.08.021c1.094.302 2.02.973 2.607 1.89zm.079 5.653l-.077.103c-.94 1.25-2.479 1.999-4.119 2.001h-.007c-1.742 0-3.39-.964-4.298-2.511a.433.433 0 00-.39-.243l-22.972.036h-.002c-1.137 0-2.1-.965-2.1-2.107-.003-1.105.996-2.113 2.096-2.115l22.97-.036a.418.418 0 00.382-.226c.913-1.566 2.563-2.539 4.306-2.542h.418l.012 7.178c0 .232.236.468.467.468l3.314-.006zm.879-.44a1.12 1.12 0 00-1.008-.626l-2.592.005-.006-3.424 2.597-.004a1.134 1.134 0 00.945-1.741c-.478-.746-1.921-2.476-5.015-2.476h-.012c-1.999.003-3.875 1.027-5.016 2.74l-.019.028-22.624.036c-1.684.002-3.159 1.49-3.156 3.183.001.847.332 1.644.93 2.243a3.133 3.133 0 002.231.928h.004l22.626-.036.018.028c1.142 1.708 3.016 2.726 5.014 2.726h.01c1.97-.002 3.825-.91 4.964-2.425.258-.345.3-.799.109-1.185z" />
          <path
            d="M37.547 57.994c.103 0 .294-.123.294-.297 0-.184-.11-.295-.295-.295l-1.682.003c-.187 0-.294.108-.294.296 0 .185.11.295.295.295l1.682-.002zm.002 1.152c.103 0 .294-.122.294-.296 0-.185-.11-.296-.295-.296l-1.682.003c-.184 0-.295.112-.294.297 0 .188.108.295.294.295l1.683-.003zm.001 1.159c.101 0 .294-.115.294-.296 0-.185-.11-.295-.294-.295l-1.683.002c-.187.001-.294.108-.293.297 0 .187.108.295.294.295l1.682-.003zm-25.357-1.247c.002 1.24-1.852 1.242-1.854.003-.002-1.244 1.852-1.247 1.854-.003" />
        </g>
      </symbol>
      <symbol id="epa-heat-a" viewBox="0 0 151 47">
        <g transform="translate(0 .564)" fill="none" fill-rule="evenodd">
          <mask id="epa-heat-ab" fill="#fff">
            <use xlinkHref="#epa-heat-aa" />
          </mask>
          <path
            d="M23.31 0v.01a5.396 5.396 0 00-3.804 1.568L1.575 19.422a5.371 5.371 0 000 7.6l17.93 17.844a5.394 5.394 0 003.805 1.566v.004h127.373V0H23.31z"
            fill="#1EA54A" mask="url(#epa-heat-ab)" />
          <mask id="epa-heat-ad" fill="#fff">
            <use xlinkHref="#epa-heat-ac" />
          </mask>
          <path fill="#FEFEFE" mask="url(#epa-heat-ad)" d="M106.91 42.613h39.527V3.275H106.91z" />
          <path
            d="M116.522 13.41a1.336 1.336 0 00-.366-.07h-.585v-.936h.585c.111 0 .255-.036.366-.072v1.079zm0 18.15a1.324 1.324 0 00-.366-.071h-.695a.996.996 0 00.11-.47.971.971 0 00-.11-.468h.695c.111 0 .255-.036.366-.071v1.08zm-2.158-.071a.468.468 0 01-.33-.145.456.456 0 01-.146-.324v-.109c0-.072.072-.18.11-.216a.52.52 0 01.147-.108c.072-.036.11-.036.183-.036.255 0 .476.216.476.469.036.289-.183.469-.44.469zm-.474-17.285h.95V11.54h-.95v2.665zm3.361 19.59c.147.07.294.07.477.07.878 0 1.207-.72 1.207-1.188v-.11c0-.215.183-.359.366-.359h.219c.183 0 .366.144.366.36v.109c0 .18.036.431.183.611-1.463.469-1.865 1.874-1.865 2.703 0 .108-.036.468-.476.468-.402 0-.477-.324-.477-.468v-2.196zm.002-22.58c0-.108.037-.468.477-.468.438 0 .476.324.476.469v21.46c0 .145-.036.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.682 19.301c.111.036.256.072.366.072h.219c.11 0 .256-.036.366-.072v1.045a1.32 1.32 0 00-.366-.071h-.219c-.11 0-.255.035-.366.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.219c-.183 0-.366-.18-.366-.36V14.42zm0-2.052c.111.036.256.071.366.071h.219c.11 0 .256-.035.366-.07v1.044a1.33 1.33 0 00-.366-.071h-.219c-.11 0-.255.035-.366.07v-1.044zm1.644-1.153c0-.109.037-.469.477-.469.438 0 .476.324.476.469v21.46c0 .11-.036.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.723 21.46v-.144c.036-.18.183-.324.366-.324h.219c.218 0 .365.144.365.36v.109c0 .144.036.289.11.468h-1.17c.071-.144.11-.324.11-.468zm-.039-2.16c.111.036.256.072.366.072h.22c.11 0 .255-.036.365-.072v1.045a1.327 1.327 0 00-.366-.071h-.219c-.146 0-.255.035-.366.07v-1.044zm.99-16.095v15.052c0 .18-.184.36-.366.36h-.22c-.182 0-.365-.144-.365-.324V14.384c.036-.18.183-.324.366-.324h.219c.182 0 .365.18.365.36zm-.99-2.052c.111.036.256.071.366.071h.22c.11 0 .255-.035.365-.07v1.044a1.338 1.338 0 00-.366-.071h-.219c-.146 0-.255.035-.366.07v-1.044zm1.719-1.153c0-.109.036-.469.476-.469.402 0 .476.324.476.469v21.46c0 .145-.072.47-.476.47-.402 0-.476-.325-.476-.47v-21.46zm1.684 21.46v-.108c0-.216.146-.36.365-.36h.183c.183 0 .366.144.366.36v.109c0 .144.036.289.11.468h-1.135c.073-.144.11-.324.11-.468zm-.036-2.16h.036c.11.036.219.072.365.072h.183c.111 0 .255-.036.366-.072v1.045a1.327 1.327 0 00-.366-.071h-.219c-.11 0-.255.035-.365.07v-1.044zm0-16.095h.036c0-.18.146-.36.365-.36h.183c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.219c-.183 0-.365-.18-.365-.36V14.42zm0-2.052h.036c.11.036.219.071.365.071h.183c.111 0 .255-.035.366-.07v1.044a1.338 1.338 0 00-.366-.071h-.219c-.11 0-.255.035-.365.07v-1.044zm1.68-1.153c0-.109.036-.469.476-.469.438 0 .476.324.476.469v21.46c0 .145-.036.47-.476.47-.402 0-.476-.325-.476-.47v-21.46zm1.722 21.46v-.108c0-.216.182-.36.365-.36h.22c.183 0 .366.144.366.36v.109c0 .144.036.289.11.468h-1.172c.073-.144.11-.324.11-.468zm-.038-2.16c.11.036.255.072.366.072h.218c.147 0 .256-.036.366-.072v1.045a1.097 1.097 0 00-.366-.071h-.218c-.11 0-.255.035-.366.07v-1.044zm0-16.095c0-.18.182-.36.366-.36h.218c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.218c-.184 0-.366-.18-.366-.36V14.42zm0-2.052c.11.036.255.071.366.071h.218c.147 0 .256-.035.366-.07v1.044a1.097 1.097 0 00-.366-.071h-.218c-.11 0-.255.035-.366.07v-1.044zm1.681-1.153c0-.109.037-.469.477-.469.402 0 .476.324.476.469v21.46c0 .145-.072.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.685 21.46v-.108c0-.216.183-.36.366-.36h.219c.219 0 .366.144.366.36v.109c0 .144.035.289.11.468h-1.172c.073-.144.11-.324.11-.468zm0-2.16c.11.036.255.072.366.072h.219c.146 0 .255-.036.366-.072v1.045a1.329 1.329 0 00-.366-.071h-.22c-.146 0-.255.035-.365.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.219 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.22a.364.364 0 01-.365-.36V14.42zm0-2.052c.11.036.255.071.366.071h.219c.146 0 .255-.035.366-.07v1.044a1.34 1.34 0 00-.366-.071h-.22c-.146 0-.255.035-.365.07v-1.044zm1.718-1.118c0-.144.036-.469.476-.469.402 0 .476.325.476.469v21.426h-.038c0 .109-.036.468-.438.468-.438 0-.476-.324-.476-.468V11.25zm1.644 21.428v-.11c0-.215.183-.36.366-.36h.219c.22 0 .366.145.366.36v.11c0 .468.33 1.189 1.208 1.189.182 0 .33-.036.475-.072v2.195c0 .144-.072.468-.475.468-.402 0-.477-.324-.477-.468 0-.83-.402-2.232-1.865-2.7.11-.216.183-.433.183-.612zm.002-2.163c.11.036.255.072.366.072h.219c.147 0 .255-.036.365-.072v1.045a1.32 1.32 0 00-.365-.071h-.22c-.146 0-.254.035-.365.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.219 0 .365.18.365.36v15.052c0 .18-.182.36-.365.36h-.22a.364.364 0 01-.365-.36V14.42zm0-2.052c.11.036.255.071.366.071h.219c.147 0 .255-.035.365-.07v1.044a1.331 1.331 0 00-.365-.071h-.22c-.146 0-.254.035-.365.07v-1.044zm2.632 20.308c0 .109-.036.468-.475.468-.44 0-.477-.324-.477-.468V11.25c0-.144.073-.469.477-.469.401 0 .475.325.475.469v21.426zm.732-2.161c.11.036.255.072.366.072h.11c.255 0 .477.215.477.468 0 .254-.222.434-.477.434h-.11c-.147 0-.255.035-.366.07v-1.044zm0-18.185c.11.036.255.072.366.072h.11c.255 0 .477.215.477.468a.46.46 0 01-.477.47h-.11c-.147 0-.255.035-.366.07v-1.08zm-21.326 24.92c.879 0 1.208-.72 1.208-1.19 0-.07.036-2.124 2.158-2.124h13.46c2.085 0 2.16 1.91 2.16 2.125 0 .469.328 1.19 1.207 1.19.878 0 1.208-.721 1.208-1.19V32.64c0-.216.182-.36.365-.36h.11c.66 0 1.209-.54 1.209-1.19-.075-.717-.623-1.222-1.283-1.222h-.11a.364.364 0 01-.366-.36V14.42c0-.18.183-.36.366-.36h.11c.66 0 1.208-.54 1.208-1.19 0-.648-.548-1.188-1.208-1.188h-.11c-.22 0-.366-.145-.366-.36v-.072c0-.469-.33-1.189-1.207-1.189-.879 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.366.36h-.217a.364.364 0 01-.366-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.207.72-1.207 1.189v.109c0 .18-.183.36-.366.36h-.22a.363.363 0 01-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.182.36-.365.36h-.22c-.182 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.365.36h-.22c-.182 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.182.36-.365.36h-.22c-.182 0-.365-.144-.365-.325v-.144c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.365.36h-.22c-.183 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.366.36h-.584v-.144c0-.396-.33-.72-.732-.72h-.95a.729.729 0 00-.732.72v2.665c0 .395.33.72.731.72h.951a.729.729 0 00.732-.72v-.145h.584c.183 0 .366.18.366.36v15.053c0 .18-.183.36-.366.36h-1.792c-.22 0-.438.071-.621.18-.073.036-.183.109-.22.18a.626.626 0 00-.146.18.271.271 0 00-.072.145.511.511 0 00-.073.25c0 .072-.036.18-.036.29 0 .071 0 .144.036.251a.933.933 0 00.33.576c.072.071.183.144.293.215.183.072.366.145.585.145h1.792c.183 0 .366.144.366.36v3.42c0 .47.33 1.19 1.208 1.19z"
            fill="#817E7A" mask="url(#epa-heat-ad)" />
          <path
            d="M41.608 23.972h4.18l-2.171-7.32-2.01 7.32zm5.573 4.644H40.15l-1.88 5.096h-5.8l8.231-22.22h5.898l8.198 22.22h-5.735l-1.88-5.096z"
            fill="#FEFEFE" mask="url(#epa-heat-ad)" />
        </g>
      </symbol>
      <symbol id="epa-heat-b" viewBox="0 0 151 47">
        <g transform="translate(0 .564)" fill="none" fill-rule="evenodd">
          <mask id="epa-heat-bb" fill="#fff">
            <use xlinkHref="#epa-heat-ba" />
          </mask>
          <path
            d="M23.31 0v.01a5.396 5.396 0 00-3.804 1.568L1.575 19.422a5.371 5.371 0 000 7.6l17.93 17.844a5.394 5.394 0 003.805 1.566v.004h127.373V0H23.31z"
            fill="#FEF100" mask="url(#epa-heat-bb)" />
          <mask id="epa-heat-bd" fill="#fff">
            <use xlinkHref="#epa-heat-bc" />
          </mask>
          <path
            d="M49.81 17.804c0 1.287-.66 3.762-3.201 4.488 3.102.462 4.389 3.234 4.389 5.412 0 3.3-1.947 6.732-7.491 6.732h-9.966V11.7h9.306c4.224 0 6.963 2.442 6.963 6.105zm-10.725 2.673h2.97c1.122 0 1.914-.726 1.914-1.88 0-1.189-.528-2.08-1.848-2.08h-3.036v3.96zm3.498 4.521h-3.498v4.62h3.531c1.716 0 2.541-1.188 2.541-2.343 0-1.056-.726-2.277-2.574-2.277z"
            fill="#FFF" mask="url(#epa-heat-bd)" />
          <path fill="#FEFEFE" mask="url(#epa-heat-bd)" d="M106.91 42.613h39.527V3.275H106.91z" />
          <path
            d="M116.522 13.41a1.336 1.336 0 00-.366-.07h-.585v-.936h.585c.111 0 .255-.036.366-.072v1.079zm0 18.15a1.324 1.324 0 00-.366-.071h-.695a.996.996 0 00.11-.47.971.971 0 00-.11-.468h.695c.111 0 .255-.036.366-.071v1.08zm-2.158-.071a.468.468 0 01-.33-.145.456.456 0 01-.146-.324v-.109c0-.072.072-.18.11-.216a.52.52 0 01.147-.108c.072-.036.11-.036.183-.036.255 0 .476.216.476.469.036.289-.183.469-.44.469zm-.474-17.285h.95V11.54h-.95v2.665zm3.361 19.59c.147.07.294.07.477.07.878 0 1.207-.72 1.207-1.188v-.11c0-.215.183-.359.366-.359h.219c.183 0 .366.144.366.36v.109c0 .18.036.431.183.611-1.463.469-1.865 1.874-1.865 2.703 0 .108-.036.468-.476.468-.402 0-.477-.324-.477-.468v-2.196zm.002-22.58c0-.108.037-.468.477-.468.438 0 .476.324.476.469v21.46c0 .145-.036.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.682 19.301c.111.036.256.072.366.072h.219c.11 0 .256-.036.366-.072v1.045a1.32 1.32 0 00-.366-.071h-.219c-.11 0-.255.035-.366.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.219c-.183 0-.366-.18-.366-.36V14.42zm0-2.052c.111.036.256.071.366.071h.219c.11 0 .256-.035.366-.07v1.044a1.33 1.33 0 00-.366-.071h-.219c-.11 0-.255.035-.366.07v-1.044zm1.644-1.153c0-.109.037-.469.477-.469.438 0 .476.324.476.469v21.46c0 .11-.036.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.723 21.46v-.144c.036-.18.183-.324.366-.324h.219c.218 0 .365.144.365.36v.109c0 .144.036.289.11.468h-1.17c.071-.144.11-.324.11-.468zm-.039-2.16c.111.036.256.072.366.072h.22c.11 0 .255-.036.365-.072v1.045a1.327 1.327 0 00-.366-.071h-.219c-.146 0-.255.035-.366.07v-1.044zm.99-16.095v15.052c0 .18-.184.36-.366.36h-.22c-.182 0-.365-.144-.365-.324V14.384c.036-.18.183-.324.366-.324h.219c.182 0 .365.18.365.36zm-.99-2.052c.111.036.256.071.366.071h.22c.11 0 .255-.035.365-.07v1.044a1.338 1.338 0 00-.366-.071h-.219c-.146 0-.255.035-.366.07v-1.044zm1.719-1.153c0-.109.036-.469.476-.469.402 0 .476.324.476.469v21.46c0 .145-.072.47-.476.47-.402 0-.476-.325-.476-.47v-21.46zm1.684 21.46v-.108c0-.216.146-.36.365-.36h.183c.183 0 .366.144.366.36v.109c0 .144.036.289.11.468h-1.135c.073-.144.11-.324.11-.468zm-.036-2.16h.036c.11.036.219.072.365.072h.183c.111 0 .255-.036.366-.072v1.045a1.327 1.327 0 00-.366-.071h-.219c-.11 0-.255.035-.365.07v-1.044zm0-16.095h.036c0-.18.146-.36.365-.36h.183c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.219c-.183 0-.365-.18-.365-.36V14.42zm0-2.052h.036c.11.036.219.071.365.071h.183c.111 0 .255-.035.366-.07v1.044a1.338 1.338 0 00-.366-.071h-.219c-.11 0-.255.035-.365.07v-1.044zm1.68-1.153c0-.109.036-.469.476-.469.438 0 .476.324.476.469v21.46c0 .145-.036.47-.476.47-.402 0-.476-.325-.476-.47v-21.46zm1.722 21.46v-.108c0-.216.182-.36.365-.36h.22c.183 0 .366.144.366.36v.109c0 .144.036.289.11.468h-1.172c.073-.144.11-.324.11-.468zm-.038-2.16c.11.036.255.072.366.072h.218c.147 0 .256-.036.366-.072v1.045a1.097 1.097 0 00-.366-.071h-.218c-.11 0-.255.035-.366.07v-1.044zm0-16.095c0-.18.182-.36.366-.36h.218c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.218c-.184 0-.366-.18-.366-.36V14.42zm0-2.052c.11.036.255.071.366.071h.218c.147 0 .256-.035.366-.07v1.044a1.097 1.097 0 00-.366-.071h-.218c-.11 0-.255.035-.366.07v-1.044zm1.681-1.153c0-.109.037-.469.477-.469.402 0 .476.324.476.469v21.46c0 .145-.072.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.685 21.46v-.108c0-.216.183-.36.366-.36h.219c.219 0 .366.144.366.36v.109c0 .144.035.289.11.468h-1.172c.073-.144.11-.324.11-.468zm0-2.16c.11.036.255.072.366.072h.219c.146 0 .255-.036.366-.072v1.045a1.329 1.329 0 00-.366-.071h-.22c-.146 0-.255.035-.365.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.219 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.22a.364.364 0 01-.365-.36V14.42zm0-2.052c.11.036.255.071.366.071h.219c.146 0 .255-.035.366-.07v1.044a1.34 1.34 0 00-.366-.071h-.22c-.146 0-.255.035-.365.07v-1.044zm1.718-1.118c0-.144.036-.469.476-.469.402 0 .476.325.476.469v21.426h-.038c0 .109-.036.468-.438.468-.438 0-.476-.324-.476-.468V11.25zm1.644 21.428v-.11c0-.215.183-.36.366-.36h.219c.22 0 .366.145.366.36v.11c0 .468.33 1.189 1.208 1.189.182 0 .33-.036.475-.072v2.195c0 .144-.072.468-.475.468-.402 0-.477-.324-.477-.468 0-.83-.402-2.232-1.865-2.7.11-.216.183-.433.183-.612zm.002-2.163c.11.036.255.072.366.072h.219c.147 0 .255-.036.365-.072v1.045a1.32 1.32 0 00-.365-.071h-.22c-.146 0-.254.035-.365.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.219 0 .365.18.365.36v15.052c0 .18-.182.36-.365.36h-.22a.364.364 0 01-.365-.36V14.42zm0-2.052c.11.036.255.071.366.071h.219c.147 0 .255-.035.365-.07v1.044a1.331 1.331 0 00-.365-.071h-.22c-.146 0-.254.035-.365.07v-1.044zm2.632 20.308c0 .109-.036.468-.475.468-.44 0-.477-.324-.477-.468V11.25c0-.144.073-.469.477-.469.401 0 .475.325.475.469v21.426zm.732-2.161c.11.036.255.072.366.072h.11c.255 0 .477.215.477.468 0 .254-.222.434-.477.434h-.11c-.147 0-.255.035-.366.07v-1.044zm0-18.185c.11.036.255.072.366.072h.11c.255 0 .477.215.477.468a.46.46 0 01-.477.47h-.11c-.147 0-.255.035-.366.07v-1.08zm-21.326 24.92c.879 0 1.208-.72 1.208-1.19 0-.07.036-2.124 2.158-2.124h13.46c2.085 0 2.16 1.91 2.16 2.125 0 .469.328 1.19 1.207 1.19.878 0 1.208-.721 1.208-1.19V32.64c0-.216.182-.36.365-.36h.11c.66 0 1.209-.54 1.209-1.19-.075-.717-.623-1.222-1.283-1.222h-.11a.364.364 0 01-.366-.36V14.42c0-.18.183-.36.366-.36h.11c.66 0 1.208-.54 1.208-1.19 0-.648-.548-1.188-1.208-1.188h-.11c-.22 0-.366-.145-.366-.36v-.072c0-.469-.33-1.189-1.207-1.189-.879 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.366.36h-.217a.364.364 0 01-.366-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.207.72-1.207 1.189v.109c0 .18-.183.36-.366.36h-.22a.363.363 0 01-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.182.36-.365.36h-.22c-.182 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.365.36h-.22c-.182 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.182.36-.365.36h-.22c-.182 0-.365-.144-.365-.325v-.144c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.365.36h-.22c-.183 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.366.36h-.584v-.144c0-.396-.33-.72-.732-.72h-.95a.729.729 0 00-.732.72v2.665c0 .395.33.72.731.72h.951a.729.729 0 00.732-.72v-.145h.584c.183 0 .366.18.366.36v15.053c0 .18-.183.36-.366.36h-1.792c-.22 0-.438.071-.621.18-.073.036-.183.109-.22.18a.626.626 0 00-.146.18.271.271 0 00-.072.145.511.511 0 00-.073.25c0 .072-.036.18-.036.29 0 .071 0 .144.036.251a.933.933 0 00.33.576c.072.071.183.144.293.215.183.072.366.145.585.145h1.792c.183 0 .366.144.366.36v3.42c0 .47.33 1.19 1.208 1.19z"
            fill="#817E7A" mask="url(#epa-heat-bd)" />
        </g>
      </symbol>
      <symbol id="epa-heat" viewBox="0 72 151 48">
        <g transform="translate(0 72.564)" fill="none" fill-rule="evenodd">
          <mask id="epa-heata" fill="#fff">
            <path d="M75.342 46.436h75.341V0H0v46.436z" />
          </mask>
          <path
            d="M23.31 0v.01a5.396 5.396 0 00-3.804 1.568L1.575 19.422a5.371 5.371 0 000 7.6l17.93 17.844a5.394 5.394 0 003.805 1.566v.004h127.373V0H23.31z"
            fill="#8CC63E" mask="url(#epa-heata)" />
          <mask id="epa-heatb" fill="#fff">
            <path d="M0 46.436h150.683V0H0z" />
          </mask>
          <path fill="#FEFEFE" mask="url(#epa-heatb)" d="M106.91 42.613h39.527V3.275H106.91z" />
          <path
            d="M116.522 13.41a1.336 1.336 0 00-.366-.07h-.585v-.936h.585c.111 0 .255-.036.366-.072v1.079zm0 18.15a1.324 1.324 0 00-.366-.071h-.695a.996.996 0 00.11-.47.971.971 0 00-.11-.468h.695c.111 0 .255-.036.366-.071v1.08zm-2.158-.071a.468.468 0 01-.33-.145.456.456 0 01-.146-.324v-.109c0-.072.072-.18.11-.216a.52.52 0 01.147-.108c.072-.036.11-.036.183-.036.255 0 .476.216.476.469.036.289-.183.469-.44.469zm-.474-17.285h.95V11.54h-.95v2.665zm3.361 19.59c.147.07.294.07.477.07.878 0 1.207-.72 1.207-1.188v-.11c0-.215.183-.359.366-.359h.219c.183 0 .366.144.366.36v.109c0 .18.036.431.183.611-1.463.469-1.865 1.874-1.865 2.703 0 .108-.036.468-.476.468-.402 0-.477-.324-.477-.468v-2.196zm.002-22.58c0-.108.037-.468.477-.468.438 0 .476.324.476.469v21.46c0 .145-.036.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.682 19.301c.111.036.256.072.366.072h.219c.11 0 .256-.036.366-.072v1.045a1.32 1.32 0 00-.366-.071h-.219c-.11 0-.255.035-.366.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.219c-.183 0-.366-.18-.366-.36V14.42zm0-2.052c.111.036.256.071.366.071h.219c.11 0 .256-.035.366-.07v1.044a1.33 1.33 0 00-.366-.071h-.219c-.11 0-.255.035-.366.07v-1.044zm1.644-1.153c0-.109.037-.469.477-.469.438 0 .476.324.476.469v21.46c0 .11-.036.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.723 21.46v-.144c.036-.18.183-.324.366-.324h.219c.218 0 .365.144.365.36v.109c0 .144.036.289.11.468h-1.17c.071-.144.11-.324.11-.468zm-.039-2.16c.111.036.256.072.366.072h.22c.11 0 .255-.036.365-.072v1.045a1.327 1.327 0 00-.366-.071h-.219c-.146 0-.255.035-.366.07v-1.044zm.99-16.095v15.052c0 .18-.184.36-.366.36h-.22c-.182 0-.365-.144-.365-.324V14.384c.036-.18.183-.324.366-.324h.219c.182 0 .365.18.365.36zm-.99-2.052c.111.036.256.071.366.071h.22c.11 0 .255-.035.365-.07v1.044a1.338 1.338 0 00-.366-.071h-.219c-.146 0-.255.035-.366.07v-1.044zm1.719-1.153c0-.109.036-.469.476-.469.402 0 .476.324.476.469v21.46c0 .145-.072.47-.476.47-.402 0-.476-.325-.476-.47v-21.46zm1.684 21.46v-.108c0-.216.146-.36.365-.36h.183c.183 0 .366.144.366.36v.109c0 .144.036.289.11.468h-1.135c.073-.144.11-.324.11-.468zm-.036-2.16h.036c.11.036.219.072.365.072h.183c.111 0 .255-.036.366-.072v1.045a1.327 1.327 0 00-.366-.071h-.219c-.11 0-.255.035-.365.07v-1.044zm0-16.095h.036c0-.18.146-.36.365-.36h.183c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.219c-.183 0-.365-.18-.365-.36V14.42zm0-2.052h.036c.11.036.219.071.365.071h.183c.111 0 .255-.035.366-.07v1.044a1.338 1.338 0 00-.366-.071h-.219c-.11 0-.255.035-.365.07v-1.044zm1.68-1.153c0-.109.036-.469.476-.469.438 0 .476.324.476.469v21.46c0 .145-.036.47-.476.47-.402 0-.476-.325-.476-.47v-21.46zm1.722 21.46v-.108c0-.216.182-.36.365-.36h.22c.183 0 .366.144.366.36v.109c0 .144.036.289.11.468h-1.172c.073-.144.11-.324.11-.468zm-.038-2.16c.11.036.255.072.366.072h.218c.147 0 .256-.036.366-.072v1.045a1.097 1.097 0 00-.366-.071h-.218c-.11 0-.255.035-.366.07v-1.044zm0-16.095c0-.18.182-.36.366-.36h.218c.183 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.218c-.184 0-.366-.18-.366-.36V14.42zm0-2.052c.11.036.255.071.366.071h.218c.147 0 .256-.035.366-.07v1.044a1.097 1.097 0 00-.366-.071h-.218c-.11 0-.255.035-.366.07v-1.044zm1.681-1.153c0-.109.037-.469.477-.469.402 0 .476.324.476.469v21.46c0 .145-.072.47-.476.47-.402 0-.477-.325-.477-.47v-21.46zm1.685 21.46v-.108c0-.216.183-.36.366-.36h.219c.219 0 .366.144.366.36v.109c0 .144.035.289.11.468h-1.172c.073-.144.11-.324.11-.468zm0-2.16c.11.036.255.072.366.072h.219c.146 0 .255-.036.366-.072v1.045a1.329 1.329 0 00-.366-.071h-.22c-.146 0-.255.035-.365.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.219 0 .366.18.366.36v15.052c0 .18-.183.36-.366.36h-.22a.364.364 0 01-.365-.36V14.42zm0-2.052c.11.036.255.071.366.071h.219c.146 0 .255-.035.366-.07v1.044a1.34 1.34 0 00-.366-.071h-.22c-.146 0-.255.035-.365.07v-1.044zm1.718-1.118c0-.144.036-.469.476-.469.402 0 .476.325.476.469v21.426h-.038c0 .109-.036.468-.438.468-.438 0-.476-.324-.476-.468V11.25zm1.644 21.428v-.11c0-.215.183-.36.366-.36h.219c.22 0 .366.145.366.36v.11c0 .468.33 1.189 1.208 1.189.182 0 .33-.036.475-.072v2.195c0 .144-.072.468-.475.468-.402 0-.477-.324-.477-.468 0-.83-.402-2.232-1.865-2.7.11-.216.183-.433.183-.612zm.002-2.163c.11.036.255.072.366.072h.219c.147 0 .255-.036.365-.072v1.045a1.32 1.32 0 00-.365-.071h-.22c-.146 0-.254.035-.365.07v-1.044zm0-16.095c0-.18.183-.36.366-.36h.219c.219 0 .365.18.365.36v15.052c0 .18-.182.36-.365.36h-.22a.364.364 0 01-.365-.36V14.42zm0-2.052c.11.036.255.071.366.071h.219c.147 0 .255-.035.365-.07v1.044a1.331 1.331 0 00-.365-.071h-.22c-.146 0-.254.035-.365.07v-1.044zm2.632 20.308c0 .109-.036.468-.475.468-.44 0-.477-.324-.477-.468V11.25c0-.144.073-.469.477-.469.401 0 .475.325.475.469v21.426zm.732-2.161c.11.036.255.072.366.072h.11c.255 0 .477.215.477.468 0 .254-.222.434-.477.434h-.11c-.147 0-.255.035-.366.07v-1.044zm0-18.185c.11.036.255.072.366.072h.11c.255 0 .477.215.477.468a.46.46 0 01-.477.47h-.11c-.147 0-.255.035-.366.07v-1.08zm-21.326 24.92c.879 0 1.208-.72 1.208-1.19 0-.07.036-2.124 2.158-2.124h13.46c2.085 0 2.16 1.91 2.16 2.125 0 .469.328 1.19 1.207 1.19.878 0 1.208-.721 1.208-1.19V32.64c0-.216.182-.36.365-.36h.11c.66 0 1.209-.54 1.209-1.19-.075-.717-.623-1.222-1.283-1.222h-.11a.364.364 0 01-.366-.36V14.42c0-.18.183-.36.366-.36h.11c.66 0 1.208-.54 1.208-1.19 0-.648-.548-1.188-1.208-1.188h-.11c-.22 0-.366-.145-.366-.36v-.072c0-.469-.33-1.189-1.207-1.189-.879 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.366.36h-.217a.364.364 0 01-.366-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.207.72-1.207 1.189v.109c0 .18-.183.36-.366.36h-.22a.363.363 0 01-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.182.36-.365.36h-.22c-.182 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.365.36h-.22c-.182 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.182.36-.365.36h-.22c-.182 0-.365-.144-.365-.325v-.144c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.365.36h-.22c-.183 0-.365-.18-.365-.36v-.109c0-.469-.33-1.189-1.208-1.189-.878 0-1.208.72-1.208 1.189v.109c0 .18-.183.36-.366.36h-.584v-.144c0-.396-.33-.72-.732-.72h-.95a.729.729 0 00-.732.72v2.665c0 .395.33.72.731.72h.951a.729.729 0 00.732-.72v-.145h.584c.183 0 .366.18.366.36v15.053c0 .18-.183.36-.366.36h-1.792c-.22 0-.438.071-.621.18-.073.036-.183.109-.22.18a.626.626 0 00-.146.18.271.271 0 00-.072.145.511.511 0 00-.073.25c0 .072-.036.18-.036.29 0 .071 0 .144.036.251a.933.933 0 00.33.576c.072.071.183.144.293.215.183.072.366.145.585.145h1.792c.183 0 .366.144.366.36v3.42c0 .47.33 1.19 1.208 1.19z"
            fill="#817E7A" mask="url(#epa-heatb)" />
          <path
            d="M41.608 23.972h4.18l-2.171-7.32-2.01 7.32zm5.573 4.644H40.15l-1.88 5.096h-5.8l8.231-22.22h5.898l8.198 22.22h-5.735l-1.88-5.096z"
            fill="#FEFEFE" mask="url(#epa-heatb)" />
        </g>
      </symbol>
      <symbol id="epa-water-a" viewBox="0 0 151 47">
        <g fill="none" fill-rule="evenodd">
          <mask id="epa-water-ab" fill="#fff">
            <use xlinkHref="#epa-water-aa" />
          </mask>
          <path
            d="M23.31 0v.01a5.396 5.396 0 00-3.804 1.568L1.575 19.422a5.371 5.371 0 000 7.6l17.93 17.844a5.394 5.394 0 003.805 1.566v.004h127.373V0H23.31z"
            fill="#1EA54A" mask="url(#epa-water-ab)" />
          <mask id="epa-water-ad" fill="#fff">
            <use xlinkHref="#epa-water-ac" />
          </mask>
          <path fill="#FEFEFE" mask="url(#epa-water-ad)"
            d="M106.91 42.613h39.527V3.275H106.91zM41.608 23.972h4.18l-2.171-7.32-2.01 7.32zm5.573 4.644H40.15l-1.88 5.096h-5.8l8.231-22.22h5.898l8.198 22.22h-5.735l-1.88-5.096z" />
          <path
            d="M138.936 28.11a1.24 1.24 0 01-1.24 1.235h-3.619a1.244 1.244 0 01-1.245-1.239V24.51c0-1.3-1.063-2.358-2.37-2.358h-11.988v-6.075h14.998c3.013 0 5.464 2.439 5.464 5.437v6.597zm-5.464-13.153h-7.869V10.08h5.462c.31 0 .563-.25.563-.56a.562.562 0 00-.563-.56h-12.048a.562.562 0 00-.563.56c0 .31.253.56.563.56h5.462v4.876h-6.568a.56.56 0 00-.562.559v7.194c0 .309.252.56.562.56h12.552c.686 0 1.244.556 1.244 1.239v3.597c0 1.3 1.063 2.357 2.37 2.357h3.618a2.362 2.362 0 002.366-2.353v-6.597c0-3.615-2.955-6.556-6.589-6.556zm-13.405 13.738l-2.825 4.166 3.084 4.72h-2.552l-1.815-3.005-1.827 3.006h-2.54l3.071-4.72-2.825-4.167h2.618l1.503 2.464 1.517-2.464zm1.271 8.886v-8.886h2.19V35.7h3.215v1.882z"
            fill="#817E7A" mask="url(#epa-water-ad)" />
        </g>
      </symbol>
      <symbol id="epa-water-b" viewBox="0 0 151 47">
        <g fill="none" fill-rule="evenodd">
          <mask id="epa-water-bb" fill="#fff">
            <use xlinkHref="#epa-water-ba" />
          </mask>
          <path
            d="M23.31 0v.01a5.396 5.396 0 00-3.804 1.568L1.575 19.422a5.371 5.371 0 000 7.6l17.93 17.844a5.394 5.394 0 003.805 1.566v.004h127.373V0H23.31z"
            fill="#FEF100" mask="url(#epa-water-bb)" />
          <mask id="epa-water-bd" fill="#fff">
            <use xlinkHref="#epa-water-bc" />
          </mask>
          <path fill="#FEFEFE" mask="url(#epa-water-bd)" d="M106.91 42.613h39.527V3.275H106.91z" />
          <path
            d="M138.936 28.11a1.24 1.24 0 01-1.24 1.235h-3.619a1.244 1.244 0 01-1.245-1.239V24.51c0-1.3-1.063-2.358-2.37-2.358h-11.988v-6.075h14.998c3.013 0 5.464 2.439 5.464 5.437v6.597zm-5.464-13.153h-7.869V10.08h5.462c.31 0 .563-.25.563-.56a.562.562 0 00-.563-.56h-12.048a.562.562 0 00-.563.56c0 .31.253.56.563.56h5.462v4.876h-6.568a.56.56 0 00-.562.559v7.194c0 .309.252.56.562.56h12.552c.686 0 1.244.556 1.244 1.239v3.597c0 1.3 1.063 2.357 2.37 2.357h3.618a2.362 2.362 0 002.366-2.353v-6.597c0-3.615-2.955-6.556-6.589-6.556zm-13.405 13.738l-2.825 4.166 3.084 4.72h-2.552l-1.815-3.005-1.827 3.006h-2.54l3.071-4.72-2.825-4.167h2.618l1.503 2.464 1.517-2.464zm1.271 8.886v-8.886h2.19V35.7h3.215v1.882z"
            fill="#817E7A" mask="url(#epa-water-bd)" />
          <path
            d="M47.81 18.368c0 1.287-.66 3.762-3.201 4.488 3.102.462 4.389 3.234 4.389 5.412 0 3.3-1.947 6.732-7.491 6.732h-9.966V12.263h9.306c4.224 0 6.963 2.442 6.963 6.105zm-10.725 2.673h2.97c1.122 0 1.914-.726 1.914-1.881 0-1.188-.528-2.079-1.848-2.079h-3.036v3.96zm3.498 4.521h-3.498v4.62h3.531c1.716 0 2.541-1.188 2.541-2.343 0-1.056-.726-2.277-2.574-2.277z"
            fill="#FFF" />
        </g>
      </symbol>
      <symbol id="epa-water" viewBox="0 0 151 47">
        <g fill="none" fill-rule="evenodd">
          <mask id="epa-watera" fill="#fff">
            <path d="M75.342 46.436h75.341V0H0v46.436z" />
          </mask>
          <path
            d="M23.31 0v.01a5.396 5.396 0 00-3.804 1.568L1.575 19.422a5.371 5.371 0 000 7.6l17.93 17.844a5.394 5.394 0 003.805 1.566v.004h127.373V0H23.31z"
            fill="#1E9546" mask="url(#epa-watera)" />
          <mask id="epa-waterb" fill="#fff">
            <path d="M0 46.436h150.683V0H0z" />
          </mask>
          <path fill="#FEFEFE" mask="url(#epa-waterb)"
            d="M106.91 42.613h39.527V3.275H106.91zM41.608 23.972h4.18l-2.171-7.32-2.01 7.32zm5.573 4.644H40.15l-1.88 5.096h-5.8l8.231-22.22h5.898l8.198 22.22h-5.735l-1.88-5.096z" />
          <path
            d="M138.936 28.11a1.24 1.24 0 01-1.24 1.235h-3.619a1.244 1.244 0 01-1.245-1.239V24.51c0-1.3-1.063-2.358-2.37-2.358h-11.988v-6.075h14.998c3.013 0 5.464 2.439 5.464 5.437v6.597zm-5.464-13.153h-7.869V10.08h5.462c.31 0 .563-.25.563-.56a.562.562 0 00-.563-.56h-12.048a.562.562 0 00-.563.56c0 .31.253.56.563.56h5.462v4.876h-6.568a.56.56 0 00-.562.559v7.194c0 .309.252.56.562.56h12.552c.686 0 1.244.556 1.244 1.239v3.597c0 1.3 1.063 2.357 2.37 2.357h3.618a2.362 2.362 0 002.366-2.353v-6.597c0-3.615-2.955-6.556-6.589-6.556zm-13.405 13.738l-2.825 4.166 3.084 4.72h-2.552l-1.815-3.005-1.827 3.006h-2.54l3.071-4.72-2.825-4.167h2.618l1.503 2.464 1.517-2.464zm1.271 8.886v-8.886h2.19V35.7h3.215v1.882z"
            fill="#817E7A" mask="url(#epa-waterb)" />
        </g>
      </symbol>
      <symbol id="ev-car" viewBox="0 0 36 36">
        <path
          d="M11.952 23.177H24.05m-12.098 0H8.977c-.952 0-1.146-.773-1.124-1.159m4.099 1.159v2.113c0 .654-.485.773-.728.75H8.977c-1.124 0-1.124-.475-1.124-1.159v-2.863m16.197 1.159h2.975c.375 0 1.124-.232 1.124-1.159m-4.099 1.159v1.704c0 .684.122 1.159 1.124 1.159h1.955c.063 0 .125-.007.187-.017.325-.05.833-.108.833-1.337v-2.668m0 0v-4.636a.69.69 0 00-.012-.136c-.1-.504-.47-1.362-1.274-1.536m-17.424-.032c-.529.182-1.586.764-1.586 1.636v4.704m1.586-6.34l1.753-5.014c.021-.062.034-.127.051-.19a.694.694 0 01.709-.517h11.997a.65.65 0 01.198.028c.313.099.837.369 1.027.859.211.545 1.214 3.514 1.689 4.866m-17.424-.032h10.05m-3.505 3.292h3.9m-3.9 1.32h3.9m6.979-4.58a1.42 1.42 0 00-.3-.032h-2.05m-5.025 0c.088-.863.714-2.59 2.512-2.59 1.799 0 2.425 1.727 2.513 2.59m-5.025 0h5.025m1.334 3.715v.473a.863.863 0 01-.863.863H23.36a.863.863 0 01-.862-.863v-.473c0-.476.386-.862.862-.862h1.624c.476 0 .863.386.863.862zm-12.35 0v.473a.863.863 0 01-.862.863H11.01a.863.863 0 01-.862-.863v-.473c0-.476.386-.862.862-.862h1.624c.476 0 .863.386.863.862z"
          stroke="#FB6058" stroke-width=".429" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="ev" viewBox="0 0 140 140">
        <path
          d="M49.43 93.195h40.462m-40.462 0h-9.95c-3.183 0-3.832-2.46-3.758-3.69m13.708 3.69v6.73c0 2.083-1.621 2.46-2.432 2.388H39.48c-3.76 0-3.76-1.513-3.76-3.69v-9.118m54.17 3.69h9.95c1.253 0 3.759-.738 3.759-3.69m-13.708 3.69v5.427c0 2.178.408 3.691 3.758 3.691h6.564c.194 0 .385-.021.577-.05 1.085-.164 2.809-.321 2.809-4.263v-8.495m0 0V74.759c0-.157-.012-.31-.044-.463-.34-1.607-1.574-4.327-4.258-4.88m-58.27-.1c-1.769.58-5.306 2.432-5.306 5.21v14.979m5.306-20.189l5.854-15.948c.077-.209.12-.43.185-.643.206-.682.858-1.626 2.363-1.626H89.58c.208 0 .415.02.614.08 1.042.31 2.816 1.171 3.456 2.742.708 1.737 4.06 11.193 5.648 15.495m-58.27-.1h33.607M62.917 79.8h13.045M62.917 84h13.045m23.336-14.584c-.315-.065-.65-.1-1.005-.1H91.44m-16.804 0c.295-2.75 2.388-8.249 8.402-8.249 6.014 0 8.107 5.5 8.402 8.25m-16.804 0H91.44M95.9 81.2v1.4a2.8 2.8 0 01-2.8 2.8h-5.6a2.8 2.8 0 01-2.8-2.8v-1.4a2.8 2.8 0 012.8-2.8h5.6a2.8 2.8 0 012.8 2.8zm-41.3 0v1.4a2.8 2.8 0 01-2.8 2.8h-5.6a2.8 2.8 0 01-2.8-2.8v-1.4a2.8 2.8 0 012.8-2.8h5.6a2.8 2.8 0 012.8 2.8zM70 44.8v5.6m11.2 54.599v11.2m-22.4-11.2v11.2m11.2-11.2v11.2"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M60.2 31.5h2.1m16.8.044H77m-3.5-7.208V31.5m0 0h-7m7 0l3.5.044m-10.5-7.208V31.5m0 0h-4.2m0 0c-.663 4.125.042 12.418 7.472 12.418 7.605 0 8.039-8.25 7.228-12.374"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="external" viewBox="0 0 22 22">
        <path d="M11.152 10.652l7.07-7.071m0 0l-4.278-.025m4.279.025l.024 4.279" stroke="#FB6058" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M11.202 6a.5.5 0 100-1v1zM17 10.315a.5.5 0 00-1 0h1zM14.5 20h-11v1h11v-1zM2 18.5v-11H1v11h1zM3.5 6h7.702V5H3.5v1zM16 10.315V18.5h1v-8.185h-1zM3.5 20A1.5 1.5 0 012 18.5H1A2.5 2.5 0 003.5 21v-1zm11 1a2.5 2.5 0 002.5-2.5h-1a1.5 1.5 0 01-1.5 1.5v1zM2 7.5A1.5 1.5 0 013.5 6V5A2.5 2.5 0 001 7.5h1z"
          fill="#FB6058" />
      </symbol>
      <symbol id="filter" viewBox="0 0 25 25">
        <path d="M4.375 5.89h3.112m3.506 0h9.632" stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="9.049" cy="5.938" stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" r="1.563" />
        <path d="M4.375 18.39h3.112m3.506 0h9.632" stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="9.049" cy="18.438" r="1.563" stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M20.625 12.14h-3.112m-3.506 0H4.375" stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
        <circle r="1.563" transform="matrix(-1 0 0 1 15.95 12.188)" stroke="#FB6058" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="flue-roof" viewBox="0 0 25 27">
        <path
          d="M9.515 0v1.09h.242v7.74h-.643v10.2l-9.05 5.033L0 24.1v2.854h1.016v-2.236L24.855 11.3l.063-.036v-1.151l-9.007 5.124-.012-6.405h-.63V1.09h.24V0H9.516zm1.342 4.539h3.31V1.261h-3.31V4.54zm0 4.33h3.31v-3.24h-3.31v3.24zm-.643 1.089h4.597v5.894l-4.597 2.563V9.958z"
          fill="#827E7A" fill-rule="evenodd" />
      </symbol>
      <symbol id="flue-wall" viewBox="0 0 12 27">
        <path
          d="M6.59 14.837h4.467v-3.175H6.59v3.175zm-3.077 1.121h2.135v-5.417H3.513v5.417zm-2.54 1.476V9.065L2.569 10.3v5.9L.973 17.434zM12 10.714H6.59v-1.12H3.203L1.049 7.928c-.033-.026-.07-.043-.106-.06V0H0v26.256h.943v-7.62a.642.642 0 00.106-.066l2.152-1.664h3.39v-1.121H12v-5.07z"
          fill="#827E7A" fill-rule="evenodd" />
      </symbol>
      <symbol id="form-icon" viewBox="0 0 30 39">
        <path
          d="M10.697.268h0a.88.88 0 00-.797.88v1.32H7.92a.88.88 0 00-.88.88v.88H.88a.487.487 0 00-.083 0 .88.88 0 00-.797.88v33a.88.88 0 101.76 0V5.988h5.28v1.32c0 .486.394.88.88.88h13.2a.88.88 0 00.88-.88v-3.96a.88.88 0 00-.88-.88h-1.98v-1.32a.88.88 0 00-.88-.88h-7.48a.487.487 0 00-.083 0zm.963 1.76h5.72v1.32c0 .486.394.88.88.88h1.98v2.2H8.8v-2.2h1.98a.88.88 0 00.88-.88v-1.32zm12.897 2.2h0a.88.88 0 00.083 1.76h2.64v31.24H4.29a.88.88 0 100 1.76h23.87a.88.88 0 00.88-.88v-33a.88.88 0 00-.88-.88h-3.52a.487.487 0 00-.083 0zm-14.506 7.907h0a.88.88 0 00-.59.302l-3.342 3.699-1.54-1.54a.879.879 0 00-1.256-.005.878.878 0 00.018 1.256l2.2 2.2a.881.881 0 001.28-.041l3.96-4.4c.542-.594.072-1.545-.73-1.471zm3.506 2.653h0a.88.88 0 00.083 1.76h11.44a.88.88 0 100-1.76H13.64a.487.487 0 00-.083 0zm-3.506 4.827h0a.88.88 0 00-.59.302l-3.342 3.699-1.54-1.54a.879.879 0 00-1.256-.005.878.878 0 00.018 1.256l2.2 2.2a.881.881 0 001.28-.041l3.96-4.4c.542-.594.072-1.545-.73-1.471zm3.506 2.653h0a.88.88 0 00.083 1.76h11.44a.88.88 0 100-1.76H13.64a.487.487 0 00-.083 0zm-3.506 4.827h0a.88.88 0 00-.59.302l-3.342 3.699-1.54-1.54a.879.879 0 00-1.256-.005.878.878 0 00.018 1.256l2.2 2.2a.881.881 0 001.28-.041l3.96-4.4c.542-.594.072-1.545-.73-1.471zm3.506 2.653h0a.88.88 0 00.083 1.76h11.44a.88.88 0 100-1.76H13.64a.487.487 0 00-.083 0z"
          stroke="currentColor" fill="none" />
      </symbol>
      <symbol id="forward" viewBox="45 39 24 41">
        <path fill="currentColor" d="M48.72 39L45 42.72l16.556 16.56L45 75.832l3.72 3.72L69 59.279" fill-rule="evenodd" />
      </symbol>
      <symbol id="garage" viewBox="0 0 100 100">
        <path
          d="M81.25 21.181l-9.375 9.375h9.375v52.777h-62.5m10.069-46.527v13.813m0 28.548V56.451m3.82-15.348l-3.82 9.516m0 0v5.832m0 0L25 45.401"
          fill="none" stroke="#fb6058" />
        <path
          d="M80.903 52.084c-5.556-.001-8.333 1.388-15.625 7.291-2.574 2.084-13.773 2.547-16.667 3.125-2.083.417-5.903 2.153-5.903 6.597 0 3.056 2.084 3.473 3.473 3.473H50m11.111 0h19.792"
          fill="none" stroke="#fb6058" />
        <circle cx="55.556" cy="72.223" fill="none" stroke="#fb6058" r="5.556" />
        <ellipse cx="28.472" cy="40.278" rx="9.722" ry="18.75" fill="none" stroke="#fb6058" />
      </symbol>
      <symbol id="green-tick" viewBox="0 0 20 20">
        <g fill="none" fill-rule="evenodd">
          <rect width="20" height="20" fill="#00B67A" fill-rule="nonzero" rx="10" />
          <path fill="#FFF" stroke="#FFF" stroke-width=".8"
            d="M7.333 13.238l.762.762 7.238-7.238L14.571 6l-7.238 7.238zm-2.666-2.666l2.666 2.666.762-.762L5.429 9.81l-.762.763z" />
        </g>
      </symbol>
      <symbol id="halfords-logo" viewBox="0 0 174 36">
        <path
          d="M163.09 35.68c6.046 0 10.739-3.131 10.739-8.217 0-9.17-12.375-7.154-12.375-10.407 0-.891.9-1.595 2.541-1.595 2.092 0 4.804.882 7.289 2.302l2.292-6.166c-2.483-1.36-6.134-2.525-9.183-2.525-5.993 0-10.401 3.431-10.401 8.635 0 8.574 11.924 6.62 11.924 9.812 0 1.008-.845 1.716-2.826 1.716-2.313 0-5.481-1.3-7.968-3.132l-2.28 6.465c2.996 2.012 6.18 3.113 10.248 3.113zm-30.174-13.482c0-3.253 1.21-6.85 4.847-6.85 3.45 0 4.708 3.403 4.708 7.098 0 3.253-.837 7.145-4.708 7.145-3.637 0-4.847-3.892-4.847-7.393zM150.119.061h-7.93V12.44h-.09c-1.631-2.315-3.918-3.501-6.9-3.501-6.853 0-10.209 6.309-10.209 12.965C124.99 29.1 128.207 36 135.804 36c2.8 0 5.13-1.278 6.667-3.694h.095v2.892h7.553zM109.258 35.14h7.923V21.777c0-3.036 2.055-5.739 5.969-5.739.13 0 .255 0 .378.005l2.099-5.763.376-1.042a8.726 8.726 0 00-1.684-.15c-3.404 0-6.109 1.626-7.274 4.714h-.093V9.695h-7.694v20.28zM81.632 9.695h-4.88V8.55c0-2.335 1.121-2.581 2.938-2.581.7 0 1.4.048 2.143.145V.154C80.578.1 79.27 0 78.013 0c-6.759 0-9.185 2.29-9.185 9.695h-3.823v5.462h3.823v20.041h7.924v-20.04h2.897zm9.822 19.952c-2.798 0-3.63-2.322-3.332-5.175.358-3.387 2.655-9.282 6.945-9.282 2.984 0 3.775 2.225 3.444 5.37-.406 3.863-2.538 9.087-7.057 9.087zm4.76-20.503c-8.763 0-15.111 6.722-16.044 15.62-.776 7.396 3.745 10.924 10.46 10.924 8.767 0 14.906-6.526 15.842-15.472.758-7.203-3.635-11.072-10.258-11.072zM54.911 35.198h7.926V.425H54.91zm-16.4-4.405c-1.772 0-3.31-.83-3.31-2.89 0-2.01 1.492-2.792 3.262-3.232 1.727-.49 3.775-.635 4.895-1.372.188 5.243-1.07 7.494-4.848 7.494zm-2.848-13.18c.376-2.152 1.773-3.183 3.965-3.183 1.632 0 3.823.736 3.823 2.646 0 1.57-.746 2.06-2.097 2.35-5.503 1.178-14.08.544-14.08 8.821 0 5.245 3.637 7.692 8.254 7.692 2.89 0 5.918-.882 7.971-3.135.094.833.189 1.618.467 2.404h7.971c-.933-2.012-.933-4.363-.933-6.52V17.567c0-7.2-5.545-8.522-11.284-8.522-2.749 0-5.545.49-7.69 1.76-2.146 1.328-3.636 3.48-3.778 6.81zM0 35.199h7.923v-13.83c0-2.788 1.306-5.438 4.292-5.438 4.338 0 3.964 3.533 3.964 7.009V35.2h7.923V18.92c0-3.675-.651-9.849-8.716-9.849-2.89 0-6.157 1.614-7.366 4.216h-.097V.251H0z"
          fill="#ff9800" />
      </symbol>
      <symbol id="hbw-flow-answer-lg" viewBox="0 0 240 350">
        <path
          d="M8 239h228m-185.791-1.54v-85.95l51.254-31.464V107h10.581v13.046l54.486 31.464v85.95M49.441 180.671h-35.3v9.209h3.069m32.231 0H17.21m0 0v41.441m0 5.372v-5.372m32.231 0H17.21"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M24.883 230.553v-34.534H41v34.534M95.396 159H100m16.116 0h-3.837m0 0H100m12.279 0v-9.593M100 159v-9.593m0 0v-9.593h12.279v9.593m-12.279 0h12.279"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M69.393 222.112h16.883v-27.627H69.393v27.627z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M86.276 208.299H69.393" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M92.416 222.112h16.883v-27.627H92.416v27.627z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M109.298 208.299H93.95m-30.698 13.814h52.185m-54.485-77.509v-15.348h1.535m6.907 10.743v-10.743h-1.535m-5.372 0v-10.744h5.372v10.744m-5.372 0h5.372m-7.676-10.744h9.976m-7.674 5.372h4.605"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          d="M128 191h28v32h-28z" />
        <path
          d="M168 171h68v10.25h-3.416m-64.584 0h64.584m0 0V237m-46.887-9.185h28.61m-28.61 0h-7.035c-2.252 0-2.71-1.937-2.658-2.905m9.693 2.905v5.298c0 1.641-1.147 1.937-1.72 1.881H178.2c-1.376 0-2.196-1.182-2.196-2.906v-7.178m38.303 2.905h7.035c.886 0 2.658-.581 2.658-2.905m-9.693 2.905v4.273c0 1.724.316 2.906 2.066 2.906h5.893c.153 0 .307-.013.451-.067.657-.244 1.283-.933 1.283-2.839v-7.178m0 0v-11.644a1.65 1.65 0 00-.025-.298c-.232-1.261-1.102-3.435-3.017-3.875m-41.202-.078c-1.251.456-3.752 1.914-3.752 4.102v11.793m3.752-15.895l4.149-12.263c.048-.142.075-.29.112-.436.134-.539.591-1.316 1.68-1.316h28.363c.165 0 .328.018.484.071.74.251 1.973.927 2.421 2.151.5 1.367 2.87 8.484 3.993 11.871m-41.202-.078h23.763m-8.286 8.033h9.224m-9.224 3.589h9.224m16.501-11.544a3.166 3.166 0 00-.71-.078h-4.847m-11.882 0c.209-2.165 1.689-6.495 5.941-6.495 4.253 0 5.733 4.33 5.941 6.495m-11.882 0h11.882m3.44 8.836v1.811a2 2 0 01-2 2h-3.817a2 2 0 01-2-2v-1.811a2 2 0 012-2h3.817a2 2 0 012 2zm-29.548 0v1.811a2 2 0 01-2 2h-3.974a2 2 0 01-2-2v-1.811a2 2 0 012-2h3.974a2 2 0 012 2z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M107 240v110m0-350v106" stroke="#FB6058" stroke-width="3" />
      </symbol>
      <symbol id="hbw-flow-answer-sm" viewBox="0 0 198 163">
        <path d="M88 0v32m12 110v21" stroke="#FB6058" stroke-width="2" />
        <path
          d="M6.6 141.8h188.1m-153.278-1.27V69.62l42.285-25.957V32.9h8.729v10.763l44.951 25.958v70.909M40.789 93.679H11.666v7.597h2.532m26.59 0h-26.59m0 0v34.188m0 4.432v-4.432m26.59 0h-26.59"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M20.529 134.831v-28.49h13.295v28.49M78.702 75.8H82.5m13.295 0H92.63m0 0H82.5m10.13 0v-7.914M82.5 75.8v-7.914m0 0v-7.914h10.13v7.914m-10.13 0h10.13"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M57.249 127.867h13.929v-22.792H57.249v22.792z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M71.178 116.472H57.249" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M76.243 127.867h13.929v-22.792H76.243v22.792z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M90.171 116.472H77.51m-25.326 11.396h43.052m-44.95-63.945V51.26h1.266m5.698 8.863V51.26h-1.266m-4.432 0v-8.864h4.432v8.864m-4.432 0h4.432m-6.333-8.863h8.23m-6.33 4.432h3.798"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          d="M105.6 102.2h23.1v26.4h-23.1z" />
        <path
          d="M138.6 85.7h56.1v8.456h-2.818m-53.282 0h53.282m0 0v45.994m-38.682-7.577h23.603m-23.603 0h-5.804c-1.857 0-2.236-1.598-2.193-2.397m7.997 2.397v4.371c0 1.353-.946 1.598-1.419 1.551h-4.766c-1.135 0-1.812-.975-1.812-2.397v-5.922m31.6 2.397h5.805c.73 0 2.192-.48 2.192-2.397m-7.997 2.397v3.525c0 1.422.261 2.397 1.705 2.397h4.821c.154 0 .308-.012.45-.07.528-.213 1.021-.792 1.021-2.327v-5.922m0 0v-9.58a1.65 1.65 0 00-.025-.298c-.198-1.044-.918-2.812-2.484-3.171m-33.992-.065c-1.032.376-3.096 1.579-3.096 3.384v9.73m3.096-13.114l3.41-10.079c.048-.142.075-.291.115-.435.123-.443.504-1.048 1.376-1.048h23.356c.165 0 .329.018.485.073.614.218 1.594.773 1.955 1.76.413 1.128 2.368 6.999 3.295 9.794m-33.992-.065h19.605m-6.836 6.627h7.61m-7.61 2.961h7.61m13.613-9.523a2.624 2.624 0 00-.586-.065h-3.999m-9.802 0c.172-1.786 1.393-5.358 4.901-5.358s4.729 3.572 4.901 5.358m-9.802 0h9.802m2.838 7.64v.794a2 2 0 01-2 2h-2.449a2 2 0 01-2-2v-.794a2 2 0 012-2h2.449a2 2 0 012 2zm-24.377 0v.794a2 2 0 01-2 2h-2.578a2 2 0 01-2-2v-.794a2 2 0 012-2h2.578a2 2 0 012 2z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hbw-flow-bottom-line-lg" viewBox="0 0 261 99">
        <circle cx="251" cy="89" r="8.5" stroke="#FB6058" stroke-width="3" />
        <path d="M3 0v18c0 11.046 8.954 20 20 20h208c11.046 0 20 8.954 20 20v21.5" stroke="#FB6058" stroke-width="3" />
      </symbol>
      <symbol id="hbw-flow-bottom-line-md" viewBox="0 0 180 99">
        <circle cx="170" cy="89" r="8.5" stroke="#FB6058" stroke-width="3" />
        <path d="M2 0v18c0 11.046 8.954 20 20 20h128c11.046 0 20 8.954 20 20v21.5" stroke="#FB6058" stroke-width="3" />
      </symbol>
      <symbol id="hbw-flow-bottom-line-sm" viewBox="0 0 20 60">
        <path d="M10 0v42" stroke="#FB6058" stroke-width="2" />
        <circle cx="10" cy="50" r="9" stroke="#FB6058" stroke-width="2" />
      </symbol>
      <symbol id="hbw-flow-choose-lg" viewBox="0 0 240 350">
        <path
          d="M107 0v40c0 6.904 5.596 12.5 12.5 12.5v0c6.904 0 12.5 5.596 12.5 12.5v39.5m0 130.5v42c0 8.56-6.94 15.5-15.5 15.5v0c-8.56 0-15.5 6.94-15.5 15.5v42"
          stroke="#FB6058" stroke-width="3" />
        <circle cx="131.907" cy="225.016" r="3.244" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M20 232.315l8.387-10.675a1 1 0 011.282-.25l.873.499m20.273 25.833l5.946-9.662a1 1 0 00-.356-1.392l-25.863-14.779m0 0c2.703-1.931 7.136-8.712 3.244-20.389-3.226-11.289 3.973-40.22 11.23-38.949.08.014.16.042.231.081 3.134 1.664 5.424 7.149.174 15.71a1.027 1.027 0 00-.075.937l2.225 5.191m8.109 51.899l6.488-2.433"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M54.06 233.936h124.07m-130.558-2.433V175.55m0-10.542v-56.764c0-2.595 2.163-3.244 3.244-3.244H213c2.595 0 3.243 2.162 3.243 3.244v84.335"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M201.645 186.091v-64.684a1 1 0 00-1-1H63.167a1 1 0 00-1 1v95.31a1 1 0 001 1h101.987" stroke="#2D3D4D"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M75.954 189.958v-40.979a1 1 0 011-1h29.626a1 1 0 011 1v40.979a1 1 0 01-1 1H76.954a1 1 0 01-1-1zm41.356 0v-40.979a1 1 0 011-1h29.625a1 1 0 011 1v40.979a1 1 0 01-1 1H118.31a1 1 0 01-1-1z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M189.483 180.416v-31.437a1 1 0 00-1-1h-29.626a1 1 0 00-1 1v40.979a1 1 0 001 1h13.191" stroke="#FB6058"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M173.264 208.798v-15.407m0 0v-34.059c0-1.892 1.135-5.676 5.676-5.676s6.217 3.784 6.488 5.676v26.761c.811-3.244 2.432-6.488 7.298-5.677 7.298 2.433 5.676 5.677 5.676 12.975.025-1.622.811-7.298 4.866-7.298 11.353-.811 8.109 13.785 8.109 13.785s1.622-6.487 6.487-6.487c3.244 0 5.677 1.622 5.677 4.865 0 4.055 1.032 8.92 0 20.273-.649 7.136-5.136 11.353-7.298 13.786v17.029h-35.681V236.37c-11.677-10.38-15.678-20.003-16.218-23.517V201.5c0-5.839 5.946-7.839 8.92-8.109z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hbw-flow-choose-sm" viewBox="0 0 198 163">
        <path d="M100 128v35m0-163v39" stroke="#FB6058" stroke-width="2" />
        <circle cx="100.225" cy="121.225" r="2.225" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M23 125.879l5.585-7.109a1 1 0 011.283-.25l.363.207m13.905 17.719l3.908-6.35a1 1 0 00-.355-1.393l-17.458-9.976m0 0c1.854-1.324 4.894-5.975 2.225-13.984-2.207-7.724 2.7-27.485 7.664-26.721.08.012.16.04.231.08 2.115 1.153 3.643 4.855.17 10.616-.17.283-.2.632-.07.936l1.46 3.408"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M46 127.437h85.55M42 125V86m0-6.5V41.225C42 39.445 43.483 39 44.225 39h111.242c1.779 0 2.224 1.483 2.224 2.225V99.07"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M147.668 94.5V51a1 1 0 00-1-1H53a1 1 0 00-1 1v64.745a1 1 0 001 1h69.639" stroke="#2D3D4D"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M61 96.48V69a1 1 0 011-1h19.692a1 1 0 011 1v27.48a1 1 0 01-1 1H62a1 1 0 01-1-1zm29 0V69a1 1 0 011-1h19.692a1 1 0 011 1v27.48a1 1 0 01-1 1H91a1 1 0 01-1-1z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M139.692 90.248V69a1 1 0 00-1-1H119a1 1 0 00-1 1v27.48a1 1 0 001 1h8.734" stroke="#FB6058"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M128.118 109.822V99.254m0 0v-23.36c0-1.298.779-3.894 3.894-3.894s4.264 2.596 4.449 3.894v18.354c.557-2.225 1.669-4.45 5.006-3.893 5.006 1.668 3.894 3.893 3.894 8.9.016-1.113.556-5.007 3.337-5.007 7.787-.556 5.562 9.456 5.562 9.456s1.113-4.45 4.45-4.45c2.225 0 3.893 1.113 3.893 3.337 0 2.782.708 6.119 0 13.906-.445 4.894-3.522 7.787-5.006 9.455v11.681h-24.473v-8.9c-8.009-7.119-10.753-13.72-11.124-16.13v-7.787c0-4.004 4.079-5.376 6.118-5.562z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hbw-flow-install-lg" viewBox="0 0 240 350">
        <path d="M128 0v98m0 147v105" stroke="#FB6058" stroke-width="3" />
        <path d="M188 237.467c0-26.982-19.422-49.602-45.555-55.637M69 237.467c0-26.982 19.421-49.602 45.555-55.637"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M96.889 189.189v35.761m63.22-35.761v28.609" stroke="#2D3D4D" stroke-width="1.5" />
        <path
          d="M168.751 231.512h7.383c2.531 0 2.461-1.353 2.109-2.029-3.036-3.717-6.306-5.508-9.492-6.071m0 0a13.333 13.333 0 00-1.055-.142c-6.155-.581-10.547 1.141-14.766 6.213H84.622c-2.11-.001-6.329 2.028-6.329 7.099 0 5.072 4.22 7.1 6.329 7.1h68.308c10.547 14.199 25.313 2.029 25.313.001 0-1.623-1.406-2.029-2.109-2.029h-7.383v-18.242zm-10.541 13.171h4.219m-4.219-4.06h4.219m-4.219 8.115h4.219"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M88.092 236.581c0 1.239-1.053 2.293-2.415 2.293-1.36 0-2.414-1.054-2.414-2.293s1.053-2.293 2.414-2.293c1.362 0 2.415 1.054 2.415 2.293z"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M128.515 165.118c8.251 0 10.886-3.562 11.173-5.344h-21.485c0 1.782 2.062 5.344 10.312 5.344z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M97.814 130.337a37.09 37.09 0 00-1.8 11.471c0 19.256 14.558 34.866 32.517 34.866 17.959 0 32.518-15.61 32.518-34.866a37.1 37.1 0 00-1.8-11.468 2.262 2.262 0 01-1.136 1.396 35.709 35.709 0 011.436 10.072c0 18.528-13.984 33.366-31.018 33.366s-31.017-14.838-31.017-33.366c0-3.516.503-6.9 1.436-10.074a2.265 2.265 0 01-1.136-1.397z"
          fill="#2D3D4D" />
        <path
          d="M151.955 105.675h0c2.757 2.836 4.423 6.723 5.396 10.992.971 4.264 1.238 8.852 1.238 13.03 0 .847-.645 1.499-1.41 1.525l-.123-.018-.525-.072c-.458-.063-1.127-.152-1.968-.259a286.573 286.573 0 00-6.841-.784c-5.555-.57-12.734-1.142-19.19-1.142-6.455 0-13.633.572-19.187 1.142-2.779.285-5.156.57-6.839.784-.841.107-1.509.196-1.967.259l-.525.072-.123.018c-.767-.026-1.414-.68-1.414-1.525 0-4.208.053-8.796.815-13.065.762-4.269 2.219-8.137 4.962-10.957h.001c2.747-2.829 6.751-4.571 11.144-5.599 4.384-1.027 9.08-1.326 13.133-1.326 4.055 0 8.535.3 12.7 1.324 4.168 1.025 7.967 2.763 10.723 5.601z"
          stroke="#2D3D4D" stroke-width="1.5" />
        <path d="M98.483 131.503c5.268-4.463 30.317-26.337 59.422-.667m-42.421 51.201l13.388 5.364 12.644-5.364"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hbw-flow-install-sm" viewBox="0 0 198 163">
        <path d="M98 141v22M98 0v39" stroke="#FB6058" stroke-width="2" />
        <path d="M139 136.275c0-18.819-13.546-34.596-31.773-38.805M56 136.275c0-18.819 13.546-34.596 31.773-38.805"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M75.452 102.603v24.942m44.095-24.942v19.954" stroke="#2D3D4D" stroke-width="1.5" />
        <path
          d="M125.574 132.122h5.15c1.765 0 1.716-.944 1.471-1.416-2.118-2.592-4.399-3.841-6.621-4.234m0 0a9.34 9.34 0 00-.736-.099c-4.292-.405-7.356.796-10.299 4.333H66.896c-1.471 0-4.414 1.415-4.414 4.952s2.943 4.952 4.414 4.952h47.643c7.357 9.904 17.656 1.416 17.656.001 0-1.132-.981-1.415-1.471-1.415h-5.15v-12.724zm-7.352 9.187h2.943m-2.943-2.832h2.943m-2.943 5.66h2.943"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M69.09 135.657c0 .731-.625 1.373-1.458 1.373-.832 0-1.457-.642-1.457-1.373 0-.73.625-1.372 1.457-1.372.833 0 1.457.642 1.457 1.372z"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M97.51 85.813c5.755 0 7.593-2.484 7.793-3.727H90.318c0 1.243 1.439 3.727 7.193 3.727z" stroke="#2D3D4D"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M76.097 61.554a25.876 25.876 0 00-1.256 8.001c0 13.43 10.155 24.319 22.68 24.319 12.527 0 22.681-10.888 22.681-24.319 0-2.802-.442-5.494-1.255-7.998a1.556 1.556 0 01-1.223 1.117c.635 2.167.978 4.478.978 6.881 0 12.703-9.58 22.819-21.18 22.819-11.601 0-21.18-10.116-21.18-22.819 0-2.404.342-4.715.977-6.882a1.56 1.56 0 01-1.222-1.119z"
          fill="#2D3D4D" />
        <path
          d="M113.696 44.51l.001.001c1.883 1.938 3.031 4.604 3.704 7.56.673 2.95.858 6.13.858 9.037 0 .468-.347.812-.745.836l-.065-.01-.367-.05a201.176 201.176 0 00-6.152-.728c-3.878-.398-8.894-.798-13.407-.798-4.514 0-9.53.4-13.406.798a200.598 200.598 0 00-6.15.728l-.367.05-.065.01c-.4-.024-.749-.37-.749-.836 0-2.936.038-6.118.565-9.073.528-2.955 1.532-5.602 3.4-7.524h.001c1.873-1.93 4.619-3.13 7.662-3.842 3.034-.71 6.291-.919 9.109-.919 2.819 0 5.923.208 8.803.917 2.883.709 5.489 1.906 7.37 3.844z"
          stroke="#2D3D4D" stroke-width="1.5" />
        <path d="M76.564 62.368c3.674-3.113 21.145-18.37 41.445-.466M88.422 97.614l9.337 3.742 8.819-3.742"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hbw-flow-pay-lg" viewBox="0 0 240 350">
        <path d="M101 118V0m0 350V230" stroke="#FB6058" stroke-width="3" />
        <path
          d="M128.979 193.176a.75.75 0 00-1.06 1.061l1.06-1.061zm23.94 26.061a.749.749 0 101.06-1.061l-1.06 1.061zm-67.225-93.665a.75.75 0 10-1.06 1.061l1.06-1.061zm47.94 50.061a.752.752 0 001.061 0 .752.752 0 000-1.061l-1.061 1.061zm-59.377-37.179a.75.75 0 10-1.06 1.061l1.06-1.061zm38.939 41.061a.75.75 0 101.061-1.061l-1.061 1.061zm6.107 38.75l-.53-.531.53.531zm-12.971-12.971l-.53-.53.53.53zm21.618 21.618l-.53-.531.53.531zm-12.971-12.971l-.53-.53.53.53zm12.14-71.277a.75.75 0 10.041 1.5l-.041-1.5zm97.3 56.956l.53.53a.749.749 0 000-1.061l-.53.531zm-23.51 23.509l-.53-.531.53.531zm-42.569-6.881a.75.75 0 10-1.061 1.061l1.061-1.061zm-40.929 29.849l.531-.53-.531.53zm-71.608-71.608l-.53.53.53-.53zm0-8.647l-.53-.53.53.53zm47.289-47.288l-.53-.531.53.531zm8.647 0l-.531.53.531-.53zm24.319 127.543l.531.531-.531-.531zm69.414-36.038a.75.75 0 00-.474-1.423l.474 1.423zm-38.068-10.439l-.565.493.565-.493zm-37.162-6.181l-.007.75.007-.75zm.845-18.68v.75-.75zm48.207.75a.75.75 0 100-1.5v1.5zm-57.771 9.393l-.749.044.749-.044zm16.396 9.335l25 25 1.06-1.061-25-25-1.06 1.061zm-43.285-67.604l49 49 1.061-1.061-49-49-1.061 1.061zm-11.438 12.882l40 40 1.061-1.061-40-40-1.06 1.061zm45.577 78.219a8.421 8.421 0 01-11.91 0l-1.061 1.061c3.875 3.875 10.157 3.875 14.031 0l-1.06-1.061zm-11.91 0a8.42 8.42 0 010-11.909l-1.061-1.061c-3.875 3.875-3.875 10.157 0 14.031l1.061-1.061zm0-11.909a8.421 8.421 0 0111.91 0l1.06-1.061c-3.874-3.875-10.156-3.875-14.031 0l1.061 1.061zm11.91 0a8.422 8.422 0 010 11.909l1.06 1.061c3.875-3.874 3.875-10.156 0-14.031l-1.06 1.061zm8.647 20.556a8.421 8.421 0 01-11.91 0l-1.061 1.061c3.875 3.875 10.157 3.875 14.031 0l-1.06-1.061zm-11.91 0a8.42 8.42 0 010-11.909l-1.061-1.061c-3.875 3.875-3.875 10.157 0 14.031l1.061-1.061zm0-11.909a8.421 8.421 0 0111.91 0l1.06-1.061c-3.874-3.875-10.156-3.875-14.031 0l1.061 1.061zm11.91 0a8.422 8.422 0 010 11.909l1.06 1.061c3.875-3.874 3.875-10.156 0-14.031l-1.06 1.061zm-.26-70.308c6.623-.179 17.272.251 27.652 2.123 10.423 1.879 20.396 5.183 25.841 10.628l1.061-1.061c-5.797-5.796-16.18-9.158-26.636-11.044-10.499-1.893-21.252-2.327-27.959-2.146l.041 1.5zm53.493 12.751l43.235 43.235 1.061-1.061-43.235-43.235-1.061 1.061zm43.235 42.174l-23.509 23.509 1.061 1.061 23.509-23.509-1.061-1.061zm-23.509 23.509c-2.984 2.984-8.313 6.621-14.554 7.82-6.194 1.19-13.323-.008-20.054-6.739l-1.061 1.061c7.104 7.104 14.747 8.429 21.398 7.151 6.603-1.269 12.191-5.091 15.332-8.232l-1.061-1.061zm-34.608 1.081l-7.431-7.431-1.061 1.061 7.431 7.431 1.061-1.061zm-47.829 21.888l-71.609-71.608-1.06 1.06 71.608 71.609 1.061-1.061zm-71.609-79.195l47.289-47.288-1.06-1.061-47.29 47.289 1.061 1.06zm54.875-47.288l56.411 56.41 1.06-1.061-56.41-56.41-1.061 1.061zm63.562 87.241l-39.242 39.242 1.061 1.061 39.241-39.242-1.06-1.061zM46.333 173.959a5.365 5.365 0 010-7.587l-1.06-1.06a6.864 6.864 0 000 9.707l1.06-1.06zm70.548 72.669a6.866 6.866 0 009.708 0l-1.061-1.061a5.364 5.364 0 01-7.586 0l-1.061 1.061zm-23.26-127.544a5.364 5.364 0 017.587 0l1.061-1.061a6.866 6.866 0 00-9.708 0l1.06 1.061zm101.377 89.552c-7.43 2.476-25.209 3.999-37.03-9.51l-1.129.987c12.392 14.162 30.93 12.514 38.633 9.946l-.474-1.423zm-37.03-9.51c-1.565-1.79-3.935-3.045-6.727-3.943-2.8-.9-6.095-1.462-9.588-1.819-6.979-.712-14.882-.613-21.403-.675l-.015 1.5c6.576.063 14.376-.036 21.266.667 3.441.352 6.621.9 9.281 1.755 2.67.859 4.746 2.004 6.057 3.502l1.129-.987zm-36.881-23.617h48.207v-1.5h-48.207v1.5zm-8.816 9.349c-.298-5.072 3.735-9.349 8.816-9.349v-1.5c-5.944 0-10.662 5.004-10.313 10.937l1.497-.088zm7.979 7.831c-4.32-.042-7.722-3.465-7.979-7.831l-1.497.088c.3 5.099 4.3 9.193 9.461 9.243l.015-1.5z"
          fill="#2D3D4D" />
      </symbol>
      <symbol id="hbw-flow-pay-sm" viewBox="0 0 198 163">
        <path d="M100 128.5V163M100 .137v57" stroke="#FB6058" stroke-width="2" />
        <path
          d="M96.138 94.72a.75.75 0 00-1.061 1.06l1.06-1.06zm17.869 19.991a.75.75 0 101.061-1.061l-1.061 1.061zM63.533 43.649a.75.75 0 00-1.066 1.056l1.066-1.056zm35.434 37.879a.75.75 0 001.066-1.056l-1.066 1.056zM54.204 52.967a.75.75 0 00-1.055 1.066l1.055-1.066zm29.268 31.066a.75.75 0 001.056-1.066l-1.056 1.066zm10.994-27.487a.75.75 0 00.04 1.5l-.04-1.5zm73.682 43.31l.531.53a.75.75 0 000-1.06l-.531.53zm-17.801 17.802l-.53-.531.53.531zm-27.009.818l-.537.525.006.005.531-.53zm-5.802-7.001a.75.75 0 10-1.072 1.05l1.072-1.05zM87.121 135.05l-.53.53.53-.53zM32.898 80.827l.53-.53-.53.53zm0-6.548l-.53-.53.53.53zm35.807-35.808l.53.53-.53-.53zm6.548 0l.53-.53-.53.53zm18.415 96.579l-.53-.53.53.53zm52.619-27.116a.75.75 0 00-.474-1.423l.474 1.423zm-28.883-8.078l.564-.494-.564.494zm-28.14-4.68l-.007.75.007-.75zm.64-14.144v-.75.75zm36.503.75a.75.75 0 000-1.5v1.5zm-43.746 6.93l.75-.044-.75.044zm12.416 7.069l18.93 18.93 1.061-1.061-18.93-18.93-1.061 1.06zm-32.61-51.076l36.5 36.823 1.066-1.056-36.5-36.823-1.066 1.056zm-9.318 9.328l30.323 30 1.056-1.066-30.324-30-1.055 1.066zm34.874 59.411a6.194 6.194 0 01-8.761 0l-1.06 1.061a7.695 7.695 0 0010.881 0l-1.06-1.061zm-8.761 0a6.195 6.195 0 010-8.761l-1.06-1.06a7.695 7.695 0 000 10.882l1.06-1.061zm0-8.761a6.195 6.195 0 018.76 0l1.061-1.06a7.695 7.695 0 00-10.882 0l1.06 1.06zm8.76 0a6.195 6.195 0 010 8.761l1.061 1.061a7.695 7.695 0 000-10.882l-1.06 1.06zm6.548 15.309a6.195 6.195 0 01-8.76 0l-1.061 1.06a7.695 7.695 0 0010.882 0l-1.06-1.06zm-8.76 0a6.195 6.195 0 010-8.761l-1.061-1.061a7.695 7.695 0 000 10.882l1.06-1.06zm0-8.761a6.195 6.195 0 018.76 0l1.06-1.061a7.695 7.695 0 00-10.881 0l1.06 1.061zm8.76 0a6.195 6.195 0 010 8.761l1.06 1.06a7.695 7.695 0 000-10.882l-1.06 1.061zm-.063-53.185c5.005-.136 13.056.19 20.902 1.605 7.888 1.422 15.39 3.916 19.471 7.997l1.06-1.06c-4.431-4.432-12.344-6.985-20.265-8.414-7.964-1.436-16.12-1.765-21.209-1.628l.04 1.5zm40.373 9.602l32.738 32.738 1.061-1.06-32.739-32.739-1.06 1.06zm32.738 31.678l-17.801 17.801 1.06 1.061 17.802-17.802-1.061-1.06zm-17.801 17.801c-2.241 2.241-6.245 4.972-10.927 5.871-4.634.891-9.971-.001-15.022-5.052l-1.061 1.06c5.425 5.425 11.274 6.444 16.366 5.465 5.044-.969 9.307-3.885 11.704-6.283l-1.06-1.061zm-25.943.825l-6.338-6.477-1.072 1.05 6.338 6.476 1.072-1.049zM87.651 134.52L33.428 80.296l-1.06 1.061L86.59 135.58l1.06-1.06zM33.428 74.81l35.808-35.808-1.06-1.061-35.809 35.808 1.06 1.06zm41.295-35.808l42.715 42.715 1.06-1.061-42.715-42.715-1.06 1.06zm48.13 65.803L93.138 134.52l1.06 1.06 29.715-29.714-1.06-1.061zM33.428 80.296a3.88 3.88 0 010-5.487l-1.06-1.06a5.38 5.38 0 000 7.608l1.06-1.06zM86.59 135.58a5.38 5.38 0 007.608 0l-1.06-1.06a3.88 3.88 0 01-5.488 0l-1.06 1.06zM69.236 39.002a3.88 3.88 0 015.487 0l1.06-1.061a5.38 5.38 0 00-7.608 0l1.06 1.06zm76.577 67.509c-5.593 1.864-18.964 3.001-27.845-7.149l-1.129.988c9.452 10.803 23.582 9.539 29.448 7.584l-.474-1.423zm-27.845-7.149c-1.217-1.39-3.047-2.354-5.175-3.038-2.138-.688-4.647-1.115-7.298-1.386-5.295-.54-11.292-.465-16.224-.512l-.014 1.5c4.986.048 10.88-.027 16.086.505 2.599.265 4.993.678 6.991 1.32 2.006.646 3.543 1.5 4.505 2.599l1.129-.988zm-28.065-17.58h36.504v-1.5H89.903v1.5zm-6.493 6.886a6.504 6.504 0 016.493-6.886v-1.5c-4.605 0-8.26 3.877-7.99 8.474l1.497-.088zm5.861 5.758c-3.168-.03-5.672-2.541-5.86-5.758l-1.498.088c.232 3.95 3.334 7.132 7.344 7.17l.014-1.5z"
          fill="#2D3D4D" />
      </symbol>
      <symbol id="hbw-flow-receive-lg" viewBox="0 0 240 350">
  
        <path class="st0" d="M144 200H60v-99h84v99zm-81-3h78v-93H63v93z" fill="#FB6058" />
        <path
          d="M78.6 182.7c1.9.5 2.9 2.2 2.9 3.6 0 2.5-1.8 4.6-4.8 4.6h-6.6v-15.3h6.1c2.7 0 4.5 1.6 4.5 4.2-.1 1-.6 2.3-2.1 2.9zm-5.7 1.4v4.2h3.5c1.3 0 2-1.1 2-2.1s-.7-2.1-2-2.1h-3.5zm3-2.4c1 0 1.7-.8 1.7-1.8 0-1.1-.6-1.8-1.8-1.8h-2.9v3.6h3zm23 1.5c0-2.8-2.2-5.1-5-5.1s-5 2.3-5 5.1 2.2 5 5 5c2.8.1 5-2.2 5-5zm2.9 0c0 4.3-3.5 7.8-7.9 7.8s-7.9-3.5-7.9-7.8c0-4.4 3.5-7.9 7.9-7.9 4.4.1 7.9 3.6 7.9 7.9zm11.9-.5l5.4 8.2h-3.4L112 185l-3.7 5.9h-3.4l5.4-8.2-4.9-7.1h3.5l3.1 4.9 3.1-4.9h3.5l-4.9 7.1zm9.8-7.1h11.7v2.6h-4.4v12.7h-2.9v-12.7h-4.4v-2.6z"
          fill-rule="evenodd" clip-rule="evenodd" fill="#fb6058" />
        <path
          d="M147.1 230.9c0 .4-.3.8-.8.8h-44.7v-.8.8h-.5l-.2-.5c-2.3-6.4-8.5-11.3-15.7-11.3-7.3 0-13.5 4.9-15.7 11.3l-.2.5h-7.5c-.4 0-.8-.3-.8-.8 0-.4.3-.8.8-.8h6.4c2.6-6.7 9.2-11.8 17-11.8s14.4 5 17 11.8h44.2c.3 0 .7.4.7.8zm49.6 0c0 .4-.3.8-.8.8h-5.8l-.2-.5c-2.3-6.4-8.5-11.3-15.7-11.3-7.3 0-13.5 4.9-15.7 11.3l-.2.5H153c-.4 0-.8-.3-.8-.8 0-.4.3-.8.8-.8h4.2c2.6-6.8 9.2-11.8 17-11.8 7.8 0 14.4 5 17 11.8h4.7c.5 0 .8.4.8.8zm2.3-33.8l-11-4.7-10.5-20.3c-1.2-2.4-3.6-3.9-6.4-3.9H155c-3.9 0-7.3 3.2-7.3 7.1v28.8H61.8c-4 .1-7.2 3.3-7.3 7.3v17.7c0 3.9 3.3 7 7.3 7h7.5l.1 1.4c.5 8.6 7.4 14.9 15.9 14.9s15.3-6.3 15.9-14.9l.1-1.4h57.2l.1 1.4c.5 8.6 7.4 14.9 15.9 14.9s15.3-6.3 15.9-14.9l.1-1.4h5.8c4 0 7.3-3.1 7.3-7v-25.6c-.4-2.7-2-5.3-4.6-6.4zm.4-1.5c3.2 1.3 5.3 4.5 5.3 7.9v25.6c0 4.8-4 8.5-8.8 8.5h-4.4c-.6 9.4-8.1 16.3-17.4 16.3-9.2 0-16.8-6.9-17.4-16.3h-54.4c-.6 9.4-8.1 16.3-17.4 16.3-9.2 0-16.8-6.9-17.4-16.3h-6c-4.8 0-8.8-3.7-8.8-8.5v-17.7c.1-4.8 3.9-8.7 8.8-8.8h84.6v-27.3c0-4.8 4-8.6 8.8-8.6H171c3.3 0 6.2 1.8 7.7 4.7l10.2 19.8 10.5 4.4zM171.3 172c1.3.1 2.4.8 3 2l8.7 17c1.2 2.3-.5 5.1-3.1 5.1h-28.3v-20.7c0-2 1.8-3.4 3.5-3.4h16.2zm1.6 2.6c-.3-.7-.9-1.1-1.7-1.1h-16.1c-1 0-2 .9-2 1.9v19.2h26.8c1.5 0 2.5-1.6 1.8-2.9l-8.8-17.1zm-87.7 73.9c-6.7 0-12.2-5.4-12.2-12.2s5.4-12.2 12.2-12.2c6.7 0 12.2 5.4 12.2 12.2s-5.5 12.2-12.2 12.2zm89.1 0c-6.7 0-12.2-5.4-12.2-12.2s5.4-12.2 12.2-12.2c6.7 0 12.2 5.4 12.2 12.2s-5.5 12.2-12.2 12.2zm-99.8-12.2c0 5.9 4.8 10.7 10.7 10.7 5.9 0 10.7-4.8 10.7-10.7 0-5.9-4.8-10.7-10.7-10.7-5.9 0-10.7 4.8-10.7 10.7zm89.1 0c0 5.9 4.8 10.7 10.7 10.7 5.9 0 10.7-4.8 10.7-10.7 0-5.9-4.8-10.7-10.7-10.7-6 0-10.7 4.8-10.7 10.7z"
          fill-rule="evenodd" clip-rule="evenodd" fill="#2d3d4d" />
        <path class="st0" fill="#FB6058"
          d="M129.5 350h-3v-56c0-4.7-3.8-8.5-8.5-8.5H56c-11.9 0-21.5-9.6-21.5-21.5v-35c0-6.3 5.2-11.5 11.5-11.5h7v3h-7c-4.7 0-8.5 3.8-8.5 8.5v35c0 10.2 8.3 18.5 18.5 18.5h62c6.3 0 11.5 5.2 11.5 11.5v56zm-27-249h-3V0h3v101z" />
      </symbol>
      <symbol id="hbw-flow-receive-sm" viewBox="0 0 198 163">
       
        <path d="M98 140v23M98 0v42m10.4 1v69.2H50V43h58.4z" fill="none" stroke="#fb6058" stroke-width="2" />
        <path
          d="M62.4 100.7c1.4.3 2.1 1.6 2.1 2.6 0 1.8-1.3 3.3-3.5 3.3h-4.7v-11h4.4c2 0 3.2 1.2 3.2 3 0 .8-.4 1.7-1.5 2.1zm-4.1 1.1v3.1h2.5c.9 0 1.4-.8 1.4-1.5s-.5-1.5-1.5-1.5h-2.4zm2.2-1.8c.7 0 1.2-.6 1.2-1.3 0-.8-.5-1.3-1.3-1.3h-2.1v2.6h2.2zm16.5 1.1c0-2-1.6-3.6-3.6-3.6s-3.6 1.6-3.6 3.6 1.6 3.6 3.6 3.6c2 .1 3.6-1.5 3.6-3.6zm2.1 0c0 3.1-2.5 5.6-5.7 5.6s-5.7-2.5-5.7-5.6c0-3.1 2.5-5.7 5.7-5.7 3.2.1 5.7 2.6 5.7 5.7zm8.5-.4l3.9 5.9H89l-2.7-4.2-2.7 4.2h-2.4l3.9-5.9-3.5-5.1h2.5l2.3 3.5 2.3-3.5h2.5l-3.6 5.1zm7.1-5.1h8.4v1.9H100v9.1h-2.1v-9.1h-3.1v-1.9z"
          fill-rule="evenodd" clip-rule="evenodd" fill="#fb6058" />
        <circle class="st2" cx="131.2" cy="139.6" r="7.7" />
        <path class="st2"
          d="M67.1 131.9c-4.2 0-7.7 3.4-7.7 7.7 0 4.3 3.4 7.7 7.7 7.7s7.7-3.4 7.7-7.7c0-4.3-3.4-7.7-7.7-7.7zm11.9 4.3v-.8.8zm57.5-29l-6.3-12.3c-.2-.5-.7-.8-1.2-.8h-11.6c-.7 0-1.5.6-1.5 1.4v13.8h19.3c1.1 0 1.8-1.1 1.3-2.1z" />
        <path class="st2"
          d="M148.9 111.5l-8-3.5-7.6-14.7c-.8-1.6-2.4-2.5-4.2-2.5h-11.6c-2.6 0-4.8 2.1-4.8 4.7v21.1H50.3c-2.6 0-4.8 2.2-4.8 4.8v12.7c0 2.5 2.1 4.6 4.8 4.6h5.8l.1 1.4c.4 6 5.1 10.3 11 10.3s10.6-4.4 11-10.3l.1-1.4h41.9l.1 1.4c.4 6 5.1 10.3 11 10.3s10.6-4.4 11-10.3l.1-1.4h4.5c2.6 0 4.8-2.1 4.8-4.6v-18.4c-.1-1.8-1.2-3.5-2.8-4.2zm-81.8 37.3c-5.1 0-9.2-4.1-9.2-9.2s4.1-9.2 9.2-9.2 9.2 4.1 9.2 9.2c0 5-4.1 9.2-9.2 9.2zm44-12.6H78.5l-.2-.5c-1.6-4.6-6-8-11.1-8s-9.5 3.4-11.1 8l-.2.5h-5.5c-.4 0-.8-.3-.8-.8 0-.4.3-.8.8-.8h4.5c1.9-4.9 6.7-8.5 12.3-8.5s10.4 3.6 12.3 8.5h31.6c.4 0 .8.3.8.8 0 .4-.4.8-.8.8zm3.3-40.7c0-1.7 1.5-2.9 3-2.9H129c1 0 2 .6 2.5 1.6l6.3 12.3c1 2-.4 4.3-2.6 4.3h-20.8V95.5zm16.8 53.2c-5.1 0-9.2-4.1-9.2-9.2s4.1-9.2 9.2-9.2 9.2 4.1 9.2 9.2-4.1 9.2-9.2 9.2zm15.6-12.5h-4.3l-.2-.5c-1.6-4.5-6-8-11.1-8s-9.5 3.4-11.1 8l-.2.5h-4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h2.9c1.9-4.9 6.7-8.5 12.4-8.5 5.6 0 10.4 3.6 12.3 8.5h3.3c.4 0 .8.3.8.8s-.4.8-.8.8z" />
        <path class="st3" fill="#2D3D4D"
          d="M137.9 106.5l-6.3-12.3c-.5-1-1.4-1.6-2.5-1.6h-11.6c-1.4 0-3 1.2-3 2.9v15.3h20.8c2.1 0 3.6-2.3 2.6-4.3zm-22-11c0-.7.7-1.4 1.5-1.4H129c.5 0 1 .3 1.2.8l6.3 12.3c.5 1-.2 2.1-1.3 2.1h-19.3V95.5zm15.3 34.5c-5.1 0-9.2 4.1-9.2 9.2s4.1 9.2 9.2 9.2 9.2-4.1 9.2-9.2-4.1-9.2-9.2-9.2zm0 16.9c-4.2 0-7.7-3.4-7.7-7.7 0-4.3 3.4-7.7 7.7-7.7s7.7 3.4 7.7 7.7c0 4.3-3.5 7.7-7.7 7.7z" />
        <path class="st3" fill="#2D3D4D"
          d="M146.8 134.7h-3.3c-1.9-4.9-6.7-8.5-12.3-8.5-5.6 0-10.4 3.6-12.4 8.5h-2.9c-.4 0-.8.3-.8.8s.3.8.8.8h4l.2-.5c1.6-4.5 6-8 11.1-8s9.5 3.4 11.1 8l.2.5h4.3c.4 0 .8-.3.8-.8s-.4-.8-.8-.8z" />
        <path class="st3" fill="#2D3D4D"
          d="M149.3 110l-7.4-3.2-7.3-14.2c-1.1-2.1-3.2-3.4-5.5-3.4h-11.6c-3.4 0-6.3 2.8-6.3 6.2V115H50.3c-3.5 0-6.3 2.9-6.3 6.3V134c0 3.4 2.9 6.1 6.3 6.1h4.3c.4 6.8 5.8 11.7 12.5 11.7 6.6 0 12.1-5 12.5-11.7h39.1c.4 6.8 5.8 11.7 12.5 11.7 6.6 0 12.1-5 12.5-11.7h3.1c3.4 0 6.3-2.7 6.3-6.1v-18.4c0-2.3-1.5-4.6-3.8-5.6zm2.3 24.2c0 2.5-2.2 4.6-4.8 4.6h-4.5l-.1 1.4c-.4 6-5.1 10.3-11 10.3s-10.6-4.4-11-10.3l-.1-1.4H78.2l-.1 1.4c-.4 6-5.1 10.3-11 10.3s-10.6-4.4-11-10.3l-.1-1.4h-5.8c-2.7 0-4.8-2.1-4.8-4.6v-12.7c0-2.7 2.2-4.8 4.8-4.8h62.3V95.5c0-2.6 2.2-4.7 4.8-4.7H129c1.8 0 3.4 1 4.2 2.5l7.6 14.7 8 3.5c1.7.8 2.7 2.4 2.8 4.2v18.5z" />
        <path class="st3" fill="#2D3D4D"
          d="M67.1 130c-5.1 0-9.2 4.1-9.2 9.2s4.1 9.2 9.2 9.2 9.2-4.1 9.2-9.2-4.1-9.2-9.2-9.2zm0 16.9c-4.2 0-7.7-3.4-7.7-7.7 0-4.3 3.4-7.7 7.7-7.7s7.7 3.4 7.7 7.7c0 4.3-3.4 7.7-7.7 7.7z" />
        <path class="st3" fill="#2D3D4D"
          d="M111.1 134.7H79.5c-1.9-4.9-6.7-8.5-12.3-8.5s-10.4 3.6-12.3 8.5h-4.5c-.4 0-.8.3-.8.8 0 .4.3.8.8.8h5.5l.2-.5c1.6-4.6 6-8 11.1-8s9.5 3.4 11.1 8l.2.5h.5v-.8.8h32.2c.4 0 .8-.3.8-.8-.1-.5-.5-.8-.9-.8z" />
      </symbol>
      <symbol id="hbw-flow-top-line-lg" viewBox="0 0 305 159">
        <path
          d="M273 0v60.5c0 8.008 6.492 14.5 14.5 14.5h.5c7.732 0 14-6.268 14-14v0c0-7.732-6.268-14-14-14H24C12.954 47 4 55.954 4 67v91.5"
          stroke="#FB6058" stroke-width="3" />
      </symbol>
      <symbol id="hbw-flow-top-line-md" viewBox="0 0 222 159">
        <path
          d="M191 .267v60.5c0 8.008 6.492 14.5 14.5 14.5h.5c7.732 0 14-6.268 14-14v0c0-7.732-6.268-14-14-14H22c-11.046 0-20 8.954-20 20v91.5"
          stroke="#FB6058" stroke-width="3" />
      </symbol>
      <symbol id="hbw-flow-top-line-sm" viewBox="0 0 43 90">
        <path
          d="M13 0v44.5C13 52.508 19.492 59 27.5 59h.5c7.732 0 14-6.268 14-14v0c0-7.732-6.268-14-14-14H17C8.163 31 1 38.163 1 47v42.5"
          stroke="#FB6058" stroke-width="2" />
      </symbol>
      <symbol id="heat" viewBox="0 0 475 474">
        <g fill="none" fill-rule="evenodd">
          <ellipse fill="url(#heata)" opacity=".05" cx="237.402" cy="237.007" rx="237.359" ry="236.969" />
          <g opacity=".1">
            <use fill="#000" filter="url(#heatb)" xlinkHref="#heatc" />
            <use fill="url(#heatd)" xlinkHref="#heatc" />
          </g>
          <g opacity=".15">
            <use fill="#000" filter="url(#heate)" xlinkHref="#heatf" />
            <use fill="url(#heatd)" xlinkHref="#heatf" />
          </g>
          <g opacity=".2">
            <use fill="#000" filter="url(#heatg)" xlinkHref="#heath" />
            <use fill="url(#heatd)" xlinkHref="#heath" />
          </g>
          <g opacity=".25">
            <use fill="#000" filter="url(#heati)" xlinkHref="#heatj" />
            <use fill="url(#heatd)" xlinkHref="#heatj" />
          </g>
          <g opacity=".3">
            <use fill="#000" filter="url(#heatk)" xlinkHref="#heatl" />
            <use fill="url(#heatd)" xlinkHref="#heatl" />
          </g>
          <g opacity=".3">
            <use fill="#000" filter="url(#heatm)" xlinkHref="#heatn" />
            <use fill="url(#heatd)" xlinkHref="#heatn" />
          </g>
        </g>
      </symbol>
      <symbol id="heating" viewBox="0 0 140 140">
        <path
          d="M30.355 18.9a2.8 2.8 0 012.8-2.8h73.693a2.8 2.8 0 012.8 2.8v92.523a2.8 2.8 0 01-2.8 2.8H33.155a2.8 2.8 0 01-2.8-2.8V18.901z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="96.286" cy="101.887" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" r="5.286" />
        <circle cx="41.182" cy="101.383" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" r="1.982" />
        <circle cx="50.982" cy="101.383" r="1.982" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M30.8 89.6h78.4" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M72.178 70.448c.313 2.046-1.155 2.672-1.155 2.672.793-4.115-3.225-7.795-3.225-7.795.313 2.816-3.081 5.99-3.081 9.335a4.79 4.79 0 004.79 4.79c2.644 0 4.788-2.047 4.788-4.742 0-3.273-2.117-4.26-2.117-4.26"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M81.2 114.801v10.672m-22.4-10.672v10.672M70 114.801v10.672" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="heating_and_hot_water" viewBox="0 0 164 164">
        <g clip-path="url(#heating_and_hot_waterclip0)">
          <path fill="#fff" d="M-19 40H84v103H-19z" />
          <rect x="2.1" y="64.396" width="57.916" height="3.944" rx="1.972" fill="#fff" stroke="#FB6058" />
          <rect x=".452" y="106.008" width="59.564" height="3.944" rx="1.972" fill="#fff" stroke="#FB6058" />
          <rect x=".452" y="106.008" width="3.944" height="3.944" rx="1.972" fill="#fff" stroke="#FB6058" />
          <rect x=".452" y="63.16" width="3.944" height="6.828" rx="1.972" fill="#fff" stroke="#FB6058" />
          <path
            d="M7.456 112.628a2.5 2.5 0 012.5-2.5h43.852a2.5 2.5 0 012.5 2.5v6.845a2.015 2.015 0 01-3.897.719l-1.196-3.131a3.5 3.5 0 00-3.27-2.252H15.502a3.5 3.5 0 00-3.344 2.467l-.876 2.833a1.956 1.956 0 01-3.825-.577v-6.904z"
            fill="#fff" stroke="#FB6058" />
          <rect x="7.456" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <rect x="14.872" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <rect x="22.288" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <rect x="29.704" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <rect x="37.12" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <rect x="44.536" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <rect x="51.952" y="61.512" width="4.356" height="51.736" rx="2.178" fill="#fff" stroke="#FB6058" />
          <path
            d="M105.32 81.064v-7.598c2.531-1.52 10.891-4.404 25.326-4.968M105.32 81.064c1.688 1.97 10.131 5.91 30.392 5.91 17.56 0 25.889-3.94 27.859-5.91m-58.251 0c1.407-1.97 9.455-5.91 30.392-5.91 20.936 0 27.296 3.94 27.859 5.91m0 0v-7.598c-1.805-1.547-8.948-4.512-23.638-4.997m-9.287-5.134v5.163m0 0a129.808 129.808 0 019.287-.03m0-5.133v5.134M125.581 78.53h2.533m5.065 0h2.533m5.909 0h2.533m-27.015 3.377h4.221m3.377 0h3.377m3.377 0h5.065m4.221 0h4.221m3.377 0h3.377m-28.704 16.885c-1.35-2.026 1.126-5.346 2.533-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.844 1.689-3.715 2.026-5.066 0zm8.833 0c-1.35-2.026 1.126-5.346 2.533-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.844 1.689-3.715 2.026-5.066 0zm8.833 0c-1.351-2.026 1.126-5.346 2.533-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.845 1.689-3.715 2.026-5.066 0zm1.298 11.819c-1.351-2.026 1.125-5.346 2.532-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.844 1.689-3.715 2.026-5.065 0zm-10.131 0c-1.35-2.026 1.126-5.346 2.533-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.844 1.689-3.715 2.026-5.066 0zm-10.13 0c-1.351-2.026 1.125-5.346 2.532-6.753 2.702 2.701 2.815 5.628 2.533 6.753-.844 1.689-3.715 2.026-5.065 0zm-10.131 0c-1.351-2.026 1.126-5.346 2.533-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.845 1.689-3.715 2.026-5.066 0zm38.834 0c-1.351-2.026 1.126-5.346 2.533-6.753 2.701 2.701 2.814 5.628 2.533 6.753-.844 1.689-3.715 2.026-5.066 0z"
            stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
          <path d="M82.715 91.96h3.72v-2.448h-3.72v-3.696h-2.449v3.696H76.57v2.448h3.697v3.72h2.448v-3.72z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="hiw-answer-ac" viewBox="0 0 170 170">
        <path
          d="M13.5 134.464H156m-107.303 0V66.703l42.56-24.806V31.612h11.496v10.285l43.956 24.806v67.761m-98.616-42.77h-27.83v5.26h2.42m25.41 0h-25.41m0 0v32.67m0 4.235v-4.235m25.41 0h-25.41"
          stroke="#212A37" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M28.733 129.019V102.5h13.705v26.519M88.84 72.753h3.63m12.705 0h-3.025m0 0h-9.68m9.68 0V65.19m-9.68 7.563V65.19m0 0v-7.562h9.68v7.562m-9.68 0h9.68"
          stroke="#212A37" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M63.823 122.364h13.31v-21.78h-13.31v21.78z" stroke="#212A37" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M77.133 111.473h-13.31" stroke="#212A37" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M81.975 122.364h13.31v-21.78h-13.31v21.78z" stroke="#212A37" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M95.286 111.473h-12.1m-24.204 10.89h41.141M57.17 61.257v-12.1h1.21m5.445 8.47v-8.47h-1.21m-4.236 0V45h4.236v4.157m-4.236 0h4.236"
          stroke="#212A37" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="#7AD0FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M135 87h24v29h-24z" />
      </symbol>
      <symbol id="hiw-answer-ev" viewBox="0 0 170 170">
        <path
          d="M61.364 125.132h48.279m-48.28 0H49.492c-3.8 0-4.573-2.93-4.485-4.395m16.357 4.395v8.014c0 2.482-1.935 2.93-2.903 2.844h-8.97c-4.484 0-4.484-1.802-4.484-4.395v-10.858m64.636 4.395h11.872c1.495 0 4.485-.879 4.485-4.395m-16.357 4.395v6.463c0 2.593.487 4.395 4.485 4.395h7.762c.278 0 .553-.028.828-.069 1.296-.193 3.282-.455 3.282-5.067v-10.117m0 0v-17.501a3.13 3.13 0 00-.065-.669c-.423-1.92-1.897-5.101-5.068-5.753m-69.529-.12c-2.11.69-6.331 2.896-6.331 6.205v17.838m6.331-24.042l6.956-18.915c.111-.301.173-.619.276-.923.271-.805 1.055-1.857 2.794-1.857h47.832c.298 0 .594.029.879.117 1.252.387 3.304 1.408 4.053 3.244.844 2.068 4.844 13.329 6.739 18.453m-69.529-.12H91.44M77.456 109.18h15.566m-15.566 5.002h15.566m27.845-17.367a5.945 5.945 0 00-1.199-.12h-8.179m-20.05 0c.352-3.274 2.85-9.823 10.025-9.823 7.176 0 9.674 6.55 10.025 9.824m-20.05 0h20.05m5.323 14.837v.296a4.02 4.02 0 01-4.02 4.02h-5.324a4.02 4.02 0 01-4.02-4.02v-.296c0-2.22 1.8-4.02 4.02-4.02h5.324c2.22 0 4.02 1.8 4.02 4.02zm-49.28 0v.296a4.02 4.02 0 01-4.02 4.02h-5.324a4.02 4.02 0 01-4.02-4.02v-.296c0-2.22 1.8-4.02 4.02-4.02h5.324c2.22 0 4.02 1.8 4.02 4.02zM25 52v84m120-84v84m4-84l-3.403-10H25v10h124z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="#00A56F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
          d="M14.166 76.5h22.667v28.333H14.166z" />
        <path d="M25 136h124" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hiw-answer-halfords-ev" viewBox="0 0 170 171">
        <path
          d="M12.423 134.612h145.808m-112.534.001V66.851l43.56-24.805V31.76h11.496v10.285l42.956 24.805v67.762m.002-47.191l9.075 9.076h-9.075m-98.618-6.655h-27.83v7.26h2.42m25.41 0h-25.41m0 0v32.671m0 4.235v-4.235m25.41 0h-25.41"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M25.733 129.168v-27.226h12.705v27.226M86.84 72.902h3.63m12.706 0h-3.026m0 0h-9.68m9.68 0v-7.563m-9.68 7.563v-7.563m0 0v-7.563h9.68v7.563m-9.68 0h9.68"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M60.824 122.513h13.31v-21.781h-13.31v21.781z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M74.134 111.622h-13.31" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M78.975 122.513h13.31v-21.781h-13.31v21.781z" stroke="#2D3D4D" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M92.286 111.622h-12.1m-24.204 10.89h41.141M54.17 61.405v-12.1h1.21m5.445 8.47v-8.47h-1.21m-4.236 0v-8.47h4.236v8.47m-4.236 0h4.236m-6.05-8.47h7.865m-6.05 4.235h3.63"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="#FF9800" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          d="M83 52.148h24v29H83z" />
      </symbol>
      <symbol id="hiw-answer" viewBox="0 0 170 170">
        <path
          d="M13.5 134.463H156m-107.303.001V66.703l42.56-24.806V31.612h11.496v10.285l43.956 24.806v67.761m.002-47.191l9.075 9.075h-9.075m-98.618-4.654h-27.83v5.26h2.42m25.41 0h-25.41m0 0v32.67m0 4.235v-4.235m25.41 0h-25.41"
          stroke="#212A37" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M28.733 129.019V102.5h13.705v26.519M88.84 72.753h3.63m12.705 0h-3.025m0 0h-9.68m9.68 0V65.19m-9.68 7.563V65.19m0 0v-7.562h9.68v7.562m-9.68 0h9.68"
          stroke="#212A37" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M63.823 122.364h13.31v-21.78h-13.31v21.78z" stroke="#212A37" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M77.133 111.473h-13.31" stroke="#212A37" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M81.975 122.364h13.31v-21.78h-13.31v21.78z" stroke="#212A37" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M95.286 111.473h-12.1m-24.204 10.89h41.141M57.17 61.257v-12.1h1.21m5.445 8.47v-8.47h-1.21m-4.236 0V45h4.236v4.157m-4.236 0h4.236"
          stroke="#212A37" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
          d="M49 39h24v29H49z" />
      </symbol>
      <symbol id="hiw-choose-halfords-ev" viewBox="0 0 170 171">
        <circle cx="90.225" cy="123.373" r="2.225" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M13 128.027l5.585-7.108a1 1 0 011.283-.251l.363.208m13.905 17.719l3.908-6.351a1 1 0 00-.355-1.392l-17.458-9.976m0 0c1.854-1.325 4.894-5.976 2.225-13.985-2.207-7.723 2.7-27.484 7.664-26.721.08.012.16.04.231.08 2.115 1.153 3.643 4.856.17 10.616a1.03 1.03 0 00-.07.936l1.46 3.409M37 130.817l4.45-1.669"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M36.45 129.586h85.1M32 127.917V89.539m0-7.231V43.373c0-1.78 1.483-2.225 2.225-2.225h111.242c1.779 0 2.224 1.484 2.224 2.225v57.846"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M137.668 97.201V53.148a1 1 0 00-1-1H43a1 1 0 00-1 1v64.746a1 1 0 001 1h69.639" stroke="#2D3D4D"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M51 98.627V71.148a1 1 0 011-1h19.692a1 1 0 011 1v27.48a1 1 0 01-1 1H52a1 1 0 01-1-1zm29 0V71.148a1 1 0 011-1h19.692a1 1 0 011 1v27.48a1 1 0 01-1 1H81a1 1 0 01-1-1z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M129.692 92.397V71.148a1 1 0 00-1-1H109a1 1 0 00-1 1v27.48a1 1 0 001 1h8.734" stroke="#FF9800"
          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M123.124 139.781v-8.899c-8.009-7.12-10.753-13.72-11.124-16.13v-7.787c0-4.005 4.079-5.377 6.118-5.562m29.479 38.378v-11.68c1.484-1.669 4.561-4.561 5.006-9.456.708-7.787 0-11.124 0-13.905 0-2.225-1.668-3.337-3.893-3.337-3.337 0-4.45 4.449-4.45 4.449s2.225-10.011-5.562-9.455c-2.781 0-3.321 3.893-3.337 5.006 0-5.006 1.112-7.231-3.894-8.9-3.337-.556-4.449 1.669-5.006 3.894V78.042c-.185-1.298-1.334-3.894-4.449-3.894-3.115 0-3.894 2.596-3.894 3.894v23.361m0 10.568v-10.568"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hiw-choose" viewBox="0 0 170 170">
        <path d="M148 101V49a8 8 0 00-8-8H40a8 8 0 00-8 8v72a8 8 0 008 8h81" stroke="#2D3D4D" stroke-width="1.5" />
        <path
          d="M12 127.879l5.585-7.109a1 1 0 011.283-.25l.363.207m13.905 17.719l3.908-6.35a1 1 0 00-.355-1.393l-17.458-9.976m0 0c1.854-1.324 4.894-5.975 2.225-13.984-2.207-7.724 2.7-27.485 7.664-26.721.08.012.16.04.231.08 2.104 1.147 3.627 4.816.226 10.524-.2.334-.201.752.007 1.08L32 95.5m5 35.169L41.45 129M142 96.5V49a2 2 0 00-2-2H40a2 2 0 00-2 2v72a2 2 0 002 2h76"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M50 96.48V71a2 2 0 012-2h17.692a2 2 0 012 2v25.48a2 2 0 01-2 2H52a2 2 0 01-2-2zm29 0V71a2 2 0 012-2h17.692a2 2 0 012 2v25.48a2 2 0 01-2 2H81a2 2 0 01-2-2zm50.692-5.232V71a2 2 0 00-2-2H110a2 2 0 00-2 2v25.48a2 2 0 002 2h7.734"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M123.124 139.633v-8.9c-8.009-7.119-10.753-13.72-11.124-16.13v-7.787c0-4.004 4.079-5.376 6.118-5.562m29.479 38.379v-11.681c1.484-1.668 4.561-4.561 5.006-9.455.708-7.787 0-11.124 0-13.906 0-2.224-1.668-3.337-3.893-3.337-3.337 0-4.45 4.45-4.45 4.45s2.225-10.012-5.562-9.456c-2.781 0-3.321 3.894-3.337 5.006 0-5.006 1.112-7.23-3.894-8.9-3.337-.555-4.449 1.67-5.006 3.894V77.893c-.185-1.297-1.334-3.893-4.449-3.893-3.115 0-3.894 2.596-3.894 3.894v23.36m0 10.568v-10.568"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hiw-cover-pay" viewBox="0 0 170 170">
        <path
          d="M75.066 115.311l-12.659 5.523 5.538-12.623m7.12 7.1l40.614-41.024M75.066 115.31l-7.12-7.1m68.04-54.436l-5.539-5.523c-.633-.631-2.373.263-3.164.789l-18.197 18.145m26.9-13.411l2.373 2.366c.527 1.052 1.108 3.63-.791 5.523-1.899 1.893-12.922 12.36-18.197 17.356m16.615-25.245l-20.307 20.512M67.945 108.21l41.141-41.025m0 0l6.593 7.1"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M60.7 35.413h56.215a6 6 0 016 6v2.678m-62.216-8.678L45 51.068m15.7-15.655v15.655H45m0 0v76.913a6 6 0 006 6h65.915a6 6 0 006-6V85.904M59.24 59.08h41.14m-41.14 9.468h22.152m-22.153 9.467h22.153"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M111.701 109.833c.179-1.665-.59-4.994-5.096-4.994-4.505 0-5.453 3.329-5.364 4.994v13.932m-4.023 0h13.678c.715.088 2.146-.526 2.146-3.68m-15.287-4.732h9.655"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hiw-installer-halfords-ev" viewBox="0 0 170 171">
        <path
          d="M130.333 131.909c0-20.558-14.797-37.792-34.708-42.39m-55.958 42.39c0-20.558 14.797-37.792 34.708-42.39M59.5 96.482v19.833m51-19.833v14.166"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M115.667 127.372h5.625c1.929 0 1.875-1.031 1.608-1.546-2.314-2.832-4.806-4.197-7.233-4.625m0 0a9.747 9.747 0 00-.804-.109c-4.689-.442-8.036.87-11.25 4.734H51.569c-1.607 0-4.822 1.545-4.822 5.409s3.215 5.409 4.822 5.409h52.044c8.036 10.819 19.287 1.546 19.287.001 0-1.237-1.072-1.546-1.608-1.546h-5.625v-13.898zm-8.031 10.035h3.215m-3.215-3.093h3.215m-3.215 6.182h3.215"
          stroke="#FF9800" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M54.284 131.234c0 .986-.837 1.818-1.91 1.818-1.075 0-1.912-.832-1.912-1.818 0-.986.837-1.818 1.911-1.818s1.911.832 1.911 1.818z"
          stroke="#FF9800" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M85.012 76.786c6.286 0 8.294-2.714 8.512-4.071h-16.37c0 1.357 1.572 4.07 7.858 4.07z" stroke="#2D3D4D"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M105.148 48.315c1.61 3.469 2.519 7.407 2.519 11.58 0 13.947-10.149 25.253-22.667 25.253-12.519 0-22.667-11.306-22.667-25.252 0-4.175.91-8.112 2.519-11.58"
          stroke="#2D3D4D" />
        <path
          d="M102.922 31.446h0c2.113 2.174 3.387 5.15 4.129 8.409.742 3.256.945 6.758.945 9.944 0 .684-.523 1.215-1.149 1.233l-.1-.014a162.141 162.141 0 00-1.898-.252c-1.282-.164-3.093-.38-5.21-.598-4.232-.434-9.7-.87-14.614-.87-4.915 0-10.38.436-14.612.87a219.225 219.225 0 00-6.707.794l-.4.056-.1.014c-.626-.018-1.152-.55-1.152-1.233 0-3.206.04-6.708.622-9.967.582-3.26 1.696-6.222 3.8-8.386h0c2.107-2.17 5.173-3.5 8.526-4.286 3.348-.783 6.932-1.012 10.023-1.012 3.093 0 6.512.229 9.693 1.011 3.183.783 6.092 2.112 8.204 4.287z"
          stroke="#2D3D4D" />
        <path d="M62.13 51.175c4.014-3.401 23.098-20.067 45.274-.509M75.083 89.677l10.2 4.087 9.634-4.087"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hiw-installer" viewBox="0 0 170 170">
        <path
          d="M130.333 131.76c0-20.557-14.797-37.791-34.708-42.39L85.5 93.5l-11.125-4.13c-19.911 4.599-34.709 21.833-34.709 42.39m45.346-55.123c6.285 0 8.293-2.714 8.512-4.07h-16.37c0 1.356 1.572 4.07 7.858 4.07z"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M107 52s.667 3.573.667 7.747C107.667 73.694 97.518 85 85 85c-12.52 0-22.5-11.306-22.5-25.253C62.5 56 63 52 63 52"
          stroke="#2D3D4D" stroke-width="1.5" />
        <path
          d="M102.365 34.59h.001c2.019 1.966 3.247 4.667 3.967 7.655.719 2.986.917 6.205.917 9.148 0 .443-.362.834-.854.856l-.079-.01a130.871 130.871 0 00-1.864-.235 225.568 225.568 0 00-5.11-.554c-4.15-.403-9.516-.808-14.343-.808-4.83 0-10.196.432-14.346.862a212.899 212.899 0 00-6.581.787l-.393.055-.075.01c-.493-.023-.855-.415-.855-.856 0-2.974.04-6.221.605-9.239.564-3.017 1.637-5.722 3.64-7.67v-.001c2.014-1.962 4.966-3.183 8.234-3.907 3.257-.722 6.75-.933 9.77-.933 3.022 0 6.353.211 9.446.93 3.098.722 5.899 1.94 7.92 3.91z"
          stroke="#2D3D4D" stroke-width="1.5" />
        <path d="M63 52c4.014-3.401 22.5-18 44 0" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M51.822 125.816h52.044c3.214-3.864 6.561-5.176 11.25-4.734.267.025.535.061.804.109 2.427.428 4.919 1.793 7.232 4.625.268.515.322 1.546-1.607 1.546h-5.625v7.727h5.625c.536 0 1.607.309 1.607 1.546 0 1.545-11.25 10.818-19.286-.001H51.822c-1.608 0-4.822-1.545-4.822-5.409s3.214-5.409 4.822-5.409z"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="hot_water" viewBox="0 0 146 146">
        <path
          d="M21.464 60.796V46.39c4.8-2.88 20.651-8.35 48.022-9.42M21.464 60.795C24.665 64.53 40.673 72 79.09 72c33.295 0 49.089-7.47 52.824-11.205m-110.45 0C24.132 57.06 39.392 49.59 79.09 49.59c39.698 0 51.757 7.47 52.824 11.205m0 0V46.39c-3.422-2.934-16.967-8.554-44.82-9.476m-17.608-9.733v9.788m0 0a245.77 245.77 0 019.604-.184c2.79 0 5.456.045 8.004.13m0-9.734v9.733M59.88 55.994h4.802m9.605 0h4.802m11.205 0h4.802m-51.223 6.403h8.004m6.403 0h6.402m6.403 0h9.605m8.003 0h8.004m6.403 0h6.403M55.079 94.412c-2.562-3.842 2.134-10.138 4.802-12.806 5.121 5.122 5.336 10.671 4.803 12.806-1.601 3.201-7.044 3.841-9.605 0zm16.748 0c-2.561-3.842 2.134-10.138 4.802-12.806 5.122 5.122 5.336 10.671 4.803 12.806-1.6 3.201-7.044 3.841-9.605 0zm16.748 0c-2.562-3.842 2.134-10.138 4.802-12.806 5.122 5.122 5.336 10.671 4.803 12.806-1.601 3.201-7.044 3.841-9.606 0zm2.46 22.409c-2.561-3.841 2.134-10.137 4.802-12.805 5.122 5.122 5.336 10.671 4.803 12.805-1.6 3.202-7.044 3.842-9.605 0zm-19.208 0c-2.561-3.841 2.134-10.137 4.802-12.805 5.122 5.122 5.336 10.671 4.803 12.805-1.6 3.202-7.044 3.842-9.605 0zm-19.209 0c-2.561-3.841 2.134-10.137 4.802-12.805 5.122 5.122 5.336 10.671 4.803 12.805-1.6 3.202-7.044 3.842-9.605 0zm-19.208 0c-2.562-3.841 2.134-10.137 4.802-12.805 5.121 5.122 5.335 10.671 4.802 12.805-1.6 3.202-7.044 3.842-9.605 0zm73.633 0c-2.561-3.841 2.134-10.137 4.802-12.805 5.122 5.122 5.336 10.671 4.803 12.805-1.601 3.202-7.044 3.842-9.605 0z"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="house-white" viewBox="0 0 171 170">
        <path d="M14.459 140.463h142.499" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M49.655 140.464V72.703l42.561-24.806V37.612h11.495v10.285l43.956 24.806v67.761m.002-47.191l9.076 9.075h-9.076m-98.617-4.654h-27.83v5.26h2.42m25.41 0h-25.41m0 0v32.67m0 4.235v-4.235m25.41 0h-25.41"
          stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M29.692 135.019V108.5h13.705v26.519m46.4-56.266h3.63m12.706 0h-3.025m0 0h-9.68m9.68 0V71.19m-9.68 7.563V71.19m0 0v-7.562h9.68v7.562m-9.68 0h9.68"
          stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M64.782 128.364h13.31v-21.78h-13.31v21.78z" stroke="#fff" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M78.092 117.473h-13.31" stroke="#fff" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M82.933 128.364h13.31v-21.78h-13.31v21.78z" stroke="#fff" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M96.245 117.473h-12.1m-24.205 10.89h41.142M58.128 67.257v-12.1h1.21m5.445 8.47v-8.47h-1.21m-4.235 0V51h4.235v4.157m-4.235 0h4.235"
          stroke="#fff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="howitworks-1" viewBox="0 0 160 160">
        <g fill="none" fill-rule="evenodd">
          <ellipse fill="#FB6058" opacity=".04" cx="79.953" cy="79.99" rx="79.953" ry="79.99" />
          <g opacity=".06">
            <use fill="#000" filter="url(#howitworks-1a)" xlinkHref="#howitworks-1b" />
            <use fill="#FB6058" xlinkHref="#howitworks-1b" />
          </g>
          <g opacity=".09">
            <use fill="#000" filter="url(#howitworks-1c)" xlinkHref="#howitworks-1d" />
            <use fill="#FB6058" xlinkHref="#howitworks-1d" />
          </g>
          <ellipse fill="#FFF9F9" cx="79.953" cy="79.99" rx="79.953" ry="79.99" />
          <ellipse fill="#FFEEEC" cx="79.953" cy="79.99" rx="60.094" ry="60.122" />
          <ellipse fill="#FFE3E1" cx="79.953" cy="79.99" rx="41.267" ry="41.287" />
          <path
            d="M17.26 93.221h22.322v-4.338H17.26v4.338zm22.322 29.132h-3.72V96.94H22.22v25.413H18.5V95.08h21.08v27.272zm-15.501 0H34V98.8h-9.92v23.553zm15.5 2.479v1.86h-21.08v-2.48h21.08v.62zm89.904-35.53l4.16 3.92h-4.16v-3.92zm0 37.39V95.08h4.917c.685 0 1.286-.418 1.533-1.056.248-.638-.223-1.351-.73-1.813l-5.72-4.93V66.039L91.044 44.066V34.34h-12.4v9.727l-9.783 5.51v2.15l11.098-6.25.544-.279v-8.379h8.68v8.38l38.442 21.97v59.523H41.442V71.293h-1.86v15.73H15.4v8.058h1.24v31.61H3v1.86h153.146v-1.861h-26.66z"
            fill="#334557" />
          <path fill="#FFF" d="M24 99h10v23H24z" />
          <path
            d="M50.477 47.726h2.113v-2.112h-2.113v2.112zm0 4.015h2.113v-2.077h-2.113v2.077zm-.59 8.52v-6.584h3.294v4.73l-3.294 1.855zm-8.445 6.908L67 52.773v-2.15l-11.883 6.693v-5.61h-.59v-6.252h.221v-1.936h-6.431v1.938h.222v6.248h-.59v9.648l-7.824 4.406-.544.279v3.396h1.86v-2.265z"
            fill="#334557" />
          <path fill="#FEFEFE" d="M133.645 93.221l-4.16-3.92v3.92h.62z" />
          <path fill="#FFF"
            d="M17.26 93.221h22.322v-4.338H17.26zm1.24 31.611v1.86h21.082v-2.48H18.5zm0-2.479h3.72V96.94h13.641v25.413h3.72V95.08h-21.08z" />
          <path fill="#FEFEFE" d="M81.123 61.61v4.34h7.44v-4.96h-7.44z" />
          <path
            d="M70.739 116.155h.464V96.94h13.02v19.215h3.721v1.859H50.122v-1.86h3.72V96.94h13.02v19.215h3.877zm6.044-44.007h2.48V59.13h11.16v13.017h2.481v1.859h-16.12v-1.86zm-35.341-.855v55.398h86.183V67.17L89.184 45.197v-8.379h-8.68v8.38l-.545.278-11.098 6.25v19.567h-27.42z"
            fill="#FFF" />
          <path fill="#FFF" d="M41.442 67.169v2.265H67v-16.66z" />
          <path fill="#FEFEFE" d="M55.702 106.237h9.3V98.8h-9.3zm0 9.918h9.3v-8.058h-9.3z" />
          <path
            d="M73.063 116.155h9.3v-8.058h-9.3v8.058zm0-9.918h9.3V98.8h-9.3v7.438zm-17.36 0h9.3V98.8h-9.3v7.438zm0 9.918h9.3v-8.058h-9.3v8.058zm12.142 0h-.982V96.94h-13.02v19.215h-3.721v1.859h37.822v-1.86h-3.721V96.94h-13.02v19.215h-3.359z"
            fill="#334557" />
          <path fill="#F6F5F3" d="M73.063 106.237h9.3V98.8h-9.3zm0 9.918h9.3v-8.058h-9.3z" />
          <path fill="#FEFEFE" d="M81.123 72.148h7.44v-4.34h-7.44z" />
          <path
            d="M88.564 61.61v4.34h-7.44v-4.96h7.44v.62zm-7.44 10.538h7.44v-4.34h-7.44v4.34zm11.78 0h-2.48V59.13h-11.16v13.017h-2.48v1.859h16.12v-1.86z"
            fill="#334557" />
          <path d="M36.066 69.434v-37.17h30.935v37.17H36.066zm32.795 1.86v-40.89H34.206v40.89h34.655z" fill="#F06A63" />
        </g>
      </symbol>
      <symbol id="howitworks-2" viewBox="0 0 160 160">
        <g fill="none" fill-rule="evenodd">
          <ellipse fill="#FFF9F9" cx="79.953" cy="79.99" rx="79.953" ry="79.99" />
          <ellipse fill="#FFEEEC" cx="79.953" cy="79.99" rx="60.094" ry="60.122" />
          <ellipse fill="#FFE3E1" cx="79.953" cy="79.99" rx="41.267" ry="41.287" />
          <g transform="translate(0 33)">
            <path
              d="M82.137 74.477c1.157 0 2.095.94 2.095 2.099 0 1.16-.938 2.1-2.095 2.1a2.097 2.097 0 01-2.095-2.1c0-1.16.938-2.1 2.095-2.1zm50.135-18.051c.368-.28.772-.491 1.195-.654V2.666a.821.821 0 00-.82-.821H30.845a.821.821 0 00-.819.821v77.14c0 .453.368.821.82.821h77.241a48.356 48.356 0 01-2.97-3.684c-1.178-1.633-2.069-3.142-2.708-4.556H37.29V9.525h89.22v42.004c.247-.038.498-.06.754-.06 2.694 0 4.885 2.195 4.885 4.896v.155l.123-.094z"
              fill="#FEFEFE" />
            <mask id="howitworks-2a" fill="#fff">
              <path d="M0 92.426h140.081V0H0z" />
            </mask>
            <g mask="url(#howitworks-2a)">
              <path fill-opacity=".634" fill="#FFF" d="M38 10h89v62H38z" />
              <path stroke="#979797" d="M38.5 10.5h88v61h-88z" />
            </g>
            <path
              d="M98.588 28.742V54.18h7.927V35.831a4.895 4.895 0 014.884-4.894 4.895 4.895 0 014.885 4.894v12.741l.122-.094c.155-.117.32-.213.485-.31V28.742H98.588z"
              fill="#FEFEFE" mask="url(#howitworks-2a)" />
            <path
              d="M71.144 56.025h21.987V26.897H71.144v29.128zm-25.603 0h21.987V26.897H45.54v29.128zm78.798-3.573c.105-.08.22-.14.329-.21V11.37H39.132v59.173h62.57c-.312-1.027-.476-2-.476-2.92v-5.299c0-3.012 2.197-5.558 5.225-6.051l.064-.01v-.238h-9.769V26.897h21.987v20.647a4.94 4.94 0 01.598-.048 4.896 4.896 0 014.886 4.894v.155l.122-.093z"
              mask="url(#howitworks-2a)" />
            <path
              d="M82.137 78.675c1.157 0 2.095-.94 2.095-2.1a2.096 2.096 0 00-2.095-2.098c-1.157 0-2.095.94-2.095 2.099 0 1.16.938 2.099 2.095 2.099"
              fill="#334557" mask="url(#howitworks-2a)" />
            <path fill="#FFF" mask="url(#howitworks-2a)" d="M46 27h21v29H46zm26 0h21v29H72z" />
            <path d="M72.985 54.18h18.303V28.741H72.985V54.18zm-1.841 1.845H93.13V26.897H71.144v29.128z" fill="#334557"
              mask="url(#howitworks-2a)" />
            <path
              d="M96.747 26.897v29.128h9.768v-1.846h-7.927V28.742h18.303v19.426a4.76 4.76 0 011.842-.624V26.897H96.747z"
              fill="#F06A63" mask="url(#howitworks-2a)" />
            <path d="M47.382 54.18h18.303V28.741H47.382V54.18zm-1.841 1.845h21.986V26.897H45.541v29.128z" fill="#334557"
              mask="url(#howitworks-2a)" />
            <path
              d="M138.245 68.947c0 1.003-.097 1.992-.287 2.954a14.857 14.857 0 01-4.081 7.65.919.919 0 00-.26.654v10.383h-20.638v-7.064a.937.937 0 00-.262-.665c-1.66-1.663-4.101-4.198-6.109-6.984-1.617-2.245-3.545-5.5-3.545-8.253v-5.298c0-2.04 1.38-3.787 3.358-4.247l.093-.022v8.239a.931.931 0 00.275.654.893.893 0 00.643.266.896.896 0 00.644-.266.92.92 0 00.276-.65V35.83a3.018 3.018 0 013.048-3.054 3.019 3.019 0 013.047 3.054v16.56c0 .507.412.92.919.92a.92.92 0 00.918-.92 3.018 3.018 0 013.047-3.054 3.019 3.019 0 013.049 3.054v3.974c0 .507.412.92.918.92a.92.92 0 00.919-.92 3.019 3.019 0 013.047-3.054 3.019 3.019 0 013.048 3.054v3.974c0 .507.412.92.919.92a.92.92 0 00.918-.92 3.019 3.019 0 013.048-3.054 3.019 3.019 0 013.048 3.054v8.61zM20.698 72.926a.925.925 0 01.064-.088c.236-.365 2.29-3.836.101-10.889-1.613-5.2.134-13.99 2.693-19.986a12.11 12.11 0 011.448-2.447c.842-1.079 1.63-1.562 2.328-1.435l.133.023.093.101c.362.388 1.39 2.143-.61 8.39-.06.193-.186.352-.308.506-.038.05-.142.181-.169.236l1.714 5.06v26.691l-8.449-4.902.962-1.26zm98.633-25.43c-.202 0-.4.024-.598.048-.654.08-1.275.29-1.841.624-.166.097-.331.193-.485.31l-.123.094v-12.74c0-2.7-2.192-4.895-4.884-4.895a4.895 4.895 0 00-4.886 4.894v20.431l-.063.01c-3.028.495-5.225 3.04-5.225 6.052v5.298c0 .92.164 1.894.477 2.92h-62.57V11.37h85.535v40.872c-.11.07-.224.13-.328.21l-.123.093v-.155c0-2.7-2.192-4.894-4.886-4.894zm7.933 3.973c-.255 0-.507.022-.755.06V9.524H37.291v62.864h65.118c.64 1.413 1.53 2.922 2.708 4.554a48.356 48.356 0 002.97 3.685H30.845a.82.82 0 01-.819-.82V2.665a.82.82 0 01.82-.821h101.801c.452 0 .82.368.82.82v53.108a4.767 4.767 0 00-1.195.653l-.123.094v-.155c0-2.7-2.191-4.896-4.885-4.896zm8.044 3.98V2.665A2.667 2.667 0 00132.648 0H30.844a2.667 2.667 0 00-2.66 2.666V36.2a.737.737 0 00-.07-.024c-3.765-1.184-6.299 4.729-6.405 4.98-2.63 6.139-4.62 15.45-2.777 21.394 1.816 5.866.323 8.824.147 9.142l-1.116 1.46-.898-.526c-.424-.242-.936-.18-1.244.151L.66 88.954l-.062.027a1.002 1.002 0 00.4 1.917.998.998 0 00.828-.44l.248-.366.124.1 14.54-15.424.053.03 16.382 9.49-3.193 4.825a.996.996 0 00.612 1.785.999.999 0 00.815-.426l.235-.333.045.032 3.767-5.652a.963.963 0 00.157-.642l-.027-.315.298-.106c.657-.231 1.338-.586 2.044-.984h71.852c.484.508.936.97 1.34 1.378l.023.022v7.635c0 .49.43.92.918.92h22.476c.49 0 .918-.43.918-.92v-10.97l.02-.022a16.755 16.755 0 004.607-11.568v-8.61a4.893 4.893 0 00-4.773-4.889z"
              fill="#334557" mask="url(#howitworks-2a)" />
            <path
              d="M135.197 57.283a3.019 3.019 0 00-3.048 3.054.92.92 0 01-.918.92.92.92 0 01-.919-.92v-3.974a3.019 3.019 0 00-3.048-3.054 3.019 3.019 0 00-3.047 3.054.92.92 0 01-.92.92.92.92 0 01-.917-.92V52.39a3.019 3.019 0 00-3.049-3.054 3.019 3.019 0 00-3.048 3.054.92.92 0 01-1.836 0V35.831a3.018 3.018 0 00-3.048-3.054 3.018 3.018 0 00-3.047 3.054v30.467a.925.925 0 01-.92.916.926.926 0 01-.917-.92v-8.239l-.094.022a4.336 4.336 0 00-3.358 4.247v5.298c0 2.753 1.928 6.008 3.546 8.253 2.007 2.786 4.448 5.32 6.108 6.984a.94.94 0 01.262.665v7.063h20.638V80.205a.919.919 0 01.26-.655 14.86 14.86 0 004.082-7.65c.19-.961.286-1.95.286-2.953v-8.61a3.019 3.019 0 00-3.048-3.054"
              fill="#FEFEFE" mask="url(#howitworks-2a)" />
          </g>
        </g>
      </symbol>
      <symbol id="howitworks-3" viewBox="0 0 160 160">
        <g fill="none" fill-rule="evenodd">
          <ellipse fill="#FFF9F9" cx="79.967" cy="80.002" rx="79.953" ry="79.99" />
          <ellipse fill="#FFEEEC" cx="79.967" cy="80.002" rx="60.094" ry="60.122" />
          <ellipse fill="#FFE3E1" cx="79.967" cy="80.002" rx="41.267" ry="41.287" />
          <path d="M71.287 35h61.307v79.546H79.139s-.35-4.78-3.75-8.673c-3.398-3.894-4.389-5.349-4.389-5.349L71.287 35z"
            fill="#FFF" />
          <path fill="#334557"
            d="M70.777 95.862h61.68v-1.645h-61.68zm49.905 12.832a3.853 3.853 0 01-3.85-3.849 3.854 3.854 0 013.85-3.849 3.854 3.854 0 013.849 3.85 3.852 3.852 0 01-3.85 3.848m0-9.234a5.392 5.392 0 00-5.385 5.385 5.392 5.392 0 005.386 5.385 5.391 5.391 0 005.385-5.385 5.392 5.392 0 00-5.385-5.385m-16.283-20.013c.239 1.564-.884 2.042-.884 2.042.607-3.146-2.465-5.959-2.465-5.959.239 2.152-2.356 4.579-2.356 7.137a3.662 3.662 0 003.662 3.661c2.022 0 3.662-1.564 3.662-3.624 0-2.503-1.62-3.257-1.62-3.257" />
          <g transform="translate(69.558 33.274)">
            <mask id="howitworks-3a" fill="#fff">
              <path d="M.614.494v66.741l6.12 6.862 2.88 8.387h54.437V.494z" />
            </mask>
            <path fill="#F06A63" mask="url(#howitworks-3a)"
              d="M.614.494v66.908l1.921 1.073V2.414H62.13v78.15H9.05l.64 1.92h54.362V.494z" />
          </g>
          <path
            d="M68.214 97.453l-.04-.023a1.013 1.013 0 00-.926 0c-.187.093-.502.337-.502.924v7.608c0 .503.46.963.962.963.504 0 .963-.46.963-.963v-5.765c6.178 4.958 9.855 12.451 9.855 20.124 0 .503.46.961.964.961.503 0 .963-.458.963-.961.005-9.175-4.57-17.723-12.24-22.868m-6.96-1.34v8.767H44.106v-8.765c5.58-1.958 11.658-1.957 17.147-.002m-18.47-1.679c-.377.188-.602.534-.602.924v10.484c0 .503.458.963.962.963h19.172c.355 0 .552-.198.658-.304.15-.15.304-.436.304-.659V95.358c0-.36-.196-.79-.634-.94-6.255-2.419-13.486-2.417-19.86.016m-6.002 5.658v5.77c0 .505.46.964.963.964.503 0 .962-.46.962-.963v-7.724c.001-.243.003-.611-.394-.808-.268-.134-.765-.192-.96.023A27.473 27.473 0 0025 120.32c0 .504.46.963.963.963.504 0 .963-.46.963-.963 0-7.929 3.587-15.28 9.855-20.228m1.903-26.772c0-1.314.189-2.627.56-3.905.87.679 2.265 1.244 4.154 1.681 2.56.595 6.064.95 9.371.95 3.308 0 6.81-.355 9.372-.95 1.838-.424 3.21-.974 4.087-1.635.361 1.265.546 2.563.546 3.86 0 7.742-6.301 14.041-14.045 14.041s-14.045-6.3-14.045-14.042zm26.672-7.65a.931.931 0 01-.245.62l-.053.058a.917.917 0 01-.627.248H41.062a.916.916 0 01-.643-.263l-.053-.06a.968.968 0 01-.172-.293.898.898 0 01-.058-.31v-4.24a4.657 4.657 0 014.652-4.652h15.929a4.645 4.645 0 014.64 4.64v4.252zm-1.447 2.882c-2.776 1.12-8.004 1.537-11.14 1.537-5.357 0-9.119-.76-11.088-1.537h22.228zM52.729 89.32c8.823 0 16-7.178 16-15.999a15.79 15.79 0 00-1.544-6.846 2.78 2.78 0 00.128-.804v-4.253a6.604 6.604 0 00-6.596-6.596H44.788a6.616 6.616 0 00-6.608 6.61v4.24c0 .236.037.477.116.756a15.79 15.79 0 00-1.568 6.893c0 8.821 7.178 15.999 16 15.999z"
            fill="#334556" />
          <path
            d="M56.638 81.957a5.812 5.812 0 01-3.82 1.41A5.81 5.81 0 0149 81.957h7.638zm-3.82 3.175a7.591 7.591 0 006.472-3.589l.845-1.35H45.502l.845 1.35a7.594 7.594 0 006.472 3.589zm18.162 29.4h-2.335v-1.645c.934.31 1.761.893 2.336 1.645zm-4.23 8.593c-1.875 0-3.65-1.036-4.622-2.692a.753.753 0 00-.671-.41H35.535c-1.141 0-2.105-.962-2.105-2.105 0-1.083 1.023-2.107 2.105-2.107h25.922a.73.73 0 00.665-.396c.981-1.667 2.756-2.703 4.629-2.703h.209v7.796c0 .406.384.79.79.79h3.188c-1.023 1.15-2.56 1.827-4.187 1.827zm4.6-7.036a1.365 1.365 0 001.15-2.101c-.547-.851-2.2-2.832-5.75-2.832-2.282 0-4.426 1.157-5.736 3.1H35.535c-1.95 0-3.662 1.71-3.662 3.662a3.666 3.666 0 003.662 3.663h25.48c1.306 1.94 3.45 3.1 5.736 3.1 2.252 0 4.376-1.029 5.682-2.752.315-.416.365-.964.133-1.43a1.355 1.355 0 00-1.221-.759h-2.829v-3.651h2.834z"
            fill="#334556" />
          <path
            d="M65.072 116.258h-1.898c-.211 0-.332.121-.332.332 0 .207.124.332.332.332h1.898c.116 0 .333-.137.333-.332 0-.208-.125-.332-.333-.332m0 1.293h-1.898c-.208 0-.332.125-.332.333 0 .21.121.332.332.332h1.898c.116 0 .333-.137.333-.332 0-.208-.125-.333-.333-.333m0 1.302h-1.898c-.211 0-.332.121-.332.331 0 .21.121.332.332.332h1.898c.114 0 .333-.129.333-.332 0-.207-.125-.331-.333-.331m-30.568-.973c0 1.391 2.092 1.391 2.092 0 0-1.396-2.092-1.396-2.092 0"
            fill="#334556" />
          <path
            d="M81.978 106.797a1.785 1.785 0 110-3.57 1.785 1.785 0 010 3.57m5.762 0a1.785 1.785 0 110-3.57 1.785 1.785 0 010 3.57"
            fill="#334557" />
        </g>
      </symbol>
      <symbol id="info-grey" viewBox="0 0 18 18">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M0 9a9 9 0 1118 0A9 9 0 010 9zm10.973 4.042l.142-.582a2.434 2.434 0 01-.355.12c-.163.044-.31.067-.44.067-.275 0-.468-.045-.58-.136-.112-.09-.168-.26-.168-.51 0-.1.017-.247.052-.44.034-.194.073-.367.116-.518l.532-1.88a2.9 2.9 0 00.107-.57 5.4 5.4 0 00.029-.433c0-.397-.14-.719-.418-.967-.278-.248-.674-.372-1.187-.372-.286 0-.588.05-.907.152-.32.101-.654.223-1.003.365l-.143.583c.104-.04.228-.08.373-.123s.286-.065.425-.065c.281 0 .47.048.57.142.1.095.15.264.15.505 0 .133-.016.281-.05.442a8.87 8.87 0 01-.12.515l-.533 1.888a5.62 5.62 0 00-.103.533 3.386 3.386 0 00-.033.463c0 .388.144.708.43.96.287.252.69.378 1.206.378.337 0 .632-.044.886-.132.254-.088.595-.217 1.022-.385zm-.095-7.64a1.1 1.1 0 00.372-.838c0-.327-.123-.607-.372-.84a1.261 1.261 0 00-.894-.349c-.35 0-.65.116-.9.349-.25.233-.375.513-.375.84 0 .328.125.608.375.838.25.23.55.346.9.346.348 0 .647-.115.894-.346z"
          fill="#A9A9A8" />
      </symbol>
      <symbol id="info" viewBox="214 472 12 26">
        <path
          d="M225.145 495.194l-.364 1.485a70.65 70.65 0 01-2.608.984c-.649.225-1.403.337-2.262.337-1.32 0-2.346-.322-3.078-.965-.732-.644-1.098-1.461-1.098-2.452 0-.384.026-.778.082-1.18.055-.402.143-.856.264-1.363l1.363-4.82c.12-.461.224-.9.306-1.313a5.67 5.67 0 00.124-1.13c0-.615-.127-1.045-.38-1.287-.255-.243-.739-.365-1.458-.365a3.82 3.82 0 00-1.083.166c-.37.11-.687.215-.953.314l.365-1.487a34.154 34.154 0 012.56-.932c.816-.259 1.587-.389 2.316-.389 1.31 0 2.322.317 3.032.95.71.634 1.065 1.456 1.065 2.469 0 .209-.024.578-.073 1.105a7.41 7.41 0 01-.273 1.454l-1.357 4.802c-.11.385-.21.826-.298 1.323-.09.493-.132.869-.132 1.121 0 .638.141 1.073.427 1.305.287.231.781.347 1.483.347.33 0 .706-.059 1.122-.174.417-.115.72-.217.908-.305zm.345-20.158c0 .837-.316 1.55-.95 2.138-.631.59-1.393.885-2.284.885-.893 0-1.657-.295-2.296-.885-.638-.587-.958-1.301-.958-2.138 0-.835.32-1.55.958-2.146.638-.594 1.403-.89 2.296-.89.89 0 1.653.297 2.285.89.633.595.949 1.31.949 2.146z"
          fill="currentColor" fill-rule="evenodd" />
      </symbol>
      <symbol id="installation-included" viewBox="0 0 35 34">
        <path
          d="M26.03 29.917c0-5.676-4.085-10.433-9.582-11.703M1 29.918c0-5.676 4.085-10.433 9.582-11.703m-4.106 1.922v5.475m14.079-5.475v3.91m1.426 4.617h1.553c.532 0 .518-.285.444-.427-.639-.782-1.327-1.158-1.997-1.277m0 0a2.8 2.8 0 00-.222-.03c-1.294-.122-2.218.24-3.106 1.307H4.286c-.444 0-1.33.427-1.33 1.493 0 1.067.886 1.494 1.33 1.494h14.367c2.219 2.986 5.325.427 5.325 0 0-.341-.296-.427-.444-.427h-1.553V26.96zm-2.217 2.77h.887m-.887-.853h.887m-.887 1.707h.887"
          stroke="#fff" stroke-linecap="round" stroke-linejoin="round" />
        <mask id="installation-includeda" fill="#fff">
          <ellipse cx="4.507" cy="29.73" rx=".666" ry=".64" />
        </mask>
        <path
          d="M4.173 29.73c0-.235.187-.36.334-.36v2c.883 0 1.666-.698 1.666-1.64h-2zm.334-.36c.148 0 .335.125.335.36h-2c0 .942.783 1.64 1.665 1.64v-2zm.335.36c0 .235-.187.36-.335.36v-2c-.882 0-1.665.698-1.665 1.64h2zm-.335.36a.348.348 0 01-.334-.36h2c0-.942-.783-1.64-1.666-1.64v2z"
          fill="#fff" mask="url(#installation-includeda)" />
        <path d="M13.518 14.7c1.735 0 2.29-.75 2.35-1.125h-4.52c0 .375.434 1.124 2.17 1.124z" stroke="#fff"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M19.6 7.622c.38 1.837.38 6.061-2.65 8.265-3.029 2.204-6.058.656-7.194-.393-1.136-.919-3.18-3.779-2.272-7.872"
          stroke="#fff" />
        <mask id="installation-includedb" fill="#fff">
          <path
            d="M13.521.582c1.72 0 3.825.253 5.04 1.505 1.215 1.25 1.44 3.394 1.44 5.162a.471.471 0 01-.464.479s-3.316-.479-6.016-.479-6.014.479-6.014.479a.472.472 0 01-.465-.479c0-1.768.044-3.912 1.26-5.162C9.517.835 11.804.582 13.522.582z" />
        </mask>
        <path
          d="M18.561 2.087l-.717.696.717-.696zm.976 5.64l-.143.99.071.01h.072v-1zm-12.03 0v1h.072l.071-.01-.143-.99zm.795-5.64l.717.697-.717-.697zm5.22-.505c.818 0 1.685.061 2.47.254.788.194 1.421.504 1.852.947l1.434-1.393C18.495.582 17.468.14 16.47-.106c-1.002-.246-2.047-.312-2.948-.312v2zm4.322 1.202c.432.444.732 1.097.918 1.912.184.81.239 1.703.239 2.553h2c0-.919-.058-1.98-.29-2.998-.23-1.011-.65-2.056-1.433-2.861l-1.434 1.394zm1.157 4.465c0-.258.211-.521.536-.521v2c.838 0 1.464-.692 1.464-1.479h-2zM7.507 6.728c.322 0 .535.26.535.521h-2c0 .79.63 1.479 1.465 1.479v-2zm.535.521c0-.889.013-1.785.158-2.6.146-.816.407-1.442.819-1.865L7.585 1.389c-.804.827-1.173 1.898-1.353 2.909-.181 1.011-.19 2.072-.19 2.951h2zm.977-4.466c.42-.431 1.084-.746 1.932-.944.835-.196 1.752-.257 2.57-.257v-2c-.9 0-1.985.066-3.026.31C9.466.132 8.38.57 7.585 1.39l1.434 1.393zm10.518 4.945l.143-.99h-.004l-.008-.002-.03-.004a25.854 25.854 0 00-.535-.071c-.36-.046-.867-.106-1.46-.167-1.179-.121-2.722-.245-4.122-.245v2c1.3 0 2.765.116 3.918.234a59.379 59.379 0 011.92.23l.027.003a.87.87 0 00.006.001h.002l.143-.99zM13.52 6.249c-1.4 0-2.942.124-4.121.245a61.319 61.319 0 00-1.994.238l-.03.004-.008.002h-.003a628.788 628.788 0 00.285 1.98h.002a2.272 2.272 0 01.032-.005 26.67 26.67 0 01.51-.068c.346-.044.836-.103 1.41-.162 1.153-.118 2.618-.234 3.917-.234v-2z"
          fill="#fff" mask="url(#installation-includedb)" />
        <path
          d="M18.202 2.435h0c.52.535.85 1.285 1.047 2.15.194.85.25 1.775.252 2.633l-.044-.006a60.827 60.827 0 00-1.864-.22c-1.173-.121-2.697-.243-4.072-.243s-2.898.122-4.07.242a60.776 60.776 0 00-1.864.22l-.045.007c0-.877.014-1.803.166-2.657.154-.864.442-1.602.952-2.126h0c.514-.529 1.284-.874 2.177-1.083.887-.208 1.846-.27 2.684-.27.84 0 1.751.062 2.59.269.84.207 1.572.55 2.091 1.084z"
          stroke="#fff" />
        <path
          d="M7.201 7.63c1.108-.94 6.377-5.54 12.498-.141m-8.922 10.769l2.816 1.128 2.66-1.128m10-3.988l1.676 1.676 2.933-2.933"
          stroke="#fff" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="28.767" cy="14.27" r="5.366" stroke="#fff" />
      </symbol>
      <symbol id="installation" viewBox="0 0 48 49">
        <path d="M41 44c0-8.244-6.528-15.156-15.313-17M1 44c0-8.244 6.528-15.156 15.313-17M10 30v8m21-9v7"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M34.215 41.741h2.458c.843 0 .82-.444.702-.666-1.01-1.22-2.1-1.808-3.16-1.993m0 0a4.496 4.496 0 00-.352-.047c-2.05-.19-3.512.375-4.917 2.04H6.201c-.702 0-2.107.666-2.107 2.33 0 1.665 1.405 2.331 2.107 2.331h22.745c3.512 4.662 8.43.667 8.43 0 0-.532-.469-.665-.703-.665h-2.459v-5.989z"
          stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M20.819 22.034c2.747 0 3.625-1.169 3.72-1.754h-7.154c0 .585.687 1.754 3.434 1.754z" stroke="#2D3D4D"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M31.152 14.38c0 6.072-4.65 10.947-10.328 10.947-5.678 0-10.328-4.875-10.328-10.946 0-6.072 4.65-10.946 10.328-10.946 5.678 0 10.328 4.874 10.328 10.946z"
          stroke="#2D3D4D" />
        <path
          d="M28.445 2.699h0c.873.886 1.412 2.112 1.73 3.487.317 1.373.405 2.857.405 4.22a.242.242 0 01-.21.245l-.016-.002-.176-.024c-.153-.02-.376-.05-.657-.085a97.811 97.811 0 00-2.284-.258c-1.853-.188-4.253-.376-6.415-.376-2.161 0-4.561.188-6.413.376a97.738 97.738 0 00-2.941.343l-.176.024-.016.002a.243.243 0 01-.211-.245c0-1.383.018-2.868.267-4.244.249-1.375.72-2.588 1.583-3.463h0c.868-.88 2.149-1.437 3.59-1.77C17.939.598 19.483.5 20.822.5c1.34 0 2.81.098 4.17.428 1.363.33 2.58.885 3.453 1.77z"
          fill="#fff" stroke="#2D3D4D" />
        <path d="M10.816 10.998c1.755-1.465 10.095-8.646 19.787-.22M16 27l5.143 2L26 27m11.285-6.5l2.143 2.143 3.75-3.75"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M47.5 20.5a7 7 0 11-14 0 7 7 0 0114 0z" stroke="#2D3D4D" />
      </symbol>
      <symbol id="installer-v2" viewBox="0 0 60 60">
        <path d="M41 45.504c0-7.256-5.223-13.339-12.25-14.961M9 45.502c0-7.255 5.223-13.338 12.25-14.96" stroke="#2D3D4D"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M16.5 32.522v9.616m17-9.616v7.693" stroke="#2D3D4D" />
        <path
          d="M35.825 43.902h1.985c.68 0 .662-.364.567-.545-.816-1-1.696-1.481-2.552-1.633m0 0a3.579 3.579 0 00-.284-.038c-1.655-.156-2.836.307-3.97 1.67H13.2c-.567 0-1.701.546-1.701 1.91 0 1.364 1.134 1.909 1.702 1.909H31.57c2.837 3.818 6.807.546 6.807 0 0-.436-.378-.545-.567-.545h-1.985v-4.906zm-2.837 3.542h1.135m-1.135-1.092h1.135m-1.135 2.182h1.135"
          stroke="#FB6058" stroke-linecap="round" stroke-linejoin="round" />
        <mask id="installer-v2a" fill="#fff">
          <ellipse cx="13.484" cy="45.266" rx=".851" ry=".818" />
        </mask>
        <path
          d="M13.335 45.266a.2.2 0 01.057-.143.135.135 0 01.092-.04v2c.985 0 1.85-.777 1.85-1.817h-2zm.149-.182c.028 0 .062.012.091.04a.2.2 0 01.058.142h-2c0 1.04.866 1.818 1.85 1.818v-2zm.149.181a.2.2 0 01-.058.143.135.135 0 01-.091.04v-2c-.985 0-1.851.777-1.851 1.818h2zm-.15.182a.135.135 0 01-.09-.04.2.2 0 01-.058-.141h2c0-1.041-.866-1.819-1.851-1.819v2z"
          fill="#FB6058" mask="url(#installer-v2a)" />
        <path d="M25.004 26.049c2.218 0 2.927-.959 3.004-1.437H22.23c0 .479.555 1.437 2.774 1.437z" stroke="#2D3D4D"
          stroke-linecap="round" stroke-linejoin="round" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M16.748 16.696a9.976 9.976 0 00-.484 3.084c0 5.178 3.915 9.376 8.744 9.376 4.83 0 8.744-4.198 8.744-9.376 0-1.08-.17-2.118-.484-3.084a.596.596 0 01-.57.44s-.126-.019-.353-.049a9 9 0 01.407 2.693c0 4.693-3.532 8.376-7.744 8.376-4.213 0-7.744-3.683-7.744-8.376 0-.946.143-1.85.407-2.693-.226.03-.352.049-.352.049a.598.598 0 01-.571-.44z"
          fill="#2D3D4D" />
        <path
          d="M31.093 10.272h0c.69.709 1.12 1.695 1.374 2.814.254 1.116.326 2.324.326 3.438 0 .065-.04.1-.076.109a56.548 56.548 0 00-.671-.09 77.543 77.543 0 00-1.847-.21c-1.498-.154-3.44-.31-5.19-.31-1.751 0-3.693.155-5.19.31a77.514 77.514 0 00-2.518.3c-.036-.01-.076-.045-.076-.11 0-1.132.015-2.342.214-3.46.2-1.12.576-2.093 1.255-2.791h0c.683-.703 1.697-1.153 2.85-1.423 1.149-.27 2.387-.349 3.465-.349 1.078 0 2.256.08 3.343.347 1.09.268 2.053.716 2.741 1.425z"
          stroke="#2D3D4D" />
        <path
          d="M16.928 17.01c1.416-1.201 8.152-7.083 15.979-.18M21.5 30.598l3.6 1.443 3.4-1.443M41.285 25.5l2.143 2.143 3.75-3.75"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M51.5 25.5a7 7 0 11-14 0 7 7 0 0114 0z" stroke="#2D3D4D" />
      </symbol>
      <symbol id="installer" viewBox="0 0 67 113">
        <g fill="none" fill-rule="evenodd">
          <path
            d="M45.355 49.496c.361.12.481.486.481.725v12.733c0 .12-.12.367-.24.487s-.247.24-.49.24H21.705c-.364 0-.73-.36-.73-.727V50.221c0-.358.246-.606.487-.725 7.677-2.912 16.338-2.912 23.894 0zM22.433 62.228h21.824v-11.4c-6.954-2.545-14.75-2.545-21.824 0v11.4z"
            fill="#334556" />
          <g transform="translate(50.715 52.927)">
            <mask id="installerb" fill="#fff">
              <use xlinkHref="#installera" />
            </mask>
            <path
              d="M1.346.207a.79.79 0 00-.731 0C.375.327.247.574.247.933v25.1c0 .365.368.725.73.725.369 0 .73-.367.73-.726V2.265c8.044 6.064 12.92 15.4 12.92 25.346 0 .367.37.726.732.726.368 0 .73-.366.73-.726C16.096 16.697 10.61 6.39 1.346.207"
              fill="#334556" mask="url(#installerb)" />
          </g>
          <path
            d="M15.608 53.014c-.242-.12-.61-.12-.73 0C5.486 59.197 0 69.504 0 80.538c0 .366.369.725.73.725.368 0 .732-.365.732-.725 0-10.067 4.754-19.402 12.92-25.466v23.766c0 .367.368.727.73.727.369 0 .73-.367.73-.727V53.62c.127-.247.006-.486-.234-.606m35.97-37.974a3.353 3.353 0 01-1.919 1.805c-1.59 1.259-7.18 2.92-16.202 2.92-8.92 0-14.485-1.625-16.145-2.878a3.353 3.353 0 01-2.01-1.794 2.12 2.12 0 00-.3 1.074c0 3.94 11.037 5.37 18.455 5.37 7.419 0 18.456-1.43 18.456-5.37 0-.401-.12-.776-.335-1.127"
            fill="#334556" />
          <path
            d="M50.07 12.076v1.647c0 .404-.159.77-.413 1.046 1.43 2.61 2.243 5.6 2.243 8.775 0 10.144-8.297 18.396-18.495 18.396S14.91 33.688 14.91 23.544c0-3.188.82-6.189 2.26-8.806a1.539 1.539 0 01-.386-1.015v-1.71a19.978 19.978 0 00-3.655 11.53c0 11.121 9.096 20.169 20.276 20.169s20.276-9.048 20.276-20.168c0-4.258-1.337-8.21-3.61-11.468"
            fill="#334556" />
          <g transform="translate(14.972 .04)">
            <mask id="installerd" fill="#fff">
              <use xlinkHref="#installerc" />
            </mask>
            <path
              d="M8.187 1.898H28.74c3.507 0 6.36 2.838 6.36 6.325v5.46c0 .86-.704 1.558-1.567 1.558H3.378a1.564 1.564 0 01-1.566-1.559V8.24c0-3.497 2.86-6.341 6.375-6.341M2.34 16.847c.327.106.676.166 1.038.166h30.155c.406 0 .794-.076 1.155-.209A3.357 3.357 0 0036.607 15c.175-.405.274-.85.274-1.318v-5.46c0-4.464-3.652-8.096-8.14-8.096H8.186C3.69.126.03 3.765.03 8.239v5.443c0 .49.108.952.3 1.37a3.355 3.355 0 002.009 1.795"
              fill="#334556" mask="url(#installerd)" />
          </g>
          <mask id="installerf" fill="#fff">
            <use xlinkHref="#installere" />
          </mask>
          <path fill="#FEFEFE" mask="url(#installerf)" d="M22.273 96.33h22.18V68.513h-22.18z" />
          <path
            d="M25.11 91.112v1.194h.98c.352 0 .547-.3.547-.594 0-.28-.195-.6-.566-.6h-.961zm.836-.668c.289 0 .465-.232.465-.52 0-.305-.176-.5-.49-.5h-.811v1.02h.836zm.729.275c.54.13.804.631.804 1.006 0 .7-.502 1.294-1.345 1.294h-1.828v-4.307h1.696c.76 0 1.245.45 1.245 1.17 0 .312-.139.674-.572.837zm4.266 1.575a1.4 1.4 0 001.396-1.42c0-.787-.616-1.424-1.396-1.424-.785 0-1.394.637-1.394 1.425 0 .787.61 1.419 1.394 1.419m0-3.632c1.232 0 2.2.988 2.2 2.213a2.196 2.196 0 01-2.2 2.206 2.19 2.19 0 01-2.193-2.206c0-1.225.962-2.213 2.193-2.213m5.512 2.057l1.508 2.3h-.955l-1.03-1.657-1.032 1.657h-.948l1.501-2.3-1.357-2.007h.961l.874 1.369.874-1.369h.96zm2.727-2.007h3.25v.732h-1.22v3.575h-.81v-3.575h-1.22z"
            fill="#F16760" mask="url(#installerf)" />
          <path d="M20.938 96.33h24.85V67.185h-24.85V96.33zM22.273 95h22.18V68.514h-22.18v26.487z" fill="#F16760"
            mask="url(#installerf)" />
          <path
            d="M27.966 34.385a7.795 7.795 0 005.555 2.304 7.788 7.788 0 005.555-2.304h-11.11zm5.555 4.075a9.565 9.565 0 01-8.142-4.49l-.852-1.357h17.988l-.853 1.356a9.563 9.563 0 01-8.14 4.49z"
            fill="#334556" mask="url(#installerf)" />
          <path
            d="M59.107 108.902c-1.318 1.732-3.476 2.765-5.771 2.765-2.445 0-4.756-1.34-6.03-3.497a.482.482 0 00-.433-.268H10.54c-1.631 0-3.01-1.371-3.01-2.993 0-1.568 1.434-2.996 3.01-2.996h36.332a.466.466 0 00.425-.25c1.285-2.173 3.595-3.514 6.038-3.514h.693v9.904c0 .25.275.523.526.523h4.8l-.248.326zm-3.899-10.685l.26.071c1.535.422 2.833 1.356 3.655 2.631l.201.314h-4.116v-3.016zm5.018 9.928a1.424 1.424 0 00-1.282-.79h-3.688v-4.902h3.694a1.43 1.43 0 001.256-.74 1.416 1.416 0 00-.05-1.452c-.646-1.002-2.604-3.333-6.82-3.333-2.714 0-5.262 1.375-6.818 3.675l-.06.09H10.54c-2.257 0-4.237 1.97-4.237 4.216 0 2.323 1.9 4.213 4.237 4.213h35.916l.06.09c1.552 2.302 4.1 3.676 6.819 3.676 2.676 0 5.2-1.215 6.749-3.25.33-.433.384-1.005.14-1.493z"
            fill="#334557" mask="url(#installerf)" />
          <path
            d="M51.547 103.499c.143 0 .406-.166.406-.403 0-.252-.151-.403-.406-.403H49.23c-.257 0-.405.147-.405.403 0 .252.151.403.405.403h2.317zm0 1.571c.143 0 .406-.165.406-.402 0-.253-.151-.404-.406-.404H49.23c-.254 0-.405.15-.405.404 0 .255.148.403.405.403h2.317zm0 1.58c.14 0 .406-.156.406-.403 0-.252-.151-.402-.406-.402H49.23c-.257 0-.405.146-.405.402s.148.403.405.403h2.317zm-39.617-1.754c0 1.69-2.554 1.69-2.554 0 0-1.696 2.555-1.696 2.555 0"
            fill="#334556" mask="url(#installerf)" />
        </g>
      </symbol>
      <symbol id="landlord-protect-plus" viewBox="0 0 150 150">
        <path d="M104.368 69.46L98.01 47H60.368v-4h-5v4h-3.215l-6.785 22.46H48.1V91h53.536V69.46h2.732z"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M62 69h26M75 56v26" stroke="currentColor" stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M75 20v21m0 56v30" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M74.757 128.808c-41.536-13.307-48.322-66.588-46.49-92.497.056-.806.603-1.471 1.373-1.717l45.12-14.4c.396-.126.82-.126 1.216 0l45.131 14.403c.765.245 1.309.902 1.372 1.702 1.995 25.583-4.576 78.428-46.502 92.5-.39.131-.828.134-1.22.009z"
          stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="landlord-service-plan" viewBox="0 0 150 150">
        <path d="M35.408 94.155h79.099M86 119v12m-22-12v12m11-12v12" stroke="currentColor" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M114.958 31.5v-8.131A3.368 3.368 0 00111.59 20H38.327a3.369 3.369 0 00-3.368 3.369v92.259a3.369 3.369 0 003.368 3.369h73.263a3.368 3.368 0 003.368-3.369V74"
          stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M111.439 65.63v-8.533c.001-1.066 0-2.133-1.066-3.2-.843-.843-7.468-5.866-.001-12.8.534-.533 1.067-.22 1.067 0v4.8c0 .89 0 2.668 2.134 2.668h2.133c1.067 0 2.133-.96 2.133-2.668v-4.8c0-.533.534-.532 1.067 0 2.137 2.134 5.867 8.534 0 12.8-1.067 1.067-1.067 2.134-1.067 3.2v8.534"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="115" cy="53" r="20" stroke="currentColor" stroke-width="2" />
        <circle cx="101.478" cy="106.551" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" r="5.333" />
        <circle cx="45.882" cy="106.042" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" r="2" />
        <circle cx="55.77" cy="106.042" r="2" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M77.155 74.832c.315 2.064-1.166 2.696-1.166 2.696.801-4.153-3.253-7.865-3.253-7.865.316 2.84-3.109 6.043-3.109 9.419a4.832 4.832 0 004.833 4.832c2.668 0 4.83-2.065 4.83-4.784 0-3.302-2.135-4.298-2.135-4.298"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="laptop" viewBox="0 0 234 175">
        <path d="M146.37 32.181v70.03H87.634v-70.03h58.736z" stroke="#FB6058" stroke-width="1.5" />
        <path
          d="M100.19 90.454c1.392.338 2.071 1.623 2.071 2.586 0 1.8-1.294 3.325-3.462 3.325H94.09V85.298h4.37c1.957 0 3.203 1.156 3.203 3.003 0 .804-.356 1.736-1.473 2.153zm-4.029 1.012v3.068h2.525c.906 0 1.407-.77 1.407-1.526 0-.723-.501-1.542-1.456-1.542h-2.476zm2.152-1.719c.745 0 1.198-.594 1.198-1.333 0-.787-.453-1.285-1.262-1.285H96.16v2.618h2.152zm16.457 1.108c0-2.024-1.586-3.662-3.592-3.662-2.023 0-3.592 1.638-3.592 3.662 0 2.025 1.569 3.647 3.592 3.647 2.006 0 3.592-1.622 3.592-3.647zm2.071 0c0 3.116-2.492 5.67-5.663 5.67s-5.647-2.554-5.647-5.67c0-3.148 2.476-5.686 5.647-5.686s5.663 2.538 5.663 5.686zm8.529-.401l3.884 5.911h-2.46l-2.653-4.257-2.655 4.257h-2.442l3.867-5.911-3.496-5.156h2.476l2.25 3.517 2.248-3.517h2.476l-3.495 5.156zm7.02-5.156h8.366v1.88h-3.139v9.187h-2.087v-9.188h-3.14v-1.879z"
          clip-rule="evenodd" fill="#FB6058" fill-rule="evenodd" />
        <path
          d="M206.64 136.33V1H27.37v135.33m-26.364 0h232c0 19.334-10.545 19.334-10.545 19.334H11.551s-10.546 0-10.546-19.334z"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M39.667 121.97V12.6h154.67v109.37H39.667zm65.733 25.96h25.045" stroke="#2D3D4D" stroke-linecap="round"
          stroke-linejoin="round" stroke-width=".72" />
      </symbol>
      <symbol id="loader" class="lds-rolling" preserveAspectRatio="xMidYMid" viewBox="0 0 100 100">
        <circle cx="50" cy="50" fill="none" stroke="currentColor" stroke-width="10" r="35"
          stroke-dasharray="164.93361431346415 56.97787143782138">
          <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50"
            keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite" />
        </circle>
      </symbol>
      <symbol id="loading" viewBox="0 0 39 39">
        <circle cx="19.5" cy="19.5" r="17.9" opacity=".5" fill="none" stroke="#fb6058" stroke-width="3.25" />
        <circle cx="35.4" cy="11.4" r="1.6" fill="#fb6058" />
      </symbol>
      <symbol id="loft" viewBox="0 0 100 100">
        <path
          d="M33.681 45.139v5.555m0 32.639V77.43m18.055-32.291v5.555m0 32.639V77.43m0-26.736H33.681m18.055 0v8.681m-18.055-8.681v8.681m0 0h18.055m-18.055 0v9.028m18.055-9.028v9.028m0 0H33.681m18.055 0v9.027m-18.055-9.027v9.027m0 0h18.055m14.583-26.736h17.014l-33.68-34.028-13.542 13.542V25H25.694v15.625L15.625 50.694h8.333"
          fill="none" stroke="#fb6058" />
      </symbol>
      <symbol id="logo-baxi" viewBox="0 0 130.91 40">
        <g data-name="Layer 2">
          <g data-name="Layer 1" fill="#005092">
            <path
              d="M0 0h21.8C30 0 34.2 3.25 34.2 10.4c0 7.49-4.85 8.3-6.4 8.79 1.36.49 7.4 2.26 7.4 10C35.2 36.35 29.31 40 23.87 40H3.67V7.17C3.67 1 .82.32 0 0zm14 15.57h6.56c1.57 0 3.42-1.07 3.42-3.89s-1.27-3.89-3.89-3.89H14zm0 16.65h6.88c2.58 0 3.82-1.82 3.82-4.29 0-2-.8-4.58-4.61-4.58H14v8.87M67.11 40l-3.35-8.86H48.31s-1 2.92-1.22 3.7c-.92 2.95 1.63 5.06 1.77 5.16H35.21L46.39 5.78C47.89 1.18 45.54.2 45 0h17.11l15.26 40zM50.86 23.36h9.9l-5.34-14-4.56 14" />
            <path
              d="M115 0H97c2.78.92 2.77 3.81 1.18 5.65l-5.57 6.51L85.58 0H71.93l14 20-17.14 20h17.63c-.4-.38-3.2-2.81-.56-5.91L93 25.7l8.31 14.3H115L99.59 18 115 0m1.12 0h14.8v40h-11.36V7.17c0-6.18-2.62-6.85-3.44-7.17" />
          </g>
        </g>
      </symbol>
      <symbol id="logo-gassafe-black" viewBox="0 0 284 308">
        <g fill="none" fill-rule="evenodd">
          <mask id="logo-gassafe-blackb" fill="#fff">
            <use xlinkHref="#logo-gassafe-blacka" />
          </mask>
          <path fill="#1A1919"
            d="M284 297c0 5.929-5.076 11-11 11H11c-5.924 0-11-5.071-11-11V12C0 6.07 5.076 1 11 1h262c5.924 0 11 5.07 11 11v285"
            mask="url(#logo-gassafe-blackb)" />
          <path fill="#FFF101"
            d="M30.009 240.934c-1.196 1.33-2.009 3.418-2.009 6.026 0 4.846 4.195 9.04 9.04 9.04h209.92c4.846 0 9.04-4.194 9.04-9.04V37.04c0-4.845-4.194-9.04-9.04-9.04-2.581 0-4.646.794-6.026 2.009L30.009 240.934" />
          <path fill="#1A1919"
            d="M133.95 185.718a6.265 6.265 0 01-2.456.513c-1.304 0-2.475-.227-3.516-.679a7.532 7.532 0 01-2.635-1.87 8.279 8.279 0 01-1.646-2.8c-.382-1.072-.572-2.227-.572-3.465 0-1.272.191-2.451.573-3.539a8.509 8.509 0 011.648-2.847 7.54 7.54 0 012.637-1.906c1.042-.46 2.214-.691 3.518-.691.874 0 1.721.131 2.539.393a7.19 7.19 0 012.218 1.154 6.334 6.334 0 011.633 1.881c.429.746.691 1.604.787 2.574h-3.576c-.223-.953-.653-1.669-1.289-2.146-.635-.477-1.406-.715-2.312-.715-.843 0-1.558.163-2.147.488a4.067 4.067 0 00-1.43 1.311 5.743 5.743 0 00-.799 1.87 9.3 9.3 0 00-.25 2.168c0 .714.083 1.41.25 2.085.167.674.433 1.282.798 1.822.365.54.842.973 1.43 1.299.587.326 1.302.488 2.144.488 1.238 0 2.195-.313 2.87-.938.675-.625 1.069-1.531 1.18-2.718h-3.768v-2.797h7.159v9.195h-2.388l-.382-1.931c-.668.859-1.407 1.459-2.218 1.801m16.242-6.44l-2.146-6.255h-.048l-2.218 6.255h4.412zm-.214-10.453l6.366 17.031h-3.887l-1.288-3.781h-6.366l-1.335 3.781h-3.767l6.438-17.031h3.839zm10.239 12.839c.207.397.48.718.822.965.342.246.743.429 1.204.548.46.119.937.179 1.43.179.333 0 .691-.027 1.072-.083a3.744 3.744 0 001.072-.322c.334-.159.612-.377.834-.656.223-.278.334-.631.334-1.06 0-.461-.146-.834-.441-1.121-.293-.285-.679-.524-1.154-.715a11.68 11.68 0 00-1.621-.5 45.676 45.676 0 01-1.834-.478 16.044 16.044 0 01-1.859-.583 6.404 6.404 0 01-1.62-.894 4.192 4.192 0 01-1.155-1.371c-.294-.548-.441-1.211-.441-1.99 0-.874.187-1.633.56-2.277a5.058 5.058 0 011.465-1.608 6.34 6.34 0 012.049-.953 8.682 8.682 0 012.286-.311c.889 0 1.743.1 2.56.298.819.198 1.545.52 2.18.965a4.868 4.868 0 011.513 1.704c.373.69.559 1.529.559 2.515h-3.625c-.031-.509-.138-.93-.321-1.264a2.099 2.099 0 00-.726-.787 3.072 3.072 0 00-1.034-.406 6.38 6.38 0 00-1.272-.119c-.301 0-.603.033-.904.096a2.429 2.429 0 00-.821.334 2.047 2.047 0 00-.605.596c-.16.238-.239.541-.239.906 0 .334.064.604.191.811.127.207.377.398.751.573.373.175.888.349 1.548.524.659.175 1.52.398 2.585.668.317.064.757.179 1.322.346a6.327 6.327 0 011.68.799 5.09 5.09 0 011.441 1.466c.404.613.607 1.396.607 2.35 0 .779-.151 1.502-.453 2.17a4.733 4.733 0 01-1.346 1.73c-.597.484-1.335.862-2.218 1.132-.882.27-1.903.406-3.063.406a10.73 10.73 0 01-2.73-.347 6.85 6.85 0 01-2.336-1.087 5.363 5.363 0 01-1.61-1.886c-.397-.762-.587-1.669-.572-2.718h3.625c0 .572.104 1.057.31 1.455m-68.72 44.156a7.282 7.282 0 002.168 2.367c.884.613 1.902 1.066 3.052 1.358 1.151.292 2.345.438 3.584.438.885 0 1.814-.103 2.787-.309a9.489 9.489 0 002.655-.969 6.038 6.038 0 001.991-1.765c.531-.734.797-1.661.797-2.778 0-1.881-1.251-3.291-3.752-4.233-2.501-.94-5.988-1.881-10.459-2.822a62.316 62.316 0 01-5.341-1.456c-1.736-.559-3.28-1.293-4.634-2.204a10.727 10.727 0 01-3.266-3.44c-.825-1.382-1.236-3.072-1.236-5.071 0-2.939.572-5.351 1.719-7.232a12.68 12.68 0 014.541-4.454c1.881-1.087 3.996-1.851 6.348-2.292a39.03 39.03 0 017.229-.663c2.468 0 4.862.236 7.185.706 2.321.471 4.392 1.266 6.215 2.383 1.822 1.118 3.336 2.604 4.541 4.457 1.205 1.854 1.924 4.193 2.16 7.017h-11.875c-.178-2.416-1.091-4.051-2.741-4.905-1.651-.855-3.596-1.283-5.836-1.283-.707 0-1.474.045-2.299.133a7.711 7.711 0 00-2.255.573 4.66 4.66 0 00-1.725 1.281c-.471.558-.707 1.308-.707 2.25 0 1.118.411 2.03 1.236 2.736.824.706 1.897 1.279 3.221 1.721 1.324.441 2.839.838 4.546 1.191 1.707.354 3.443.736 5.208 1.147 1.824.412 3.604.913 5.34 1.501 1.736.588 3.281 1.367 4.635 2.338a11.303 11.303 0 013.265 3.619c.825 1.441 1.237 3.221 1.237 5.339 0 3.001-.604 5.516-1.809 7.545-1.205 2.03-2.778 3.663-4.719 4.898-1.94 1.236-4.16 2.103-6.659 2.603-2.5.5-5.043.75-7.63.75a37.86 37.86 0 01-7.761-.792c-2.53-.528-4.779-1.409-6.748-2.641-1.971-1.233-3.588-2.862-4.852-4.887-1.264-2.025-1.955-4.564-2.072-7.617h11.875c0 1.344.28 2.498.841 3.462m61.505-9.903c-.797.265-1.652.486-2.566.662-.915.176-1.873.324-2.877.441a53.689 53.689 0 00-3.008.44c-.944.177-1.873.413-2.787.707-.914.293-1.711.69-2.389 1.19a5.662 5.662 0 00-1.638 1.895c-.414.765-.619 1.735-.619 2.911 0 1.117.205 2.058.619 2.822a4.726 4.726 0 001.681 1.808c.707.442 1.533.75 2.478.926.944.176 1.917.264 2.92.264 2.477 0 4.395-.411 5.752-1.234 1.356-.823 2.36-1.808 3.008-2.955.649-1.146 1.047-2.307 1.195-3.483.147-1.176.221-2.116.221-2.822v-4.674c-.531.471-1.195.839-1.99 1.102zm-24.743-17.688a15.02 15.02 0 014.937-4.672c1.998-1.175 4.246-2.012 6.745-2.512 2.498-.5 5.01-.75 7.538-.75 2.292 0 4.613.163 6.964.486 2.352.324 4.498.956 6.438 1.897 1.939.942 3.525 2.25 4.76 3.926 1.234 1.677 1.851 3.897 1.851 6.661V227c0 2.058.117 4.029.354 5.911.234 1.882.646 3.294 1.234 4.235l-12.705-.038a17.419 17.419 0 01-.574-2.148c-.147-.73-.25-1.476-.309-2.236-2 2.064-4.351 3.508-7.056 4.333a28.287 28.287 0 01-8.293 1.238c-2.175 0-4.205-.265-6.087-.794-1.881-.529-3.528-1.353-4.939-2.471-1.412-1.117-2.515-2.53-3.309-4.236s-1.19-3.736-1.19-6.089c0-2.588.456-4.721 1.368-6.398.912-1.677 2.09-3.015 3.532-4.015 1.441-1 3.09-1.751 4.944-2.251 1.855-.5 3.725-.897 5.608-1.192a96.492 96.492 0 015.563-.706c1.824-.176 3.443-.44 4.857-.794 1.412-.352 2.531-.867 3.356-1.544.824-.677 1.205-1.662 1.146-2.957 0-1.353-.221-2.427-.662-3.222a4.87 4.87 0 00-1.767-1.852c-.737-.441-1.592-.735-2.563-.883a21.18 21.18 0 00-3.139-.221c-2.474 0-4.418.53-5.832 1.587-1.414 1.058-2.239 2.821-2.473 5.288h-12.501c.176-2.937.911-5.377 2.204-7.316zm42.4 1.691v-8.375h7.496v-3.528c0-4.057 1.262-7.378 3.785-9.966 2.524-2.587 6.339-3.881 11.449-3.881 1.117 0 2.234.047 3.351.141 1.117.094 2.204.172 3.263.234v9.313a40.857 40.857 0 00-4.586-.25c-1.705 0-2.925.397-3.659 1.19-.736.794-1.103 2.132-1.103 4.013v2.734h8.643v8.375h-8.643v37.188h-12.5V199.92h-7.496m59.898 2.279c-1.499-1.644-3.778-2.466-6.834-2.466-1.999 0-3.66.338-4.983 1.013-1.323.675-2.382 1.511-3.175 2.509-.794.998-1.352 2.056-1.675 3.171-.325 1.116-.515 2.114-.574 2.994h20.372c-.588-3.17-1.632-5.577-3.131-7.221zm-14.155 23.985c1.881 1.824 4.586 2.736 8.115 2.736 2.527 0 4.703-.628 6.525-1.884 1.822-1.257 2.939-2.587 3.352-3.991h11.023c-1.764 5.466-4.469 9.374-8.113 11.725-3.646 2.35-8.055 3.525-13.229 3.525-3.586 0-6.818-.574-9.697-1.72-2.879-1.147-5.316-2.78-7.312-4.897-1.996-2.117-3.536-4.646-4.622-7.588-1.086-2.941-1.628-6.176-1.628-9.707 0-3.411.557-6.587 1.672-9.528 1.116-2.941 2.7-5.485 4.754-7.633 2.054-2.146 4.506-3.838 7.356-5.073 2.849-1.235 6.008-1.854 9.477-1.854 3.88 0 7.261.75 10.142 2.248 2.881 1.499 5.247 3.512 7.098 6.04 1.852 2.527 3.19 5.406 4.013 8.638.823 3.233 1.117 6.612.882 10.137h-32.894c.176 4.06 1.205 7.002 3.086 8.826zm16.925-52.84v.821h-3.535v9.312h-.969v-9.312h-3.52v-.821h8.024m2.055 0l3.509 8.885 3.495-8.885h1.42v10.133h-.969v-8.756h-.028l-3.464 8.756h-.908l-3.478-8.756h-.028v8.756h-.969v-10.133h1.42" />
          <path
            d="M146.855 270.181c.858 0 1.501-.19 1.93-.57.429-.381.644-1 .644-1.856 0-.825-.215-1.423-.644-1.796-.429-.373-1.072-.559-1.93-.559h-4.098v4.781h4.098zm1.337-7.687c.764 0 1.452.123 2.064.369a4.745 4.745 0 011.575 1.012c.438.428.772.924 1.002 1.488.23.563.346 1.17.346 1.821 0 1-.211 1.865-.633 2.595-.422.73-1.111 1.286-2.067 1.667v.048c.461.127.843.322 1.145.584.303.262.549.572.739.93.191.359.33.751.417 1.181.088.43.148.859.18 1.289.016.27.031.588.047.954.016.365.044.739.084 1.121.04.382.103.743.19 1.085.088.342.219.633.394.871h-3.744c-.206-.541-.334-1.184-.381-1.932a28.8 28.8 0 00-.215-2.148c-.127-.89-.397-1.542-.811-1.956-.413-.413-1.087-.62-2.025-.62h-3.742v6.656h-3.75v-17.015h9.185zm20.774 0v3.14h-8.989v3.641h8.25v2.906h-8.25v4.188h9.179v3.14h-12.929v-17.015h12.739m12.806 16.878a6.282 6.282 0 01-2.456.512c-1.303 0-2.475-.226-3.516-.678-1.042-.454-1.92-1.077-2.635-1.871a8.24 8.24 0 01-1.645-2.799c-.382-1.073-.573-2.228-.573-3.466 0-1.272.192-2.45.574-3.538a8.495 8.495 0 011.647-2.848 7.546 7.546 0 012.638-1.905c1.042-.461 2.214-.692 3.517-.692.874 0 1.721.131 2.539.393a7.2 7.2 0 012.218 1.154 6.334 6.334 0 011.633 1.881c.43.746.691 1.605.787 2.574h-3.576c-.223-.953-.653-1.669-1.288-2.146-.636-.477-1.407-.715-2.313-.715-.843 0-1.557.163-2.146.488a4.061 4.061 0 00-1.431 1.311 5.762 5.762 0 00-.799 1.87 9.3 9.3 0 00-.25 2.168c0 .715.083 1.41.25 2.085.167.675.433 1.282.798 1.822.365.54.842.973 1.43 1.299.588.326 1.303.488 2.144.488 1.239 0 2.196-.312 2.87-.937.676-.625 1.069-1.531 1.18-2.719h-3.767v-2.797h7.158v9.195h-2.388l-.382-1.931c-.668.859-1.407 1.46-2.218 1.802m8.012.137h3.75v-17.031h-3.75zm9.907-4.192c.207.397.481.719.823.966a3.58 3.58 0 001.203.548c.461.119.937.178 1.43.178.333 0 .691-.027 1.073-.083a3.694 3.694 0 001.072-.322c.334-.159.612-.377.834-.655.223-.279.334-.632.334-1.061 0-.461-.147-.834-.44-1.121-.295-.285-.68-.523-1.157-.715a11.565 11.565 0 00-1.619-.5 44.47 44.47 0 01-1.835-.477 16.162 16.162 0 01-1.858-.583 6.38 6.38 0 01-1.619-.895 4.198 4.198 0 01-1.157-1.371c-.294-.548-.44-1.211-.44-1.99 0-.874.186-1.633.56-2.277a5.077 5.077 0 011.464-1.608 6.36 6.36 0 012.049-.953 8.693 8.693 0 012.286-.311c.89 0 1.744.1 2.561.298a6.417 6.417 0 012.18.965 4.874 4.874 0 011.512 1.704c.373.691.56 1.53.56 2.515h-3.625c-.032-.509-.139-.93-.321-1.264a2.104 2.104 0 00-.725-.787 3.1 3.1 0 00-1.035-.405 6.398 6.398 0 00-1.273-.119c-.3 0-.602.032-.903.095a2.407 2.407 0 00-.82.334 2.082 2.082 0 00-.608.596c-.158.238-.237.541-.237.906 0 .334.063.605.19.811.127.207.377.398.75.573.374.175.89.35 1.549.525.659.174 1.522.397 2.585.667.318.064.759.179 1.322.346a6.358 6.358 0 011.68.799 5.084 5.084 0 011.442 1.467c.405.612.607 1.395.607 2.349 0 .78-.151 1.502-.453 2.17a4.725 4.725 0 01-1.347 1.73c-.597.485-1.336.862-2.218 1.133-.881.27-1.903.405-3.063.405-.938 0-1.848-.115-2.73-.347a6.84 6.84 0 01-2.336-1.087 5.376 5.376 0 01-1.609-1.885c-.397-.763-.589-1.669-.572-2.719h3.625c0 .572.103 1.057.309 1.455m11.27-9.683v-3.14h13.955v3.14h-5.103v13.875h-3.75v-13.875h-5.102m28.649-3.14v3.14h-8.989v3.641h8.25v2.906h-8.25v4.188h9.18v3.14h-12.93v-17.015h12.739m10.564 7.687c.857 0 1.501-.19 1.93-.57.428-.381.643-1 .643-1.856 0-.825-.215-1.423-.643-1.796-.429-.373-1.073-.559-1.93-.559h-4.099v4.781h4.099zm1.337-7.687c.762 0 1.451.123 2.063.369a4.73 4.73 0 011.574 1.012 4.38 4.38 0 011.003 1.488 4.75 4.75 0 01.346 1.821c0 1-.211 1.865-.633 2.595-.422.73-1.11 1.286-2.066 1.667v.048c.461.127.843.322 1.144.584.302.262.549.572.74.93.191.359.33.751.418 1.181.087.43.146.859.177 1.289.017.27.033.588.048.954.017.365.044.739.084 1.121.04.382.104.743.191 1.085.088.342.218.633.393.871h-3.743c-.207-.541-.334-1.184-.382-1.932a29.34 29.34 0 00-.215-2.148c-.127-.89-.397-1.542-.81-1.956-.413-.413-1.088-.62-2.026-.62h-3.742v6.656h-3.75v-17.015h9.186z"
            fill="#FFF101" />
        </g>
      </symbol>
      <symbol id="logo-gassafe-reg" viewBox="0 0 70 95">
        <path
          d="M3.113 79.833S0 79.833 0 82.695v8.525s0 2.863 3.113 2.863h63.641s3.113 0 3.113-2.863v-8.525s0-2.862-3.113-2.862H3.113z"
          fill="#FFED00" />
        <path
          d="M3.147 0S.034 0 .034 3.079v69.3s0 3.078 3.113 3.078h63.641s3.113 0 3.113-3.078v-69.3S69.901 0 66.788 0H3.147z"
          fill="#000" />
        <path
          d="M63.676 8.466c0-1.272-1.05-2.309-2.335-2.309-.643 0-1.218.268-1.658.67l-52.78 52.2a2.29 2.29 0 00-.644 1.606c0 1.272 1.083 2.343 2.369 2.376h52.747c1.286 0 2.334-1.037 2.334-2.309V8.466"
          fill="#FFED00" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M34.037 66.89c-.101.202-.27.336-.507.436.135 0 .236.067.304.167a.61.61 0 01.17.235l.1.3c0 .05.01.101.018.151.008.05.017.1.017.15v.235c.033.067.033.167.033.268 0 .1 0 .2.034.267l.102.201h-.914a1.06 1.06 0 01-.101-.468 3.727 3.727 0 01-.033-.267c-.009-.093-.017-.18-.035-.269-.034-.2-.102-.368-.203-.468-.102-.134-.27-.167-.508-.167h-.913v1.64h-.913v-4.184h2.266c.17 0 .373.034.508.1a.82.82 0 01.406.235c.101.1.17.234.237.368a.918.918 0 01.101.435c0 .234-.067.469-.169.636zm-.913-.032a.66.66 0 00.169-.469c0-.2-.068-.335-.17-.435-.1-.1-.27-.134-.473-.134h-1.015v1.171h1.015c.203 0 .372-.033.474-.133z"
          fill="#FFED00" />
        <path
          d="M38.266 65.084v.803h-2.233v.87h2.064v.736h-2.064v1.004h2.3v.77H35.12v-4.183h3.146zm3.181 4.149c-.203.067-.406.134-.61.134-.338 0-.609-.067-.88-.167-.27-.1-.473-.268-.642-.469a2.288 2.288 0 01-.541-1.54c0-.3.033-.601.135-.869.102-.268.237-.502.406-.703.17-.2.406-.368.643-.468.27-.1.541-.167.88-.167.203 0 .44.033.642.1.203.067.373.167.542.268.169.134.304.267.406.468.101.168.169.402.203.636h-.88a1.182 1.182 0 00-.304-.535c-.17-.1-.339-.168-.576-.168-.203 0-.372.034-.541.134a.811.811 0 00-.338.335c-.102.134-.17.3-.203.468-.034.168-.068.335-.068.536 0 .167.034.334.068.502.033.167.101.3.203.434.101.134.203.235.338.335.135.067.338.134.541.134.305 0 .542-.067.71-.234.17-.168.272-.368.305-.67h-.947v-.669h1.76V69.3h-.576l-.101-.468c-.203.167-.372.334-.575.401zm2.165-4.149h.914v4.183h-.914v-4.183zm2.538 3.179c.067.1.135.167.203.234.067.067.169.1.304.134.102.034.237.034.339.034.067 0 .169 0 .27-.034.102 0 .17-.033.27-.067.069-.033.136-.1.204-.167.067-.067.067-.168.067-.268s-.033-.2-.101-.268a.74.74 0 00-.27-.167c-.102-.033-.237-.1-.407-.134-.135-.033-.304-.067-.44-.134-.169-.033-.304-.1-.473-.133a2.523 2.523 0 01-.406-.235c-.102-.1-.203-.2-.27-.334a1.107 1.107 0 01-.102-.502c0-.201.033-.402.135-.57.101-.166.203-.3.372-.4.135-.101.305-.168.508-.235.203-.067.372-.067.575-.067.237 0 .44.034.643.067.203.034.372.134.541.234.17.1.27.235.372.402.102.167.136.368.136.602h-.914a.662.662 0 00-.068-.3.886.886 0 00-.169-.202 2.398 2.398 0 00-.27-.1c-.102-.034-.203-.034-.305-.034-.068 0-.135 0-.237.034a.477.477 0 00-.203.067c-.068.033-.101.1-.135.134a.479.479 0 00-.068.234c0 .067 0 .134.034.2.034.068.101.101.203.135.101.033.203.1.372.133.17.034.372.1.643.168.068.033.203.033.338.1.136.034.271.1.406.2.136.101.271.202.373.369.101.134.135.335.135.569 0 .2-.034.368-.102.535a1.014 1.014 0 01-.338.435c-.135.134-.338.201-.541.268s-.474.1-.745.1c-.237 0-.473-.033-.676-.1-.203-.067-.406-.134-.576-.268a1.368 1.368 0 01-.406-.468c-.101-.201-.135-.402-.135-.67h.914a.709.709 0 000 .469zm2.808-2.376v-.803h3.45v.803h-1.251v3.38h-.947v-3.38h-1.252zm7.139-.803v.803h-2.233v.87h2.064v.736h-2.064v1.004h2.267v.77h-3.18v-4.183h3.146z"
          fill="#FFED00" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M60.292 66.89c-.101.202-.27.336-.507.436.135 0 .237.067.304.167a.6.6 0 01.17.235l.1.3c0 .05.01.101.018.151.008.05.017.1.017.15v.235c.034.067.034.167.034.268 0 .1 0 .2.033.267l.102.201h-.914a1.06 1.06 0 01-.101-.468 3.678 3.678 0 01-.033-.267 2.614 2.614 0 00-.035-.269c-.034-.2-.101-.368-.203-.468-.101-.134-.27-.167-.507-.167h-.914v1.64h-.913v-4.184h2.267c.203 0 .372.034.507.1.17.034.305.135.406.235.102.1.17.234.237.368a.919.919 0 01.101.435c0 .234-.067.469-.169.636zm-.913-.032a.66.66 0 00.169-.469c0-.2-.068-.335-.17-.435-.1-.1-.27-.134-.473-.134H57.89v1.171h1.015c.203 0 .372-.033.474-.133z"
          fill="#FFED00" />
        <path
          d="M18.561 87.622c.065.552.32.933.768 1.144.23.108.494.161.795.161.573 0 .998-.182 1.273-.547.276-.366.414-.77.414-1.214 0-.537-.165-.953-.494-1.246a1.696 1.696 0 00-1.177-.44c-.333 0-.62.064-.859.193a1.845 1.845 0 00-.607.537l-.838-.049.586-4.14h3.996v.934h-3.271l-.328 2.138c.18-.136.35-.239.51-.307a2.59 2.59 0 01.994-.177c.705 0 1.303.228 1.794.682.49.455.736 1.032.736 1.73a2.96 2.96 0 01-.677 1.923c-.448.555-1.164.832-2.148.832a2.761 2.761 0 01-1.665-.526c-.48-.355-.749-.897-.806-1.628h1.004zm6.225.102c.029.534.235.903.618 1.107.197.107.419.16.666.16.462 0 .856-.19 1.182-.574.326-.386.556-1.169.693-2.347-.215.34-.482.58-.8.72a2.546 2.546 0 01-1.021.204c-.741 0-1.329-.231-1.762-.693-.43-.462-.644-1.056-.644-1.783 0-.698.213-1.313.639-1.843.426-.53 1.054-.794 1.885-.794 1.12 0 1.894.504 2.32 1.514.237.555.355 1.25.355 2.084 0 .942-.142 1.776-.425 2.503-.469 1.21-1.264 1.816-2.384 1.816-.752 0-1.323-.197-1.714-.591-.39-.394-.585-.888-.585-1.483h.977zm1.467-1.579a1.64 1.64 0 001.047-.376c.319-.254.478-.696.478-1.326 0-.566-.143-.987-.43-1.263-.282-.279-.644-.419-1.085-.419-.472 0-.848.16-1.128.479-.275.315-.413.737-.413 1.267 0 .501.122.9.365 1.198.244.294.632.44 1.166.44zm3.54 3.438c.035-.663.171-1.24.407-1.73.24-.49.706-.936 1.397-1.337l1.031-.596c.462-.269.786-.498.972-.688.294-.297.44-.637.44-1.02 0-.448-.133-.802-.402-1.064-.269-.265-.627-.397-1.074-.397-.663 0-1.121.25-1.375.752-.136.268-.212.64-.226 1.117h-.983c.011-.67.134-1.216.37-1.638.42-.745 1.16-1.117 2.22-1.117.88 0 1.523.238 1.927.714.409.476.613 1.006.613 1.59 0 .616-.217 1.142-.65 1.579-.25.254-.7.562-1.348.924l-.736.408a4.747 4.747 0 00-.827.553c-.358.312-.584.657-.677 1.037h4.2v.913h-5.28zm8.996-7.724c.86 0 1.457.224 1.794.672.34.444.51.902.51 1.374h-.956c-.057-.304-.149-.542-.274-.714-.233-.322-.585-.483-1.058-.483-.54 0-.97.25-1.29.752-.318.497-.495 1.212-.53 2.143.221-.326.5-.57.837-.73a2.417 2.417 0 011.031-.216c.645 0 1.207.206 1.687.618.48.412.72 1.026.72 1.842 0 .699-.228 1.318-.683 1.859-.454.537-1.102.805-1.944.805-.72 0-1.34-.272-1.864-.816-.523-.548-.784-1.468-.784-2.76 0-.957.117-1.768.35-2.434.447-1.275 1.265-1.912 2.454-1.912zm-.07 7.063c.508 0 .888-.17 1.139-.51.254-.344.381-.749.381-1.214 0-.394-.113-.768-.338-1.123-.226-.358-.636-.537-1.23-.537-.416 0-.78.138-1.096.414-.312.276-.467.691-.467 1.246 0 .487.141.897.424 1.23.286.33.682.494 1.187.494zm4.334-1.3c.065.552.32.933.769 1.144.229.108.494.161.794.161.573 0 .998-.182 1.273-.547.276-.366.414-.77.414-1.214 0-.537-.165-.953-.494-1.246a1.696 1.696 0 00-1.176-.44c-.333 0-.62.064-.86.193a1.845 1.845 0 00-.607.537l-.838-.049.586-4.14h3.996v.934h-3.271l-.328 2.138c.18-.136.35-.239.51-.307a2.59 2.59 0 01.994-.177c.706 0 1.304.228 1.794.682.49.455.736 1.032.736 1.73a2.96 2.96 0 01-.677 1.923c-.447.555-1.163.832-2.148.832a2.761 2.761 0 01-1.665-.526c-.48-.355-.749-.897-.806-1.628h1.005zm7.74-5.73c.996 0 1.715.41 2.16 1.23.343.633.515 1.502.515 2.604 0 1.046-.156 1.91-.467 2.595-.451.98-1.19 1.471-2.213 1.471-.924 0-1.611-.4-2.063-1.203-.376-.67-.564-1.568-.564-2.696 0-.874.113-1.624.339-2.25.422-1.168 1.187-1.752 2.293-1.752zm-.01 7.02c.5 0 .9-.223 1.197-.667.297-.444.446-1.27.446-2.481 0-.874-.107-1.592-.322-2.154-.215-.566-.632-.849-1.252-.849-.569 0-.986.269-1.251.806-.262.534-.392 1.321-.392 2.363 0 .785.084 1.415.252 1.891.258.727.698 1.09 1.322 1.09zM22.161 56.953c.136.234.339.468.575.602.237.167.508.268.779.368.304.067.609.1.913.1a5 5 0 00.71-.067c.238-.067.474-.133.677-.234.203-.1.373-.267.508-.468s.203-.435.203-.703c0-.468-.338-.837-.981-1.104-.643-.235-1.557-.469-2.707-.736-.474-.1-.947-.235-1.387-.368a5.06 5.06 0 01-1.218-.57 2.72 2.72 0 01-.846-.903 2.671 2.671 0 01-.338-1.305c0-.77.135-1.372.44-1.874.304-.468.71-.87 1.184-1.137.473-.268 1.049-.469 1.658-.603a11.74 11.74 0 011.894-.167c.643 0 1.252.067 1.861.167.61.134 1.15.335 1.624.603.474.3.88.669 1.184 1.137.305.469.508 1.071.576 1.807h-3.113c-.034-.636-.27-1.037-.71-1.271-.44-.235-.948-.335-1.523-.335-.17 0-.372 0-.61.033-.202.034-.405.067-.575.134a1.103 1.103 0 00-.44.335c-.135.134-.168.335-.168.569 0 .301.101.535.304.703.203.167.507.334.846.435.338.1.744.2 1.184.3.44.101.88.201 1.353.302.474.1.948.234 1.388.401.44.168.845.368 1.218.603.338.234.643.569.846.937.203.368.338.836.338 1.372 0 .77-.17 1.405-.474 1.94-.304.536-.71.937-1.218 1.272a5.607 5.607 0 01-1.725.67c-.643.133-1.32.2-1.997.2-.676 0-1.353-.067-2.03-.2a5.81 5.81 0 01-1.759-.67 3.493 3.493 0 01-1.252-1.272c-.338-.535-.507-1.17-.541-1.974h3.112c0 .402.068.703.237.97z"
          fill="#000" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M41.965 59.83l.023.034v-.034h-.023zm-.281-1.07c.032.478.126.835.281 1.07h-3.36c-.017-.1-.043-.192-.068-.284a2.79 2.79 0 01-.068-.285 2.919 2.919 0 01-.067-.568 3.955 3.955 0 01-1.827 1.104 6.807 6.807 0 01-2.166.335 5.901 5.901 0 01-1.59-.201c-.474-.134-.913-.335-1.286-.636a3.45 3.45 0 01-.845-1.104c-.203-.435-.305-.97-.305-1.573 0-.67.135-1.205.372-1.64.237-.435.542-.77.914-1.037a4.981 4.981 0 011.285-.569 10.18 10.18 0 011.455-.301l.18-.024c.44-.058.858-.114 1.275-.143.474-.034.88-.1 1.252-.201.372-.1.677-.234.88-.402.203-.167.304-.435.304-.77 0-.367-.067-.635-.169-.836-.135-.2-.27-.368-.474-.468a1.641 1.641 0 00-.676-.235 5.683 5.683 0 00-.812-.066c-.643 0-1.15.133-1.523.401-.372.268-.575.736-.643 1.372h-3.248c.034-.736.237-1.372.575-1.874a3.996 3.996 0 011.286-1.205c.508-.3 1.117-.501 1.76-.635a9.524 9.524 0 011.962-.201c.609 0 1.218.067 1.827.134a6.21 6.21 0 011.692.502c.507.234.947.569 1.251 1.004.305.435.474 1.003.474 1.706v6.124c0 .502.034 1.037.102 1.506zm-3.403-2.84c.028-.248.053-.464.053-.607v-1.104c-.135.1-.305.2-.508.267a4 4 0 01-.676.168c-.237.033-.474.067-.744.1l-.39.05c-.127.017-.253.034-.389.05a5.44 5.44 0 00-.71.168 1.75 1.75 0 00-.61.3 1.255 1.255 0 00-.439.503c-.102.167-.17.435-.17.736s.069.535.17.736c.101.201.27.368.44.469.169.1.406.2.643.234.236.033.473.067.744.067.643 0 1.15-.134 1.489-.335.338-.234.609-.468.778-.77.169-.3.27-.602.304-.903l.015-.13z"
          fill="#000" />
        <path
          d="M42.123 50.26v-2.142h1.963v-.903c0-1.037.338-1.907.98-2.577.644-.669 1.659-1.003 2.978-1.003.305 0 .575 0 .88.033.304.033.575.033.846.067v2.41a13.266 13.266 0 00-1.184-.068c-.44 0-.779.1-.948.301-.203.201-.27.536-.27 1.038v.703h2.266v2.141h-2.266v9.604H44.12V50.26h-1.997z"
          fill="#000" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M59.852 49.925c.474.67.846 1.406 1.05 2.242.202.837.27 1.707.202 2.644h-8.56c.068 1.037.339 1.807.812 2.275.474.469 1.185.703 2.098.703.643 0 1.218-.167 1.692-.502.473-.334.778-.67.88-1.037H60.9c-.44 1.405-1.15 2.41-2.097 3.011-.948.603-2.098.904-3.451.904-.948 0-1.794-.134-2.538-.435a5.374 5.374 0 01-1.895-1.272 5.013 5.013 0 01-1.218-1.94c-.304-.77-.44-1.607-.44-2.51 0-.87.136-1.673.44-2.443.305-.77.71-1.405 1.252-1.974a6.502 6.502 0 011.929-1.305 6.488 6.488 0 012.47-.469c1.015 0 1.894.168 2.639.57a4.93 4.93 0 011.86 1.538zm-7.308 2.811h5.312c-.135-.836-.406-1.438-.846-1.84-.406-.435-.98-.636-1.793-.636-.507 0-.947.1-1.285.268-.339.167-.61.368-.813.636-.203.234-.372.502-.44.803l-.037.163c-.053.226-.098.416-.098.606zM37.048 42.53l1.59 4.183h-.947l-.304-.937h-1.59l-.339.937h-.947l1.59-4.183h.947zm-1.015 2.577h1.083l-.541-1.54-.542 1.54z"
          fill="#000" />
        <path
          d="M32.988 46.68a1.34 1.34 0 01-.609.133 2.42 2.42 0 01-.88-.167c-.27-.1-.473-.267-.642-.468a2.4 2.4 0 01-.406-.703c-.102-.268-.136-.535-.136-.87 0-.301.034-.602.136-.87.101-.268.237-.502.406-.703.169-.2.406-.368.643-.468.27-.1.541-.167.88-.167.202 0 .44.033.642.1.203.067.372.167.541.301.17.134.305.268.406.468.102.168.17.402.203.636h-.88a1.18 1.18 0 00-.304-.535 1.059 1.059 0 00-.575-.167c-.203 0-.372.033-.541.133-.136.067-.27.201-.372.335a1.15 1.15 0 00-.203.468c-.034.168-.068.335-.068.536 0 .167.034.335.068.502.034.167.101.301.203.435.101.134.203.234.372.334.135.067.338.134.541.134.305 0 .542-.067.71-.234.17-.167.271-.368.305-.67h-.947v-.702h1.793v2.276h-.609l-.102-.469c-.203.167-.372.335-.575.402zm6.564-1.004c.034.1.135.167.203.234.101.067.17.1.304.134.102.033.237.033.373.033.067 0 .169 0 .27-.033.102 0 .17-.034.271-.067a.34.34 0 00.203-.167c.068-.067.068-.168.068-.268s-.034-.2-.102-.268a.738.738 0 00-.27-.167c-.136-.033-.271-.1-.407-.134a10.971 10.971 0 00-.473-.1c-.17-.034-.305-.1-.474-.134a2.609 2.609 0 01-.406-.234 1.3 1.3 0 01-.27-.335 1.108 1.108 0 01-.102-.502c0-.2.034-.401.135-.569.102-.167.203-.301.372-.401.136-.1.305-.168.508-.235.203-.066.372-.066.575-.066.237 0 .44.033.643.066.203.034.372.134.541.235.17.1.271.234.372.435.102.167.136.368.136.635h-.914a.661.661 0 00-.067-.3.89.89 0 00-.17-.202c-.067-.033-.169-.066-.27-.1-.102-.033-.203-.033-.305-.033-.067 0-.135 0-.236.033-.068 0-.136.034-.204.067-.067.034-.101.1-.135.134-.034.067-.068.134-.068.234 0 .067 0 .134.034.2.034.068.102.101.203.135.102.033.237.1.373.134.169.033.372.1.642.167.068 0 .203.033.339.1.135.034.27.1.406.201.135.1.27.2.372.368.101.168.135.335.135.569 0 .2-.034.368-.101.535-.068.168-.203.302-.339.435-.135.134-.338.201-.541.268s-.474.1-.744.1c-.237 0-.474-.033-.677-.1-.203-.067-.406-.134-.575-.268a1.366 1.366 0 01-.406-.468c-.102-.2-.136-.402-.136-.67h.914c-.102.168-.068.302 0 .369z"
          fill="#000" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M61.07 44.873c0-.67-.541-1.205-1.218-1.205-.676 0-1.218.502-1.218 1.205 0 .669.542 1.204 1.218 1.204.643 0 1.218-.502 1.218-1.204zm-.203 0c0 .602-.44 1.037-1.015 1.037a1.02 1.02 0 01-1.015-1.037c0-.603.44-1.038 1.015-1.038a1.02 1.02 0 011.015 1.038zm-1.488-.703h.541c.338 0 .508.134.508.401 0 .268-.17.369-.373.402l.406.636h-.236l-.407-.602h-.236v.602h-.237v-1.44h.034zm.473.636h-.236v-.435h.304c.135 0 .304 0 .304.2 0 .235-.169.235-.372.235z"
          fill="#000" />
      </symbol>
      <symbol id="logo-gassafe" viewBox="0 0 79 82">
        <g fill="none" fill-rule="evenodd">
          <g transform="translate(4.815 .086)">
            <mask id="logo-gassafea" fill="#fff">
              <path d="M0 .236v72.876h73.065V.236H0z" />
            </mask>
            <path
              d="M.694 68.202A2.98 2.98 0 000 70.117a2.999 2.999 0 003.003 2.995h67.06a2.998 2.998 0 003.002-2.995V3.231A2.998 2.998 0 0070.062.236a2.99 2.99 0 00-1.9.676L.693 68.202z"
              fill="#FFED00" mask="url(#logo-gassafea)" />
          </g>
          <path
            d="M38.824 52.765c-.262.11-.525.165-.792.165-.42 0-.798-.073-1.134-.219a2.427 2.427 0 01-.85-.601 2.659 2.659 0 01-.53-.9 3.29 3.29 0 01-.184-1.115c0-.409.061-.788.185-1.138.123-.35.3-.655.53-.915.232-.26.515-.465.851-.613a2.78 2.78 0 011.134-.222c.282 0 .555.042.819.126.264.084.502.208.715.371.212.163.388.365.526.605.139.24.223.516.254.828h-1.153c-.072-.307-.21-.537-.415-.69a1.21 1.21 0 00-.746-.23 1.41 1.41 0 00-.692.157c-.19.105-.343.245-.461.421a1.843 1.843 0 00-.258.601 2.985 2.985 0 000 1.368c.054.217.14.412.258.586.117.174.27.313.46.418.19.105.42.157.692.157.4 0 .708-.1.925-.302.218-.2.345-.492.38-.874h-1.214v-.9h2.308v2.958h-.77l-.123-.621c-.215.276-.454.469-.715.579m5.236-2.071l-.692-2.012h-.015l-.715 2.012h1.422zm-.069-3.362l2.052 5.477h-1.252l-.416-1.216h-2.052l-.43 1.216h-1.215l2.075-5.477h1.238zm3.301 4.128a.875.875 0 00.265.311c.11.08.24.138.388.177.149.038.302.057.461.057.108 0 .223-.009.346-.027.123-.017.238-.052.346-.103a.755.755 0 00.269-.21.53.53 0 00.108-.342.48.48 0 00-.143-.36 1.14 1.14 0 00-.372-.23 3.773 3.773 0 00-.523-.161 14.222 14.222 0 01-.59-.154 5.246 5.246 0 01-.6-.187 2.077 2.077 0 01-.523-.288 1.353 1.353 0 01-.372-.44 1.334 1.334 0 01-.142-.64c0-.282.06-.526.18-.733.12-.207.278-.38.473-.517.194-.138.414-.24.66-.306.246-.067.492-.1.737-.1a3.5 3.5 0 01.826.095c.264.064.498.168.703.31.204.143.367.326.487.549.12.222.18.492.18.808h-1.168a.962.962 0 00-.104-.406.677.677 0 00-.233-.253.996.996 0 00-.334-.13 2.062 2.062 0 00-.41-.039c-.097 0-.195.01-.291.03a.783.783 0 00-.265.108.667.667 0 00-.195.192.514.514 0 00-.077.291c0 .108.02.195.061.261a.545.545 0 00.242.184c.12.057.287.113.5.17.212.055.49.127.833.214.102.02.244.057.426.11.182.055.362.14.542.258.179.117.334.275.464.472.13.196.196.448.196.755 0 .25-.049.483-.146.698-.097.215-.242.4-.434.556-.193.156-.43.277-.715.364-.285.087-.613.13-.988.13a3.44 3.44 0 01-.88-.111 2.2 2.2 0 01-.753-.35 1.724 1.724 0 01-.519-.606 1.806 1.806 0 01-.184-.874h1.168c0 .184.034.34.1.468M25.137 65.66c.18.31.413.564.699.761.285.197.613.343.984.437.37.094.756.14 1.155.14.285 0 .585-.032.899-.099.313-.065.599-.17.856-.311.257-.142.47-.331.642-.568.171-.236.256-.534.256-.893 0-.605-.403-1.058-1.21-1.361-.806-.303-1.93-.605-3.371-.908a20.046 20.046 0 01-1.722-.468 5.895 5.895 0 01-1.494-.709 3.45 3.45 0 01-1.053-1.106c-.266-.444-.398-.988-.398-1.63 0-.946.184-1.72.554-2.326a4.082 4.082 0 011.464-1.432 6.422 6.422 0 012.046-.737 12.62 12.62 0 012.331-.213c.796 0 1.568.075 2.316.227a6.135 6.135 0 012.004.766c.587.36 1.076.837 1.464 1.433.389.596.62 1.348.696 2.256h-3.828c-.057-.777-.352-1.302-.884-1.577-.532-.275-1.16-.412-1.881-.412-.228 0-.476.014-.741.042-.267.029-.509.09-.728.185a1.5 1.5 0 00-.555.411c-.152.18-.228.421-.228.724 0 .36.132.653.398.88.266.227.612.411 1.039.553.427.142.915.27 1.465.383.55.114 1.11.237 1.68.37a17.18 17.18 0 011.721.482c.56.189 1.058.44 1.494.752.437.311.787.7 1.053 1.163.266.464.398 1.036.398 1.717 0 .965-.194 1.774-.582 2.427a4.66 4.66 0 01-1.522 1.574 6.241 6.241 0 01-2.147.838 12.543 12.543 0 01-4.962-.014 6.632 6.632 0 01-2.176-.85 4.856 4.856 0 01-1.564-1.57c-.407-.652-.63-1.468-.668-2.45h3.829c0 .432.09.803.27 1.113m19.83-3.184a6.557 6.557 0 01-.827.212c-.295.057-.604.105-.928.143-.323.037-.646.084-.97.141a7.377 7.377 0 00-.898.227 2.66 2.66 0 00-.77.383 1.8 1.8 0 00-.528.61c-.134.245-.2.557-.2.935 0 .36.066.662.2.908.133.246.313.44.542.581.228.142.494.242.799.298.304.057.618.085.94.085.8 0 1.418-.132 1.855-.397.438-.264.761-.582.97-.95.21-.369.338-.742.386-1.12.047-.378.07-.68.07-.908v-1.503c-.17.152-.385.27-.64.355m-7.978-5.688a4.83 4.83 0 011.592-1.502 6.951 6.951 0 012.174-.808 12.368 12.368 0 012.43-.241c.74 0 1.488.052 2.246.156.758.104 1.45.307 2.075.61a4.09 4.09 0 011.535 1.262c.398.54.597 1.254.597 2.142v7.632c0 .663.037 1.296.114 1.901.075.606.208 1.06.398 1.362l-4.096-.012a5.6 5.6 0 01-.185-.69 5.992 5.992 0 01-.1-.72 5.266 5.266 0 01-2.275 1.394 9.14 9.14 0 01-2.674.398 7.259 7.259 0 01-1.962-.255 4.495 4.495 0 01-1.592-.795 3.696 3.696 0 01-1.067-1.362c-.256-.549-.384-1.202-.384-1.959 0-.832.147-1.518.441-2.057a3.68 3.68 0 011.139-1.291 5.113 5.113 0 011.594-.724c.598-.16 1.2-.288 1.808-.383.607-.094 1.205-.17 1.793-.227a10.95 10.95 0 001.566-.255c.455-.114.817-.28 1.082-.497.266-.217.389-.534.37-.95 0-.436-.071-.781-.214-1.037a1.565 1.565 0 00-.57-.595 2.246 2.246 0 00-.826-.284 6.8 6.8 0 00-1.012-.071c-.797 0-1.424.17-1.88.51-.456.34-.722.907-.797 1.7h-4.03c.056-.944.293-1.729.71-2.352m13.67.544v-2.693h2.417v-1.135c0-1.305.406-2.372 1.22-3.205.814-.832 2.044-1.248 3.69-1.248.361 0 .721.015 1.081.046.36.03.711.055 1.052.075v2.995a13.13 13.13 0 00-1.478-.08c-.55 0-.943.127-1.18.382s-.355.685-.355 1.29v.88h2.786v2.693h-2.786V69.29h-4.03V57.332h-2.417zm19.311.732c-.483-.528-1.218-.793-2.203-.793-.645 0-1.18.109-1.607.326a3.05 3.05 0 00-1.023.807 2.95 2.95 0 00-.54 1.02c-.105.358-.167.68-.185.963h6.567c-.19-1.02-.526-1.794-1.01-2.323m-4.562 7.713c.606.587 1.478.88 2.616.88.814 0 1.516-.202 2.103-.606.588-.404.948-.832 1.08-1.283h3.555c-.569 1.757-1.44 3.014-2.616 3.77-1.175.756-2.597 1.134-4.265 1.134-1.156 0-2.198-.185-3.126-.553a6.485 6.485 0 01-2.357-1.575 7.083 7.083 0 01-1.49-2.44c-.35-.946-.525-1.986-.525-3.122 0-1.097.179-2.118.539-3.064a7.19 7.19 0 011.532-2.454 7.212 7.212 0 012.372-1.631c.919-.398 1.937-.596 3.055-.596 1.251 0 2.341.24 3.27.723a6.518 6.518 0 012.289 1.942 7.972 7.972 0 011.293 2.777 10.3 10.3 0 01.284 3.26H64.412c.056 1.306.388 2.252.995 2.838m5.456-16.991v.263h-1.14v2.995h-.312V49.05h-1.135v-.263zm.663 0l1.13 2.857 1.128-2.857h.457v3.258h-.312v-2.816h-.009l-1.117 2.816h-.293l-1.12-2.816h-.01v2.816h-.312v-3.258z"
            fill="#1D1C1B" />
          <path
            d="M38.47 78.64c.277 0 .485-.062.623-.184.138-.123.207-.322.207-.597 0-.265-.069-.458-.207-.578-.138-.12-.346-.18-.622-.18h-1.322v1.538h1.322zm.432-2.473c.246 0 .468.04.665.119.198.079.367.187.508.325.141.138.249.297.323.479.074.18.111.376.111.585 0 .322-.068.6-.203.835-.136.235-.359.413-.667.536v.015a.937.937 0 01.608.487c.06.116.106.242.134.38.028.138.047.276.058.414l.015.307c.005.118.014.238.027.36.013.124.034.24.062.35a.81.81 0 00.126.28h-1.207a2.118 2.118 0 01-.123-.622 9.22 9.22 0 00-.069-.69c-.04-.287-.128-.496-.261-.63-.133-.132-.35-.199-.653-.199H37.15v2.14h-1.21v-5.47h2.962zm6.698 0v1.01h-2.9v1.17h2.66v.935H42.7v1.347h2.96v1.01h-4.169v-5.472zm4.128 5.427c-.261.11-.525.165-.792.165-.42 0-.798-.073-1.134-.218a2.424 2.424 0 01-.849-.601 2.659 2.659 0 01-.53-.9 3.29 3.29 0 01-.185-1.115c0-.409.062-.788.185-1.138.123-.35.3-.655.531-.916a2.43 2.43 0 01.85-.612 2.78 2.78 0 011.135-.223c.281 0 .554.043.818.126.264.085.502.209.715.372.213.163.388.365.527.605.138.24.223.516.253.827H50.1c-.071-.306-.21-.536-.415-.69a1.21 1.21 0 00-.745-.23c-.272 0-.503.053-.693.157-.19.105-.343.245-.46.422a1.843 1.843 0 00-.258.601 2.985 2.985 0 000 1.368c.054.217.139.412.257.586.118.173.271.312.46.417.19.105.42.157.692.157.4 0 .708-.1.926-.301.218-.201.344-.492.38-.875h-1.215v-.899h2.308v2.957h-.77l-.123-.621c-.215.276-.453.47-.715.58m2.582.044h1.209v-5.477h-1.209zm3.194-1.349a.875.875 0 00.265.311c.11.08.24.138.388.176.149.039.303.058.461.058.108 0 .223-.009.346-.027.123-.018.239-.052.346-.104a.75.75 0 00.269-.21.528.528 0 00.107-.341.481.481 0 00-.142-.36 1.146 1.146 0 00-.372-.23 3.763 3.763 0 00-.522-.162 14.235 14.235 0 01-.592-.153 5.246 5.246 0 01-.6-.187 2.07 2.07 0 01-.521-.288 1.354 1.354 0 01-.373-.441 1.334 1.334 0 01-.142-.64c0-.281.06-.525.18-.732.12-.207.278-.38.473-.517a2.04 2.04 0 01.66-.307c.246-.066.492-.1.737-.1a3.5 3.5 0 01.826.096c.264.064.498.168.703.31.205.143.367.326.487.548.12.222.18.492.18.809h-1.168a.966.966 0 00-.104-.406.679.679 0 00-.233-.254 1 1 0 00-.334-.13 2.062 2.062 0 00-.41-.038c-.097 0-.194.01-.291.03a.779.779 0 00-.265.108.67.67 0 00-.196.192.516.516 0 00-.076.29c0 .108.02.195.061.262a.552.552 0 00.242.184c.12.056.287.113.5.169.212.056.49.127.833.215.102.02.245.057.426.11.182.054.362.14.542.257.179.118.334.275.464.472.13.197.196.449.196.756 0 .25-.049.483-.146.697-.097.215-.242.4-.434.557-.192.156-.43.277-.715.364-.284.087-.614.13-.988.13-.302 0-.595-.037-.88-.111a2.2 2.2 0 01-.753-.35 1.724 1.724 0 01-.519-.606 1.802 1.802 0 01-.184-.874h1.168c0 .184.034.34.1.468m3.634-3.114v-1.01h4.499v1.01h-1.646v4.462h-1.209v-4.462zm9.236-1.01v1.01h-2.898v1.17h2.66v.935h-2.66v1.347h2.96v1.01h-4.169v-5.472zm3.405 2.473c.277 0 .485-.062.623-.184.138-.123.207-.322.207-.597 0-.265-.069-.458-.207-.578-.138-.12-.346-.18-.622-.18h-1.322v1.538h1.322zm.432-2.473c.246 0 .468.04.665.119.198.079.367.187.508.325.14.138.248.297.323.479.074.18.111.376.111.585 0 .322-.068.6-.204.835-.135.235-.358.413-.666.536v.015a.937.937 0 01.608.487c.061.116.106.242.134.38.028.138.047.276.058.414l.015.307c.005.118.014.238.027.36.013.124.034.24.062.35a.81.81 0 00.126.28h-1.206a2.118 2.118 0 01-.124-.622 9.577 9.577 0 00-.069-.69c-.04-.287-.128-.496-.261-.63-.133-.132-.35-.199-.653-.199H70.46v2.14h-1.21v-5.47h2.962z"
            fill="currentColor" />
        </g>
      </symbol>
      <symbol id="logo-mark-text" viewBox="0 0 51 61">
        <g fill="none" fill-rule="evenodd">
          <path fill="#FB6058"
            d="M11.137 49.674c1.164.284 1.733 1.366 1.733 2.177 0 1.514-1.083 2.798-2.898 2.798h-3.94v-9.314h3.656c1.638 0 2.681.973 2.681 2.528 0 .676-.298 1.46-1.232 1.811zm-3.371.852v2.582h2.112c.758 0 1.177-.649 1.177-1.284 0-.609-.42-1.298-1.218-1.298H7.766zm1.8-1.447c.623 0 1.002-.5 1.002-1.122 0-.662-.379-1.081-1.056-1.081H7.766v2.203h1.8zM20.33 53.08c1.679 0 3.006-1.364 3.006-3.068 0-1.703-1.327-3.082-3.006-3.082-1.692 0-3.005 1.379-3.005 3.082 0 1.704 1.313 3.069 3.005 3.069m0-7.854c2.653 0 4.739 2.135 4.739 4.785 0 2.622-2.086 4.772-4.74 4.772-2.652 0-4.724-2.15-4.724-4.772 0-2.65 2.072-4.785 4.725-4.785m11.874 4.447l3.25 4.975h-2.059l-2.22-3.582-2.22 3.582H26.91l3.236-4.975-2.925-4.34h2.071l1.883 2.961 1.881-2.96h2.072zm5.876-4.339h7v1.581h-2.626v7.733h-1.747v-7.733h-2.626z" />
          <mask id="logo-mark-textb" fill="#fff">
            <use xlinkHref="#logo-mark-texta" />
          </mask>
          <path fill="#FB6058" d="M0 60.2h50.4V0H0v60.2zm1.63-1.639h47.14V1.64H1.63v56.92z"
            mask="url(#logo-mark-textb)" />
        </g>
      </symbol>
      <symbol id="logo-mark" viewBox="64 11 63 74">
        <g fill="none" fill-rule="evenodd" transform="translate(64 11)">
          <path
            d="M18.826 48.595v7.151h5.91c2.12 0 3.296-1.797 3.296-3.556 0-1.685-1.175-3.595-3.41-3.595h-5.796zm5.038-4.007c1.743 0 2.803-1.385 2.803-3.108 0-1.834-1.06-2.994-2.954-2.994h-4.887v6.102h5.038zm4.395 1.648c3.258.786 4.85 3.782 4.85 6.029 0 4.193-3.031 7.75-8.108 7.75H13.977V34.217h10.228c4.584 0 7.501 2.696 7.501 7.002 0 1.873-.833 4.045-3.447 5.018z"
            fill="#F95B47" />
          <mask id="logo-marka" fill="#fff">
            <path d="M0 73.72h62.697V.45H.001z" />
          </mask>
          <path d="M0 73.722h62.696V.452H0v73.27zm4.622-4.57h53.452V5.02H4.622v64.133z" fill="#F95B47"
            mask="url(#logo-marka)" />
        </g>
      </symbol>
      <symbol id="logo-refcom" viewBox="0 0 111 110">
        <rect x="1" width="110" height="110" rx="5" fill="#fff" />
        <path fill="url(#logo-refcompattern0)" d="M.764 0H98.92v44.729H.764z" />
      </symbol>
      <symbol id="logo-text" viewBox="136 20 110 29">
        <path
          d="M141.323 36.18v7.51h5.908c2.118 0 3.291-1.888 3.291-3.735 0-1.77-1.173-3.774-3.406-3.774h-5.793zm5.036-4.205c1.74 0 2.801-1.455 2.801-3.264 0-1.925-1.06-3.145-2.953-3.145h-4.884v6.409h5.036zm4.39 1.73c3.256.825 4.846 3.97 4.846 6.328 0 4.403-3.028 8.137-8.1 8.137h-11.017V21.085h10.221c4.58 0 7.496 2.831 7.496 7.351 0 1.966-.834 4.246-3.446 5.268zm25.706 9.905c4.695 0 8.405-3.97 8.405-8.923 0-4.953-3.71-8.963-8.405-8.963-4.732 0-8.404 4.01-8.404 8.963 0 4.953 3.672 8.923 8.404 8.923m0-22.84c7.42 0 13.25 6.212 13.25 13.917 0 7.625-5.83 13.876-13.25 13.876s-13.212-6.25-13.212-13.876c0-7.705 5.792-13.916 13.212-13.916m33.201 12.933l9.086 14.467h-5.755l-6.207-10.418-6.21 10.418h-5.714l9.047-14.467-8.178-12.619h5.792l5.263 8.609 5.261-8.609h5.792zm16.432-12.619h19.57v4.6h-7.343V48.17h-4.883V25.685h-7.344z"
          fill="currentColor" fill-rule="evenodd" />
      </symbol>
      <symbol id="logo-trustpilot" viewBox="0 0 516.085 58.186">
        <path
          d="M119.272 3.316c0 .558-.029 1.059-.088 1.472-.068.425-.161.757-.296 1.012a1.5 1.5 0 01-.487.558 1.243 1.243 0 01-.655.177h-16.289v50.214c0 .245-.063.449-.181.63-.119.178-.33.322-.626.423a7.134 7.134 0 01-1.184.275c-.494.064-1.113.11-1.856.11-.715 0-1.329-.047-1.836-.11a7.14 7.14 0 01-1.213-.275c-.296-.101-.5-.245-.622-.423a1.107 1.107 0 01-.178-.63V6.534H77.464c-.238 0-.449-.058-.648-.177a1.317 1.317 0 01-.466-.558c-.122-.254-.219-.587-.296-1.012a8.8 8.8 0 01-.106-1.472c0-.57.034-1.07.106-1.5.077-.438.174-.787.296-1.054.119-.263.275-.462.466-.584A1.28 1.28 0 0177.464 0h40.282c.241 0 .456.064.655.178.187.122.352.322.487.584.135.266.228.616.296 1.054.059.43.088.929.088 1.5zm49.414 53.432c0 .245-.042.449-.144.63-.084.178-.275.331-.575.439-.301.127-.714.211-1.252.277a20.77 20.77 0 01-2.149.092c-.778 0-1.412-.034-1.903-.092-.49-.065-.885-.161-1.184-.294a1.527 1.527 0 01-.694-.561 3.281 3.281 0 01-.431-.898l-5.32-13.648a52.507 52.507 0 00-1.946-4.273c-.673-1.294-1.472-2.411-2.398-3.357a9.792 9.792 0 00-3.265-2.188c-1.252-.524-2.757-.786-4.517-.786h-5.152v24.657a.99.99 0 01-.203.63c-.135.178-.338.322-.626.423a6.48 6.48 0 01-1.158.275 14.56 14.56 0 01-1.862.11c-.744 0-1.361-.047-1.852-.11a7.172 7.172 0 01-1.193-.275c-.296-.101-.498-.245-.626-.423a1.144 1.144 0 01-.173-.63V3.138c0-1.167.301-1.979.918-2.444.609-.46 1.255-.694 1.944-.694h12.304c1.463 0 2.681.043 3.65.114.968.069 1.843.161 2.618.251 2.237.384 4.212.997 5.93 1.831 1.714.833 3.156 1.891 4.323 3.18a12.81 12.81 0 012.614 4.407c.583 1.649.872 3.482.872 5.478 0 1.947-.263 3.68-.787 5.219-.519 1.532-1.268 2.889-2.254 4.069a14.936 14.936 0 01-3.536 3.067c-1.37.87-2.91 1.597-4.614 2.194a10.79 10.79 0 012.601 1.582 14.124 14.124 0 012.17 2.306c.668.901 1.302 1.93 1.908 3.101a50.655 50.655 0 011.785 3.937l5.19 12.748c.419 1.075.685 1.833.808 2.259.111.435.179.768.179 1.005zm-11.594-40.68c0-2.267-.508-4.187-1.523-5.752-1.015-1.561-2.716-2.691-5.101-3.376-.749-.21-1.59-.357-2.529-.452-.943-.089-2.17-.131-3.693-.131h-6.492v19.515h7.521c2.03 0 3.776-.251 5.261-.741 1.476-.496 2.707-1.18 3.688-2.061a8.044 8.044 0 002.174-3.108c.462-1.192.694-2.493.694-3.894zm63.173 20.15c0 3.463-.498 6.557-1.514 9.272-1.011 2.723-2.458 5.016-4.344 6.897-1.878 1.878-4.17 3.299-6.873 4.268-2.694.978-5.769 1.461-9.203 1.461-3.148 0-6.006-.456-8.595-1.367-2.579-.909-4.8-2.253-6.653-4.034-1.849-1.777-3.27-4.006-4.268-6.667-.998-2.676-1.497-5.76-1.497-9.249V1.413c0-.237.055-.437.173-.622.118-.174.325-.312.613-.414a6.39 6.39 0 011.151-.263c.466-.077 1.083-.114 1.849-.114.706 0 1.307.037 1.806.114.495.069.888.161 1.167.263.275.102.478.24.597.414.118.185.177.385.177.622v34.45c0 2.648.321 4.952.965 6.916.651 1.971 1.582 3.607 2.799 4.913a11.54 11.54 0 004.408 2.955c1.713.658 3.646.993 5.795.993 2.199 0 4.161-.318 5.874-.976 1.725-.64 3.176-1.608 4.37-2.902 1.188-1.296 2.094-2.897 2.723-4.81.639-1.898.952-4.131.952-6.689V1.413a1.1 1.1 0 01.178-.622c.118-.174.322-.312.613-.414a6.463 6.463 0 011.167-.263c.486-.077 1.1-.114 1.832-.114.706 0 1.298.037 1.78.114a5.78 5.78 0 011.146.263c.284.102.486.24.621.414.132.185.191.385.191.622v34.805zm43.425 5.257c0 2.657-.495 5.018-1.468 7.082a15.214 15.214 0 01-4.057 5.255c-1.733 1.444-3.772 2.53-6.115 3.258-2.348.724-4.864 1.092-7.576 1.092-1.895 0-3.645-.157-5.256-.488-1.62-.316-3.059-.705-4.327-1.174-1.265-.466-2.331-.944-3.189-1.438-.858-.496-1.45-.915-1.781-1.266-.343-.35-.592-.799-.748-1.331-.161-.541-.242-1.264-.242-2.166 0-.638.029-1.167.089-1.59.055-.423.144-.767.262-1.029.114-.262.262-.443.431-.546.177-.101.385-.151.613-.151.406 0 .985.251 1.726.741.744.498 1.695 1.04 2.863 1.614 1.163.589 2.568 1.135 4.208 1.633 1.646.512 3.545.77 5.698.77 1.628 0 3.126-.211 4.479-.655 1.353-.439 2.518-1.057 3.498-1.853a8.184 8.184 0 002.245-2.947c.516-1.163.778-2.487.778-3.972 0-1.606-.363-2.977-1.082-4.107-.732-1.137-1.688-2.132-2.885-2.992a25.185 25.185 0 00-4.086-2.354 1099.15 1099.15 0 00-4.69-2.186 48.416 48.416 0 01-4.674-2.467 19.698 19.698 0 01-4.057-3.185c-1.197-1.223-2.166-2.661-2.91-4.301-.741-1.642-1.112-3.617-1.112-5.913 0-2.364.431-4.462 1.294-6.31a12.85 12.85 0 013.577-4.653c1.528-1.248 3.347-2.208 5.458-2.859 2.105-.66 4.389-.986 6.83-.986 1.253 0 2.508.114 3.781.325 1.261.221 2.457.516 3.577.876 1.122.364 2.117.774 2.995 1.218.863.452 1.447.82 1.718 1.1.28.275.461.49.545.651.094.161.166.368.221.61.058.249.106.546.131.9.029.347.05.796.05 1.345 0 .529-.03.994-.072 1.401-.039.41-.11.757-.199 1.032-.085.27-.212.482-.369.609a.866.866 0 01-.544.198c-.327 0-.82-.207-1.511-.617a50.947 50.947 0 00-2.508-1.375c-.99-.509-2.166-.969-3.511-1.396-1.358-.418-2.881-.635-4.568-.635-1.568 0-2.94.217-4.102.635-1.163.427-2.128.99-2.88 1.678a6.733 6.733 0 00-1.705 2.487 8.38 8.38 0 00-.567 3.063c0 1.569.364 2.919 1.092 4.06.728 1.137 1.695 2.141 2.901 3.011a25.893 25.893 0 004.129 2.397c1.543.728 3.117 1.468 4.715 2.214a61.558 61.558 0 014.716 2.439 20.155 20.155 0 014.132 3.144 14.43 14.43 0 012.914 4.284c.753 1.646 1.125 3.583 1.125 5.82zm47.812-38.167c0 .566-.03 1.057-.089 1.468-.064.419-.156.76-.291 1.01a1.38 1.38 0 01-.492.558 1.184 1.184 0 01-.649.177H293.72v50.13c0 .238-.058.449-.181.623-.116.181-.327.325-.624.43a6.774 6.774 0 01-1.183.262c-.496.077-1.114.116-1.853.116-.719 0-1.332-.039-1.831-.116a6.644 6.644 0 01-1.206-.262c-.3-.105-.507-.249-.629-.43a1.107 1.107 0 01-.179-.623V6.521h-16.261c-.236 0-.453-.054-.648-.177a1.245 1.245 0 01-.469-.558 4.477 4.477 0 01-.293-1.01 8.601 8.601 0 01-.113-1.468c0-.563.041-1.071.113-1.492.078-.438.179-.791.293-1.054.119-.263.275-.462.469-.584.196-.114.412-.178.648-.178h40.208c.238 0 .458.064.649.178.19.122.364.322.492.584.135.263.227.616.291 1.054.059.422.089.929.089 1.492z"
          fill="#231f20" />
        <path
          d="M356.234 16.96c0 2.897-.477 5.507-1.438 7.838-.958 2.326-2.317 4.314-4.095 5.947-1.773 1.646-3.951 2.915-6.535 3.808-2.581.898-5.654 1.341-9.243 1.341h-6.575v20.855c0 .245-.073.449-.204.63-.135.178-.341.322-.625.423a6.36 6.36 0 01-1.169.275c-.485.064-1.106.11-1.851.11-.744 0-1.367-.047-1.852-.11a6.825 6.825 0 01-1.193-.275c-.303-.101-.508-.245-.626-.423a1.085 1.085 0 01-.179-.63V3.316c0-1.193.313-2.042.94-2.554.627-.508 1.323-.762 2.106-.762h12.393c1.258 0 2.454.051 3.607.161 1.145.101 2.508.325 4.065.669 1.57.342 3.169.987 4.797 1.924a15.14 15.14 0 014.136 3.468c1.138 1.374 2.009 2.96 2.618 4.767.613 1.806.923 3.794.923 5.971zm-8.104.626c0-2.356-.44-4.322-1.324-5.899-.876-1.583-1.966-2.767-3.265-3.541-1.299-.774-2.64-1.269-4.023-1.475a28.007 28.007 0 00-4.056-.313h-7.114v23.224h6.937c2.326 0 4.259-.299 5.793-.896 1.536-.6 2.829-1.418 3.87-2.48 1.044-1.06 1.838-2.326 2.378-3.802.533-1.476.804-3.084.804-4.818zm26.316 39.125c0 .236-.058.447-.178.621s-.326.318-.626.419c-.296.107-.684.194-1.179.271-.482.073-1.104.107-1.844.107-.711 0-1.32-.034-1.823-.107-.508-.077-.91-.165-1.201-.271-.296-.101-.508-.245-.627-.419a1.118 1.118 0 01-.174-.621V1.426c0-.241.068-.45.195-.626.135-.174.355-.318.672-.415a6.626 6.626 0 011.197-.271A12.458 12.458 0 01370.619 0c.74 0 1.361.043 1.844.114.495.069.884.165 1.179.271.3.097.507.241.626.415.12.176.178.384.178.626v55.285zm44.254-1.878c0 .587-.026 1.09-.084 1.492-.061.402-.157.754-.297 1.032-.13.282-.291.496-.486.621-.19.136-.422.208-.698.208h-25.602c-.685 0-1.327-.232-1.941-.698-.609-.462-.914-1.277-.914-2.44V1.434c0-.237.052-.449.175-.626.123-.178.329-.322.622-.423a6.647 6.647 0 011.215-.271A13.257 13.257 0 01392.527 0c.741 0 1.358.043 1.852.114.495.073.885.165 1.185.271.299.101.508.245.626.423.122.178.182.39.182.626v50.081h20.763c.276 0 .508.067.698.204.195.131.356.327.486.58.14.254.237.587.297 1.01.058.415.084.922.084 1.524zm52.438-26.458c0 4.552-.54 8.668-1.615 12.331-1.075 3.658-2.681 6.772-4.818 9.347-2.132 2.566-4.813 4.543-8.035 5.923-3.223 1.382-6.992 2.07-11.285 2.07-4.243 0-7.912-.63-11.006-1.889-3.091-1.27-5.654-3.104-7.673-5.521-2.017-2.407-3.523-5.401-4.509-8.975-.987-3.576-1.489-7.686-1.489-12.33 0-4.445.545-8.481 1.616-12.122 1.074-3.626 2.69-6.715 4.843-9.259 2.149-2.546 4.835-4.499 8.063-5.88C438.457.694 442.212 0 446.513 0c4.154 0 7.77.626 10.853 1.874 3.074 1.252 5.639 3.075 7.691 5.468 2.045 2.399 3.567 5.363 4.571 8.89 1.002 3.529 1.51 7.576 1.51 12.143zm-7.93.522c0-3.198-.288-6.168-.851-8.917-.57-2.74-1.515-5.122-2.833-7.144-1.318-2.016-3.084-3.598-5.287-4.731-2.205-1.126-4.936-1.696-8.185-1.696-3.248 0-5.977.612-8.184 1.83-2.204 1.223-3.994 2.843-5.372 4.878-1.382 2.038-2.366 4.413-2.96 7.125-.597 2.721-.892 5.575-.892 8.565 0 3.312.274 6.362.828 9.158.55 2.779 1.481 5.186 2.782 7.202 1.309 2.023 3.059 3.596 5.247 4.709 2.189 1.12 4.939 1.679 8.255 1.679 3.273 0 6.031-.61 8.268-1.833 2.229-1.221 4.031-2.863 5.394-4.943 1.36-2.081 2.335-4.485 2.913-7.207.58-2.732.877-5.627.877-8.675zm52.877-25.581c0 .558-.03 1.059-.093 1.472-.055.425-.156.757-.293 1.012-.132.25-.295.436-.487.558a1.212 1.212 0 01-.646.177h-16.287v50.214c0 .245-.064.449-.192.63-.113.178-.321.322-.62.423-.3.103-.693.198-1.189.275-.485.064-1.108.11-1.858.11a14.82 14.82 0 01-1.831-.11 6.953 6.953 0 01-1.208-.275c-.298-.101-.508-.245-.625-.423a1.1 1.1 0 01-.18-.63V6.534h-16.3c-.229 0-.444-.058-.639-.177a1.322 1.322 0 01-.476-.558 4.487 4.487 0 01-.289-1.012 8.267 8.267 0 01-.11-1.472c0-.57.034-1.07.11-1.5a4.94 4.94 0 01.289-1.054c.121-.263.281-.462.476-.584.195-.113.41-.177.639-.177h40.289c.238 0 .45.064.646.178.193.122.355.322.487.584.137.266.237.616.293 1.054.064.43.094.929.094 1.5z"
          fill="#929497" />
        <path d="M0 .012v25.665s19.433 9.25 29.297 25.788c0 0 4.352-36.523 28.785-51.453H0z" fill="#f9a220" />
        <linearGradient id="logo-trustpilota" gradientUnits="userSpaceOnUse" x1="29.041" y1="58.186" x2="29.041"
          y2="25.006">
          <stop offset="0" stop-color="#e37a27" />
          <stop offset=".472" stop-color="#f9a220" />
        </linearGradient>
        <path
          d="M0 46.824v11.363h26.11c0-.001-9.947-10.593-26.11-11.363zm33.118 11.362h24.964v-33.18c-14.128 3.827-24.964 33.18-24.964 33.18z"
          fill="url(#logo-trustpilota)" />
        <linearGradient id="logo-trustpilotb" gradientUnits="userSpaceOnUse" x1="28.926" y1="22.669" x2="28.926"
          y2=".394">
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#f9a220" />
        </linearGradient>
        <path d="M.388.401L57.465.395s-9.366 3.308-19.46 21.915c0 0-22.076 2.246-37.564-4.496L.388.401z" opacity=".5"
          fill="url(#logo-trustpilotb)" />
        <linearGradient id="logo-trustpilotc" gradientUnits="userSpaceOnUse" x1="29.041" y1=".242" x2="29.041"
          y2="57.957">
          <stop offset="0" stop-color="#4a484a" />
          <stop offset="1" />
        </linearGradient>
        <path
          d="M29.297 51.466C19.433 34.927 0 25.677 0 25.677v21.146c16.128.794 26.11 11.363 26.11 11.363h7.008s10.836-29.353 24.964-33.18V.012l-.074.045C33.641 15.015 29.297 51.466 29.297 51.466z"
          fill="url(#logo-trustpilotc)" />
      </symbol>
      <symbol id="logo-vokera" viewBox="0 0 379.97 99.62">
        <g data-name="Layer 2">
          <g data-name="Layer 1">
            <path fill="#8c8c8c"
              d="M233.87 68.92c1.15 10.21 8.91 16.39 18.55 16.39 8.63 0 14.23-4 18.55-9.35l12.36 9.35c-8.05 9.92-18.26 13.8-29.19 13.8-20.85 0-37.52-14.52-37.52-36.23s16.68-36.26 37.52-36.26c19.27 0 32.35 13.51 32.35 37.52v4.78zm35.37-12.94c-.14-10.06-6.76-16.36-17.54-16.36-10.21 0-16.53 6.47-17.83 16.39z" />
            <path fill="#999"
              d="M233.87 68.92c1.15 10.21 8.91 16.39 18.55 16.39 8.63 0 14.23-4 18.55-9.35l12.36 9.35c-8.05 9.92-18.26 13.8-29.19 13.8-20.85 0-37.52-14.52-37.52-36.23s16.68-36.26 37.52-36.26c19.27 0 32.35 13.51 32.35 37.52v4.78zm35.37-12.94c-.14-10.06-6.76-16.36-17.54-16.36-10.21 0-16.53 6.47-17.83 16.39z" />
            <path fill="#999" d="M173.1 62.54l.19.19 35.66 35.04h20.49L193.6 62.54l35.84-35.22h-20.5l-35.65 35.04" />
            <path fill="#93c01f" fill-rule="evenodd" d="M219.89 0h20.61l22.2 22.2h-20.61L219.89 0z" />
            <path fill="#999"
              d="M0 1h19.83l26.38 73.15L73.42 1H92L52.9 97.8H38zM155.66.82h15.98v96.95h-15.98zm224.21 56.67c0-.63 0-1.29-.07-2.3-.22-8.74-3.62-16.57-11.1-22s-17.63-8.47-28.21-8.47-20.73 3-28.21 8.47a33.38 33.38 0 00-4.94 4.36v-9.17h-17.25v69h17.25V60.44c0-.79.18-2.54.43-4.73a21.7 21.7 0 018.44-12.23 36.05 36.05 0 0121.49-6.7c8.06 0 16.06 1.62 21.53 6.88 2.9 2.78 4 6.76 4.38 10.78H360c-17.25 0-43.42 1.58-43.42 24 0 13.66 12.36 20.7 25.16 20.7 9.2 0 17.25-3.45 22.14-11.21h.43v9.49h15.53V57.74c.04-.12.03-.12.03-.25zm-16.52 12.9c0 9.63-5.61 15.82-17.4 15.82-5.61 0-12.08-2.73-12.08-9.06 0-9.92 16.39-10.64 25.88-10.64h3.59z" />
            <path fill="#6e6e6e" d="M233.8 69.03l-1.62-1.44h52.75l1.56 1.36-52.69.08z" />
            <path fill="#cfcfcf" d="M233.8 55.98l-1.41 1.41 38.87.1-2.12-1.59-35.34.08z" />
            <path fill="#6e6e6e"
              d="M270.96 75.98l.3 2.12 9.42 7.41 2.62-.2-12.34-9.33zM252 38.11c-10.92-.1-19.49 7.45-19.59 19.28l1.41-1.41s1-11.22 10.71-15.21c0 0 8.83-3.28 17.51 1.33a15.7 15.7 0 017.12 13.8l1.92 1.29c-.33-13.67-8.35-18.98-19.08-19.08z" />
            <path fill="url(#logo-vokeraa)"
              d="M266.47 84.89a21.88 21.88 0 01-11.76 4.71c-9.16.84-14.36-3.92-14.36-3.92-5.9-4.67-6.56-12.27-6.56-12.27L232 71.87c.92 13.47 10.8 19.28 20.41 19.28 8.63 0 14.56-3.35 18.87-8.67l-.28-2.12a31.26 31.26 0 01-4.53 4.53z"
              transform="translate(0 -4.38)" />
            <path fill="#6e6e6e"
              d="M254.17 97.53c-21.79.24-36.13-16.17-35.89-34.76l-1.65-.15a37.18 37.18 0 003.13 15.5 34.8 34.8 0 009.91 12.81 38.21 38.21 0 0016.87 7.49 41.55 41.55 0 0016-.18 34.8 34.8 0 0015.24-7.27 48 48 0 005.55-5.68l-2.73.32c-7.41 8.37-15.6 11.8-26.43 11.92z" />
            <path fill="url(#logo-vokerab)"
              d="M284.42 53.52A32.07 32.07 0 00275.81 39a31.15 31.15 0 00-10.13-6.12l-.62 1.85c12.48 4.56 20 16.95 19.83 33.66l-.08 3.34h-.15l1.82 1.59s.69-12.12-2.06-19.8z"
              transform="translate(0 -4.38)" />
            <path opacity=".82" fill="url(#logo-vokerac)"
              d="M218.28 66.89c.4-20.06 16.71-33.82 34.94-34.06a33.09 33.09 0 0111.85 1.91l.62-1.85s-7.93-3.43-20.21-1a39.17 39.17 0 00-16.38 7.75 36.94 36.94 0 00-9.78 13.43 39.71 39.71 0 00-2.69 14l1.66.13c-.01-.14-.01-.2-.01-.31z"
              transform="translate(0 -4.38)" />
            <path fill="#cfcfcf"
              d="M.01.99l2.11 1.5 36.57 93.65-.66 1.65L.01.99zM73.42.95l-27.3 73.18.04 4.34 28.8-75.99L73.42.95z" />
            <path fill="#cfcfcf" d="M.01.99h19.71L18.38 2.5 2.12 2.49.01.99zm92-.04H73.42l1.54 1.53 14.52.01L92.01.95" />
            <path fill="#fff" opacity=".49" d="M240.5 0l-1.21 1.6-15.6-.08 18.96 18.96-.56 1.72L219.89 0h20.61z" />
            <path fill="#cfcfcf" d="M155.65.8v96.94l1.7-1.71V2.41L155.65.8z" />
            <path fill="#cfcfcf"
              d="M155.65.8l1.7 1.61 12.73.02L171.7.8h-16.05zm53.23 26.42l-35.76 35.33 2.24-.06 34.68-33.66-1.16-1.61z" />
            <path fill="#cfcfcf"
              d="M229.35 27.35h-20.47l1.16 1.48 15.51.1 3.8-1.58zm60.81 1.11l1.5 1.59.07 65.68-1.64 1.66.07-68.93zm0 0h17.17l-1.49 1.59h-14.18l-1.5-1.59z" />
            <path fill="url(#logo-vokerad)"
              d="M307.33 41.91s9.12-12.05 32.19-12.81c0 0 16.56-1 29.64 8.8a26 26 0 0110.54 20.68h-1.6s0-13.86-11.82-21c0 0-13.26-8.76-30-7 0 0-16.78.68-27.26 11.42a25.19 25.19 0 00-3.59 5.09z"
              transform="translate(0 -4.38)" />
            <path fill="#6e6e6e"
              d="M38.03 97.79l.66-1.65h12.82L89.95 2.13 92.01.95 52.87 97.8l-14.84-.01zM19.82.98l-1.38 1.48 27.72 75.89v-4.4L19.82.98z" />
            <path fill="#6e6e6e" opacity=".25" d="M240.5 0l-1.4 1.4 19.07 19.07-15.52.01-.56 1.72h20.61L240.5 0z" />
            <path fill="#6e6e6e"
              d="M191.78 62.39l33.77-33.46 3.78-1.49-35.7 35.09m-20.52.07l2.25-.11 34.07 33.77 16.22.1 3.81 1.36-20.49.11-35.86-35.23zM171.65.81v96.91l-15.99.01 1.75-1.73 12.69.02-.06-93.59 1.61-1.62z" />
            <path fill="#6e6e6e"
              d="M192.09 62.49l34.07 34.08 3.2 1.17-35.74-35.25h-1.53zm187.74-.74s.08-4.35-.13-7.55h-1.61s.23 4.06.14 6.34v35.31l1.74 1.59z" />
            <path opacity=".52" fill="#1d1d1b" d="M379.97 97.44l-1.74-1.59h-12.22l-1.71 1.58 15.67.01z" />
            <path fill="#6e6e6e" d="M363.93 87.92h.43v9.5l1.65-1.62-.2-10.85-1.88 2.97z" />
            <path fill="url(#logo-vokerae)"
              d="M365 59.94l-1.35-1.12s-13.22-.2-19.93.88a54.64 54.64 0 00-9.37 1.88c-6.19 1.81-13.15 5.46-16 12.38a22.49 22.49 0 00-1.72 9.11 18.79 18.79 0 005 12.69l1.28-.92s-8.93-8.75-2.33-22.24c0 0 4.75-9.68 21.78-11.54A162.32 162.32 0 01365 59.94z"
              transform="translate(0 -4.38)" />
            <path fill="#6e6e6e"
              d="M322.84 90.46s6.72 8.64 23.42 7a26 26 0 0015-8.06 30.57 30.57 0 004.61-6.32l-.1 1.86-1.88 3a23.35 23.35 0 01-9.94 8.77s-4.9 2.91-14.42 2.41c0 0-11-.23-18-7.72zm-32.77 6.93l1.66-1.66h14.03l1.63 1.64-17.32.02z" />
            <path opacity=".5" fill="#6e6e6e" d="M307.31 28.39l-1.94 1.94v12.51s1.16-3.33 2-5.24z" />
            <path fill="#6e6e6e"
              d="M363.43 66.52s-19.81-1-26.43 4.48a7.74 7.74 0 00-2.93 8.22s.86 3.91 5.94 5.94l-.63 1.17s-9.06-3.94-6.36-11.9c0 0 1.46-6.29 13.08-8.29 0 0 1.8-.51 11.69-1h7z" />
            <path fill="url(#logo-vokeraf)"
              d="M363.32 70.9v5.13a15.67 15.67 0 01-3.43 9.45 15.89 15.89 0 01-10.53 4.93 19.78 19.78 0 01-9.35-.86l-.63 1.17a18.38 18.38 0 005.8 1.07A24.88 24.88 0 00356 90s5.37-1.77 7.68-8.56c0 0 1.36-2.79 1.16-11.93z"
              transform="translate(0 -4.38)" />
            <path fill="#b1b1b1"
              d="M112.67 26.62c-20.93 0-37.9 16.34-37.9 36.5s17 36.5 37.9 36.5 37.9-16.34 37.9-36.5-16.97-36.5-37.9-36.5zm-.08 59a22.84 22.84 0 1122.84-22.84 22.84 22.84 0 01-22.84 22.84z" />
            <path fill="#6e6e6e"
              d="M363.74 54.3s-1-4.79-1.46-5.9c0 0-1.48-6.19-11.1-9.62a42.48 42.48 0 00-25.59-.16s-10.28 3.08-15.25 11.06a19.66 19.66 0 00-1.63 3.06 13.18 13.18 0 00-1 3.62c-.09.6-.17 1.23-.19 1.7a21.06 21.06 0 00-.16 2.23h-1.47s0-.49.28-3.25a20.55 20.55 0 015.64-11.55c7.3-7.88 19.35-9.49 17.6-9.15a41.61 41.61 0 0120.19.58s7.45 2.06 10.94 6.24a16 16 0 013.75 7.77s.51 3.2.7 4.67l-1.29-1.07" />
            <path fill="#6e6e6e"
              d="M305.87 60.25l-.11 35.48 1.58 1.62V60.29l-1.47-.04zm-193.2-33.63c-20.93 0-37.9 16.34-37.9 36.5s17 36.5 37.9 36.5 37.9-16.34 37.9-36.5-16.97-36.5-37.9-36.5zm0 71.55c-20.11 0-36.41-15.7-36.41-35.06s16.3-35.06 36.41-35.06 36.41 15.7 36.41 35.06-16.3 35.07-36.41 35.07z" />
            <path fill="#6e6e6e"
              d="M112.59 38.5a24.25 24.25 0 1024.25 24.25 24.25 24.25 0 00-24.25-24.25zm0 47.09a22.84 22.84 0 1122.84-22.84 22.84 22.84 0 01-22.84 22.87z" />
            <path fill="url(#logo-vokerag)"
              d="M82.67 87.37a34 34 0 01-6.41-19.87c0-19.36 16.3-35.06 36.41-35.06a37.25 37.25 0 0119.48 5.45l1.06-1.06A38.77 38.77 0 00112.67 31c-20.93 0-37.9 16.34-37.9 36.5a35.4 35.4 0 006.85 20.92z"
              transform="translate(0 -4.38)" />
            <path fill="url(#logo-vokerah)" d="M129.3 51.57A22.84 22.84 0 0197 83.84l-1 1a24.24 24.24 0 0034.3-34.27z"
              transform="translate(0 -4.38)" />
            <path fill="#6e6e6e" d="M364.36 97.33l1.64-1.64h12.24l1.69 1.7" />
          </g>
        </g>
      </symbol>
      <symbol id="logo-worcesterbosch" viewBox="0 0 156 43">
        <g fill="none" fill-rule="evenodd">
          <g transform="translate(0 22.829)">
            <mask id="logo-worcesterboscha" fill="#fff">
              <path d="M44.694.008H0v50.156h155.796V.008H0v23.936h44.694V.008z" />
            </mask>
            <path
              d="M31.251-27.16s3.535-2.712 7.032-2.798c0 0 7.031-.896 6.367 6.38 0 0-1.962-6.287-10.635-1.05L13.445-8.862l-.005-.028s-3.533 2.714-7.03 2.8c0 0-7.031.896-6.367-6.381 0 0 1.95 6.271 10.635 1.019l.01-.046 20.562-15.66z"
              fill="#0051A0" mask="url(#logo-worcesterboscha)" />
          </g>
          <path
            d="M31.638 3.173S35.173.461 38.671.376c0 0 7.032-.896 6.366 6.38 0 0-1.962-6.287-10.635-1.051L13.833 21.473l-.006-.028s-3.533 2.712-7.028 2.799c0 0-7.032.895-6.366-6.381 0 0 1.949 6.27 10.635 1.017l.01-.046 20.56-15.66z"
            fill="#E30620" />
          <path
            d="M37.14 24.304h4.316l1.223-6.238c.36-2.005.432-2.478.63-4.46.287 2.185.377 2.748.79 4.933l1.151 5.765h4.298l3.163-15.901h-3.9l-1.26 7.05c-.287 1.779-.36 2.275-.539 4.031-.162-1.71-.215-2.184-.593-4.167l-1.384-6.914h-3.4l-1.312 6.914c-.36 1.937-.412 2.41-.63 4.19-.143-1.757-.215-2.23-.557-4.166l-1.24-6.938h-3.92l3.164 15.901zm36.995-12.59c.702 0 1.962.045 1.962 1.87 0 1.801-1.277 1.845-1.943 1.845H72.48v-3.715h1.654zm-5.682 12.59h4.028v-5.608h1.187l2.465 5.608h4.458l-3.093-6.419c.593-.293 1.133-.54 1.637-1.216.917-1.171.97-2.59.97-3.198 0-1.532-.503-3.694-2.192-4.595-.774-.406-1.924-.496-3.13-.496h-6.33v15.924zm27.117 0h10.59V20.86h-6.562v-3.154h5.627V14.26h-5.627v-2.365h6.023V8.403H95.57zm24.059-11.982h3.218v11.982h4.008V12.322h3.22V8.38h-10.446zm11.356 11.982h10.588V20.86h-6.562v-3.154h5.628V14.26h-5.628v-2.365h6.023V8.403h-10.05z"
            fill="#0051A0" />
          <g transform="translate(143.487 7.878)">
            <mask id="logo-worcesterboschb" fill="#fff">
              <path d="M12.31 16.427V.502H.175v15.925H12.31z" />
            </mask>
            <path
              d="M5.854 3.836c.702 0 1.961.045 1.961 1.87 0 1.801-1.276 1.846-1.941 1.846H4.202V3.836h1.652zm-5.68 12.59h4.028v-5.608h1.185l2.463 5.609h4.46l-3.092-6.42c.591-.293 1.132-.54 1.636-1.215.916-1.172.97-2.591.97-3.2 0-1.53-.503-3.693-2.193-4.594C8.857.592 7.708.502 6.502.502H.175v15.925z"
              fill="#0051A0" mask="url(#logo-worcesterboschb)" />
          </g>
          <path
            d="M91.301 13.604c-.655-1.14-1.718-1.881-2.92-1.881-1.988 0-3.6 2.03-3.6 4.538 0 2.506 1.612 4.539 3.6 4.539 1.17 0 2.18-.716 2.838-1.806l3.111 2.012c-1.306 2.162-3.485 3.575-5.948 3.575-3.999 0-7.24-3.724-7.24-8.32 0-4.595 3.241-8.32 7.24-8.32 0 0 3.432-.463 5.703 3.697l-2.784 1.966zm15.66 8.073c.596.88 1.322 1.776 3.17 2.584.997.435 2.07.473 3.058.473.728 0 3.378 0 5.038-2.05.596-.743 1.29-1.919 1.173-3.583-.266-3.802-3.664-4.142-6.592-4.75-1.137-.248-1.461-.75-1.461-1.517 0-.81.926-1.503 1.636-1.503.67 0 1.702.074 2.467.523.577.362.74.584.945.9l2.313-2.4c-.466-.586-.502-.769-1.492-1.354-1.082-.654-2.31-1.166-3.821-1.166-2.369 0-4.685.978-5.544 3.322a5.86 5.86 0 00-.372 2.051c0 3.357 2.465 4.204 3.68 4.587.538.157 2.976.597 3.443.8.727.293.719 1.369.7 1.48-.173 1.08-1.626 1.188-1.962 1.188a5.003 5.003 0 01-1.847-.36c-1.139-.452-1.648-1.071-2.133-1.663l-2.399 2.438zM59.912 7.819c-4.754 0-7.083 3.97-7.083 8.475 0 3.59 1.621 8.364 7.008 8.364 5.163 0 7.122-4.551 7.122-8.543 0-3.745-1.847-8.296-7.047-8.296m-.012 3.97c.436 0 2.307.068 3.046 2.273.218.631.284 1.33.284 2.094 0 2.993-1.371 4.592-3.33 4.592-2.306 0-3.393-1.666-3.393-4.435 0-3.286 1.652-4.524 3.393-4.524M38.502 37.372h1.788c.35 0 .513.023.666.082.234.082.572.316.572.925 0 .34-.129.527-.222.632-.269.282-.631.34-1.367.363h-1.437v-2.002zm-1.963-4.766v8.267h3.832c.794-.023 1.589-.058 2.267-.516a2.145 2.145 0 00.982-1.79c0-.41-.117-.715-.187-.856-.2-.409-.597-.854-1.566-1.03a2.567 2.567 0 00-.328-.058c.375-.082.492-.117.69-.2.362-.163 1.075-.62 1.075-1.697 0-.622-.233-1.008-.397-1.219-.561-.749-1.472-.842-2.325-.89-.21 0-.433-.01-.655-.01H36.54zm1.963 1.546h1.776c.233 0 .362.023.467.058.503.164.537.586.537.761 0 .188-.046.329-.07.387-.069.188-.222.398-.689.48a5.75 5.75 0 01-.467.035h-1.554v-1.721zm9.558.432c-1.543-.093-2.384.54-2.804 1.043-.69.831-.713 1.828-.713 2.179 0 1.277.513 2.026.876 2.377.84.82 2.08.878 2.395.878 1.145 0 1.858-.422 2.209-.715.42-.351 1.074-1.146 1.074-2.528 0-.984-.362-1.957-1.121-2.577-.573-.457-1.204-.621-1.916-.657m-.352 1.418c.48-.024.76.117.924.234.35.246.642.715.642 1.64 0 .913-.443 1.744-1.437 1.744-.677 0-1.028-.386-1.168-.597-.117-.186-.315-.632-.315-1.229 0-.902.385-1.418.712-1.616.257-.14.562-.165.642-.176m8.355.503c-.105-.082-.21-.164-.327-.234-.327-.199-.818-.374-1.204-.374-.257 0-.49.058-.572.105-.222.093-.28.258-.28.363 0 .117.07.221.163.293.175.117.619.187.818.222l.806.176c.959.246 1.811.703 1.811 1.838 0 .504-.187.867-.292 1.02-.538.807-1.659 1.124-2.582 1.124-.327 0-1.343-.06-2.232-.609-.362-.222-.608-.468-.725-.575l1.146-1.03c.105.106.21.211.327.292.397.317.946.551 1.45.551.08 0 .245-.011.442-.059.27-.058.632-.21.632-.55-.012-.422-.549-.468-1.203-.574-1.274-.21-1.87-.504-2.174-1.112a1.58 1.58 0 01-.187-.761c0-.163.035-.528.257-.913.28-.504.97-1.136 2.325-1.136a4.54 4.54 0 012.209.585c.175.105.338.21.502.327l-1.11 1.031zm6.217.317a2.055 2.055 0 00-.2-.352c-.104-.13-.444-.504-.993-.504-.128 0-.385.012-.654.212-.62.468-.642 1.51-.642 1.627 0 .528.175 1.206.584 1.545.116.095.373.294.77.294a1.25 1.25 0 001.006-.551c.058-.094.105-.163.175-.363l1.355.703a5.28 5.28 0 01-.42.586 2.99 2.99 0 01-2.29 1.054c-.27 0-.807-.035-1.426-.34-1.145-.562-1.601-1.65-1.601-2.87 0-.28.012-.866.303-1.544.69-1.606 2.198-1.734 2.758-1.734.48 0 1.555.094 2.372 1.124.07.094.117.164.234.352l-1.332.761zm3.96-1.417c.036-.035.187-.21.35-.327a2.248 2.248 0 011.38-.482c.817 0 1.285.387 1.483.609.129.14.235.317.293.493.105.269.14.573.163 1.158v4.017h-1.787v-3.197c-.012-.467.023-1.089-.422-1.393a.876.876 0 00-.502-.153 1.02 1.02 0 00-.537.165c-.385.27-.444.69-.432 1.534v3.044h-1.8v-8.267h1.8l.012 2.799zm14.22-.222c-.036-.06-.268-.434-.526-.657-.525-.48-1.157-.503-1.39-.503-.152 0-.7.012-1.134.294-.49.315-1.028 1.04-1.028 2.365 0 .175 0 1.334.573 2.036.21.258.49.398.595.458.152.07.527.222.993.222.141 0 .398 0 .713-.105.164-.06.666-.258.911-.762.129-.257.165-.609.176-.68h-1.846v-1.568h3.587v4.59h-1.612v-.76c-.222.199-.397.34-.502.409-.772.516-1.601.54-1.93.54-.291 0-.898-.024-1.587-.352-1.496-.727-2.127-2.377-2.127-3.97 0-.61.117-2.81 2.032-3.888.34-.2 1.029-.526 2.034-.526 1.542 0 2.489.726 2.92 1.112.293.257.48.491.714.796l-1.566.949zm6.45 1.1l-.596.094c-.059.011-.34.047-.56.128-.48.164-.562.469-.562.926v3.442h-1.787v-6.124h1.74v.644c.152-.13.316-.293.713-.456.49-.2.83-.224 1.051-.246v1.592zm3.985-1.699c-1.543-.093-2.385.54-2.805 1.043-.69.831-.713 1.828-.713 2.179 0 1.277.514 2.026.876 2.377.841.82 2.08.878 2.396.878 1.144 0 1.858-.422 2.208-.715.42-.351 1.075-1.146 1.075-2.528 0-.984-.363-1.957-1.122-2.577-.572-.457-1.203-.621-1.915-.657m-.352 1.418c.48-.024.76.117.924.234.35.246.642.715.642 1.64 0 .913-.444 1.744-1.437 1.744-.678 0-1.028-.386-1.168-.597-.117-.186-.316-.632-.316-1.229 0-.902.385-1.418.713-1.616.256-.14.56-.165.642-.176m10.002-1.242v6.113h-1.799V40.3c-.128.14-.2.21-.268.27-.222.174-.76.48-1.508.48-.14 0-.819 0-1.298-.375-.676-.54-.63-1.523-.619-2.273v-3.641h1.812v3.806c0 .386 0 .69.339.878.092.07.257.14.525.14.573 0 .877-.328.958-.55.06-.153.06-.305.07-.468v-3.806h1.788z"
            fill="#0051A0" />
          <g transform="translate(101.775 33.796)">
            <mask id="logo-worcesterboschc" fill="#fff">
              <path d="M.336 9.197h5.97V.824H.336v8.373z" />
            </mask>
            <path
              d="M2.158 6.679c.526.328.853.516 1.496.516.571 0 .935-.13 1.134-.223.197-.093.619-.362.956-.878.224-.351.55-1.043.562-2.084 0-.855-.199-1.756-.819-2.413C4.823.883 4.004.824 3.654.824c-.748 0-1.227.282-1.448.468-.13.095-.165.153-.257.27L1.936.988h-1.6v8.208h1.822V6.68zm1.46-4.542c.211.057.398.186.538.35.444.527.421 1.323.421 1.499 0 .656-.152 1.02-.293 1.277-.105.187-.35.633-.969.633-.281 0-.49-.106-.573-.165a1.16 1.16 0 01-.35-.327c-.34-.504-.316-1.242-.316-1.37 0-1.325.62-1.933 1.285-1.933.048 0 .14 0 .257.036z"
              fill="#0051A0" mask="url(#logo-worcesterboschc)" />
          </g>
        </g>
      </symbol>
      <symbol id="logo" viewBox="189 108 90 107">
        <g fill="currentColor" fill-rule="evenodd">
          <path
            d="M202.677 196.773v4.537h3.721c1.334 0 2.073-1.14 2.073-2.257 0-1.07-.739-2.28-2.145-2.28h-3.649zm3.172-2.542c1.097 0 1.764-.879 1.764-1.971 0-1.164-.667-1.9-1.86-1.9h-3.076v3.871h3.172zm2.765 1.046c2.051.499 3.053 2.399 3.053 3.824 0 2.66-1.908 4.916-5.103 4.916h-6.939v-16.365h6.439c2.884 0 4.721 1.71 4.721 4.442 0 1.187-.525 2.565-2.17 3.183zm16.192 5.985c2.957 0 5.294-2.399 5.294-5.392 0-2.993-2.337-5.415-5.294-5.415-2.98 0-5.293 2.422-5.293 5.415s2.313 5.392 5.293 5.392m0-13.8c4.674 0 8.347 3.753 8.347 8.408 0 4.608-3.673 8.384-8.347 8.384-4.673 0-8.322-3.776-8.322-8.384 0-4.655 3.65-8.408 8.322-8.408m20.914 7.814l5.723 8.741h-3.625l-3.91-6.294-3.912 6.294h-3.6l5.7-8.74-5.152-7.625h3.649l3.315 5.202 3.314-5.202h3.649zm10.35-7.624h12.328v2.78h-4.626v13.585h-3.076v-13.586h-4.626z" />
          <path d="M0 106.45h89.986V0H0v106.45zm2.909-2.898h84.168V2.898H2.909v100.654z" mask="url(#logomask-2)"
            transform="translate(189 108)" />
        </g>
      </symbol>
      <symbol id="math-equals" viewBox="170 58 32 26">
        <path d="M198.519 63.5H173.48m25.039 15H173.48" stroke="currentColor" stroke-width="5" stroke-linecap="square"
          fill="none" />
      </symbol>
      <symbol id="math-plus" viewBox="-3 55 32 32">
        <path d="M25.519 71.5H.48M13.5 58.481V83.52" fill-rule="evenodd" stroke-linecap="square" stroke="currentColor"
          stroke-width="5" />
      </symbol>
      <symbol id="menu" viewBox="14 13 21 21">
        <path fill="currentColor" fill-rule="evenodd" d="M14 13h21v3H14zm0 9h21v3H14zm0 9h21v3H14z" />
      </symbol>
      <symbol id="nav-aircons" viewBox="0 0 60 60">
        <path
          d="M19.5 30.5a.5.5 0 000-1v1zm18 0a.5.5 0 000-1v1zm-15-1a.5.5 0 000 1v-1zm18.5 0a.5.5 0 000 1v-1zm-5.843-12.016a.5.5 0 100 1v-1zm6.636 1a.5.5 0 100-1v1zm-6.636-5.079a.5.5 0 000 1v-1zm6.636 1a.5.5 0 100-1v1zm-6.636 7.028a.5.5 0 000 1v-1zm6.636 1a.5.5 0 100-1v1zm-6.755 2.961a.5.5 0 000 1v-1zm6.635 1a.5.5 0 100-1v1zm15.774 8.588l-.433.25.433-.25zm-1.166 16.023l.413.282-.413-.282zm-52.096.052l-.344.363.344-.363zM2.78 34.886l.293.405-.293-.405zm-.431 11.329a.5.5 0 000 1v-1zm55.112 1a.5.5 0 100-1v1zm-.264 1.917a.5.5 0 100-1v1zm-54.451-1a.5.5 0 000 1v-1zm4.445 3.554a.5.5 0 10.096-.996l-.096.996zm47.592-2.973a.5.5 0 00-.987-.162l.987.162zm-2.26 1.977a.5.5 0 00.096.996l-.096-.996zm-24.799-6.648a.5.5 0 000 1v-1zm3.965 1a.5.5 0 100-1v1zM17 9.5h26v-1H17v1zM44.5 11v17h1V11h-1zm-29 17V11h-1v17h1zm4 1.5H17v1h2.5v-1zm18 0h-15v1h15v-1zm5.5 0h-2v1h2v-1zM14.5 28a2.5 2.5 0 002.5 2.5v-1a1.5 1.5 0 01-1.5-1.5h-1zm30 0a1.5 1.5 0 01-1.5 1.5v1a2.5 2.5 0 002.5-2.5h-1zM43 9.5a1.5 1.5 0 011.5 1.5h1A2.5 2.5 0 0043 8.5v1zm-26-1a2.5 2.5 0 00-2.5 2.5h1A1.5 1.5 0 0117 9.5v-1zm15.11 11.021a6.674 6.674 0 01-6.674 6.674v1a7.674 7.674 0 007.674-7.674h-1zm-6.674 6.674a6.674 6.674 0 01-6.673-6.674h-1a7.674 7.674 0 007.673 7.674v-1zm-6.673-6.674a6.674 6.674 0 016.673-6.673v-1a7.674 7.674 0 00-7.673 7.673h1zm6.673-6.673a6.674 6.674 0 016.674 6.673h1a7.674 7.674 0 00-7.674-7.673v1zm4.283 6.673a4.282 4.282 0 01-4.282 4.282v1a5.282 5.282 0 005.282-5.282h-1zm-4.282 4.282a4.282 4.282 0 01-4.283-4.282h-1a5.282 5.282 0 005.283 5.282v-1zm-4.283-4.282a4.282 4.282 0 014.283-4.283v-1a5.282 5.282 0 00-5.283 5.283h1zm4.283-4.283a4.282 4.282 0 014.282 4.283h1a5.282 5.282 0 00-5.282-5.283v1zm1.89 4.283a1.891 1.891 0 01-1.89 1.891v1a2.891 2.891 0 002.89-2.89h-1zm-1.89 1.891a1.891 1.891 0 01-1.892-1.89h-1a2.891 2.891 0 002.891 2.89v-1zm-1.892-1.89c0-1.045.847-1.892 1.891-1.892v-1a2.891 2.891 0 00-2.891 2.891h1zm1.891-1.892a1.89 1.89 0 011.891 1.891h1a2.891 2.891 0 00-2.89-2.891v1zm9.721.854h6.636v-1h-6.636v1zm0-4.079h6.636v-1h-6.636v1zm0 8.028h6.636v-1h-6.636v1zm-.12 3.961h6.636v-1h-6.635v1zM20.47 29.952h1.217v-1H20.47v1zm1.217.627H20.47v1h1.217v-1zm-1.217 0a.313.313 0 01-.314-.313h-1c0 .725.588 1.313 1.314 1.313v-1zm1.53-.313c0 .173-.14.313-.313.313v1c.725 0 1.313-.588 1.313-1.313h-1zm-.313-.314c.173 0 .313.14.313.314h1c0-.726-.588-1.314-1.313-1.314v1zm-1.217-1c-.726 0-1.314.588-1.314 1.314h1c0-.173.14-.314.314-.314v-1zm18.246 1h1.217v-1h-1.217v1zm1.217.627h-1.217v1h1.217v-1zm-1.217 0a.313.313 0 01-.314-.313h-1c0 .725.588 1.313 1.314 1.313v-1zm1.53-.313c0 .173-.14.313-.313.313v1c.725 0 1.313-.588 1.313-1.313h-1zm-.313-.314c.173 0 .313.14.313.314h1c0-.726-.588-1.314-1.313-1.314v1zm-1.217-1c-.726 0-1.314.588-1.314 1.314h1c0-.173.14-.314.314-.314v-1zM2.99 35.33h54.136v-1H2.99v1zm54.023-.098c.076.132.168.428.246.917.075.47.13 1.066.164 1.751.067 1.37.047 3.07-.056 4.797-.103 1.726-.29 3.469-.55 4.925-.132.729-.28 1.378-.444 1.915-.166.544-.34.941-.507 1.187l.827.563c.25-.37.46-.879.636-1.458.18-.587.336-1.277.471-2.03.271-1.506.46-3.289.565-5.043.105-1.754.126-3.49.057-4.905a17.638 17.638 0 00-.176-1.86c-.08-.5-.192-.955-.367-1.258l-.866.499zm-1.116 15.456H4.53v1H55.9v-1zm-51.369.007c-.673-.64-1.813-2.217-1.813-4.236h-1c0 2.38 1.326 4.202 2.125 4.961l.688-.725zm-1.813-4.236c0-.731-.048-1.768-.098-2.907-.05-1.146-.104-2.406-.115-3.604-.013-1.202.017-2.322.127-3.193.056-.436.129-.79.22-1.054.095-.279.185-.383.222-.41l-.585-.81c-.293.21-.468.557-.584.896a6.483 6.483 0 00-.265 1.252c-.119.94-.148 2.115-.135 3.33.012 1.216.065 2.492.116 3.637.05 1.151.097 2.16.097 2.863h1zm1.813 4.229a.056.056 0 01-.011-.001s.005.002.011.008l-.688.725a.996.996 0 00.688.268v-1zm51.338.036a.102.102 0 01.04-.036c.006-.003.004 0-.008 0v1a.95.95 0 00.795-.401l-.827-.563zm1.26-15.394c.013 0 .006.002-.015-.007a.22.22 0 01-.098-.091l.866-.5c-.19-.331-.54-.402-.753-.402v1zm-54.136-1a.852.852 0 00-.504.15l.585.811a.222.222 0 01-.076.037c-.013.003-.017.002-.005.002v-1zm-.643 12.885H57.46v-1H2.348v1zm54.848.917H5.52v1h51.676v-1zm-51.676 0H2.745v1H5.52v-1zm-.494.58c.073.444.252 1.113.565 1.699.3.561.809 1.198 1.599 1.275l.096-.996c-.267-.026-.552-.262-.813-.75a4.809 4.809 0 01-.46-1.389l-.987.162zm48.769-.16c-.06.366-.212.923-.46 1.387-.261.49-.546.725-.813.751l.096.996c.79-.077 1.299-.714 1.599-1.275a5.803 5.803 0 00.565-1.698l-.987-.162zm-26.072-3.51h3.965v-1h-3.965v1z"
          fill="#2D3D4D" />
      </symbol>
      <symbol id="nav-boilers" viewBox="0 0 60 60">
        <path
          d="M12 5v-.5a.5.5 0 00-.5.5h.5zm35.999 0h.5a.5.5 0 00-.5-.5V5zm0 44.998v.5a.5.5 0 00.5-.5h-.5zm-35.999 0h-.5a.5.5 0 00.5.5v-.5zm0-11.456a.5.5 0 000 1v-1zm35.999 1a.5.5 0 100-1v1zm-17.011-9.274l.21-.453-.846-.395.141.923.495-.075zm-.525 1.213l-.49-.095-.18.924.866-.37-.196-.46zm-1.464-3.539l.338-.369-.982-.899.147 1.323.497-.055zm6.3 22.2a.5.5 0 10-1 0h1zm-1 4.845a.5.5 0 101 0h-1zm-8.6-4.844a.5.5 0 00-1 0h1zm-1 4.845a.5.5 0 101 0h-1zm5.8-4.846a.5.5 0 10-1 0h1zm-1 4.845a.5.5 0 001 0h-1zM12 5.5h35.999v-1H12v1zM47.499 5v44.998h1V5h-1zm.5 44.498H12v1h35.999v-1zm-35.499.5V5h-1v44.998h1zm31.399-5.257a1.9 1.9 0 01-1.9 1.9v1a2.9 2.9 0 002.9-2.9h-1zm-1.9 1.9a1.9 1.9 0 01-1.9-1.9h-1a2.9 2.9 0 002.9 2.9v-1zm-1.9-1.9a1.9 1.9 0 011.9-1.9v-1a2.9 2.9 0 00-2.9 2.9h1zm1.9-1.9a1.9 1.9 0 011.9 1.9h1a2.9 2.9 0 00-2.9-2.9v1zm-25.098 2.2a.4.4 0 01-.4.4v1a1.4 1.4 0 001.4-1.4h-1zm-.4.4a.4.4 0 01-.4-.4h-1a1.4 1.4 0 001.4 1.4v-1zm-.4-.4a.4.4 0 01.4-.4v-1a1.4 1.4 0 00-1.4 1.4h1zm.4-.4c.22 0 .4.18.4.4h1a1.4 1.4 0 00-1.4-1.4v1zm4.598.4a.4.4 0 01-.4.4v1a1.4 1.4 0 001.4-1.4h-1zm-.4.4a.4.4 0 01-.4-.4h-1a1.4 1.4 0 001.4 1.4v-1zm-.4-.4c0-.22.18-.4.4-.4v-1a1.4 1.4 0 00-1.4 1.4h1zm.4-.4a.4.4 0 01.4.4h1a1.4 1.4 0 00-1.4-1.4v1zM12 39.543h35.999v-1H12v1zm18.493-9.199c.05.324-.042.488-.106.568a.481.481 0 01-.131.115c-.002 0-.002 0 0 0l.004-.002.003-.001.002-.001h.001l.197.459a35.57 35.57 0 00.198.459l.003-.002.006-.002a1.04 1.04 0 00.177-.101c.093-.064.211-.16.323-.301.233-.294.404-.737.312-1.342l-.989.151zm.461 1.233c.215-1.117-.226-2.137-.665-2.832a6.526 6.526 0 00-.924-1.144l-.019-.018-.006-.006-.002-.002-.339.368-.338.369.002.001.011.01.049.048a5.512 5.512 0 01.72.907c.383.607.674 1.357.53 2.11l.981.189zm-2.452-3.579c.024.212-.02.458-.13.76a9.212 9.212 0 01-.463.984c-.353.681-.809 1.535-.809 2.439h1c0-.614.315-1.241.697-1.98.18-.348.375-.724.513-1.098.138-.375.234-.788.186-1.215l-.994.11zM27.1 32.18a2.674 2.674 0 002.675 2.675v-1c-.925 0-1.675-.75-1.675-1.675h-1zm2.675 2.675c1.466 0 2.674-1.144 2.674-2.653h-1c0 .937-.74 1.653-1.674 1.653v1zm2.674-2.653c0-.861-.282-1.455-.592-1.838a2.238 2.238 0 00-.578-.507 1.32 1.32 0 00-.075-.04l-.003-.001H31.2v-.001l-.212.453-.212.453h-.002l-.002-.002-.002-.001h-.002l.011.005a1.238 1.238 0 01.3.268c.17.212.37.586.37 1.21h1zm1.85 17.94v4.845h1v-4.845h-1zm-9.6 0v4.846h1v-4.845h-1zm4.8 0v4.845h1v-4.845h-1z"
          fill="#2D3D4D" />
      </symbol>
      <symbol id="nav-ev" viewBox="0 0 60 60">
        <path
          d="M17.485 47.38h25.034m-25.034 0h-6.156c-1.97 0-2.371-1.515-2.326-2.273m8.482 2.273v4.144c0 1.284-1.003 1.515-1.505 1.471h-4.651c-1.204 0-2.329-1.027-2.326-2.273v-5.615M42.52 47.38h6.156c.775 0 2.325-.455 2.325-2.273M42.52 47.38v3.163c0 .12.009.238.037.354.199.793.867 2.098 2.288 2.098h4.034c.138 0 .275-.012.407-.05.673-.198 1.715-.759 1.715-1.822v-6.016m0 0V36.05c0-.112-.008-.223-.032-.333-.218-.992-.982-2.644-2.63-2.981m-36.052-.062c-1.094.357-3.283 1.497-3.283 3.209v9.224m3.284-12.433l3.604-9.526c.06-.155.092-.319.145-.476.14-.416.547-.96 1.449-.96H30.07M12.286 32.673H33.08m-7.25 6.284h8.07m-8.07 2.807h8.07m14.438-9.03a3.088 3.088 0 00-.621-.061h-4.241m4.862.062c-.982-2.65-3.056-8.217-3.494-9.287-.39-.954-1.463-1.482-2.11-1.68a1.487 1.487 0 00-.437-.058H30.07m3.01 10.963c.182-1.693 1.477-5.08 5.198-5.08 3.72 0 5.016 3.387 5.198 5.08m-10.396 0h10.396M30.07 21.711v-4.679m0 0c-4.596 0-4.925-5.08-4.514-7.62m4.514 7.62c4.706 0 4.879-5.08 4.377-7.62m-10.26 0h1.369m10.26 0h-1.369M33.08 5v4.412m0 0h-6.156m6.156 0h1.367M26.924 5v4.412m0 0h-1.368m18.93 33.155h-2.84a2 2 0 01-2-2v-.545a2 2 0 012-2h2.84a2 2 0 012 2v.545a2 2 0 01-2 2zM20.63 40.022v.545a2 2 0 01-2 2h-2.977a2 2 0 01-2-2v-.545a2 2 0 012-2h2.977a2 2 0 012 2z"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="nav-home-cover" viewBox="0 0 43 43">
        <path d="M29.919 19.912l-1.823-6.439h-10.79v-1.146h-1.434v1.146h-.921l-1.945 6.439h.783v6.175h15.347v-6.175h.783z"
          stroke="currentColor" stroke-width=".573" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M17.773 19.78h7.454M21.5 16.053v7.454" stroke="currentColor" stroke-width=".86" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M21.5 5.733v6.02m0 16.054v8.6" stroke="currentColor" stroke-width=".287" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M21.43 36.925C9.523 33.11 7.578 17.837 8.103 10.409a.558.558 0 01.394-.492L21.43 5.789a.573.573 0 01.349 0l12.937 4.129c.22.07.376.258.394.488.572 7.334-1.312 22.482-13.331 26.517a.567.567 0 01-.35.002z"
          stroke="currentColor" stroke-width=".86" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="nav-home" viewBox="0 0 60 60">
        <path
          d="M51.875 28.938c0 12.115-9.822 21.937-21.938 21.937C17.822 50.875 8 41.053 8 28.937 8 16.822 17.822 7 29.938 7c12.115 0 21.937 9.822 21.937 21.938z"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M40.427 28.937c0 5.794-4.697 10.491-10.492 10.491-5.794 0-10.492-4.697-10.492-10.491 0-5.795 4.698-10.492 10.492-10.492 5.795 0 10.492 4.697 10.492 10.492z"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M36.617 28.937a6.677 6.677 0 11-13.353 0 6.677 6.677 0 0113.353 0zM16.583 17.492a1.908 1.908 0 103.815 0 1.908 1.908 0 00-3.815 0z"
          stroke="#2D3D4D" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M40.428 48.5v3.237a2 2 0 01-2 2h-16.03a2 2 0 01-2-2V48.5" stroke="#2D3D4D" />
      </symbol>
      <symbol id="next" viewBox="-293 376 24 41">
        <path d="M-289.3 416.8l-3.7-3.7 16.6-16.6-16.6-16.6 3.7-3.7 20.3 20.3" />
      </symbol>
      <symbol id="no-installer-day" viewBox="0 0 84 141">
        <g fill="none" fill-rule="evenodd">
          <path fill="#344557"
            d="M83.22 80.493a1.5 1.5 0 11-1.44 2.63c-.127-.067-13.079-6.946-39.317-6.946s-39.19 6.879-39.318 6.948a1.5 1.5 0 01-1.44-2.632c.545-.3 13.661-7.316 40.758-7.316s40.212 7.017 40.757 7.316zm-52.757 50.205a1.5 1.5 0 01-1.5-1.5v-22.66a1.5 1.5 0 013 0v22.66a1.5 1.5 0 01-1.5 1.5m12 10a1.5 1.5 0 01-1.5-1.5v-32.66a1.5 1.5 0 013 0v32.66a1.5 1.5 0 01-1.5 1.5m12-10a1.5 1.5 0 01-1.5-1.5v-22.66a1.5 1.5 0 013 0v22.66a1.5 1.5 0 01-1.5 1.5m15.459-42.239a4.516 4.516 0 00-4.51 4.51 4.516 4.516 0 004.51 4.51 4.516 4.516 0 004.51-4.51 4.516 4.516 0 00-4.51-4.51m0 12.02c-4.142 0-7.51-3.37-7.51-7.51 0-4.141 3.368-7.51 7.51-7.51 4.141 0 7.51 3.369 7.51 7.51 0 4.14-3.369 7.51-7.51 7.51M15.1 93.964a3.005 3.005 0 11-6.01 0 3.005 3.005 0 016.01 0m9 0a3.005 3.005 0 11-6.01 0 3.005 3.005 0 016.01 0" />
          <path fill="#F06B63" d="M3.926 104.427h77.073V3.186H3.926v101.241zm-3 3h83.073V.186H.926v107.241z" />
          <path fill="#344557"
            d="M59.972 29.562c-.586.586-1.27.851-1.529.592l-7.08-7.08c-.259-.259.006-.944.591-1.53.586-.585 1.271-.85 1.53-.592l7.08 7.081c.26.258-.006.943-.592 1.53" />
          <path fill="#344557"
            d="M51.955 29.562c-.586-.586-.851-1.27-.592-1.529l7.08-7.08c.259-.259.944.006 1.53.591.585.586.85 1.271.592 1.53l-7.081 7.08c-.258.26-.943-.006-1.53-.592m-18.982 0c-.586.586-1.27.851-1.529.592l-7.08-7.08c-.259-.259.006-.944.591-1.53.586-.585 1.271-.85 1.53-.592l7.08 7.081c.26.258-.006.943-.592 1.53" />
          <path fill="#344557"
            d="M24.955 29.562c-.586-.586-.851-1.27-.592-1.529l7.08-7.08c.259-.259.944.006 1.53.591.585.586.85 1.271.592 1.53l-7.081 7.08c-.258.26-.943-.006-1.53-.592" />
        </g>
      </symbol>
      <symbol id="open-plan-area" viewBox="0 0 100 100">
        <path
          d="M39.427 88.195l44.99-.001a.999.999 0 001-.999V19.75a1 1 0 00-1-1H75.85M27.664 88.195l-10.692-.001a.999.999 0 01-1-.999V19.75a1 1 0 011-1h47.186m-36.38 68.857V74.094"
          fill="none" stroke="#fb6058" />
        <path d="M27.778 74.094c3.703-.392 11.111 1.88 11.111 14.101" fill="none" stroke="#fb6058"
          stroke-dasharray="1,4" />
        <path d="M75.694 19.338v13.513" fill="none" stroke="#fb6058" />
        <path d="M75.694 32.851c-3.703.392-11.111-1.88-11.111-14.101" fill="none" stroke="#fb6058"
          stroke-dasharray="1,4" />
      </symbol>
      <symbol id="outdoor-unit-floor" viewBox="0 0 100 100">
        <g fill="none">
          <path d="M50 69.792V100m-67.361-34.028h38.194m58.334 0h38.194" stroke="#2d3d4d" />
          <path
            d="M78.667 29.778c0-.823-.677-1.5-1.5-1.5H22.833a1.508 1.508 0 00-1.499 1.5v37.82c0 .822.677 1.499 1.499 1.5h54.334c.823 0 1.5-.677 1.5-1.5v-37.82z"
            stroke="#fb6058" />
          <path
            d="M57.507 48.688c0 8.705-7.058 15.763-15.764 15.763S25.98 57.393 25.98 48.688c0-8.706 7.057-15.764 15.763-15.764s15.764 7.058 15.764 15.764zm4.682-4.647H75.2m-13.011 2.323H75.2m-13.011 2.324l13.011-.001m-13.011 2.324H75.2m-13.011 2.324l13.011-.001M61.957 64.951h13.01m-.267-30.9c0-.622-.505-1.127-1.127-1.127h-9.758a1.132 1.132 0 00-1.093 1.126c0 .606.488 1.109 1.093 1.127h9.758c.622 0 1.127-.505 1.127-1.126zM35.203 70.295c0-.622-.505-1.127-1.126-1.127h-2.324a1.132 1.132 0 00-1.093 1.126c0 .606.488 1.109 1.093 1.127h2.324c.621 0 1.126-.505 1.126-1.126zm35.779 0c0-.622-.505-1.127-1.126-1.127h-2.323c-.596.031-1.07.53-1.07 1.126 0 .597.474 1.096 1.07 1.127h2.323c.621 0 1.126-.505 1.126-1.126z"
            stroke="#fb6058" />
          <path d="M15.7 65.972L0 86.805m86.689-20.833L100 86.805M-1.042 79.167l102.431-.001" stroke="#2d3d4d" />
        </g>
      </symbol>
      <symbol id="outdoor-unit-high" viewBox="0 0 100 100">
        <g fill="none">
          <path
            d="M23.111 43a.894.894 0 00-.888-.889h-4.167a.89.89 0 000 1.778h4.167a.895.895 0 00.888-.889zm25.695 0a.895.895 0 00-.889-.889H43.75a.895.895 0 00-.889.889c0 .49.399.889.889.889h4.167a.895.895 0 00.889-.889z"
            stroke="#2d3d4d" />
          <path
            d="M54.121 13.806c0-.823-.677-1.5-1.5-1.5H13.806c-.823 0-1.5.677-1.5 1.5v25.871c0 .823.677 1.5 1.5 1.5h38.815c.823 0 1.5-.677 1.5-1.5V13.806z"
            stroke="#fb6058" />
          <path
            d="M37.858 26.741c0 6.14-4.977 11.117-11.117 11.117-6.139 0-11.117-4.977-11.117-11.117 0-6.139 4.978-11.117 11.117-11.117 6.14 0 11.117 4.978 11.117 11.117zm3.487-3.319h9.294m-9.294 1.66h9.294m-9.294 1.659h9.294m-9.294 1.66h9.294m-9.294 1.66h9.294m-9.46 8.297h9.293m-.333-22.072a.667.667 0 00-.662-.662h-6.97a.666.666 0 00-.627.662c0 .35.278.643.627.662h6.97a.665.665 0 00.661-.662h.001z"
            stroke="#fb6058" />
          <path
            d="M112 47.834c0-.828-.672-1.501-1.5-1.501H68.667c-.828 0-1.501.673-1.501 1.501l.001 41.833c0 .823.677 1.5 1.5 1.5H110.5c.823 0 1.5-.677 1.5-1.5V47.834z"
            stroke="#2d3d4d" />
          <path
            d="M108.528 50.306c0-.274-.226-.5-.5-.5H71.139c-.274 0-.5.226-.5.5v36.889c0 .274.226.5.5.5h36.889c.274 0 .5-.226.5-.5V50.306zM89.449 50l-.001 37.5M71 69h30.448"
            stroke="#2d3d4d" />
          <path
            d="M12.847 57.986v9.375m0 32.639V67.361m22.222-9.375v9.375m0 32.639V67.361m-22.222 0h22.222M12.847 78.472h22.222M12.847 89.236h22.222"
            stroke="#fb6058" />
        </g>
      </symbol>
      <symbol id="outdoor-unit-low" viewBox="0 0 100 100">
        <g fill="none">
          <path
            d="M23.111 90.916a.895.895 0 00-.888-.889h-4.167a.895.895 0 00-.889.889c0 .491.398.889.889.889h4.167c.49 0 .888-.398.888-.889zm25.695 0a.895.895 0 00-.889-.889H43.75a.895.895 0 00-.889.889.895.895 0 00.889.889h4.167a.895.895 0 00.889-.889z"
            stroke="#2d3d4d" />
          <path
            d="M54.121 61.722c0-.823-.677-1.5-1.5-1.5H13.806c-.823 0-1.5.677-1.5 1.5v25.872c0 .827.672 1.5 1.5 1.5h38.815c.828 0 1.5-.673 1.5-1.5V61.722z"
            stroke="#fb6058" />
          <path
            d="M37.858 74.658c0 6.139-4.977 11.116-11.117 11.116-6.139 0-11.117-4.977-11.117-11.116 0-6.14 4.978-11.117 11.117-11.117 6.14 0 11.117 4.977 11.117 11.117zm3.487-3.319h9.294m-9.294 1.66h9.294m-9.294 1.659h9.294m-9.294 1.659h9.294m-9.294 1.66h9.294m-9.46 8.297h9.293m-.333-22.071a.667.667 0 00-.662-.662h-6.97a.665.665 0 00-.636.661c0 .354.283.648.636.662h6.97a.664.664 0 00.661-.661h.001z"
            stroke="#fb6058" />
          <path
            d="M112 15.889c0-.823-.677-1.5-1.5-1.5H68.667c-.828 0-1.501.672-1.501 1.5l.001 41.833c0 .823.677 1.5 1.5 1.5H110.5c.823 0 1.5-.677 1.5-1.5V15.889z"
            stroke="#2d3d4d" />
          <path
            d="M108.528 18.361c0-.274-.226-.5-.5-.5H71.139c-.274 0-.5.226-.5.5V55.25c0 .274.226.5.5.5h36.889c.274 0 .5-.226.5-.5V18.361zM89.5 18v37.5M71.052 37H101.5"
            stroke="#2d3d4d" />
        </g>
      </symbol>
      <symbol id="padlock-white" viewBox="0 0 20 32">
        <g fill="#FFF" fill-rule="evenodd">
          <path
            d="M10 32c3.107-.098 5.729-1.165 7.767-3.786C19.32 26.272 20 24.137 20 21.71V12c-2.233 0-4.078 1.747-4.078 3.981H4.078C4.078 13.748 2.233 12 0 12v9.71c0 2.426.68 4.562 2.136 6.504 2.04 2.622 4.758 3.69 7.865 3.786zm0-12.524c.971 0 1.845.874 1.845 1.943 0 .777-.486 1.456-1.165 1.747.388 1.65.777 3.495.68 3.69 0 .29-.68.485-1.36.485s-1.359-.194-1.359-.486c-.098-.194.194-2.038.582-3.69a2.003 2.003 0 01-1.068-1.746c0-1.068.778-1.943 1.845-1.943z" />
          <path
            d="M10.048 2.032c2.626 0 5.348 1.644 6.32 4.158.778 1.838.583 2.998.583 4.931 0 .194 0 .968.097.87.389-.193.583-.386.973-.58.292-.096.779-.29.973-.29 0-1.934.098-3.288-.487-5.125C17.34 2.514 13.743 0 10.047 0c-3.791 0-7.292 2.514-8.46 5.996-.68 1.837-.582 3.191-.582 5.126.292 0 .778.193.973.29.388.193.68.386 1.069.58.098.097 0-.677 0-.87 0-1.935-.098-3.094.583-4.932 1.069-2.514 3.695-4.158 6.418-4.158z" />
        </g>
      </symbol>
      <symbol id="page-loader" viewBox="0 0 115 115">
        <circle opacity=".5" cx="57.5" cy="57.5" r="55" stroke="#FB6058" stroke-width="5" />
        <circle cx="42.5" cy="110.5" r="2.5" fill="#FB6058" />
      </symbol>
      <symbol id="pdf" viewBox="0 0 162 183">
        <g fill="none">
          <g transform="translate(8 2)">
            <use fill="#000" filter="url(#pdfa)" xlinkHref="#pdfb" />
            <use fill="#FB6058" fill-rule="evenodd" xlinkHref="#pdfb" />
          </g>
          <path d="M153.816 48.858h-36.297c-5.748 0-10.413-4.665-10.413-10.413V2.104l46.71 46.754z" fill="#C93E30" />
          <path
            d="M114.905 80.945c1.745 0 2.599-1.52 2.599-2.994 0-1.526-.89-2.999-2.599-2.999h-9.934c-1.942 0-3.025 1.609-3.025 3.384v24.413c0 2.176 1.239 3.384 2.916 3.384 1.666 0 2.91-1.208 2.91-3.384v-6.7h6.009c1.864 0 2.796-1.526 2.796-3.041 0-1.484-.932-2.958-2.796-2.958h-6.009v-9.105h7.133zm-33.578-5.993h-7.269c-1.973 0-3.374 1.354-3.374 3.363v24.455c0 2.494 2.015 3.275 3.457 3.275h7.628c9.029 0 14.99-5.94 14.99-15.11-.004-9.694-5.617-15.983-15.432-15.983zm.35 25.064h-4.432V80.981h3.994c6.045 0 8.674 4.056 8.674 9.653 0 5.238-2.583 9.382-8.237 9.382zM55.048 74.952h-7.201c-2.036 0-3.171 1.343-3.171 3.384v24.413c0 2.176 1.301 3.384 3.05 3.384 1.75 0 3.052-1.208 3.052-3.384V95.62h4.514c5.57 0 10.169-3.946 10.169-10.293 0-6.21-4.436-10.376-10.413-10.376zm-.12 14.943h-4.15v-9.21h4.15c2.562 0 4.191 1.999 4.191 4.607-.005 2.604-1.63 4.603-4.191 4.603z"
            fill="#FFF" />
        </g>
      </symbol>
      <symbol id="phonebox" viewBox="362 70 94 118">
        <g fill="none" fill-rule="evenodd">
          <path
            d="M426.25 137.625a1.876 1.876 0 003.75 0c0-11.371-9.254-20.625-20.625-20.625a1.876 1.876 0 000 3.75c9.305 0 16.875 7.57 16.875 16.875z"
            fill="#E1E1DD" />
          <path
            d="M436.285 148.996l-15-5.625a1.875 1.875 0 00-1.984.43l-4.528 4.527a65.218 65.218 0 01-16.097-16.098l4.523-4.527c.52-.519.688-1.297.43-1.984l-5.625-15a1.87 1.87 0 00-1.754-1.219h-1.047c-4.66 0-9.043 1.816-12.34 5.109a18.19 18.19 0 00-5.363 12.95c0 3.238.863 6.429 2.496 9.226a84.13 84.13 0 0030.223 30.223 18.352 18.352 0 009.222 2.492 18.19 18.19 0 0012.946-5.363 17.325 17.325 0 005.113-12.34v-1.047c0-.781-.484-1.48-1.215-1.754zm-6.547 12.488a14.466 14.466 0 01-10.297 4.266 14.6 14.6 0 01-7.336-1.984 80.37 80.37 0 01-28.875-28.875 14.57 14.57 0 01-1.98-7.332c0-3.891 1.516-7.547 4.266-10.297a13.618 13.618 0 019.437-4.012l4.742 12.652-4.769 4.774a1.875 1.875 0 00-.235 2.367 69.093 69.093 0 0019.27 19.269 1.874 1.874 0 002.367-.234l4.774-4.769 12.648 4.742a13.616 13.616 0 01-4.012 9.433z"
            fill="#E1E1DD" />
          <path stroke="#E1E1DD" stroke-width="4" d="M364 72h90v114h-90z" />
          <path
            d="M409.375 109.5a1.876 1.876 0 000 3.75c13.441 0 24.375 10.934 24.375 24.375a1.876 1.876 0 003.75 0c0-15.508-12.617-28.125-28.125-28.125z"
            fill="#E1E1DD" />
        </g>
      </symbol>
      <symbol id="phoneus" viewBox="0 0 293 328">
        <g fill="none" fill-rule="evenodd">
          <path
            d="M121.21 223.84l9.447-8.689-.839-1.38c-.136-.225-13.636-22.675-10.597-41.854 1.289-8.125 3.763-13.552 6.156-18.8 2.872-6.295 5.581-12.24 5.817-21.742.28-11.29 1.758-29.856 2.15-34.645a4.832 4.832 0 011.915-.128c1.863.23 4.819 1.568 8.309 6.635a1.965 1.965 0 002.73.502 1.97 1.97 0 00.5-2.737c-3.467-5.03-7.188-7.825-11.06-8.303-3.114-.375-5.151.94-5.372 1.091l-.761.53-.077.922c-.018.227-1.933 22.937-2.26 36.036-.213 8.697-2.644 14.03-5.459 20.203-2.388 5.234-5.093 11.164-6.463 19.818-2.85 17.98 7.271 37.71 10.307 43.117l-14.705 13.522c-.382.35-.581.82-.618 1.299l8.242-7.526 2.637 2.13zm106.565-16.775a1.961 1.961 0 00-2.767.223c-.22.258-22.122 25.916-35.548 31.878-10.212 4.535-18.544 3.087-18.622 3.071a1.945 1.945 0 00-1.532.354l-6.992 5.204c-2.513 1.868-4.13 3.568-5.178 5.043l3.062 2.472c.75-1.115 2.08-2.59 4.455-4.357l6.345-4.721c2.598.3 10.548.75 20.053-3.47 14.234-6.319 36.025-31.84 36.947-32.923a1.97 1.97 0 00-.223-2.774"
            fill="#334557" />
          <path
            d="M167.399 255.2l-47.477-38.32a2.295 2.295 0 00-2.991.091L.755 323.031c-1.552 1.418-.552 4.009 1.55 4.009a2.3 2.3 0 001.55-.601l113.26-103.397a2.297 2.297 0 012.993-.09l41.018 33.105a2.31 2.31 0 01.428 3.138l-45.867 64.198c-1.09 1.525-.002 3.647 1.871 3.647h.002a2.3 2.3 0 001.87-.962l48.397-67.74a2.309 2.309 0 00-.428-3.138m37.667-67.61c0 4.367-3.533 7.908-7.891 7.908-4.358 0-7.891-3.54-7.891-7.908s3.533-7.909 7.89-7.909c4.36 0 7.892 3.541 7.892 7.909"
            fill="#334557" />
          <g transform="translate(248.513 -.422)">
            <mask id="phoneusb" fill="#fff">
              <use xlinkHref="#phoneusa" />
            </mask>
            <path
              d="M2.226 4.419c21.057 0 38.195 17.18 38.195 38.277 0 .926.804 1.73 1.726 1.73.923 0 1.725-.804 1.725-1.73C43.872 19.636 25.237.96 2.226.96 1.303.96.501 1.764.501 2.69c0 .924.802 1.73 1.725 1.73"
              fill="#334557" mask="url(#phoneusb)" />
          </g>
          <path
            d="M250.74 18.986c12.769 0 23.237 10.491 23.237 23.288 0 .926.804 1.73 1.726 1.73.923 0 1.726-.804 1.726-1.73 0-14.762-11.967-26.747-26.69-26.747-.923 0-1.725.804-1.725 1.73 0 .924.802 1.73 1.726 1.73m0 14.989a8.253 8.253 0 018.28 8.298c0 .926.804 1.73 1.726 1.73.924 0 1.726-.804 1.726-1.73 0-6.455-5.291-11.757-11.732-11.757-.924 0-1.726.804-1.726 1.73 0 .923.802 1.729 1.726 1.729"
            fill="#334557" />
          <path d="M250.361 115.091l-3.857 3.865v4.768l3.857-3.864v-4.769zm-3.856 51.121l3.856-3.865v-4.797l-3.856 3.864z"
            fill="#F06A63" />
          <path
            d="M247.555 181.488a4.11 4.11 0 01-1.05.744v23.404h-99.062V47.066h99.061v55.385c2.246-2.292 2.864-2.693 2.864-2.693a6.14 6.14 0 01.993-.703V42.367H143.588v167.968H250.36v-31.659l-2.806 2.812z"
            fill="#F06A63" />
          <path d="M250.361 136.188l-1.838 1.842a4.161 4.161 0 01-2.019 1.11v5.946l3.857-3.865v-5.033z" fill="#F06A63" />
          <path
            d="M242.548 138.03l-2.17-2.175a4.253 4.253 0 010-5.99l12.252-12.279a4.207 4.207 0 012.986-1.234c1.083 0 2.166.412 2.989 1.234l2.17 2.177a4.254 4.254 0 010 5.988l-12.252 12.28a4.203 4.203 0 01-2.987 1.234 4.204 4.204 0 01-2.988-1.235m20.67-20.717l-2.171-2.176a7.619 7.619 0 00-5.43-2.249c-2.056 0-3.985.8-5.431 2.25l-12.253 12.278a7.653 7.653 0 00-2.245 5.444c0 2.06.797 3.993 2.245 5.444l2.171 2.175a7.615 7.615 0 005.43 2.25 7.616 7.616 0 005.433-2.25l12.252-12.279a7.65 7.65 0 002.244-5.444 7.655 7.655 0 00-2.244-5.443"
            fill="#334557" />
          <path
            d="M237.313 164.638l-2.17-2.175a4.255 4.255 0 010-5.99l22.721-22.77a4.202 4.202 0 012.987-1.236c1.083 0 2.166.412 2.988 1.236l2.171 2.176a4.254 4.254 0 010 5.988l-22.722 22.771a4.213 4.213 0 01-2.988 1.235 4.21 4.21 0 01-2.987-1.235m31.14-31.208l-2.17-2.176a7.62 7.62 0 00-5.432-2.25 7.623 7.623 0 00-5.432 2.25l-22.72 22.77c-2.995 3.003-2.995 7.886-.002 10.888l2.172 2.175a7.616 7.616 0 005.432 2.25 7.615 7.615 0 005.43-2.25l22.723-22.771c2.994-3.001 2.994-7.885 0-10.886"
            fill="#334557" />
          <path
            d="M241.58 181.488l-2.17-2.175a4.253 4.253 0 010-5.99l14.186-14.218a4.208 4.208 0 012.988-1.236c1.083 0 2.166.412 2.987 1.236l2.172 2.176a4.254 4.254 0 010 5.988l-14.188 14.22a4.206 4.206 0 01-2.987 1.234 4.202 4.202 0 01-2.988-1.235m22.607-22.656l-2.172-2.176a7.614 7.614 0 00-5.43-2.25 7.62 7.62 0 00-5.433 2.25l-14.186 14.218a7.653 7.653 0 00-2.246 5.444 7.65 7.65 0 002.246 5.444l2.17 2.175a7.615 7.615 0 005.43 2.25 7.616 7.616 0 005.433-2.25l14.188-14.22a7.648 7.648 0 002.244-5.441 7.65 7.65 0 00-2.244-5.444m-18.315-39.242l-3.104-3.912s-4.567-4.305.537-9.845c4.92-5.338 6.063-6.076 6.063-6.076a6.376 6.376 0 014.186-1.542c1.898 0 3.622.824 4.283 2.115 2.097 4.093.371 6.895-3.794 11.07-2.627 2.635-8.171 8.19-8.171 8.19m15.04-20.843c-1.245-2.426-4.132-3.996-7.356-3.996h-.002a9.83 9.83 0 00-6.334 2.29c-1.11.84-3.373 3.099-6.454 6.442-5.318 5.772-3.309 11.748-.57 14.518l2.971 3.745a3.445 3.445 0 002.705 1.308 3.45 3.45 0 002.443-1.014l8.174-8.191c4.065-4.075 7.757-8.598 4.422-15.102"
            fill="#334557" />
        </g>
      </symbol>
      <symbol id="pickdate" viewBox="0 0 42 54">
        <path fill="currentColor" fill-rule="evenodd"
          d="M30 44.651L28.349 43l-7.35 7.349L13.65 43 12 44.651l8.999 9.002zm9.54-42.203h-6.104v-.75a1.593 1.593 0 00-3.185 0v.75h-4.177v-.75a1.593 1.593 0 00-3.184 0v.75h-4.177v-.75a1.593 1.593 0 00-3.185 0v.75h-4.176v-.75a1.593 1.593 0 00-3.185 0v.75H2.064c-.88 0-1.593.714-1.593 1.593v30.778c0 .879.714 1.592 1.593 1.592h37.47c.878 0 1.591-.713 1.591-1.592V4.041a1.58 1.58 0 00-1.585-1.593zm-1.592 30.779H3.665V5.633h4.512v.414a1.593 1.593 0 003.185 0v-.414h4.177v.414a1.593 1.593 0 003.184 0v-.414H22.9v.414a1.593 1.593 0 003.184 0v-.414h4.177v.414a1.593 1.593 0 003.185 0v-.414h4.51v27.594h-.008z" />
        <path
          d="M28.23 18.156h-3.442c-.704 0-1.274.57-1.274 1.274v3.376c0 .704.57 1.274 1.274 1.274h3.443c.703 0 1.273-.57 1.273-1.274V19.43c0-.703-.57-1.274-1.273-1.274zm-17.242-3.411c0 2.137-3.206 2.137-3.206 0 0-2.139 3.206-2.139 3.206 0m5.689 0c0 2.137-3.205 2.137-3.205 0 0-2.139 3.205-2.139 3.205 0m5.712 0c0 2.137-3.208 2.137-3.208 0 0-2.139 3.208-2.139 3.208 0m5.709 0c0 2.137-3.205 2.137-3.205 0 0-2.139 3.205-2.139 3.205 0m5.732 0c0 2.137-3.205 2.137-3.205 0 0-2.139 3.205-2.139 3.205 0M10.988 27.484c0 2.137-3.206 2.137-3.206 0 0-2.14 3.206-2.14 3.206 0m5.689 0c0 2.137-3.205 2.137-3.205 0 0-2.14 3.205-2.14 3.205 0m5.712 0c0 2.137-3.208 2.137-3.208 0 0-2.14 3.208-2.14 3.208 0m-11.401-6.349c0 2.14-3.206 2.14-3.206 0 0-2.137 3.206-2.137 3.206 0m5.689 0c0 2.14-3.205 2.14-3.205 0 0-2.137 3.205-2.137 3.205 0m5.712 0c0 2.14-3.208 2.14-3.208 0 0-2.137 3.208-2.137 3.208 0m11.441 0c0 2.14-3.205 2.14-3.205 0 0-2.137 3.205-2.137 3.205 0"
          fill="currentColor" fill-rule="evenodd" />
      </symbol>
      <symbol id="play-icon" viewBox="0 0 11 14">
        <path fill="#FFF" fill-rule="evenodd"
          d="M1.862.476L10.21 6a1.2 1.2 0 010 2.002l-8.348 5.523A1.2 1.2 0 010 12.523V1.477a1.2 1.2 0 011.862-1z" />
      </symbol>
      <symbol id="popular-choice" viewBox="0 0 16 15">
        <path
          d="M7.715.864a.3.3 0 01.57 0L9.82 5.523a.3.3 0 00.284.206h4.96a.3.3 0 01.176.544L11.235 9.14a.3.3 0 00-.11.338l1.531 4.647a.3.3 0 01-.46.338l-4.021-2.88a.3.3 0 00-.35 0l-4.022 2.88a.3.3 0 01-.46-.338L4.876 9.48a.3.3 0 00-.11-.338L.759 6.273a.3.3 0 01.175-.544h4.96a.3.3 0 00.285-.206L7.715.863z"
          fill="#000" />
      </symbol>
      <symbol id="product-central-heating-output" viewBox="0 0 30 30">
        <path d="M5.25 4.385c0-.35.284-.635.635-.635h18.23c.35 0 .635.284.635.635V27H5.25V4.385z" stroke="currentColor"
          stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M27.944 26.66h-3.193v-1.58h3.193v1.578zm-22.694 0H2.328v-1.58H5.25v1.578z" stroke="currentColor"
          stroke-width=".375" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M29.522 24.064h-.79V28.5h-.788v-4.436h-.79v-1.613h2.368v1.613zm-26.404 0h-.79V28.5H1.54v-4.436H.75v-1.613h2.368v1.613z"
          stroke="currentColor" stroke-width=".563" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M9 23.425V8.865c0-1.09 1.578-1.09 1.578 0m2.172 14.56V8.865c0-1.09 1.578-1.09 1.578 0m2.281 14.56V8.865c0-1.09 1.578-1.09 1.578 0m2.183 14.56V8.865c0-1.09 1.577-1.09 1.577 0"
          stroke="currentColor" stroke-width=".423" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="product-cooling-power" viewBox="0 0 32 32">
        <path
          d="M15.876.8v8.444m0 21.956v-7.6m0-14.356l-5.227-5.91m5.227 5.91L21.6 2.77m-5.725 6.474V23.6m0 0l5.476 6.193M15.876 23.6L10.4 29.793m20.8-13.917h-8.444m-21.956 0h7.6m14.356 0l5.91-5.227m-5.91 5.227L29.23 21.6m-6.474-5.724H8.4m0 0L2.207 21.35M8.4 15.876L2.207 10.4"
          stroke="currentColor" stroke-linecap="round" />
        <path
          d="M26.065 6.502a.5.5 0 10-.707-.707l.707.707zM5.654 25.498a.5.5 0 00.707.707l-.707-.707zM25.358 5.795L5.654 25.498l.707.707L26.065 6.503l-.707-.707z"
          fill="currentColor" />
        <path
          d="M25.358 26.206a.5.5 0 10.707-.708l-.707.707zM6.36 5.794a.5.5 0 00-.707.707l.707-.707zm19.704 19.703L6.36 5.795l-.707.707 19.704 19.704.707-.708z"
          fill="currentColor" />
      </symbol>
      <symbol id="product-flow-rate" viewBox="0 0 32 32">
        <path
          d="M1.6 11.297V4.98h19.324c4.162 0 5.203 3.22 5.203 4.83v6.69c0 1.189-.991 1.486-1.487 1.486h-3.345c-1.189 0-1.486-.99-1.486-1.486v-3.716c0-1.19-1.239-1.487-1.858-1.487H1.601zM13.12.892V4.98"
          stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M7.546.892h11.148" stroke="currentColor" stroke-linecap="round" />
        <circle opacity=".5" cx="23.2" cy="20.8" r=".8" fill="currentColor" />
        <circle opacity=".5" cx="21.6" cy="24" fill="currentColor" r=".8" />
        <circle opacity=".5" cx="24.8" cy="24" fill="currentColor" r=".8" />
        <circle opacity=".5" cx="20" cy="27.2" fill="currentColor" r=".8" />
        <circle opacity=".5" cx="23.2" cy="27.2" r=".8" fill="currentColor" />
        <circle opacity=".5" cx="26.4" cy="27.2" r=".8" fill="currentColor" />
        <circle opacity=".5" cx="18.4" cy="30.4" r=".8" fill="currentColor" />
        <circle opacity=".5" cx="21.6" cy="30.4" r=".8" fill="currentColor" />
        <circle opacity=".5" cx="24.8" cy="30.4" r=".8" fill="currentColor" />
        <circle opacity=".5" cx="28" cy="30.4" fill="currentColor" r=".8" />
      </symbol>
      <symbol id="product-heating-power" viewBox="0 0 32 32">
        <path d="M22.7 15.6a6.3 6.3 0 11-12.6 0 6.3 6.3 0 0112.6 0z" stroke="currentColor" />
        <path
          d="M15.972.8v6.05m4.772-1.925L19.62 7.4M11.2 4.925L12.323 7.4M27.2 5.2l-4.491 4.4m8.491 6.133h-5.5m1.75 4.534L25.2 19.2m2.25-8l-2.25 1.067m2 14.133l-4-4.267M16.267 31.2v-6.05m-4.534 1.925L12.8 24.6m8 2.475L19.733 24.6M5.6 26.8l4.267-4.4M.8 15.467h6.05m-1.925-4.534L7.4 12m-2.475 8L7.4 18.933M5.2 4.8l4.4 4.267"
          stroke="currentColor" stroke-linecap="round" />
      </symbol>
      <symbol id="product-power-output" viewBox="0 0 23 30">
        <path clip-rule="evenodd" d="M22.4.6H11.512L1.806 15h8.605L.8 29.4l21.23-17.992h-9.08L22.4.6z"
          stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="product-warranty" viewBox="0 0 30 30">
        <path
          d="M14.024 2.452a.97.97 0 011.202 0c.47.371 1.1.47 1.662.263a.97.97 0 011.143.372c.333.498.9.787 1.5.763a.97.97 0 01.972.707 1.72 1.72 0 001.19 1.19.97.97 0 01.706.973 1.72 1.72 0 00.764 1.499.97.97 0 01.372 1.143 1.72 1.72 0 00.263 1.662.97.97 0 010 1.202 1.72 1.72 0 00-.263 1.662.97.97 0 01-.372 1.143 1.72 1.72 0 00-.764 1.5.97.97 0 01-.706.972 1.72 1.72 0 00-1.19 1.19.97.97 0 01-.973.706 1.72 1.72 0 00-1.499.764.97.97 0 01-1.143.372 1.72 1.72 0 00-1.662.263.97.97 0 01-1.202 0 1.72 1.72 0 00-1.662-.263.97.97 0 01-1.143-.372 1.72 1.72 0 00-1.5-.764.97.97 0 01-.972-.706 1.72 1.72 0 00-1.19-1.19.97.97 0 01-.707-.973 1.72 1.72 0 00-.763-1.499.97.97 0 01-.372-1.143 1.72 1.72 0 00-.263-1.662.97.97 0 010-1.202c.371-.47.47-1.1.263-1.662a.97.97 0 01.372-1.143c.498-.333.787-.9.764-1.5a.97.97 0 01.706-.972 1.72 1.72 0 001.19-1.19.97.97 0 01.973-.707 1.72 1.72 0 001.499-.763.97.97 0 011.143-.372 1.72 1.72 0 001.662-.263z"
          stroke="currentColor" stroke-width=".75" />
        <path
          d="M8.625 18.75l-3.178 7.062a.2.2 0 00.253.27l2.356-.884a.2.2 0 01.26.124l.902 2.707a.2.2 0 00.37.023l3.537-7.427m7.5-2.25l3.537 7.428a.2.2 0 01-.25.273l-2.343-.878a.2.2 0 00-.26.124l-.902 2.707a.2.2 0 01-.37.023L16.5 20.625"
          stroke="currentColor" stroke-width=".75" />
        <path d="M10.5 12l2.864 3L19.5 9" stroke="currentColor" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="protect-plus" viewBox="0 0 150 150">
        <path d="M104.368 69.46L98.01 47H60.368v-4h-5v4h-3.215l-6.785 22.46H48.1V91h53.536V69.46h2.732z"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M62 69h26M75 56v26" stroke="currentColor" stroke-width="3" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M75 20v21m0 56v30" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M74.757 128.808c-41.536-13.307-48.322-66.588-46.49-92.497.056-.806.603-1.471 1.373-1.717l45.12-14.4c.396-.126.82-.126 1.216 0l45.131 14.403c.765.245 1.309.902 1.372 1.702 1.995 25.583-4.576 78.428-46.502 92.5-.39.131-.828.134-1.22.009z"
          stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="question_mark" viewBox="0 0 170 170">
        <path
          d="M64 62.562C64.537 54.042 71.405 37 94.586 37c28.976 0 37.025 33.018 22.001 47.929-15.025 14.911-34.343 10.65-34.343 31.953V127"
          stroke="#FB6058" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="82" cy="149" r="3" fill="#FB6058" />
      </symbol>
      <symbol id="radiator" viewBox="0 0 164 164">
        <path fill="#fff" d="M0 0h164v164H0z" />
        <rect x="33.55" y="38.798" width="92.308" height="6.372" rx="3.186" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="30.926" y="105.054" width="94.932" height="6.372" rx="3.186" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="30.926" y="105.054" width="6.372" height="6.372" rx="3.186" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="30.926" y="36.83" width="6.372" height="10.964" rx="2.25" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <path
          d="M42.078 113.864a2.25 2.25 0 012.25-2.25h73.376a2.25 2.25 0 012.25 2.25v13.68a2.25 2.25 0 01-2.25 2.25h-1.695a2.25 2.25 0 01-2.102-1.447l-2.588-6.775a3.748 3.748 0 00-3.503-2.412H53.623a3.75 3.75 0 00-3.583 2.643l-1.98 6.405a2.25 2.25 0 01-2.15 1.586h-1.582a2.25 2.25 0 01-2.25-2.25v-13.68z"
          fill="#fff" stroke="#FB6058" stroke-width="1.5" />
        <rect x="42.078" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="53.886" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="65.694" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="77.502" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="89.31" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="101.118" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
        <rect x="112.926" y="34.206" width="7.028" height="82.468" rx="3.514" fill="#fff" stroke="#FB6058"
          stroke-width="1.5" />
      </symbol>
      <symbol id="right-arrow" viewBox="0 0 30 30">
        <path d="M5 15h20m0 0L15 5m10 10L15 25" stroke="#fff" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="rotate" viewBox="0 0 188 188">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M145.755 134.437c-.054-.347-.344-1.398-1.243-1.252-.899.146-.677.908-.621 1.252.875 5.379.508 13.411-4.372 18.034-3.22 3.05-7.589 3.561-11.193 3.704a.3.3 0 01-.31-.299v-4.35a.3.3 0 00-.451-.259l-9.947 5.774a.3.3 0 000 .519l9.947 5.771a.3.3 0 00.451-.26v-4.109a.3.3 0 01.289-.301c3.867-.165 8.964-1.007 12.479-4.339 5.242-4.965 5.897-14.199 4.971-19.885zM41.712 53.594c.054.347.344 1.399 1.243 1.253.9-.146.677-.908.621-1.253-.875-5.379-.508-13.41 4.371-18.033 3.221-3.05 7.59-3.56 11.194-3.704.17-.007.31.13.31.3v4.349a.3.3 0 00.45.26l9.948-5.776a.3.3 0 000-.519l-9.948-5.77a.3.3 0 00-.45.26v4.11a.301.301 0 01-.289.3c-3.867.164-8.964 1.007-12.479 4.339-5.242 4.965-5.897 14.199-4.97 19.884z"
          fill="#FB6058" stroke="#FB6058" />
        <path stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
          d="M57.516 44.324h74.568v98.181H57.516z" />
      </symbol>
      <symbol id="screener-summary-menu" viewBox="0 0 60 60">
        <path
          d="M16.524 15.957h30.765l2.119 8.263H31.06m-17.37-8.262h-1.359l-2.118 8.262h3.478m17.689.035h17.539v21.399h-37.43V34.538m2.465-10.283h-2.465v8.09M9.255 45.638h41.667"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M32.553 30.337h3.108V26.17h-3.108v4.167z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M35.52 28.404h-2.967m-.638 1.916h4.66" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M38.936 30.337h3.108V26.17h-3.108v4.167z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M41.902 28.404h-2.966m-.638 1.916h4.661" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M42.128 30.337h3.107V26.17h-3.107v4.167z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M45.094 28.404h-2.966m-.958 1.916h4.661M15.84 24.255h13.124m-14.283 1.007v20.392h15.36V25.262M14.68 33.51h15.172"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M18.192 31.608h3.742V26.17h-3.742v5.438z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M21.793 28.723h-3.601" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M23.298 31.608h3.743V26.17h-3.743v5.438z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M26.9 28.723h-3.602m-6.383 2.873h11.229" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M18.192 42.03h3.742v-6.285h-3.742v6.285z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M21.793 38.617h-3.601" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M23.298 42.03h3.743v-6.285h-3.743v6.285z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M26.9 38.617h-3.602m-6.383 3.511h11.229" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M38.936 42.03h3.743v-6.285h-3.743v6.285z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M42.538 38.617h-3.602" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M42.447 42.03h3.743v-6.285h-3.743v6.285z" stroke="#697A8E" stroke-width=".75"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M46.049 38.617h-3.602m-4.468 3.511h9.057" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd" d="M14.56 25.278l-.837-.957 8.837-7.725 8.732 7.619-.836.957-7.895-6.889-8.001 6.995z"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M18.192 24.182V22.34m1.595 1.729v-3.324m1.915 3.53v-4.807m1.596 4.783v-5.102m1.596 4.896v-3.62m1.596 3.732v-2.136"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd"
          d="M14.637 32.234h-3.362l-.424 2.119h3.786v-2.119zm17.597 11.067h3.531v-7.556h-3.53V43.3zm-1.277 2.12h6.356v-1.06h-6.356v1.06z"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd"
          d="M33.51 42.488h1.484v-2.913H33.51v2.913zm0-3.932h1.484v-1.854H33.51v1.854zm-1.914 5.908h5.085v-1.06h-5.085v1.06zM14.043 17.537h2.542v-3.814h-2.542v3.814z"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="screening-summary-baths" viewBox="0 0 36 36">
        <path
          d="M2.806 14.065c.363 0 .936.035 1.688.124m28.7-.124c-.337 0-.776.014-1.266.05m0 0c-1.55.115-2.037-.05-5.181 1.341-1.688 1.267-6.747 2.13-13.922.44-3.61-1.202-6.466-1.484-8.331-1.707m27.434-.073c0 2.809-1.169 7.586-5.181 9.948M4.494 14.189c1.333 5.398 2.55 8.12 5.065 9.548m0 0c2.263 1.285 3.815 1.76 9.212 1.96 2.587.095 6.386-.697 7.976-1.633m-17.188-.327c0 .997-.338 3.074-1.689 3.412m18.877-3.085c.18 1.028.792 3.085 1.805 3.085"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="9.559" cy="12.799" r="1.266" stroke="#697A8E" stroke-width=".75" stroke-linecap="round"
          stroke-linejoin="round" />
        <circle cx="12.935" cy="10.266" stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round"
          r="1.266" />
        <circle cx="23.065" cy="10.266" stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round"
          r="1.266" />
        <circle cx="26.863" cy="12.377" stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round"
          r=".844" />
        <circle cx="21.799" cy="14.909" stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round"
          r="1.688" />
        <circle cx="17.156" cy="12.799" stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round"
          r="2.11" />
      </symbol>
      <symbol id="screening-summary-bed" viewBox="0 0 36 36">
        <path
          d="M5.82 16.865H3.255v4.752h1.71m.855-4.752V11.68h24.788v5.184m-24.788 0h2.137m22.65 0h2.138v4.752h-1.71m-.427-4.752h-2.564m-23.08 4.752v2.592m0-2.592h26.071m0 0v2.592M7.957 16.865v-1.728h8.974v1.728m-8.974 0h8.974m0 0h1.283m0 0v-1.728h9.83v1.728m-9.83 0h9.83"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="screening-summary-showers" viewBox="0 0 36 36">
        <path
          d="M5.292 14.99v-3.552c1.184-.71 5.093-2.058 11.841-2.322M5.293 14.99c.789.92 4.736 2.763 14.209 2.763 8.21 0 12.104-1.842 13.025-2.763m-27.235 0c.658-.921 4.421-2.763 14.21-2.763 9.788 0 12.762 1.842 13.025 2.763m0 0v-3.553c-.844-.723-4.184-2.109-11.052-2.336m-4.342-2.4v2.414m0 0a60.623 60.623 0 014.342-.014m0-2.4v2.4m-6.71 4.705h1.184m2.368 0h1.185m2.763 0h1.184m-12.63 1.579h1.973m1.579 0h1.578m1.58 0h2.367m1.974 0h1.973m1.58 0H27M13.581 23.28c-.631-.948.526-2.5 1.184-3.158 1.263 1.263 1.316 2.631 1.184 3.158-.394.789-1.736.947-2.368 0zm4.129 0c-.63-.948.527-2.5 1.185-3.158 1.263 1.263 1.316 2.631 1.184 3.158-.395.789-1.737.947-2.368 0zm4.13 0c-.631-.948.527-2.5 1.184-3.158 1.263 1.263 1.316 2.631 1.185 3.158-.395.789-1.737.947-2.369 0zm.607 5.525c-.631-.947.526-2.5 1.184-3.157 1.263 1.263 1.316 2.631 1.184 3.157-.394.79-1.737.948-2.368 0zm-4.737 0c-.63-.947.527-2.5 1.185-3.157 1.263 1.263 1.316 2.631 1.184 3.157-.395.79-1.737.948-2.368 0zm-4.736 0c-.631-.947.527-2.5 1.184-3.157 1.263 1.263 1.316 2.631 1.185 3.157-.395.79-1.737.948-2.369 0zm-4.736 0c-.632-.947.526-2.5 1.184-3.157 1.263 1.263 1.316 2.631 1.184 3.157-.394.79-1.737.948-2.368 0zm18.156 0c-.631-.947.526-2.5 1.184-3.157 1.263 1.263 1.316 2.631 1.184 3.157-.394.79-1.736.948-2.368 0z"
          stroke="#697A8E" stroke-width=".75" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="search-teal" viewBox="0 0 13 13">
        <path d="M8.695 9.105a4.632 4.632 0 10-6.127-6.947 4.632 4.632 0 006.127 6.947zm0 0L12 12.41" stroke="#3599A5"
          stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="search" viewBox="0 0 21 21">
        <circle class="a" cx="8.76" cy="8.33" r="7.25" fill="none" stroke="currentColor" stroke-width="2" />
        <path class="b" fill="none" stroke="currentColor" stroke-width="3" d="M13.85 13.87l6.01 6.01" />
      </symbol>
      <symbol id="service-plan" viewBox="0 0 150 150">
        <path d="M35.408 94.155h79.099M86 119v12m-22-12v12m11-12v12" stroke="currentColor" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M114.958 31.5v-8.131A3.368 3.368 0 00111.59 20H38.327a3.369 3.369 0 00-3.368 3.369v92.259a3.369 3.369 0 003.368 3.369h73.263a3.368 3.368 0 003.368-3.369V74"
          stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M111.439 65.63v-8.533c.001-1.066 0-2.133-1.066-3.2-.843-.843-7.468-5.866-.001-12.8.534-.533 1.067-.22 1.067 0v4.8c0 .89 0 2.668 2.134 2.668h2.133c1.067 0 2.133-.96 2.133-2.668v-4.8c0-.533.534-.532 1.067 0 2.137 2.134 5.867 8.534 0 12.8-1.067 1.067-1.067 2.134-1.067 3.2v8.534"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="115" cy="53" r="20" stroke="currentColor" stroke-width="2" />
        <circle cx="101.478" cy="106.551" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" r="5.333" />
        <circle cx="45.882" cy="106.042" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" r="2" />
        <circle cx="55.77" cy="106.042" r="2" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M77.155 74.832c.315 2.064-1.166 2.696-1.166 2.696.801-4.153-3.253-7.865-3.253-7.865.316 2.84-3.109 6.043-3.109 9.419a4.832 4.832 0 004.833 4.832c2.668 0 4.83-2.065 4.83-4.784 0-3.302-2.135-4.298-2.135-4.298"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="showers" viewBox="570 31 32 27">
        <g fill="#6c6c6c" fill-rule="evenodd">
          <path
            d="M601.579 37.638a.521.521 0 00-.099-.301c-1.184-1.672-6.533-2.577-12.533-2.818v-1.995c0-.29-.182-.524-.473-.524s-.474.234-.474.524v1.96a73.827 73.827 0 00-3.79-.012v-1.948c0-.29-.182-.524-.473-.524s-.474.234-.474.524V34.5c-6.316.216-12.543 1.21-13.18 3.11-.019.054-.083.11-.083.168v3.34c0 .011.062.02.062.033 0 .016.023.036.023.054 0 2.868 10.34 3.625 15.8 3.625 5.46 0 15.792-.757 15.792-3.625 0-.02-.054-.038-.055-.056 0-.011-.043-.02-.043-.033v-3.478zm-15.757-2.137c7.918 0 13.862 1.163 14.81 2.324v1.943c-2.843-1.695-10.44-2.189-14.843-2.189-4.402 0-11.684.494-14.842 2.189v-1.887c.632-.994 6.063-2.38 14.875-2.38zm.078 8.28c-9.533 0-14.734-1.702-14.734-2.576 0-.875 5.2-2.577 14.734-2.577 9.532 0 14.733 1.702 14.733 2.577-.001.874-5.202 2.576-14.733 2.576z" />
          <path
            d="M595.02 41.93c0 .546-1.49.546-1.49 0 0-.547 1.49-.547 1.49 0m-4.21 0c0 .546-1.49.546-1.49 0 0-.547 1.49-.547 1.49 0m-4.21 0c0 .546-1.49.546-1.49 0 0-.547 1.49-.547 1.49 0m-4.208 0c0 .546-1.49.546-1.49 0 0-.547 1.49-.547 1.49 0m-4.211 0c0 .546-1.49.546-1.49 0 0-.547 1.49-.547 1.49 0m12.629-2.097c0 .548-1.49.548-1.49 0 0-.547 1.49-.547 1.49 0m-4.21 0c0 .548-1.49.548-1.49 0 0-.547 1.49-.547 1.49 0m-4.208 0c0 .548-1.49.548-1.49 0 0-.547 1.49-.547 1.49 0m-1.293 6.835a.525.525 0 00-.377.173c-.21.235-2.041 2.405-1.32 3.842.335.663 1 1.14 1.776 1.14.85 0 1.598-.542 1.862-1.226.587-1.523-1.334-3.518-1.555-3.736-.104-.103-.247-.205-.386-.193m.959 3.472c-.104.27-.482.474-.88.474-.22 0-.624-.062-.833-.482-.231-.462.259-1.415.791-2.136.532.639 1.136 1.59.922 2.144m3.777-3.472a.529.529 0 00-.376.173c-.21.235-2.041 2.405-1.32 3.842.335.663.997 1.14 1.774 1.14.85 0 1.599-.542 1.862-1.226.587-1.523-1.334-3.518-1.555-3.736-.103-.103-.25-.205-.385-.193m.958 3.472c-.104.27-.482.474-.88.474-.22 0-.624-.062-.833-.482-.231-.462.259-1.415.793-2.136.532.639 1.134 1.59.92 2.144m3.777-3.472a.525.525 0 00-.376.173c-.21.235-2.041 2.325-1.32 3.76.335.663.998 1.06 1.776 1.06.85 0 1.598-.46 1.862-1.145.587-1.524-1.334-3.48-1.555-3.696a.52.52 0 00-.386-.152m.957 3.472c-.104.27-.482.474-.88.474-.22 0-.623-.062-.832-.482-.232-.462.258-1.415.792-2.136.532.639 1.134 1.59.92 2.144m-10.429 2.816a.525.525 0 00-.377.174c-.21.234-2.041 2.248-1.32 3.684.335.663 1 .985 1.776.985.85 0 1.598-.385 1.862-1.07.587-1.524-1.334-3.44-1.555-3.658-.104-.102-.247-.12-.386-.115m.959 3.473c-.104.269-.482.473-.88.473-.22 0-.624-.062-.833-.481-.231-.462.259-1.415.791-2.136.532.639 1.136 1.589.922 2.144m-5.695-3.473a.525.525 0 00-.376.174c-.21.234-2.041 2.324-1.32 3.76.335.663.997 1.06 1.776 1.06.85 0 1.598-.46 1.861-1.146.587-1.523-1.333-3.48-1.556-3.696a.534.534 0 00-.385-.152m.96 3.473c-.104.269-.483.473-.88.473-.221 0-.624-.062-.833-.481-.232-.462.259-1.415.791-2.136.532.639 1.135 1.589.922 2.144m8.512-3.473a.529.529 0 00-.376.174c-.21.234-2.041 2.248-1.32 3.684.335.663.997.985 1.774.985.85 0 1.599-.385 1.862-1.07.587-1.524-1.334-3.44-1.555-3.658-.103-.102-.25-.12-.385-.115m.958 3.473c-.104.269-.482.473-.88.473-.22 0-.624-.062-.833-.481-.231-.462.259-1.415.793-2.136.532.639 1.134 1.589.92 2.144m3.777-3.473a.525.525 0 00-.376.174c-.21.234-2.041 2.324-1.32 3.76.335.663.998 1.06 1.776 1.06.85 0 1.598-.46 1.862-1.146.587-1.523-1.334-3.48-1.555-3.696a.546.546 0 00-.386-.152m.957 3.473c-.104.269-.482.473-.88.473-.22 0-.623-.062-.832-.481-.232-.462.258-1.415.792-2.136.532.639 1.134 1.589.92 2.144m3.778-3.473a.523.523 0 00-.375.174c-.21.234-2.041 2.248-1.32 3.684.333.663.996.985 1.774.985.85 0 1.599-.385 1.862-1.07.587-1.524-1.334-3.44-1.554-3.658-.104-.102-.25-.12-.387-.115m.957 3.473c-.103.269-.482.473-.88.473-.22 0-.623-.062-.832-.481-.232-.462.258-1.415.791-2.136.533.639 1.137 1.589.921 2.144" />
        </g>
      </symbol>
      <symbol id="smart-home" viewBox="0 0 140 140">
        <circle cx="69.995" cy="66.683" r="34.773" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <circle cx="69.996" cy="66.681" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          r="16.63" />
        <circle cx="69.996" cy="66.682" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
          r="10.583" />
        <circle r="3.024" transform="matrix(-1 0 0 1 51.852 48.54)" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path d="M54.12 97.82v6.271a2.8 2.8 0 002.8 2.8h26.15a2.8 2.8 0 002.8-2.8V97.82" stroke="#2D3D4D"
          stroke-width="1.5" />
      </symbol>
      <symbol id="snowflake" viewBox="239 19 43 44">
        <path
          d="M281.367 41.014h-4.775l2.995-2.995a.333.333 0 000-.472.333.333 0 00-.472 0l-3.469 3.467h-5.41l2.995-2.995a.333.333 0 000-.472.336.336 0 00-.474 0l-3.467 3.467h-8.132l5.75-5.75h4.905c.186 0 .334-.15.334-.336a.335.335 0 00-.334-.337h-4.235l3.825-3.824h4.905a.335.335 0 00.334-.337.334.334 0 00-.334-.336h-4.235l3.375-3.375a.336.336 0 000-.475.333.333 0 00-.472 0l-3.375 3.376v-4.235a.335.335 0 10-.67 0v4.905l-3.825 3.824v-4.227a.333.333 0 10-.667 0v4.905l-5.75 5.75V32.41l3.467-3.467a.336.336 0 000-.474.333.333 0 00-.472 0l-2.996 2.994v-5.41l3.468-3.466a.336.336 0 000-.474.333.333 0 00-.472 0l-2.996 2.995v-4.772a.338.338 0 00-.338-.336.335.335 0 00-.336.336v4.774l-2.995-2.995a.336.336 0 00-.475 0 .333.333 0 000 .472l3.47 3.47v5.409l-2.995-2.995a.336.336 0 00-.475.474l3.47 3.467v8.133l-5.75-5.75v-4.906a.337.337 0 00-.672 0v4.236l-3.825-3.826v-4.907a.335.335 0 00-.337-.334.334.334 0 00-.336.334v4.235l-3.375-3.375a.336.336 0 00-.475.474l3.376 3.376h-4.235a.335.335 0 000 .67h4.905l3.824 3.824h-4.235a.336.336 0 000 .673h4.905l5.75 5.75h-8.124l-3.467-3.467a.333.333 0 00-.472 0 .333.333 0 000 .472l2.995 2.995h-5.412l-3.47-3.467a.333.333 0 00-.471 0 .333.333 0 000 .472l2.995 2.995h-4.772a.329.329 0 00-.336.331c0 .185.15.336.336.336h4.772l-2.995 2.995a.336.336 0 00.474.475l3.47-3.47h5.409l-2.995 2.995a.336.336 0 00.474.475l3.467-3.47h8.133l-5.75 5.75h-4.908a.332.332 0 00-.334.336c0 .185.151.334.334.334h4.235l-3.825 3.825h-4.905a.334.334 0 000 .67h4.235l-3.373 3.37a.336.336 0 00.475.475l3.375-3.376v4.236c0 .185.148.333.334.333a.332.332 0 00.333-.333v-4.903l3.825-3.825v4.235c0 .186.151.334.334.334a.334.334 0 00.336-.334v-4.907l5.75-5.75v8.132l-3.47 3.467a.336.336 0 00.475.474l2.996-2.995v5.412l-3.47 3.467a.333.333 0 000 .472.327.327 0 00.237.098.325.325 0 00.237-.098l2.996-2.995v4.772a.335.335 0 10.67.003l-.001-4.773 2.995 2.995a.335.335 0 10.475-.474l-3.47-3.467v-5.41l2.995 2.995a.34.34 0 00.474 0 .336.336 0 000-.474l-3.47-3.467v-8.132l5.751 5.75v4.905a.334.334 0 10.668 0v-4.238l3.825 3.826v4.905c0 .185.15.333.336.333a.332.332 0 00.333-.333V53.07l3.376 3.376a.33.33 0 00.472 0 .336.336 0 000-.475l-3.376-3.375h4.236a.334.334 0 100-.67h-4.903l-3.825-3.825h4.235a.334.334 0 00.336-.333.337.337 0 00-.336-.337h-4.905l-5.75-5.748h8.132l3.467 3.47a.34.34 0 00.474 0 .336.336 0 000-.474l-2.994-2.996h5.41l3.466 3.47a.34.34 0 00.474 0 .336.336 0 000-.474l-2.995-2.996h4.775a.332.332 0 00.333-.333.334.334 0 00-.333-.336z"
          fill="#00A4B6" fill-rule="evenodd" opacity=".4" />
      </symbol>
      <symbol id="social-facebook" viewBox="0 0 50 50">
        <circle cx="25" cy="25" r="24.5" fill="none" stroke="#eae9e9" />
        <path
          d="M35.1 33.7c0 .6-.5 1.1-1.1 1.1h-5.1v-7.7h2.6l.4-3h-3v-1.9c0-.9.2-1.5 1.5-1.5H32V18c-.3 0-1.2-.1-2.3-.1-2.3 0-3.9 1.4-3.9 4v2.2h-2.6v3h2.6v7.7h-9.6c-.6 0-1.1-.5-1.1-1.1V16.1c0-.6.5-1.1 1.1-1.1H34c.6 0 1.1.5 1.1 1.1v17.6"
          fill="#3d5b9b" />
      </symbol>
      <symbol id="social-instagram" viewBox="0 0 50 50">
        <circle cx="25" cy="25" r="24.5" stroke="#EAE9E9" />
        <path
          d="M29.652 37h-9.304C16.297 37 13 33.704 13 29.653v-9.306C13 16.297 16.297 13 20.348 13h9.304C33.703 13 37 16.296 37 20.347v9.303c.003 4.054-3.294 7.35-7.348 7.35zm-9.304-22.202a5.56 5.56 0 00-5.553 5.552v9.303a5.56 5.56 0 005.553 5.552h9.304a5.56 5.56 0 005.553-5.552v-9.306a5.56 5.56 0 00-5.553-5.552h-9.304v.003z"
          fill="#0A000F" />
        <path
          d="M25 31.071a6.077 6.077 0 01-6.07-6.07A6.077 6.077 0 0125 18.934a6.077 6.077 0 016.07 6.069c.003 3.346-2.72 6.07-6.07 6.07zm0-10.303a4.235 4.235 0 00-4.232 4.23A4.235 4.235 0 0025 29.23a4.235 4.235 0 004.232-4.231A4.237 4.237 0 0025 20.768zm8.314-2.499a1.58 1.58 0 11-3.16 0 1.58 1.58 0 013.16 0z"
          fill="#0A000F" />
      </symbol>
      <symbol id="social-linkedin" viewBox="0 0 50 50">
        <circle cx="25" cy="25" r="24.5" fill="none" stroke="#eae9e9" />
        <path
          d="M15 17.3c0-1.3 1-2.3 2.4-2.3 1.5 0 2.3 1 2.4 2.3 0 1.3-.9 2.3-2.4 2.3-1.5.1-2.4-.9-2.4-2.3zm15.2 4c-2.3 0-3.7 1.3-3.9 2.3v-2.1h-4.4c.1 1.1 0 13.5 0 13.5h4.4v-7.4c0-.4 0-.7.1-1 .3-.8 1-1.7 2.1-1.7 1.5 0 2.2 1.3 2.2 3.1v7h4.5v-7.5c0-4.3-2.2-6.2-5-6.2zm-14.7.1h3.9v13.5h-3.9V21.4z"
          fill="#0077b5" />
      </symbol>
      <symbol id="social-pinterest" viewBox="0 0 275 275">
        <g fill="none" fill-rule="evenodd">
          <path
            d="M274.857 137.819c0 75.768-61.423 137.19-137.191 137.19S.476 213.587.476 137.819C.476 62.05 61.898.628 137.666.628S274.857 62.05 274.857 137.819"
            fill="#FFF" />
          <path
            d="M137.507 15.849c-67.362 0-121.97 54.608-121.97 121.97 0 51.671 32.148 95.805 77.52 113.577-1.066-9.65-2.029-24.452.424-34.989 2.215-9.517 14.302-60.627 14.302-60.627s-3.649-7.305-3.649-18.107c0-16.959 9.831-29.621 22.07-29.621 10.406 0 15.432 7.814 15.432 17.181 0 10.465-6.662 26.111-10.101 40.609-2.873 12.143 6.088 22.044 18.061 22.044 21.68 0 38.343-22.861 38.343-55.855 0-29.203-20.983-49.622-50.948-49.622-34.704 0-55.074 26.031-55.074 52.931 0 10.483 4.039 21.724 9.077 27.835.996 1.208 1.142 2.266.845 3.497-.925 3.855-2.983 12.14-3.386 13.834-.532 2.233-1.769 2.706-4.079 1.631-15.235-7.092-24.758-29.364-24.758-47.252 0-38.475 27.955-73.809 80.589-73.809 42.31 0 75.192 30.149 75.192 70.443 0 42.037-26.504 75.866-63.29 75.866-12.361 0-23.979-6.421-27.956-14.006 0 0-6.116 23.289-7.598 28.994-2.755 10.594-10.188 23.874-15.161 31.975 11.413 3.534 23.541 5.44 36.115 5.44 67.362 0 121.97-54.607 121.97-121.969s-54.608-121.97-121.97-121.97"
            fill="#bd081c" />
        </g>
      </symbol>
      <symbol id="social-twitter" viewBox="0 0 50 50">
        <circle cx="25" cy="25" r="24.5" fill="none" stroke="#eae9e9" />
        <path
          d="M35 17.3c-.8.5-1.7.8-2.7 1-.8-.8-1.9-1.3-3.1-1.3-2.3 0-4.2 1.9-4.2 4.3 0 .3 0 .7.1 1-3.5-.2-6.6-1.9-8.7-4.5-.4.6-.6 1.4-.6 2.1 0 1.5.7 2.8 1.9 3.5-.7 0-1.3-.2-1.9-.5v.1c0 2.1 1.5 3.8 3.4 4.2-.4.1-.7.2-1.1.2-.3 0-.5 0-.8-.1.5 1.7 2.1 2.9 3.9 3-1.4 1-3.2 1.7-5.2 1.7-.3 0-.7 0-1-.1 1.9 1.2 4.1 1.9 6.5 1.9 7.8 0 12-6.5 12-12.1v-.6c.8-.6 1.5-1.4 2.1-2.2-.8.3-1.6.6-2.4.7.8-.5 1.5-1.3 1.8-2.3"
          fill="#1da1f2" />
      </symbol>
      <symbol id="social-youtube" viewBox="0 0 1022 719">
        <path
          d="M1011.25 155.562s-9.98-70.389-40.608-101.386C931.8 13.493 888.262 13.291 868.296 10.91 725.359.577 510.946.577 510.946.577h-.444s-214.408 0-357.35 10.333c-19.967 2.381-63.489 2.583-102.345 43.266-30.626 30.997-40.592 101.386-40.592 101.386S0 238.222 0 320.881v77.493c0 82.658 10.215 165.317 10.215 165.317s9.966 70.39 40.592 101.387c38.856 40.684 89.896 39.397 112.629 43.661C245.153 716.575 510.725 719 510.725 719s214.634-.323 357.571-10.655c19.966-2.382 63.504-2.583 102.346-43.267 30.628-30.997 40.608-101.387 40.608-101.387s10.2-82.659 10.2-165.317v-77.493c0-82.659-10.2-165.319-10.2-165.319zM405.272 492.281l-.047-287 276 144-275.953 143z"
          fill="#cd201f" fill-rule="evenodd" />
      </symbol>
      <symbol id="star" viewBox="0 0 18 17">
        <path d="M8.56 13.5l-5.29 2.781 1.01-5.89L0 6.219l5.914-.86L8.56 0l2.645 5.36 5.914.859-4.28 4.172 1.01 5.89z"
          fill="#FB6058" fill-rule="evenodd" />
      </symbol>
      <symbol id="success" viewBox="0 0 22 22">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M0 11C0 4.925 4.924 0 11 0c6.075 0 11 4.925 11 11s-4.925 11-11 11C4.924 22 0 17.075 0 11zm15.832-3.445a1 1 0 00-1.664-1.11L9.505 13.44 6.7 10.686a1 1 0 00-1.402 1.428l3.667 3.6a1 1 0 001.533-.16l5.333-8z"
          fill="#39B54A" />
      </symbol>
      <symbol id="sunlight-lots" viewBox="0 0 188 188">
        <path d="M26.111 161.889l26.111-62.666H26.111v62.666z" fill="#dedede" fill-rule="nonzero" />
        <path
          d="M127.919 47.388c0-7.224 5.857-13.08 13.081-13.08 7.224 0 13.081 5.856 13.081 13.08S148.224 60.469 141 60.469c-7.224 0-13.081-5.857-13.081-13.081z"
          fill="#fb6058" fill-rule="nonzero" stroke="#fb6058" stroke-width="2" />
        <path
          d="M141 70.5l.109-4.257M141 27.757l.109-4.257M129.25 67.352l2.129-3.687m19.243-33.33l2.128-3.687M120.648 58.75l3.687-2.129m33.33-19.242l3.686-2.129M117.5 46.903l4.257.097m38.486-.097L164.5 47m-43.852-11.75l3.687 2.129m33.33 19.242l3.686 2.129m-32.1-32.102l2.128 3.686m19.243 33.331l2.128 3.687"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M161.889 100.223c0-.549-.451-1-1-1H27.111c-.549 0-1 .451-1 1v60.666c0 .549.451 1 1 1h133.778c.549 0 1-.451 1-1v-60.666z"
          fill="none" stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M26.111 161.889h134.778c.549 0 1-.451 1-1v-60.666c0-.549-.451-1-1-1H51.569" fill="none" stroke="#fb6058"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M148.834 113.277c0-.549-.451-1-1-1h-22.806c-.549 0-1 .451-1 1v29.333c0 .549.451 1 1 1h22.806c.549 0 1-.451 1-1v-29.333zm-41.778 0c-.001-.552-.449-1-1-1H83.25c-.549 0-1 .451-1 1v29.333c0 .549.451 1 1 1h22.806c.548-.001.999-.452.999-1l.001-29.333zm-63.972-2.304h20.889v43.083H43.084v-43.083zm-6.528 47H70.5v3.916H36.556v-3.916z"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M47 130.556h13.056v18.277H47v-18.277zm0-15.666h13.056v11.75H47v-11.75zm-6.527 39.166h26.111v3.916H40.473v-3.916zM22.194 93.347l34.924-20.236"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M22.194 99.222l70.011-40.191c.303-.174.677-.177.983-.008l72.617 40.199" fill="none" stroke="#2d3d4d"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="sunlight-not-much" viewBox="0 0 188 188">
        <path d="M124.68 161.889l26.764-62.666H26.111v62.666h98.569z" fill="#dedede" fill-rule="nonzero" />
        <path
          d="M127.919 40.861c0-7.224 5.857-13.081 13.081-13.081 7.224 0 13.081 5.857 13.081 13.081 0 7.224-5.857 13.08-13.081 13.08-7.224 0-13.081-5.856-13.081-13.08z"
          fill="#fb6058" fill-rule="nonzero" stroke="#fb6058" stroke-width="2" />
        <path
          d="M141 63.973l.109-4.257M141 21.23l.109-4.258M129.25 60.824l2.129-3.686m19.243-33.33l2.128-3.687m-32.102 32.102l3.687-2.129m33.33-19.242l3.686-2.129M117.5 40.376l4.257.097m38.486-.097l4.257.097m-43.852-11.75l3.687 2.128m33.33 19.243l3.686 2.128m-32.1-32.102l2.128 3.687m19.243 33.331l2.128 3.686"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M161.889 100.223c0-.549-.451-1-1-1H27.111c-.549 0-1 .451-1 1v60.666c0 .549.451 1 1 1h133.778c.549 0 1-.451 1-1v-60.666z"
          fill="none" stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M41.778 110.973h20.889v43.083H41.778v-43.083zm-6.528 47h33.944v3.916H35.25v-3.916z" fill="none"
          stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M45.694 130.556H58.75v18.277H45.694v-18.277zm0-15.666H58.75v11.75H45.694v-11.75zm-6.527 39.166h26.111v3.916H39.167v-3.916z"
          fill="none" stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M150.792 99.223h10.097c.549 0 1 .451 1 1v60.666c0 .549-.451 1-1 1h-36.861" fill="none" stroke="#fb6058"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M135.777 113.277c0-.549-.451-1-1-1h-29.333c-.549 0-1 .451-1 1v29.333c0 .549.451 1 1 1h29.333c.549 0 1-.451 1-1v-29.333z"
          fill="none" stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M135.777 134.473v8.139c0 .549-.451 1-1 1h-2.916M22.194 93.347l69.36-40.189a.999.999 0 01.982-.012l59.561 32.68"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M22.194 99.222l70.011-40.191c.303-.174.677-.177.983-.008l72.617 40.199" fill="none" stroke="#2d3d4d"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="sunlight-some" viewBox="0 0 188 188">
        <path d="M78.986 161.889l26.764-62.666H26.111v62.666h52.875z" fill="#dedede" fill-rule="nonzero" />
        <path
          d="M127.919 47.388c0-7.224 5.857-13.08 13.081-13.08 7.224 0 13.081 5.856 13.081 13.08S148.224 60.469 141 60.469c-7.224 0-13.081-5.857-13.081-13.081z"
          fill="#fb6058" fill-rule="nonzero" stroke="#fb6058" stroke-width="2" />
        <path
          d="M141 70.5l.109-4.257M141 27.757l.109-4.257M129.25 67.352l2.129-3.687m19.243-33.33l2.128-3.687M120.648 58.75l3.687-2.129m33.33-19.242l3.686-2.129M117.5 46.903l4.257.097m38.486-.097L164.5 47m-43.852-11.75l3.687 2.129m33.33 19.242l3.686 2.129m-32.1-32.102l2.128 3.686m19.243 33.331l2.128 3.687"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M161.889 100.223c0-.549-.451-1-1-1H27.111c-.549 0-1 .451-1 1v60.666c0 .549.451 1 1 1h133.778c.549 0 1-.451 1-1v-60.666z"
          fill="none" stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M105.097 99.223h55.792c.549 0 1 .451 1 1v60.666c0 .549-.451 1-1 1H77.028" fill="none" stroke="#fb6058"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M135.777 113.277c0-.549-.451-1-1-1h-29.333c-.549 0-1 .451-1 1v29.333c0 .549.451 1 1 1h29.333c.549 0 1-.451 1-1v-29.333z"
          fill="none" stroke="#fb6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M43.084 110.973h20.889v43.083H43.084v-43.083zm-6.528 47H70.5v3.916H36.556v-3.916z" fill="none"
          stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M47 130.556h13.056v18.277H47v-18.277zm0-15.666h13.056v11.75H47v-11.75zm-6.527 39.166h26.111v3.916H40.473v-3.916z"
          fill="none" stroke="#2d3d4d" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M22.194 93.347l69.36-40.189a.999.999 0 01.982-.012l21.7 11.906" fill="none" stroke="#fb6058"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M22.194 99.222l70.011-40.191c.303-.174.677-.177.983-.008l72.617 40.199" fill="none" stroke="#2d3d4d"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="tea-cup-icon" viewBox="0 0 24 26">
        <path
          d="M20.279 18.188h.843c1.587 0 2.878-1.261 2.878-2.812 0-1.55-1.291-2.812-2.878-2.812h-.719v-1.07a.622.622 0 00-.63-.614H1.986a.622.622 0 00-.629.614v5.203c0 3.452 1.936 6.47 4.802 8.074H.63a.622.622 0 00-.629.614c0 .34.281.615.629.615h21.209a.622.622 0 00.629-.614.622.622 0 00-.63-.615h-6.236c2.445-1.37 4.212-3.765 4.678-6.583zm.843-4.396c.893 0 1.62.71 1.62 1.584 0 .873-.727 1.584-1.62 1.584h-.719v-3.168h.719zM6.468 15.558l.829.579v2.359H5.64v-2.359l.828-.58zm4.412 9.213c-4.558 0-8.266-3.622-8.266-8.074v-4.589H5.84v2.378l-1.195.835a.611.611 0 00-.263.5v3.29c0 .339.281.614.629.614h2.915a.622.622 0 00.629-.615v-3.288a.61.61 0 00-.263-.5l-1.195-.836V12.11h12.048v4.589c0 4.45-3.708 8.073-8.265 8.073zM9.23 8.647c.1.05.208.073.316.073.222 0 .438-.1.556-.28.176-.268.069-.608-.238-.76a1.78 1.78 0 01-.696-.606c-.516-.779-.212-1.775.674-2.232l.025-.011c.733-.368 1.258-.962 1.478-1.673a2.43 2.43 0 00-.315-2.092A2.99 2.99 0 009.86.05c-.307-.153-.698-.06-.873.207-.176.267-.07.607.238.76.288.143.529.353.696.605.252.38.318.823.187 1.247-.13.419-.437.77-.866.989l-.02.007C7.71 4.625 7.187 6.313 8.06 7.63c.28.425.685.776 1.17 1.017zm2.649-1.017c.28.424.685.775 1.17 1.017.1.05.21.073.317.073.222 0 .438-.1.556-.28.175-.268.069-.608-.237-.761a1.777 1.777 0 01-.697-.605 1.449 1.449 0 01-.187-1.247c.128-.417.434-.765.86-.985l.026-.011c1.513-.76 2.035-2.448 1.163-3.765A2.986 2.986 0 0013.68.05c-.307-.153-.698-.06-.873.207-.175.268-.069.608.238.76.288.143.529.353.696.605.517.781.212 1.78-.68 2.236-.006.003-.013.004-.019.008-.733.367-1.258.961-1.477 1.673-.22.71-.109 1.454.314 2.092z"
          fill="#334557" />
      </symbol>
      <symbol id="tick-circle" viewBox="0 0 20 20">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M10 20c5.523 0 10-4.477 10-10S15.523 0 10 0 0 4.477 0 10s4.477 10 10 10zm5-12.833L13.87 6l-5.483 5.667L6.13 9.333 5 10.5 8.387 14 15 7.167z"
          fill="#fff" />
      </symbol>
      <symbol id="tick-inactive" viewBox="0 0 55 55">
        <g transform="translate(1 1)" fill="none" fill-rule="evenodd">
          <path
            d="M19.517 36.4l2.097 2.096L41.54 18.571l-2.099-2.098-19.925 19.926zm-7.341-7.342l7.341 7.341 2.097-2.097-7.34-7.342-2.098 2.098z"
            fill="currentColor" />
          <circle stroke="currentColor" stroke-width="2" cx="26.5" cy="26.5" r="26.5" />
        </g>
      </symbol>
      <symbol id="tick-v2" viewBox="0 0 30 30">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M10.849 20.212l1.252 1.226L24 9.788 22.746 8.56 10.85 20.212zm-4.385-4.296l4.384 4.292 1.252-1.226-4.383-4.293-1.253 1.226z"
          fill="#FB6058" />
        <path
          d="M10.849 20.212l-.35-.357-.365.357.365.357.35-.357zm1.252 1.226l-.35.357.35.343.35-.343-.35-.357zM24 9.788l.35.356.364-.357-.365-.357-.35.357zM22.746 8.56l.35-.358-.35-.342-.35.342.35.358zM6.464 15.915l-.35-.357-.365.357.365.358.35-.357zm4.384 4.293l-.35.357.35.343.35-.343-.35-.357zm1.252-1.226l.35.357.365-.357-.365-.358-.35.358zm-4.383-4.293l.35-.358-.35-.342-.35.342.35.358zm2.782 5.88l1.252 1.226.7-.714-1.252-1.226-.7.714zm1.952 1.226l11.9-11.65-.7-.715-11.9 11.65.7.715zM24.35 9.43l-1.253-1.227-.7.715 1.254 1.227.699-.715zm-1.953-1.227L10.5 19.855l.7.714L23.097 8.918l-.7-.715zm-16.283 8.07l4.384 4.292.7-.714-4.384-4.293-.7.715zm5.084 4.292l1.252-1.226-.7-.715-1.252 1.227.7.714zm1.252-1.94L8.067 14.33l-.7.715 4.383 4.293.7-.715zM7.367 14.33l-1.253 1.227.7.715 1.253-1.227-.7-.715z"
          fill="#FB6058" />
      </symbol>
      <symbol id="tick-white" viewBox="0 0 27 19">
        <path d="M.184 10.276l7.926 7.927L26.107.206" stroke-width="2" stroke="#FFF" fill="none" />
      </symbol>
      <symbol id="tree" viewBox="0 0 67 94">
        <path
          d="M44.08 65.884a21.892 21.892 0 01-11.02-2.963 21.892 21.892 0 01-11.02 2.963C9.89 65.884 0 55.999 0 43.844c0-7.975 4.324-15.282 11.183-19.169a21.824 21.824 0 01-.164-2.64C11.019 9.885 20.91 0 33.059 0 45.21 0 55.1 9.886 55.1 22.04c0 .874-.056 1.757-.164 2.639 6.854 3.883 11.183 11.19 11.183 19.17 0 12.15-9.89 22.035-22.04 22.035zm-11.02-8.981a2.75 2.75 0 011.576.493 16.426 16.426 0 009.444 2.978c9.114 0 16.53-7.415 16.53-16.53 0-6.526-3.867-12.458-9.85-15.108a2.75 2.75 0 01-1.57-3.117c.267-1.207.405-2.408.405-3.584 0-9.115-7.415-16.53-16.53-16.53-9.116 0-16.53 7.415-16.53 16.53 0 1.17.133 2.377.405 3.584a2.76 2.76 0 01-1.571 3.117c-5.983 2.65-9.849 8.582-9.849 15.109 0 9.114 7.414 16.53 16.53 16.53 3.389 0 6.656-1.027 9.444-2.98a2.732 2.732 0 011.566-.492z"
          fill="currentColor" fill-rule="evenodd" />
        <path
          d="M32.753 93.758A2.753 2.753 0 0130 91.005V59.753a2.754 2.754 0 015.506 0v31.252a2.753 2.753 0 01-2.753 2.753z"
          fill="currentColor" fill-rule="evenodd" />
      </symbol>
      <symbol id="trustpilot-logo-lightbg" viewBox="0 0 516.085 58.186">
        <path
          d="M119.272 3.316c0 .558-.029 1.059-.088 1.472-.068.425-.161.757-.296 1.012a1.5 1.5 0 01-.487.558 1.243 1.243 0 01-.655.177h-16.289v50.214c0 .245-.063.449-.181.63-.119.178-.33.322-.626.423a7.134 7.134 0 01-1.184.275c-.494.064-1.113.11-1.856.11-.715 0-1.329-.047-1.836-.11a7.14 7.14 0 01-1.213-.275c-.296-.101-.5-.245-.622-.423a1.107 1.107 0 01-.178-.63V6.534H77.464c-.238 0-.449-.058-.648-.177a1.317 1.317 0 01-.466-.558c-.122-.254-.219-.587-.296-1.012a8.8 8.8 0 01-.106-1.472c0-.57.034-1.07.106-1.5.077-.438.174-.787.296-1.054.119-.263.275-.462.466-.584A1.28 1.28 0 0177.464 0h40.282c.241 0 .456.064.655.178.187.122.352.322.487.584.135.266.228.616.296 1.054.059.43.088.929.088 1.5zm49.414 53.432c0 .245-.042.449-.144.63-.084.178-.275.331-.575.439-.301.127-.714.211-1.252.277a20.77 20.77 0 01-2.149.092c-.778 0-1.412-.034-1.903-.092-.49-.065-.885-.161-1.184-.294a1.527 1.527 0 01-.694-.561 3.281 3.281 0 01-.431-.898l-5.32-13.648a52.507 52.507 0 00-1.946-4.273c-.673-1.294-1.472-2.411-2.398-3.357a9.792 9.792 0 00-3.265-2.188c-1.252-.524-2.757-.786-4.517-.786h-5.152v24.657a.99.99 0 01-.203.63c-.135.178-.338.322-.626.423a6.48 6.48 0 01-1.158.275 14.56 14.56 0 01-1.862.11c-.744 0-1.361-.047-1.852-.11a7.172 7.172 0 01-1.193-.275c-.296-.101-.498-.245-.626-.423a1.144 1.144 0 01-.173-.63V3.138c0-1.167.301-1.979.918-2.444.609-.46 1.255-.694 1.944-.694h12.304c1.463 0 2.681.043 3.65.114.968.069 1.843.161 2.618.251 2.237.384 4.212.997 5.93 1.831 1.714.833 3.156 1.891 4.323 3.18a12.81 12.81 0 012.614 4.407c.583 1.649.872 3.482.872 5.478 0 1.947-.263 3.68-.787 5.219-.519 1.532-1.268 2.889-2.254 4.069a14.936 14.936 0 01-3.536 3.067c-1.37.87-2.91 1.597-4.614 2.194a10.79 10.79 0 012.601 1.582 14.124 14.124 0 012.17 2.306c.668.901 1.302 1.93 1.908 3.101a50.655 50.655 0 011.785 3.937l5.19 12.748c.419 1.075.685 1.833.808 2.259.111.435.179.768.179 1.005zm-11.594-40.68c0-2.267-.508-4.187-1.523-5.752-1.015-1.561-2.716-2.691-5.101-3.376-.749-.21-1.59-.357-2.529-.452-.943-.089-2.17-.131-3.693-.131h-6.492v19.515h7.521c2.03 0 3.776-.251 5.261-.741 1.476-.496 2.707-1.18 3.688-2.061a8.044 8.044 0 002.174-3.108c.462-1.192.694-2.493.694-3.894zm63.173 20.15c0 3.463-.498 6.557-1.514 9.272-1.011 2.723-2.458 5.016-4.344 6.897-1.878 1.878-4.17 3.299-6.873 4.268-2.694.978-5.769 1.461-9.203 1.461-3.148 0-6.006-.456-8.595-1.367-2.579-.909-4.8-2.253-6.653-4.034-1.849-1.777-3.27-4.006-4.268-6.667-.998-2.676-1.497-5.76-1.497-9.249V1.413c0-.237.055-.437.173-.622.118-.174.325-.312.613-.414a6.39 6.39 0 011.151-.263c.466-.077 1.083-.114 1.849-.114.706 0 1.307.037 1.806.114.495.069.888.161 1.167.263.275.102.478.24.597.414.118.185.177.385.177.622v34.45c0 2.648.321 4.952.965 6.916.651 1.971 1.582 3.607 2.799 4.913a11.54 11.54 0 004.408 2.955c1.713.658 3.646.993 5.795.993 2.199 0 4.161-.318 5.874-.976 1.725-.64 3.176-1.608 4.37-2.902 1.188-1.296 2.094-2.897 2.723-4.81.639-1.898.952-4.131.952-6.689V1.413a1.1 1.1 0 01.178-.622c.118-.174.322-.312.613-.414a6.463 6.463 0 011.167-.263c.486-.077 1.1-.114 1.832-.114.706 0 1.298.037 1.78.114a5.78 5.78 0 011.146.263c.284.102.486.24.621.414.132.185.191.385.191.622v34.805zm43.425 5.257c0 2.657-.495 5.018-1.468 7.082a15.214 15.214 0 01-4.057 5.255c-1.733 1.444-3.772 2.53-6.115 3.258-2.348.724-4.864 1.092-7.576 1.092-1.895 0-3.645-.157-5.256-.488-1.62-.316-3.059-.705-4.327-1.174-1.265-.466-2.331-.944-3.189-1.438-.858-.496-1.45-.915-1.781-1.266-.343-.35-.592-.799-.748-1.331-.161-.541-.242-1.264-.242-2.166 0-.638.029-1.167.089-1.59.055-.423.144-.767.262-1.029.114-.262.262-.443.431-.546.177-.101.385-.151.613-.151.406 0 .985.251 1.726.741.744.498 1.695 1.04 2.863 1.614 1.163.589 2.568 1.135 4.208 1.633 1.646.512 3.545.77 5.698.77 1.628 0 3.126-.211 4.479-.655 1.353-.439 2.518-1.057 3.498-1.853a8.184 8.184 0 002.245-2.947c.516-1.163.778-2.487.778-3.972 0-1.606-.363-2.977-1.082-4.107-.732-1.137-1.688-2.132-2.885-2.992a25.185 25.185 0 00-4.086-2.354 1099.15 1099.15 0 00-4.69-2.186 48.416 48.416 0 01-4.674-2.467 19.698 19.698 0 01-4.057-3.185c-1.197-1.223-2.166-2.661-2.91-4.301-.741-1.642-1.112-3.617-1.112-5.913 0-2.364.431-4.462 1.294-6.31a12.85 12.85 0 013.577-4.653c1.528-1.248 3.347-2.208 5.458-2.859 2.105-.66 4.389-.986 6.83-.986 1.253 0 2.508.114 3.781.325 1.261.221 2.457.516 3.577.876 1.122.364 2.117.774 2.995 1.218.863.452 1.447.82 1.718 1.1.28.275.461.49.545.651.094.161.166.368.221.61.058.249.106.546.131.9.029.347.05.796.05 1.345 0 .529-.03.994-.072 1.401-.039.41-.11.757-.199 1.032-.085.27-.212.482-.369.609a.866.866 0 01-.544.198c-.327 0-.82-.207-1.511-.617a50.947 50.947 0 00-2.508-1.375c-.99-.509-2.166-.969-3.511-1.396-1.358-.418-2.881-.635-4.568-.635-1.568 0-2.94.217-4.102.635-1.163.427-2.128.99-2.88 1.678a6.733 6.733 0 00-1.705 2.487 8.38 8.38 0 00-.567 3.063c0 1.569.364 2.919 1.092 4.06.728 1.137 1.695 2.141 2.901 3.011a25.893 25.893 0 004.129 2.397c1.543.728 3.117 1.468 4.715 2.214a61.558 61.558 0 014.716 2.439 20.155 20.155 0 014.132 3.144 14.43 14.43 0 012.914 4.284c.753 1.646 1.125 3.583 1.125 5.82zm47.812-38.167c0 .566-.03 1.057-.089 1.468-.064.419-.156.76-.291 1.01a1.38 1.38 0 01-.492.558 1.184 1.184 0 01-.649.177H293.72v50.13c0 .238-.058.449-.181.623-.116.181-.327.325-.624.43a6.774 6.774 0 01-1.183.262c-.496.077-1.114.116-1.853.116-.719 0-1.332-.039-1.831-.116a6.644 6.644 0 01-1.206-.262c-.3-.105-.507-.249-.629-.43a1.107 1.107 0 01-.179-.623V6.521h-16.261c-.236 0-.453-.054-.648-.177a1.245 1.245 0 01-.469-.558 4.477 4.477 0 01-.293-1.01 8.601 8.601 0 01-.113-1.468c0-.563.041-1.071.113-1.492.078-.438.179-.791.293-1.054.119-.263.275-.462.469-.584.196-.114.412-.178.648-.178h40.208c.238 0 .458.064.649.178.19.122.364.322.492.584.135.263.227.616.291 1.054.059.422.089.929.089 1.492z"
          fill="#231f20" />
        <path
          d="M356.234 16.96c0 2.897-.477 5.507-1.438 7.838-.958 2.326-2.317 4.314-4.095 5.947-1.773 1.646-3.951 2.915-6.535 3.808-2.581.898-5.654 1.341-9.243 1.341h-6.575v20.855c0 .245-.073.449-.204.63-.135.178-.341.322-.625.423a6.36 6.36 0 01-1.169.275c-.485.064-1.106.11-1.851.11-.744 0-1.367-.047-1.852-.11a6.825 6.825 0 01-1.193-.275c-.303-.101-.508-.245-.626-.423a1.085 1.085 0 01-.179-.63V3.316c0-1.193.313-2.042.94-2.554.627-.508 1.323-.762 2.106-.762h12.393c1.258 0 2.454.051 3.607.161 1.145.101 2.508.325 4.065.669 1.57.342 3.169.987 4.797 1.924a15.14 15.14 0 014.136 3.468c1.138 1.374 2.009 2.96 2.618 4.767.613 1.806.923 3.794.923 5.971zm-8.104.626c0-2.356-.44-4.322-1.324-5.899-.876-1.583-1.966-2.767-3.265-3.541-1.299-.774-2.64-1.269-4.023-1.475a28.007 28.007 0 00-4.056-.313h-7.114v23.224h6.937c2.326 0 4.259-.299 5.793-.896 1.536-.6 2.829-1.418 3.87-2.48 1.044-1.06 1.838-2.326 2.378-3.802.533-1.476.804-3.084.804-4.818zm26.316 39.125c0 .236-.058.447-.178.621s-.326.318-.626.419c-.296.107-.684.194-1.179.271-.482.073-1.104.107-1.844.107-.711 0-1.32-.034-1.823-.107-.508-.077-.91-.165-1.201-.271-.296-.101-.508-.245-.627-.419a1.118 1.118 0 01-.174-.621V1.426c0-.241.068-.45.195-.626.135-.174.355-.318.672-.415a6.626 6.626 0 011.197-.271A12.458 12.458 0 01370.619 0c.74 0 1.361.043 1.844.114.495.069.884.165 1.179.271.3.097.507.241.626.415.12.176.178.384.178.626v55.285zm44.254-1.878c0 .587-.026 1.09-.084 1.492-.061.402-.157.754-.297 1.032-.13.282-.291.496-.486.621-.19.136-.422.208-.698.208h-25.602c-.685 0-1.327-.232-1.941-.698-.609-.462-.914-1.277-.914-2.44V1.434c0-.237.052-.449.175-.626.123-.178.329-.322.622-.423a6.647 6.647 0 011.215-.271A13.257 13.257 0 01392.527 0c.741 0 1.358.043 1.852.114.495.073.885.165 1.185.271.299.101.508.245.626.423.122.178.182.39.182.626v50.081h20.763c.276 0 .508.067.698.204.195.131.356.327.486.58.14.254.237.587.297 1.01.058.415.084.922.084 1.524zm52.438-26.458c0 4.552-.54 8.668-1.615 12.331-1.075 3.658-2.681 6.772-4.818 9.347-2.132 2.566-4.813 4.543-8.035 5.923-3.223 1.382-6.992 2.07-11.285 2.07-4.243 0-7.912-.63-11.006-1.889-3.091-1.27-5.654-3.104-7.673-5.521-2.017-2.407-3.523-5.401-4.509-8.975-.987-3.576-1.489-7.686-1.489-12.33 0-4.445.545-8.481 1.616-12.122 1.074-3.626 2.69-6.715 4.843-9.259 2.149-2.546 4.835-4.499 8.063-5.88C438.457.694 442.212 0 446.513 0c4.154 0 7.77.626 10.853 1.874 3.074 1.252 5.639 3.075 7.691 5.468 2.045 2.399 3.567 5.363 4.571 8.89 1.002 3.529 1.51 7.576 1.51 12.143zm-7.93.522c0-3.198-.288-6.168-.851-8.917-.57-2.74-1.515-5.122-2.833-7.144-1.318-2.016-3.084-3.598-5.287-4.731-2.205-1.126-4.936-1.696-8.185-1.696-3.248 0-5.977.612-8.184 1.83-2.204 1.223-3.994 2.843-5.372 4.878-1.382 2.038-2.366 4.413-2.96 7.125-.597 2.721-.892 5.575-.892 8.565 0 3.312.274 6.362.828 9.158.55 2.779 1.481 5.186 2.782 7.202 1.309 2.023 3.059 3.596 5.247 4.709 2.189 1.12 4.939 1.679 8.255 1.679 3.273 0 6.031-.61 8.268-1.833 2.229-1.221 4.031-2.863 5.394-4.943 1.36-2.081 2.335-4.485 2.913-7.207.58-2.732.877-5.627.877-8.675zm52.877-25.581c0 .558-.03 1.059-.093 1.472-.055.425-.156.757-.293 1.012-.132.25-.295.436-.487.558a1.212 1.212 0 01-.646.177h-16.287v50.214c0 .245-.064.449-.192.63-.113.178-.321.322-.62.423-.3.103-.693.198-1.189.275-.485.064-1.108.11-1.858.11a14.82 14.82 0 01-1.831-.11 6.953 6.953 0 01-1.208-.275c-.298-.101-.508-.245-.625-.423a1.1 1.1 0 01-.18-.63V6.534h-16.3c-.229 0-.444-.058-.639-.177a1.322 1.322 0 01-.476-.558 4.487 4.487 0 01-.289-1.012 8.267 8.267 0 01-.11-1.472c0-.57.034-1.07.11-1.5a4.94 4.94 0 01.289-1.054c.121-.263.281-.462.476-.584.195-.113.41-.177.639-.177h40.289c.238 0 .45.064.646.178.193.122.355.322.487.584.137.266.237.616.293 1.054.064.43.094.929.094 1.5z"
          fill="#929497" />
        <path d="M0 .012v25.665s19.433 9.25 29.297 25.788c0 0 4.352-36.523 28.785-51.453H0z" fill="#f9a220" />
        <linearGradient id="trustpilot-logo-lightbga" gradientUnits="userSpaceOnUse" x1="29.041" y1="58.186" x2="29.041"
          y2="25.006">
          <stop offset="0" stop-color="#e37a27" />
          <stop offset=".472" stop-color="#f9a220" />
        </linearGradient>
        <path
          d="M0 46.824v11.363h26.11c0-.001-9.947-10.593-26.11-11.363zm33.118 11.362h24.964v-33.18c-14.128 3.827-24.964 33.18-24.964 33.18z"
          fill="url(#trustpilot-logo-lightbga)" />
        <linearGradient id="trustpilot-logo-lightbgb" gradientUnits="userSpaceOnUse" x1="28.926" y1="22.669" x2="28.926"
          y2=".394">
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#f9a220" />
        </linearGradient>
        <path d="M.388.401L57.465.395s-9.366 3.308-19.46 21.915c0 0-22.076 2.246-37.564-4.496L.388.401z" opacity=".5"
          fill="url(#trustpilot-logo-lightbgb)" />
        <linearGradient id="trustpilot-logo-lightbgc" gradientUnits="userSpaceOnUse" x1="29.041" y1=".242" x2="29.041"
          y2="57.957">
          <stop offset="0" stop-color="#4a484a" />
          <stop offset="1" />
        </linearGradient>
        <path
          d="M29.297 51.466C19.433 34.927 0 25.677 0 25.677v21.146c16.128.794 26.11 11.363 26.11 11.363h7.008s10.836-29.353 24.964-33.18V.012l-.074.045C33.641 15.015 29.297 51.466 29.297 51.466z"
          fill="url(#trustpilot-logo-lightbgc)" />
      </symbol>
      <symbol id="trustpilot-logo" viewBox="0 0 1132.8 278.2">
        <path
          d="M297.7 98.6h114.7V120h-45.1v120.3h-24.8V120h-44.9V98.6zm109.8 39.1h21.2v19.8h.4c.7-2.8 2-5.5 3.9-8.1 1.9-2.6 4.2-5.1 6.9-7.2 2.7-2.2 5.7-3.9 9-5.3 3.3-1.3 6.7-2 10.1-2 2.6 0 4.5.1 5.5.2s2 .3 3.1.4v21.8c-1.6-.3-3.2-.5-4.9-.7-1.7-.2-3.3-.3-4.9-.3-3.8 0-7.4.8-10.8 2.3-3.4 1.5-6.3 3.8-8.8 6.7-2.5 3-4.5 6.6-6 11s-2.2 9.4-2.2 15.1v48.8h-22.6V137.7zm164 102.6h-22.2V226h-.4c-2.8 5.2-6.9 9.3-12.4 12.4-5.5 3.1-11.1 4.7-16.8 4.7-13.5 0-23.3-3.3-29.3-10s-9-16.8-9-30.3v-65.1H504v62.9c0 9 1.7 15.4 5.2 19.1 3.4 3.7 8.3 5.6 14.5 5.6 4.8 0 8.7-.7 11.9-2.2 3.2-1.5 5.8-3.4 7.7-5.9 2-2.4 3.4-5.4 4.3-8.8.9-3.4 1.3-7.1 1.3-11.1v-59.5h22.6v102.5zm38.5-32.9c.7 6.6 3.2 11.2 7.5 13.9 4.4 2.6 9.6 4 15.7 4 2.1 0 4.5-.2 7.2-.5s5.3-1 7.6-1.9c2.4-.9 4.3-2.3 5.9-4.1 1.5-1.8 2.2-4.1 2.1-7-.1-2.9-1.2-5.3-3.2-7.1-2-1.9-4.5-3.3-7.6-4.5-3.1-1.1-6.6-2.1-10.6-2.9-4-.8-8-1.7-12.1-2.6-4.2-.9-8.3-2.1-12.2-3.4-3.9-1.3-7.4-3.1-10.5-5.4-3.1-2.2-5.6-5.1-7.4-8.6-1.9-3.5-2.8-7.8-2.8-13 0-5.6 1.4-10.2 4.1-14 2.7-3.8 6.2-6.8 10.3-9.1 4.2-2.3 8.8-3.9 13.9-4.9 5.1-.9 10-1.4 14.6-1.4 5.3 0 10.4.6 15.2 1.7 4.8 1.1 9.2 2.9 13.1 5.5 3.9 2.5 7.1 5.8 9.7 9.8 2.6 4 4.2 8.9 4.9 14.6h-23.6c-1.1-5.4-3.5-9.1-7.4-10.9-3.9-1.9-8.4-2.8-13.4-2.8-1.6 0-3.5.1-5.7.4-2.2.3-4.2.8-6.2 1.5-1.9.7-3.5 1.8-4.9 3.2-1.3 1.4-2 3.2-2 5.5 0 2.8 1 5 2.9 6.7 1.9 1.7 4.4 3.1 7.5 4.3 3.1 1.1 6.6 2.1 10.6 2.9 4 .8 8.1 1.7 12.3 2.6 4.1.9 8.1 2.1 12.1 3.4 4 1.3 7.5 3.1 10.6 5.4 3.1 2.3 5.6 5.1 7.5 8.5 1.9 3.4 2.9 7.7 2.9 12.7 0 6.1-1.4 11.2-4.2 15.5-2.8 4.2-6.4 7.7-10.8 10.3-4.4 2.6-9.4 4.6-14.8 5.8-5.4 1.2-10.8 1.8-16.1 1.8-6.5 0-12.5-.7-18-2.2-5.5-1.5-10.3-3.7-14.3-6.6-4-3-7.2-6.7-9.5-11.1-2.3-4.4-3.5-9.7-3.7-15.8H610zm74.6-69.7h17.1v-30.8h22.6v30.8h20.4v16.9h-20.4v54.8c0 2.4.1 4.4.3 6.2.2 1.7.7 3.2 1.4 4.4.7 1.2 1.8 2.1 3.3 2.7 1.5.6 3.4.9 6 .9 1.6 0 3.2 0 4.8-.1 1.6-.1 3.2-.3 4.8-.7v17.5c-2.5.3-5 .5-7.3.8-2.4.3-4.8.4-7.3.4-6 0-10.8-.6-14.4-1.7-3.6-1.1-6.5-2.8-8.5-5-2.1-2.2-3.4-4.9-4.2-8.2-.7-3.3-1.2-7.1-1.3-11.3v-60.5h-17.1v-17.1zm76.1 0h21.4v13.9h.4c3.2-6 7.6-10.2 13.3-12.8 5.7-2.6 11.8-3.9 18.5-3.9 8.1 0 15.1 1.4 21.1 4.3 6 2.8 11 6.7 15 11.7 4 5 6.9 10.8 8.9 17.4 2 6.6 3 13.7 3 21.2 0 6.9-.9 13.6-2.7 20-1.8 6.5-4.5 12.2-8.1 17.2-3.6 5-8.2 8.9-13.8 11.9-5.6 3-12.1 4.5-19.7 4.5-3.3 0-6.6-.3-9.9-.9-3.3-.6-6.5-1.6-9.5-2.9-3-1.3-5.9-3-8.4-5.1-2.6-2.1-4.7-4.5-6.5-7.2h-.4v51.2h-22.6V137.7zm79 51.4c0-4.6-.6-9.1-1.8-13.5-1.2-4.4-3-8.2-5.4-11.6-2.4-3.4-5.4-6.1-8.9-8.1-3.6-2-7.7-3.1-12.3-3.1-9.5 0-16.7 3.3-21.5 9.9-4.8 6.6-7.2 15.4-7.2 26.4 0 5.2.6 10 1.9 14.4 1.3 4.4 3.1 8.2 5.7 11.4 2.5 3.2 5.5 5.7 9 7.5 3.5 1.9 7.6 2.8 12.2 2.8 5.2 0 9.5-1.1 13.1-3.2 3.6-2.1 6.5-4.9 8.8-8.2 2.3-3.4 4-7.2 5-11.5.9-4.3 1.4-8.7 1.4-13.2zm39.9-90.5h22.6V120h-22.6V98.6zm0 39.1h22.6v102.6h-22.6V137.7zm42.8-39.1H945v141.7h-22.6V98.6zm91.9 144.5c-8.2 0-15.5-1.4-21.9-4.1-6.4-2.7-11.8-6.5-16.3-11.2-4.4-4.8-7.8-10.5-10.1-17.1-2.3-6.6-3.5-13.9-3.5-21.8 0-7.8 1.2-15 3.5-21.6 2.3-6.6 5.7-12.3 10.1-17.1 4.4-4.8 9.9-8.5 16.3-11.2 6.4-2.7 13.7-4.1 21.9-4.1s15.5 1.4 21.9 4.1c6.4 2.7 11.8 6.5 16.3 11.2 4.4 4.8 7.8 10.5 10.1 17.1 2.3 6.6 3.5 13.8 3.5 21.6 0 7.9-1.2 15.2-3.5 21.8-2.3 6.6-5.7 12.3-10.1 17.1-4.4 4.8-9.9 8.5-16.3 11.2-6.4 2.7-13.7 4.1-21.9 4.1zm0-17.9c5 0 9.4-1.1 13.1-3.2 3.7-2.1 6.7-4.9 9.1-8.3 2.4-3.4 4.1-7.3 5.3-11.6 1.1-4.3 1.7-8.7 1.7-13.2 0-4.4-.6-8.7-1.7-13.1s-2.9-8.2-5.3-11.6c-2.4-3.4-5.4-6.1-9.1-8.2-3.7-2.1-8.1-3.2-13.1-3.2s-9.4 1.1-13.1 3.2c-3.7 2.1-6.7 4.9-9.1 8.2-2.4 3.4-4.1 7.2-5.3 11.6-1.1 4.4-1.7 8.7-1.7 13.1 0 4.5.6 8.9 1.7 13.2 1.1 4.3 2.9 8.2 5.3 11.6 2.4 3.4 5.4 6.2 9.1 8.3 3.7 2.2 8.1 3.2 13.1 3.2zm58.4-87.5h17.1v-30.8h22.6v30.8h20.4v16.9h-20.4v54.8c0 2.4.1 4.4.3 6.2.2 1.7.7 3.2 1.4 4.4.7 1.2 1.8 2.1 3.3 2.7 1.5.6 3.4.9 6 .9 1.6 0 3.2 0 4.8-.1 1.6-.1 3.2-.3 4.8-.7v17.5c-2.5.3-5 .5-7.3.8-2.4.3-4.8.4-7.3.4-6 0-10.8-.6-14.4-1.7-3.6-1.1-6.5-2.8-8.5-5-2.1-2.2-3.4-4.9-4.2-8.2-.7-3.3-1.2-7.1-1.3-11.3v-60.5h-17.1v-17.1z"
          fill="#191919" />
        <path fill="#00b67a"
          d="M271.3 98.6H167.7L135.7 0l-32.1 98.6L0 98.5l83.9 61L51.8 258l83.9-60.9 83.8 60.9-32-98.5 83.8-60.9z" />
        <path fill="#005128" d="M194.7 181.8l-7.2-22.3-51.8 37.6z" />
      </symbol>
      <symbol id="trustpilot-rating-1" viewBox="0 0 512 96">
        <path fill="#ff3722" d="M0 0h96v96H0z" />
        <path d="M104 0h96v96h-96zm104 0h96v96h-96zm104 0h96v96h-96zm104 0h96v96h-96z" fill="#e5e5e5" />
        <path
          d="M48 64.7L62.6 61l6.1 18.8L48 64.7zm33.6-24.3H55.9L48 16.2l-7.9 24.2H14.4l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM152 64.7l14.6-3.7 6.1 18.8L152 64.7zm33.6-24.3h-25.7L152 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM256 64.7l14.6-3.7 6.1 18.8L256 64.7zm33.6-24.3h-25.7L256 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM360 64.7l14.6-3.7 6.1 18.8L360 64.7zm33.6-24.3h-25.7L360 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM464 64.7l14.6-3.7 6.1 18.8L464 64.7zm33.6-24.3h-25.7L464 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15z"
          fill="#fff" />
      </symbol>
      <symbol id="trustpilot-rating-2" viewBox="0 0 512 96">
        <path d="M0 0h96v96H0zm104 0h96v96h-96z" fill="#ff8622" />
        <path d="M208 0h96v96h-96zm104 0h96v96h-96zm104 0h96v96h-96z" fill="#e5e5e5" />
        <path
          d="M48 64.7L62.6 61l6.1 18.8L48 64.7zm33.6-24.3H55.9L48 16.2l-7.9 24.2H14.4l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM152 64.7l14.6-3.7 6.1 18.8L152 64.7zm33.6-24.3h-25.7L152 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM256 64.7l14.6-3.7 6.1 18.8L256 64.7zm33.6-24.3h-25.7L256 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM360 64.7l14.6-3.7 6.1 18.8L360 64.7zm33.6-24.3h-25.7L360 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM464 64.7l14.6-3.7 6.1 18.8L464 64.7zm33.6-24.3h-25.7L464 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15z"
          fill="#fff" />
      </symbol>
      <symbol id="trustpilot-rating-3" viewBox="0 0 512 96">
        <path d="M0 0h96v96H0zm104 0h96v96h-96zm104 0h96v96h-96z" fill="#ffce00" />
        <path d="M312 0h96v96h-96zm104 0h96v96h-96z" fill="#e5e5e5" />
        <path
          d="M48 64.7L62.6 61l6.1 18.8L48 64.7zm33.6-24.3H55.9L48 16.2l-7.9 24.2H14.4l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM152 64.7l14.6-3.7 6.1 18.8L152 64.7zm33.6-24.3h-25.7L152 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM256 64.7l14.6-3.7 6.1 18.8L256 64.7zm33.6-24.3h-25.7L256 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM360 64.7l14.6-3.7 6.1 18.8L360 64.7zm33.6-24.3h-25.7L360 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM464 64.7l14.6-3.7 6.1 18.8L464 64.7zm33.6-24.3h-25.7L464 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15z"
          fill="#fff" />
      </symbol>
      <symbol id="trustpilot-rating-4" viewBox="0 0 512 96">
        <path d="M0 0h96v96H0zm104 0h96v96h-96zm104 0h96v96h-96zm104 0h96v96h-96z" fill="#73cf11" />
        <path fill="#e5e5e5" d="M416 0h96v96h-96z" />
        <path
          d="M48 64.7L62.6 61l6.1 18.8L48 64.7zm33.6-24.3H55.9L48 16.2l-7.9 24.2H14.4l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM152 64.7l14.6-3.7 6.1 18.8L152 64.7zm33.6-24.3h-25.7L152 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM256 64.7l14.6-3.7 6.1 18.8L256 64.7zm33.6-24.3h-25.7L256 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM360 64.7l14.6-3.7 6.1 18.8L360 64.7zm33.6-24.3h-25.7L360 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM464 64.7l14.6-3.7 6.1 18.8L464 64.7zm33.6-24.3h-25.7L464 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15z"
          fill="#fff" />
      </symbol>
      <symbol id="trustpilot-rating-5" viewBox="0 0 512 96">
        <path d="M0 0h96v96H0zm104 0h96v96h-96zm104 0h96v96h-96zm104 0h96v96h-96zm104 0h96v96h-96z" fill="#00b67a" />
        <path
          d="M48 64.7L62.6 61l6.1 18.8L48 64.7zm33.6-24.3H55.9L48 16.2l-7.9 24.2H14.4l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM152 64.7l14.6-3.7 6.1 18.8L152 64.7zm33.6-24.3h-25.7L152 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM256 64.7l14.6-3.7 6.1 18.8L256 64.7zm33.6-24.3h-25.7L256 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM360 64.7l14.6-3.7 6.1 18.8L360 64.7zm33.6-24.3h-25.7L360 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15zM464 64.7l14.6-3.7 6.1 18.8L464 64.7zm33.6-24.3h-25.7L464 16.2l-7.9 24.2h-25.7l20.8 15-7.9 24.2 20.8-15 12.8-9.2 20.7-15z"
          fill="#fff" />
      </symbol>
      <symbol id="ui-booking" viewBox="0 0 150 150">
        <path
          d="M43.367 32.717h-7.579c-3.751 0-6.788 2.906-6.788 6.495v78.011c0 3.589 3.037 6.494 6.788 6.494h79.424c3.751 0 6.788-2.905 6.788-6.494V39.212c0-3.59-3.037-6.495-6.788-6.495h-7.914"
          stroke="currentColor" stroke-width="2" />
        <circle cx="51.597" cy="66.12" r="4.241" stroke="currentColor" stroke-width="2.712" />
        <circle cx="76.522" cy="66.277" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="64.605" cy="66.328" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="88.905" cy="66.277" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="101.287" cy="66.277" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="64.138" cy="82.788" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="51.755" cy="82.788" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="76.522" cy="82.788" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="88.905" cy="82.788" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="101.287" cy="82.788" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="64.138" cy="99.299" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="51.755" cy="99.299" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="76.522" cy="99.299" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="88.905" cy="99.299" r="3.628" fill="currentColor" stroke="currentColor" />
        <circle cx="101.287" cy="99.299" r="3.628" fill="currentColor" stroke="currentColor" />
        <path d="M53.893 32.717H70.5m11.533 0H97" stroke="currentColor" stroke-width="2" stroke-linecap="square" />
        <path
          d="M75.813 25.717c2.723 0 4.812 2.046 4.812 4.426v8.962c0 2.38-2.089 4.426-4.813 4.426-2.723 0-4.812-2.046-4.812-4.426v-8.962c0-2.38 2.089-4.426 4.813-4.426zM48.098 26c2.724 0 4.813 2.045 4.813 4.426v8.961c0 2.38-2.089 4.426-4.813 4.426-2.723 0-4.812-2.045-4.812-4.426v-8.961c0-2.38 2.089-4.426 4.812-4.426zm54.805 0c2.724 0 4.813 2.045 4.813 4.426v8.961c0 2.38-2.089 4.426-4.813 4.426-2.723 0-4.812-2.045-4.812-4.426v-8.961c0-2.38 2.089-4.426 4.812-4.426z"
          stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="ui-bullet" viewBox="0 0 9 9">
        <circle cx="4.5" cy="4.5" r="4.5" fill="#FB6058" />
      </symbol>
      <symbol id="ui-calendar" viewBox="0 0 60 60">
        <path d="M46.5 10.5H48a3 3 0 013 3V51a3 3 0 01-3 3H12a3 3 0 01-3-3V13.5a3 3 0 013-3h1.912m26.588 0H33m-6 0h-7.5"
          stroke="currentColor" stroke-width="1.2" />
        <rect x="13.5" y="6" width="6" height="9" rx="3" stroke="currentColor" stroke-width="1.2" />
        <rect x="27" y="6" width="6" height="9" rx="3" stroke="currentColor" stroke-width="1.2" />
        <rect x="40.5" y="6" width="6" height="9" rx="3" stroke="currentColor" stroke-width="1.2" />
        <path d="M9 20.625c16.625-.006 42-.018 42-.018" stroke="currentColor" stroke-width="1.2" />
        <path d="M24 38.59l4.355 4.91L39 31.5" stroke="currentColor" stroke-width="1.2" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-caution" viewBox="0 0 40 40">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M20 0C8.953 0 0 8.955 0 20s8.953 20 20 20c11.045 0 20-8.955 20-20S31.045 0 20 0z" fill="#B00020" />
        <path
          d="M17.052 6.667l.77 18.35h4.394l.77-18.35h-5.934zm-.385 23.462c0 1.869 1.502 3.204 3.314 3.204 1.81 0 3.352-1.335 3.352-3.204 0-1.832-1.541-3.167-3.352-3.167-1.812 0-3.314 1.335-3.314 3.167z"
          fill="#fff" />
      </symbol>
      <symbol id="ui-clock" viewBox="0 0 60 60">
        <path
          d="M29.857 3.675c14.492 0 26.464 11.704 26.468 26.176-.282 14.489-11.985 26.192-26.474 26.474C15.374 56.321 3.675 44.63 3.675 29.857c0-14.765 11.693-26.182 26.182-26.182z"
          stroke="currentColor" stroke-width="1.35" />
        <path d="M27.898 34.05h11.475m-11.475 0V15.825" stroke="currentColor" stroke-width="1.35" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-cross-round" viewBox="0 0 20 20">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M0 10C0 4.477 4.477 0 10 0s10 4.477 10 10-4.477 10-10 10S0 15.523 0 10zm14.707-4.707a1 1 0 010 1.414L11.414 10l3.293 3.293a1 1 0 01-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 01-1.414-1.414L8.586 10 5.293 6.707a1 1 0 011.414-1.414L10 8.586l3.293-3.293a1 1 0 011.414 0z"
          fill="currentColor" />
      </symbol>
      <symbol id="ui-edit" viewBox="0 0 30 30">
        <path fill="currentColor"
          d="M24.947 5.366L11.469 18.844l-2.875-2.875L22.072 2.49zM10.375 19.938L7.5 17.063 6.437 21l3.938-1.063zM6 25h19v1H6z" />
      </symbol>
      <symbol id="ui-email" viewBox="0 0 40 40">
        <path
          d="M5 10h30M5 10v20m0-20l7.5 7.5 3.75 3.75M35 10v20m0-20L23.75 21.25M35 30H5m30 0l-11.25-8.75M5 30l11.25-8.75m0 0l2.336 2.336a2 2 0 002.828 0l2.336-2.336"
          stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="ui-filters" viewBox="0 0 25 25">
        <path d="M4.375 6.875h16.25m-13.65 3.75h11.05M9.9 14.375h5.2m-3.9 3.75h2.6" stroke="currentColor"
          stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-info" viewBox="0 0 40 40">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M20 0C8.953 0 0 8.955 0 20s8.953 20 20 20c11.045 0 20-8.955 20-20S31.045 0 20 0z" fill="#D7D6D6" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M24.7 27.69l-.316 1.291a61.49 61.49 0 01-2.27.856c-.565.197-1.222.294-1.97.294-1.148 0-2.041-.28-2.678-.84-.638-.56-.956-1.272-.956-2.134 0-.334.023-.677.071-1.027.048-.35.125-.745.23-1.186l1.187-4.196c.105-.402.195-.783.266-1.143.073-.358.108-.686.108-.983 0-.536-.11-.91-.331-1.121-.221-.21-.643-.317-1.268-.317-.308 0-.622.049-.943.144a12.83 12.83 0 00-.83.273l.317-1.294a29.7 29.7 0 012.23-.811c.71-.225 1.38-.338 2.015-.338 1.14 0 2.02.275 2.639.827.617.551.927 1.266.927 2.148 0 .182-.021.503-.064.962a6.45 6.45 0 01-.238 1.266l-1.18 4.18c-.097.335-.184.719-.26 1.15-.077.43-.115.757-.115.977 0 .555.123.934.372 1.135.25.202.68.302 1.291.302.287 0 .614-.05.977-.15.363-.1.627-.19.79-.266zm.3-17.548c0 .729-.274 1.35-.826 1.862-.55.513-1.213.77-1.988.77a2.85 2.85 0 01-2-.77c-.554-.512-.833-1.133-.833-1.862 0-.726.279-1.35.834-1.867.555-.518 1.221-.775 1.999-.775.775 0 1.438.259 1.988.775.552.518.826 1.14.826 1.867z"
          fill="#fff" />
      </symbol>
      <symbol id="ui-installer" viewBox="0 0 60 60">
        <g clip-path="url(#ui-installerclip0)" stroke="currentColor" stroke-width=".968">
          <path
            d="M52.933 53.655c0-10.4-7.486-19.118-17.558-21.444L30.253 34.3l-5.628-2.09C14.552 34.538 7.066 43.257 7.066 53.656m22.94-27.886c3.18 0 4.196-1.373 4.306-2.06h-8.281c0 .686.795 2.06 3.975 2.06z"
            stroke-linecap="round" stroke-linejoin="round" />
          <path
            d="M41.13 13.306s.337 1.807.337 3.919C41.467 24.28 36.333 30 30 30s-11.382-5.72-11.382-12.775c0-1.896.253-3.92.253-3.92" />
          <path
            d="M38.712 4.573h0c1.002.976 1.617 2.322 1.978 3.823.361 1.499.461 3.118.461 4.603a.338.338 0 01-.323.328l-.03-.004a114.8 114.8 0 00-3.532-.4c-2.101-.203-4.82-.408-7.266-.408-2.449 0-5.168.219-7.268.436a108.422 108.422 0 00-3.532.426l-.028.005a.34.34 0 01-.324-.329c0-1.505.021-3.139.304-4.655.284-1.515.82-2.86 1.812-3.825h0c1-.973 2.47-1.585 4.115-1.95 1.638-.362 3.397-.469 4.92-.469 1.525 0 3.201.107 4.755.469 1.557.362 2.954.972 3.958 1.95z" />
          <path
            d="M18.87 13.306c2.031-1.72 11.383-9.106 22.26 0M13.216 50.648h26.328c1.626-1.955 3.32-2.618 5.691-2.395.135.013.271.031.407.055 1.228.217 2.488.907 3.659 2.34.135.26.162.782-.813.782h-2.846v3.91h2.846c.27 0 .813.156.813.781 0 .782-5.692 5.473-9.757 0H13.216c-.813 0-2.44-.782-2.44-2.736 0-1.955 1.627-2.737 2.44-2.737z"
            stroke-linecap="round" stroke-linejoin="round" />
        </g>
      </symbol>
      <symbol id="ui-lock" viewBox="0 0 40 40">
        <path
          d="M8 19.686C8 17.65 9.679 16 11.75 16h16.5c2.071 0 3.75 1.65 3.75 3.686v12.628C32 34.35 30.321 36 28.25 36h-16.5C9.679 36 8 34.35 8 32.314V19.686zM27 16v-5.106a1.76 1.76 0 00-.01-.196C26.784 6.964 23.733 4 20 4c-3.734 0-6.785 2.964-6.99 6.698a1.76 1.76 0 00-.01.196V16"
          stroke="currentColor" />
        <path
          d="M20 26.039c1.105 0 2-.904 2-2.02A2.01 2.01 0 0020 22c-1.105 0-2 .904-2 2.02a2.01 2.01 0 002 2.019zm0 0V30"
          stroke="currentColor" stroke-linecap="round" />
      </symbol>
      <symbol id="ui-minus-outline" viewBox="0 0 32 32">
        <rect x="1" y="1.213" width="30" height="30" rx="15" stroke="currentColor" />
        <path d="M24.485 16.213H7.515" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-minus" viewBox="0 0 32 32">
        <path d="M24.485 16.213H7.515" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-phone" viewBox="0 0 20 20">
        <path
          d="M11.736 13.163C10.9 12.921 7.84 9.59 7.201 8.64c-.126-.189-.216-.297 0-.512.27-.27 1.215-1.427 1.35-2.208.108-.625-1.08-1.912-1.7-2.343-.352-.216-.703-.534-2.43.7-2.376 1.697-.54 5.467 2.43 8.672 2.969 3.204 6.316 4.282 7.53 4.012 1.594-.354 2.619-2.209 2.619-2.99-.054-.943-1.944-2.316-2.618-2.316-.567-.08-1.809 1.75-2.646 1.508zm-.864-5.952c.594.01 1.874.437 2.24 2.074m-2.24-4.04c1.251.054 3.833.938 4.157 4.04m-4.022-5.952c1.728.027 5.345 1.255 5.993 5.952"
          stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-plus-outline" viewBox="0 0 32 32">
        <rect x="1" y="1.213" width="30" height="30" rx="15" stroke="currentColor" />
        <path d="M16 24.698V7.728m8.485 8.485H7.515" stroke="currentColor" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-plus" viewBox="0 0 40 40">
        <path d="M6 20h28M20 6v28" stroke="currentColor" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-refresh" viewBox="0 0 40 40">
        <path opacity=".01" fill="#D8D8D8" d="M0 0h40v40H0z" />
        <path d="M30.657 5.343V11H25M8.343 33.657V28H14" stroke="currentColor" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M8.51 28c2.53 3.627 6.732 6 11.49 6 7.732 0 14-6.268 14-14 0-1.762-.325-3.447-.92-5m-2.58-4.26A13.966 13.966 0 0020 6C12.268 6 6 12.268 6 20c0 1.39.203 2.733.58 4"
          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-repair" viewBox="0 0 150 150">
        <circle cx="75.5" cy="75.5" r="50.5" stroke="currentColor" stroke-width="2" stroke-dasharray="16 16" />
        <path
          d="M51.34 91.144l7.543 6.915a3 3 0 004.148-.09l16.62-16.62c.523-.523 1.288-.706 2.01-.552C85.91 81.7 93.636 81.547 98 75c4.144-6.216 2.996-12.755 1.64-16.142-.266-.664-1.125-.735-1.564-.17l-5.475 7.04c-.38.487-.965.787-1.573.684-2.989-.505-7.444-3.165-7.123-7.668.035-.481.3-.91.68-1.208 2-1.572 5.293-4.062 6.93-5.295.507-.382.535-1.135.007-1.49C87.128 47.797 78.664 46.837 72 53.5c-5.977 5.977-5.361 11.448-3.931 14.47.437.923.376 2.071-.356 2.785l-16.441 16.03a3 3 0 00.067 4.36z"
          stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="ui-reset" viewBox="0 0 40 40">
        <path opacity=".01" fill="#D8D8D8" d="M0 0h40v40H0z" />
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M8.95 26.612a13.62 13.62 0 0014.873 6.658c7.331-1.665 11.835-9.05 10.264-16.333-1.571-7.282-8.903-11.86-16.235-10.195A13.482 13.482 0 007.208 19.443l-2.205-2.216a1.022 1.022 0 00-1.485 0 1.035 1.035 0 000 1.493l4.138 4.16c.213.214.425.32.743.32.212 0 .53-.106.743-.32l4.139-4.266a1.035 1.035 0 000-1.494 1.022 1.022 0 00-1.486 0l-2.672 2.77c.024-5.255 3.702-10.063 9.149-11.275 6.284-1.457 12.463 2.496 13.93 8.738 1.466 6.242-2.514 12.484-8.798 13.836-5.237 1.145-10.37-1.248-12.778-5.617a.52.52 0 01-.105-.208.52.52 0 00-.105-.208c-.314-.208-.628-.416-1.047-.313-.42.105-.838.625-.734 1.145l.315.624z"
          fill="currentcolor" />
      </symbol>
      <symbol id="ui-settings" viewBox="0 0 40 40">
        <path d="M5 8h5.745m6.473 0H35" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="13.745" cy="8.077" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" r="3" />
        <path d="M5 32h5.745m6.473 0H35" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="13.745" cy="32.154" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" r="3" />
        <path d="M35 20h-5.745m-6.473 0H5" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" />
        <circle r="3" transform="matrix(-1 0 0 1 26.255 19.615)" stroke="currentColor" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-tick-outline" viewBox="0 0 18 18">
        <circle cx="9" cy="9" r="8.5" stroke="currentColor" />
        <path d="M5.146 8.633L7.95 11.57l4.909-5.143" stroke="currentColor" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="ui-tick-round" viewBox="0 0 20 20">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M10 0C4.477 0 0 4.477 0 10s4.477 10 10 10 10-4.477 10-10S15.523 0 10 0zm5.25 7.162a1 1 0 00-1.5-1.324l-5.401 6.11-2.07-2.574a1 1 0 10-1.559 1.252l2.813 3.5a1 1 0 001.529.036l6.187-7z"
          fill="currentColor" />
      </symbol>
      <symbol id="ui-tick" viewBox="0 0 40 40">
        <path d="M8 20l7.636 9.333L32 10.667" stroke="currentColor" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="up" viewBox="24 20 17 11">
        <path fill="currentColor" fill-rule="evenodd"
          d="M40.897 29.45L39.347 31l-6.9-6.899L25.55 31 24 29.45 32.447 21" />
      </symbol>
      <symbol id="utility-room" viewBox="0 0 100 100">
        <path fill="none" stroke="#fb6058" d="M22.222 18.75h55.556v59.722H22.222z" />
        <path fill="none" stroke="#fb6058"
          d="M29.167 78.472h6.25v2.778h-6.25zm-3.473-56.944h6.25v2.778h-6.25zm39.584 56.944h6.25v2.778h-6.25zM22.917 26.389h54.166" />
        <circle cx="64.583" cy="22.917" r="1.389" fill="none" stroke="#fb6058" />
        <circle cx="68.75" cy="22.917" r="1.389" fill="none" stroke="#fb6058" />
        <circle cx="72.917" cy="22.917" r="1.389" fill="none" stroke="#fb6058" />
        <circle cx="50" cy="53.472" r="13.889" fill="none" stroke="#fb6058" />
        <circle cx="50" cy="53.472" r="17.361" fill="none" stroke="#fb6058" />
        <path
          d="M36.806 54.514l3.239-2.082a2.996 2.996 0 013.244 0l1.485.954a3 3 0 003.422-.123l.618-.464a3.001 3.001 0 013.7.078l.251.205a2.998 2.998 0 003.607.145l.878-.608a3 3 0 013.508.067l2.437 1.828"
          fill="none" stroke="#fb6058" />
      </symbol>
      <symbol id="warning" viewBox="0 0 19 19">
        <g fill="none" fill-rule="evenodd">
          <path d="M9.463 0a9.464 9.464 0 100 18.927A9.464 9.464 0 009.464 0z" fill="#F86257" />
          <path
            d="M8.474 3.638h1.979c.15 0 .274.122.274.272l-.546 7.508c0 .15-.122.272-.273.272H9.02a.272.272 0 01-.272-.272L8.202 3.91a.27.27 0 01.272-.272zm.989 11.653a1.33 1.33 0 110-2.66 1.33 1.33 0 010 2.66z"
            fill="#FFF" />
        </g>
      </symbol>
      <symbol id="airingcupboard-middle" viewBox="0 0 188 188">
        <path
          d="M32.368 92.351h-1.941L41.589 48h4.689m-13.91 44.351v54.891m0-54.89h121.82m0 0h1.941L145.937 48H53.723m100.465 44.351v54.891m4.368 0H28"
          fill="none" stroke="#2d3d4d" stroke-width="2" />
        <path fill="none" stroke="#2d3d4d" stroke-width="2" d="M46 39h7.52v15.04H46z" />
        <path fill="none" stroke="currentColor" stroke-width="2" d="M87 110h14v16H87z" />
      </symbol>
      <symbol id="airingcupboard-outsidewall" viewBox="0 0 188 188">
        <path
          d="M32.368 92.351h-1.941L41.589 48h4.689m-13.91 44.351v54.891m0-54.89h121.82m0 0h1.941L145.937 48H53.723m100.465 44.351v54.891m4.368 0H28"
          fill="none" stroke="#2d3d4d" stroke-width="2" />
        <path fill="none" stroke="#2d3d4d" stroke-width="2" d="M46 39h7.52v15.04H46z" />
        <path fill="none" stroke="currentColor" stroke-width="2" d="M131 111h14v16h-14z" />
      </symbol>
      <symbol id="bedroom" viewBox="0 0 100 100">
        <path
          d="M78.385 52.999V26.91c.087-1.1-.312-3.299-2.604-3.299H24.74c-.955 0-2.865.66-2.865 3.299v26.089m-3.125 9.596v13.794h5.99l2.864-6.597h44.792l3.385 6.597h5.469V62.595m-62.5 0v-9.596h62.5v9.596m-62.5 0h62.5"
          fill="none" stroke="currentColor" />
        <path d="M50 47.222H27.778v5.556H50m0-5.556v5.556m0-5.556h22.222v5.556H50" fill="none" stroke="currentColor" />
      </symbol>
      <symbol id="boundary-less-than" viewBox="0 0 100 100">
        <path
          d="M0 18.75h17.708l3.82 13.194h-3.82M0 31.944h17.708m0 0V81.25M.347 38.194h3.82v18.229m0 18.23v-18.23m3.819 18.23H.347m3.82-18.23H.347M99.306 18.75H53.125l-3.819 13.194h3.819m46.181 0H53.125m0 0V81.25m33.333-43.056H66.667v18.229m0 18.23v-18.23m-3.82 18.23h27.084m-23.264-18.23h19.791m.001-17.881l-.001 36.111m30.556-36.459H97.222v18.229m0 18.23v-18.23m-3.819 18.23h27.083m-23.264-18.23h19.792"
          fill="none" stroke="#2d3d4d" />
        <path d="M20.834 39.236h11.111v16.667H20.834V39.236zm2.777 13.889h5.556m-5.556-2.777h5.556" fill="none"
          stroke="currentColor" />
        <path
          d="M49.312 49.312a.502.502 0 000-.707l-3.182-3.182a.5.5 0 00-.707.707l2.828 2.829-2.828 2.828a.5.5 0 00.707.707l3.182-3.182zm-14.943-.707a.5.5 0 000 .707l3.182 3.182a.5.5 0 00.707-.707l-2.829-2.828 2.829-2.829a.5.5 0 00-.707-.707l-3.182 3.182zm14.589-.146H34.722v1h14.236v-1z"
          fill="currentColor" fill-rule="nonzero" />
        <path d="M0 82.292l99.653-.001" fill="none" stroke="#2d3d4d" />
      </symbol>
      <symbol id="boundary-more-than" viewBox="0 0 188 188">
        <clipPath id="boundary-more-thana">
          <path d="M0 0h188v188H0z" />
        </clipPath>
        <g clip-path="url(#boundary-more-thana)">
          <g stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
            <path
              d="M0 35.25h33.292l7.18 24.806h-7.18m0 0H0m33.292 0v92.694M.653 71.806h7.18v34.27m0 0v34.271m0-34.271H.653m14.36 34.271H.654M188 35.25h-33.292l-7.18 24.806h7.18m0 0H188m-33.292 0v92.694m32.639-80.944h-7.18v34.27m0 0v34.271m0-34.271h7.18m-14.361 34.271h14.361"
              stroke="#2d3d4d" />
            <path d="M60.056 73.764H39.167v31.333h20.889zM44.39 94.653h10.444m-10.445 5.222h10.445z" stroke="#fb6058" />
          </g>
          <path
            d="M150.193 122.749a.999.999 0 000-1.414l-6.364-6.364a.999.999 0 10-1.414 1.414l5.657 5.657-5.657 5.657a1 1 0 001.414 1.414zm-111.4-1.414a1 1 0 000 1.414l6.364 6.364a1 1 0 101.414-1.414l-5.657-5.657 5.657-5.657a1 1 0 10-1.414-1.414zm110.693-.293H39.5v2h109.986z"
            fill="#fb6058" />
          <path d="M0 154.708h187.347" stroke="#2d3d4d" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
          <path
            d="M83.017 97v-2.772h-4.242l3.17-3.591c.84-.945 1.45-2.163 1.45-3.486 0-2.478-2.058-4.18-4.494-4.18-2.604 0-4.39 1.639-4.81 4.264l2.689.462c.168-1.092.903-1.911 1.953-1.911 1.092 0 1.722.63 1.722 1.365 0 .525-.315 1.07-.735 1.554l-5.65 6.699V97zm2.863-1.628c0 1.078.859 1.848 1.893 1.848s1.914-.77 1.914-1.848c0-1.056-.88-1.826-1.914-1.826s-1.892.77-1.892 1.826zm14.11-3.454c0 1.166-1.189 2.222-2.86 2.222-.903 0-1.937-.616-2.509-1.496l-2.596 1.672c1.1 1.716 3.102 2.904 5.104 2.904 3.63 0 5.94-2.244 5.94-5.302 0-3.212-2.112-4.928-4.708-4.928-.946 0-2.068.242-2.64.748l.374-2.332h6.006V82.59h-8.448l-1.188 7.678 1.694.88c.814-1.276 2.31-1.584 3.168-1.584 1.496 0 2.662.748 2.662 2.354zm17.089-2.442c.902 0 1.694.704 1.694 2.046V97h3.036v-5.83c0-3.102-1.518-4.664-3.96-4.664-.726 0-2.09.22-3.08 1.826-.572-1.188-1.672-1.804-3.3-1.804-1.1 0-2.2.616-2.684 1.694v-1.496h-3.058V97h3.058v-5.434c0-1.562.858-2.09 1.782-2.09.88 0 1.672.682 1.694 1.98V97h3.058v-5.434c0-1.496.792-2.09 1.76-2.09zm12.947 2.904h3.41v-2.244h-3.41v-3.388h-2.244v3.388h-3.388v2.244h3.388v3.41h2.244z"
            fill="#fb6058" />
        </g>
      </symbol>
      <symbol id="car" viewBox="0 0 188 188">
        <path
          d="M62.414 121.035h63.181m-63.181 0H46.878c-4.972 0-5.985-4.035-5.87-6.052m21.406 6.052v11.035c0 3.418-2.532 4.035-3.798 3.917H46.878c-5.87 0-5.87-2.482-5.87-6.052v-14.952m84.587 6.052h15.536c1.956 0 5.869-1.211 5.869-6.052m-21.405 6.052v8.9c0 3.57.637 6.052 5.869 6.052h10.264c.292 0 .581-.033.869-.079 1.693-.27 4.403-.508 4.403-6.994v-13.931m0 0V90.736a3.21 3.21 0 00-.057-.634c-.513-2.626-2.436-7.15-6.66-8.064m-90.989-.163c-2.761.949-8.285 3.987-8.285 8.544v24.564m8.285-33.108l9.17-26.235c.1-.29.16-.592.237-.89.294-1.123 1.303-2.75 3.713-2.75h62.711c.314 0 .625.033.924.126 1.624.504 4.412 1.916 5.415 4.502 1.104 2.848 6.339 18.355 8.819 25.41m-90.989-.163h52.478M83.474 99.067h20.369m-20.37 6.887h20.37m36.44-23.915a7.406 7.406 0 00-1.569-.164h-10.703m-26.239 0c.46-4.51 3.729-13.528 13.119-13.528 9.391 0 12.66 9.018 13.12 13.528m-26.239 0h26.239m6.965 18.916v3.439a4.019 4.019 0 01-4.02 4.02h-9.448a4.019 4.019 0 01-4.02-4.02v-3.439a4.02 4.02 0 014.02-4.02h9.448a4.02 4.02 0 014.02 4.02zm-64.49 0v3.439a4.02 4.02 0 01-4.02 4.02h-9.448a4.02 4.02 0 01-4.02-4.02v-3.439c0-2.22 1.8-4.02 4.02-4.02h9.449c2.22 0 4.02 1.8 4.02 4.02z"
          stroke="#2D3D4D" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="conservatory" viewBox="0 0 100 100">
        <path d="M81.25 19.792V81.25h-62.5" fill="none" stroke="currentColor" />
        <path
          d="M81.25 27.083H43.115l-19.504 22.57 3.494-.001v30.903m-2.452-8.68h55.903m-53.125 2.778h53.125m-53.472-25h53.472"
          fill="none" stroke="currentColor" />
        <path d="M44.444 27.43L34.375 49.652V71.18m12.153-43.402v43.75m12.318-43.75v43.75m11.987-43.75v43.75" fill="none"
          stroke="currentColor" />
      </symbol>
      <symbol id="day" viewBox="0 0 188 188">
        <path
          d="M127.435 53.166h6.32c3.713 0 6.718 2.934 6.718 6.56v76.757c0 3.625-3.005 6.56-6.718 6.56H55.146c-3.712 0-6.718-2.935-6.718-6.56V59.726c0-3.626 3.006-6.56 6.718-6.56h6.377m54.296.001H99.382m-11.612 0H73.886"
          stroke="#2D3D4D" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd"
          d="M93.629 45c-3.177 0-5.753 2.404-5.753 5.37v8.87c0 2.965 2.576 5.37 5.753 5.37s5.753-2.405 5.753-5.37v-8.87c0-2.966-2.576-5.37-5.753-5.37zm27.941 0c-3.177 0-5.753 2.404-5.753 5.37v8.87c0 2.965 2.576 5.37 5.753 5.37s5.753-2.405 5.753-5.37v-8.87c0-2.966-2.576-5.37-5.753-5.37zm-54.241 0c-3.178 0-5.753 2.404-5.753 5.37v8.87c0 2.965 2.575 5.37 5.753 5.37 3.177 0 5.752-2.405 5.752-5.37v-8.87c0-2.966-2.575-5.37-5.752-5.37z"
          stroke="#2D3D4D" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M76 99.929l14.21 15.225a1 1 0 001.513-.057L115 86" stroke="#2D3D4D" stroke-width="4"
          stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="distance-16-25m" viewBox="0 0 188 188">
        <rect x="101.528" y="20.583" width="65.889" height="14.972" rx="1" stroke="#2D3D4D" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M42.376 127.346a1 1 0 001.414 0l6.364-6.364a1 1 0 10-1.414-1.414l-5.657 5.657-5.657-5.657a1.001 1.001 0 00-1.414 1.414l6.364 6.364zm94.109-77.136a1.001 1.001 0 00-1.415 0l-6.364 6.364a1.001 1.001 0 001.415 1.414l5.656-5.657 5.657 5.657a1.001 1.001 0 001.415-1.414l-6.364-6.364zm-92.402 76.429V89.055h-2v37.584h2zm1-38.584h88.694v-2H45.083v2zm91.694-3V50.917h-2v34.138h2zm-3 3a3 3 0 003-3h-2a1 1 0 01-1 1v2zm-89.694 1a1 1 0 011-1v-2a3 3 0 00-3 3h2zm.119-23.969L39 67.564v2.539l2.312-.991v9.682h2.89V65.086zm8.628 4.996c-1.693 0-2.56 1.053-2.725 1.528-.062-1.817.496-3.902 2.477-3.902.62 0 1.198.29 1.57 1.073l2.704-1.156c-.991-2.064-2.56-2.56-4.17-2.621-5.079-.165-5.801 5.12-5.47 9.29.247 2.869 2.29 4.706 4.85 4.706 2.952 0 4.913-1.755 4.913-4.356 0-3.117-2.23-4.562-4.149-4.562zm1.156 4.397c0 .97-.805 1.775-1.775 1.775a1.79 1.79 0 01-1.776-1.775c0-.97.806-1.775 1.776-1.775s1.775.805 1.775 1.775zm13.902-5.326v-3.695h-2.849v3.695h-1.755v2.27h1.755v7.37h2.849v-7.37h2.023v-2.27h-2.023zm8.059 6.998c-1.177 0-2.167-.867-2.167-2.209 0-1.3.99-2.188 2.167-2.188 1.197 0 2.188.888 2.188 2.188 0 1.342-.99 2.21-2.188 2.21zm0 2.808c2.725 0 5.016-1.982 5.016-5.017 0-3.034-2.29-4.995-5.016-4.995-2.725 0-4.996 1.96-4.996 4.995s2.271 5.017 4.996 5.017zm20.639-.165v-2.725h-4.17l3.117-3.53c.826-.93 1.425-2.127 1.425-3.427 0-2.436-2.023-4.108-4.418-4.108-2.56 0-4.315 1.61-4.727 4.19l2.642.454c.165-1.073.888-1.878 1.92-1.878 1.073 0 1.693.62 1.693 1.342 0 .516-.31 1.053-.723 1.527l-5.553 6.586v1.569h8.794zm9.823-4.769c0 1.094-1.115 2.085-2.683 2.085-.847 0-1.817-.578-2.354-1.404l-2.436 1.57c1.032 1.61 2.911 2.724 4.79 2.724 3.406 0 5.573-2.106 5.573-4.975 0-3.014-1.981-4.624-4.417-4.624-.888 0-1.941.227-2.478.702l.351-2.188h5.636v-2.643h-7.927l-1.115 7.205 1.59.825c.764-1.197 2.167-1.486 2.972-1.486 1.404 0 2.498.702 2.498 2.209zm15.83-2.291c.846 0 1.589.66 1.589 1.92v5.14h2.849v-5.47c0-2.912-1.425-4.377-3.716-4.377-.681 0-1.961.206-2.89 1.713-.537-1.115-1.569-1.693-3.097-1.693-1.032 0-2.064.578-2.518 1.59v-1.404h-2.869v9.64h2.869v-5.098c0-1.466.805-1.961 1.672-1.961.826 0 1.569.64 1.59 1.857v5.203h2.869v-5.1c0-1.403.743-1.96 1.652-1.96z"
          fill="#FB6058" />
        <rect x="20.583" y="135.473" width="45" height="30.158" rx="1" stroke="#FB6058" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M46 150.793c0 5.576-4.521 10.097-10.098 10.097-5.576 0-10.097-4.521-10.097-10.097 0-5.577 4.52-10.098 10.097-10.098S46 145.216 46 150.793zm4.704-2.509h10.201m-10.201-6.269h10.201m-10.201 10.804h10.201m-10.383 7.627h10.201"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <rect x="26.87" y="165.916" width="4.372" height="2.501" rx="1.251" fill="#FB6058" />
        <rect x="54.924" y="165.916" width="4.372" height="2.501" rx="1.251" fill="#FB6058" />
      </symbol>
      <symbol id="distance-3-to-5m" viewBox="0 0 188 188">
        <rect fill="transparent" x="61.056" y="33.223" width="65.889" height="14.972" rx="1" stroke="#2D3D4D"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M93.293 126.707a1 1 0 001.414 0l6.364-6.364a.999.999 0 10-1.414-1.414L94 124.586l-5.657-5.657a1 1 0 10-1.414 1.414l6.364 6.364zm1.414-38.414a1 1 0 00-1.414 0l-6.364 6.364a1 1 0 001.414 1.414L94 90.414l5.657 5.657a1 1 0 101.414-1.414l-6.364-6.364zM95 126V89h-2v37h2z"
          fill="#FB6058" />
        <rect x="71.5" y="135.473" width="45" height="30.158" rx="1" stroke="#FB6058" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M96.917 150.793c0 5.576-4.521 10.097-10.098 10.097-5.576 0-10.097-4.521-10.097-10.097 0-5.577 4.52-10.098 10.097-10.098s10.098 4.521 10.098 10.098zm4.704-2.509h10.202m-10.202-6.269h10.202m-10.202 10.804h10.202m-10.385 7.627h10.202"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <rect x="77.787" y="165.916" width="4.372" height="2.501" rx="1.251" fill="#FB6058" />
        <rect x="105.841" y="165.916" width="4.372" height="2.501" rx="1.251" fill="#FB6058" />
        <path
          d="M62.725 72.036c2.079-.084 1.89 1.575 1.869 1.827-.084.714-.924 1.533-1.974 1.47-.777-.042-1.743-.672-2.037-1.323L58 75.29c.756 1.597 2.457 2.752 4.389 2.878 2.751.189 5.187-1.554 5.418-3.906.126-1.47-.84-3.192-2.352-3.738 1.008-.567 1.764-1.554 1.764-2.688.063-1.974-1.68-3.738-4.074-3.864-1.848-.105-3.465.735-4.284 2.037l2.373 1.449c.252-.42.735-.756 1.449-.756 1.029 0 1.386.903 1.386 1.323 0 .063.105 1.49-1.344 1.49h-1.449v2.52h1.449zm16.955-3.843v-3.76h-2.9v3.76h-1.785v2.31h1.785V78h2.898v-7.497h2.058v-2.31H79.68zm8.598 7.119c-1.197 0-2.205-.882-2.205-2.247 0-1.323 1.008-2.226 2.205-2.226 1.218 0 2.226.903 2.226 2.226 0 1.365-1.008 2.247-2.226 2.247zm0 2.856c2.772 0 5.103-2.016 5.103-5.103 0-3.087-2.331-5.082-5.103-5.082s-5.082 1.995-5.082 5.082c0 3.087 2.31 5.103 5.082 5.103zm20.283-5.019c0 1.113-1.134 2.12-2.73 2.12-.861 0-1.848-.587-2.394-1.427l-2.478 1.596c1.05 1.638 2.961 2.772 4.872 2.772 3.465 0 5.67-2.142 5.67-5.061 0-3.066-2.016-4.704-4.494-4.704-.903 0-1.974.23-2.52.714l.357-2.226h5.733v-2.688h-8.064l-1.134 7.329 1.617.84c.777-1.218 2.205-1.512 3.024-1.512 1.428 0 2.541.714 2.541 2.247zm16.503-2.331c.861 0 1.617.672 1.617 1.953V78h2.898v-5.565c0-2.961-1.449-4.452-3.78-4.452-.693 0-1.995.21-2.94 1.743-.546-1.134-1.596-1.722-3.15-1.722-1.05 0-2.1.588-2.562 1.617v-1.428h-2.919V78h2.919v-5.187c0-1.491.819-1.995 1.701-1.995.84 0 1.596.65 1.617 1.89V78h2.919v-5.187c0-1.428.756-1.995 1.68-1.995z"
          fill="#FB6058" />
      </symbol>
      <symbol id="distance-6-to-15m" viewBox="0 0 188 188">
        <rect x="101.528" y="20.583" width="65.889" height="14.972" rx="1" stroke="#2D3D4D" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M42.376 127.346a1 1 0 001.414 0l6.364-6.364a1 1 0 10-1.414-1.414l-5.657 5.657-5.657-5.657a1.001 1.001 0 00-1.414 1.414l6.364 6.364zm94.109-77.136a1.001 1.001 0 00-1.415 0l-6.364 6.364a1.001 1.001 0 001.415 1.414l5.656-5.657 5.657 5.657a1.001 1.001 0 001.415-1.414l-6.364-6.364zm-92.402 76.429V89.055h-2v37.584h2zm1-38.584h88.694v-2H45.083v2zm91.694-3V50.917h-2v34.138h2zm-3 3a3 3 0 003-3h-2a1 1 0 01-1 1v2zm-89.694 1a1 1 0 011-1v-2a3 3 0 00-3 3h2z"
          fill="#FB6058" />
        <rect x="20.583" y="135.473" width="45" height="30.158" rx="1" stroke="#FB6058" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M46 150.793c0 5.576-4.521 10.097-10.098 10.097-5.576 0-10.097-4.521-10.097-10.097 0-5.577 4.52-10.098 10.097-10.098S46 145.216 46 150.793zm4.704-2.509h10.201m-10.201-6.269h10.201m-10.201 10.804h10.201m-10.383 7.627h10.201"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <rect x="26.87" y="165.916" width="4.372" height="2.501" rx="1.251" fill="#FB6058" />
        <rect x="54.924" y="165.916" width="4.372" height="2.501" rx="1.251" fill="#FB6058" />
        <path
          d="M52.459 69.082c-1.626 0-2.459 1.053-2.618 1.528-.06-1.817.476-3.902 2.38-3.902.595 0 1.15.29 1.507 1.073l2.597-1.156c-.952-2.064-2.459-2.56-4.005-2.621-4.878-.165-5.572 5.12-5.255 9.29.238 2.869 2.201 4.706 4.66 4.706 2.835 0 4.72-1.755 4.72-4.356 0-3.117-2.142-4.562-3.986-4.562zm1.11 4.397c0 .97-.773 1.775-1.705 1.775s-1.705-.805-1.705-1.775c0-.97.773-1.775 1.705-1.775s1.705.805 1.705 1.775zm13.353-5.326v-3.695h-2.736v3.695H62.5v2.27h1.686v7.37h2.736v-7.37h1.943v-2.27h-1.943zm7.741 6.998c-1.13 0-2.082-.867-2.082-2.209 0-1.3.951-2.188 2.082-2.188 1.15 0 2.101.888 2.101 2.188 0 1.342-.951 2.21-2.101 2.21zm0 2.808c2.617 0 4.818-1.982 4.818-5.017 0-3.034-2.201-4.995-4.818-4.995-2.618 0-4.799 1.96-4.799 4.995s2.181 5.017 4.799 5.017zm16.135-13.873l-4.996 2.478v2.539l2.22-.991v9.682h2.776V64.086zm9.735 8.939c0 1.094-1.07 2.085-2.578 2.085-.813 0-1.745-.578-2.26-1.404l-2.34 1.57c.992 1.61 2.796 2.724 4.6 2.724 3.272 0 5.354-2.106 5.354-4.975 0-3.014-1.904-4.624-4.243-4.624-.853 0-1.864.227-2.38.702l.337-2.188h5.413v-2.643h-7.614l-1.07 7.205 1.526.825c.734-1.197 2.082-1.486 2.856-1.486 1.348 0 2.399.702 2.399 2.209zm15.204-2.291c.813 0 1.527.66 1.527 1.92v5.14H120v-5.47c0-2.912-1.368-4.377-3.569-4.377-.654 0-1.884.206-2.776 1.713-.515-1.115-1.507-1.693-2.974-1.693-.992 0-1.983.578-2.419 1.59v-1.404h-2.756v9.64h2.756v-5.098c0-1.466.773-1.961 1.606-1.961.793 0 1.507.64 1.527 1.857v5.203h2.756v-5.1c0-1.403.714-1.96 1.586-1.96z"
          fill="#FB6058" />
      </symbol>
      <symbol id="fitted-back-to-back" viewBox="0 0 100 100">
        <g fill="none">
          <path d="M0 36.806h57.292M0-11.806l57.292 48.611m-5.208.348l-.001 55.208m-3.472-55.208v55.208M0 92.361h100"
            stroke="#2d3d4d" />
          <path stroke="currentColor" d="M43.056 42.361h5.556v5.556h-5.556z" />
          <path
            d="M41.32 44.097c-.464-.578-1.737-1.388-3.126 0-1.388 1.389-3.125.579-3.819 0-.579-.578-2.083-1.388-3.472 0"
            stroke="#2388ef" />
          <path
            d="M41.319 45.906c-.463-.579-1.736-1.389-3.125 0-1.389 1.388-3.125.578-3.819 0-.343-.343-1.011-.767-1.787-.767m8.731 2.575c-.463-.579-1.736-1.389-3.125 0-1.388 1.389-3.125.579-3.819 0"
            stroke="#2388ef" />
          <path
            d="M20.833 54.167h8.333v18.056h-8.333V54.167zm0 8.598h8.333M12.5 54.167h8.333v18.056H12.5V54.167zm0 8.598h8.333m10.417 9.457H10.417"
            stroke="#2d3d4d" />
          <path d="M52.084 42.361h9.028v13.542h-9.028V42.361zm2.256 11.285h4.514m-4.514-2.257h4.514"
            stroke="currentColor" />
        </g>
      </symbol>
      <symbol id="fitted-somewhere-else" viewBox="0 0 100 100">
        <path d="M0 36.806h57.292M0-11.806l57.292 48.611m-5.208.348l-.001 55.208m-3.472-55.208v55.208M0 92.361h100"
          fill="none" stroke="#2d3d4d" />
        <path fill="none" stroke="currentColor" d="M43.056 42.361h5.556v5.556h-5.556z" />
        <path d="M41.32 44.097c-.464-.578-1.737-1.388-3.126 0-1.388 1.389-3.125.579-3.819 0-.579-.578-2.083-1.388-3.472 0"
          fill="none" stroke="#2388ef" />
        <path
          d="M41.319 45.906c-.463-.579-1.736-1.389-3.125 0-1.389 1.388-3.125.578-3.819 0-.343-.343-1.011-.767-1.787-.767m8.731 2.575c-.463-.579-1.736-1.389-3.125 0-1.388 1.389-3.125.579-3.819 0"
          fill="none" stroke="#2388ef" />
        <path fill="none" stroke="#2d3d4d"
          d="M6.25 54.167h8.333v18.056H6.25zm0 8.598h8.334m-16.667-8.598H6.25v18.056h-8.333zm0 8.598H6.25m10.417 9.457H-4.166" />
        <path fill="none" stroke="currentColor" d="M16.667 80.556h17.361v11.879H16.667z" />
        <circle cx="22.695" cy="86.584" r="3.599" fill="none" stroke="currentColor" />
        <path d="M28.163 85.657h3.768m-3.768-2.316h3.768m-3.768 3.992l3.768-.001m-3.836 2.818h3.768" fill="none"
          stroke="currentColor" />
        <path
          d="M25.341 76.048a.5.5 0 00.707 0l3.182-3.182a.5.5 0 00-.707-.707l-2.829 2.828-2.828-2.828a.5.5 0 00-.707.707l3.182 3.182zm15.29-24.318a.5.5 0 00-.707 0l-3.182 3.182a.5.5 0 00.707.707l2.829-2.828 2.828 2.828a.5.5 0 00.707-.707l-3.182-3.182zM26.194 75.695v-5.618h-1v5.618h1zm4.5-10.118h4.584v-1h-4.584v1zm10.084-5.5v-7.994h-1v7.994h1zm-5.5 5.5a5.5 5.5 0 005.5-5.5h-1a4.5 4.5 0 01-4.5 4.5v1zm-9.084 4.5a4.5 4.5 0 014.5-4.5v-1a5.5 5.5 0 00-5.5 5.5h1z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="floor-1st-floor" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="1.5" d="M47.967 95.838h91.292v30.946H47.967z" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M64.988 101.253h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.737h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 120.595v-10.831 10.831zm4.643 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.641 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
        <g opacity=".3" fill="none" stroke="#2d3d4d" stroke-width="1.5">
          <path d="M47.967 96.225V65.278h91.292v30.947" />
          <path
            d="M64.988 70.693h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.738h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 90.035V79.204v10.831zm4.643 0V79.204v10.831zm4.642 0V79.204v10.831zm4.641 0V79.204v10.831zm4.642 0V79.204v10.831zm4.642 0V79.204v10.831zM47.967 64.891V33.944h91.292v30.947" />
          <path
            d="M64.988 39.359h9.284V58.7h-9.284zm-13.153 0h9.284V58.7h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.738h9.283V58.7h-9.283zm9.284 0h16.247V58.7h-16.247zm-44.099 0h30.173V58.7H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 58.701V47.87v10.831zm4.643 0V47.87v10.831zm4.642 0V47.87v10.831zm4.641 0V47.87v10.831zm4.642 0V47.87v10.831zm4.642 0V47.87v10.831zm-63.972 95.355h104.444H41.778zm97.481-27.272v27.078H47.967v-27.078" />
          <path
            d="M64.988 131.426h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm52.609-7.737h9.284v22.436h-9.284z" />
          <path
            d="M126.881 131.426h9.284v22.436h-9.284zm-47.967 0h30.172v19.341H78.914zm0 8.511h30.172-30.172zm3.626 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
        </g>
      </symbol>
      <symbol id="floor-2nd-3rd-floor" viewBox="0 0 188 188">
        <path clip-rule="evenodd" d="M47.966 96.224h91.292V65.277H47.966v30.947z" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd"
          d="M64.987 90.034h9.284V70.692h-9.284v19.342zm-13.152 0h9.284V70.692h-9.284v19.342zm0-11.604h8.914-8.914zm13.152 0h8.914-8.914zm48.741 11.604h9.284V70.692h-9.284v19.342zm9.284 0h16.247V70.692h-16.247v19.342zm-44.098 0h30.172V70.692H78.914v19.342zm0-10.831h30.172-30.172zm60.344 0h-16.247 16.247zM82.54 90.034v-10.83 10.83zm4.642 0v-10.83 10.83zm4.642 0v-10.83 10.83zm4.642 0v-10.83 10.83zm4.641 0v-10.83 10.83zm4.643 0v-10.83 10.83z"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M47.967 64.89V33.945h91.292v30.947" stroke="#FB6058" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path clip-rule="evenodd"
          d="M64.988 58.7h9.284V39.36h-9.284V58.7zm-13.153 0h9.284V39.36h-9.284V58.7zm0-11.603h8.914-8.914zm13.153 0h8.914-8.914zM113.729 58.7h9.283V39.36h-9.283V58.7zm9.284 0h16.247V39.36h-16.247V58.7zm-44.099 0h30.173V39.36H78.914V58.7zm0-10.83h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 58.701v-10.83V58.7zm4.643 0v-10.83V58.7zm4.642 0v-10.83V58.7zm4.641 0v-10.83V58.7zm4.642 0v-10.83V58.7zm4.642 0v-10.83V58.7z"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <g opacity=".3" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
          <path d="M139.259 95.838v30.946H47.967V95.838" />
          <path clip-rule="evenodd"
            d="M64.988 120.594h9.284v-19.341h-9.284v19.341zm-13.153 0h9.284v-19.341h-9.284v19.341zm0-11.604h8.914-8.914zm13.153 0h8.914-8.914zm48.741 11.604h9.283v-19.341h-9.283v19.341zm9.284 0h16.247v-19.341h-16.247v19.341zm-44.099 0h30.173v-19.341H78.914v19.341zm0-10.83h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 120.595v-10.831 10.831zm4.643 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.641 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm-63.972 33.461h104.444H41.778z" />
          <path d="M139.259 126.784v27.078H47.967v-27.078" />
          <path clip-rule="evenodd"
            d="M64.988 150.767h9.284v-19.341h-9.284v19.341zm-13.153 0h9.284v-19.341h-9.284v19.341zm0-11.604h8.914-8.914zm13.153 0h8.914-8.914zm52.609 14.699h9.284v-22.436h-9.284v22.436z" />
          <path clip-rule="evenodd"
            d="M126.881 153.862h9.284v-22.436h-9.284v22.436zm-47.967-3.095h30.172v-19.341H78.914v19.341zm0-10.83h30.172-30.172zm3.626 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
        </g>
      </symbol>
      <symbol id="floor-2nd-floor" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="1.5" d="M47.966 65.277h91.292v30.947H47.966z" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M64.987 70.692h9.284v19.341h-9.284zm-13.152 0h9.284v19.341h-9.284zm0 7.738h8.914-8.914zm13.152 0h8.914-8.914zm48.741-7.738h9.284v19.341h-9.284zm9.284 0h16.247v19.341h-16.247zm-44.098 0h30.172v19.341H78.914zm0 8.511h30.172-30.172zm60.344 0h-16.247 16.247zM82.54 90.034V79.203v10.831zm4.642 0V79.203v10.831zm4.642 0V79.203v10.831zm4.642 0V79.203v10.831zm4.641 0V79.203v10.831zm4.643 0V79.203v10.831z" />
        <g opacity=".3" fill="none" stroke="#2d3d4d" stroke-width="1.5">
          <path d="M139.259 95.838v30.946H47.967V95.838" />
          <path
            d="M64.988 101.253h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.737h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 120.595v-10.831 10.831zm4.643 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.641 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zM47.967 64.891V33.944h91.292v30.947" />
          <path
            d="M64.988 39.359h9.284V58.7h-9.284zm-13.153 0h9.284V58.7h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.738h9.283V58.7h-9.283zm9.284 0h16.247V58.7h-16.247zm-44.099 0h30.173V58.7H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 58.701V47.87v10.831zm4.643 0V47.87v10.831zm4.642 0V47.87v10.831zm4.641 0V47.87v10.831zm4.642 0V47.87v10.831zm4.642 0V47.87v10.831zm-63.972 95.355h104.444H41.778zm97.481-27.272v27.078H47.967v-27.078" />
          <path
            d="M64.988 131.426h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm52.609-7.737h9.284v22.436h-9.284z" />
          <path
            d="M126.881 131.426h9.284v22.436h-9.284zm-47.967 0h30.172v19.341H78.914zm0 8.511h30.172-30.172zm3.626 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
        </g>
      </symbol>
      <symbol id="floor-3rd-floor-plus" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="1.5" d="M47.966 33.944h91.292v30.947H47.966z" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M64.987 39.359h9.284V58.7h-9.284zm-13.152 0h9.284V58.7h-9.284zm0 7.738h8.914-8.914zm13.152 0h8.914-8.914zm48.741-7.738h9.284V58.7h-9.284zm9.284 0h16.247V58.7h-16.247zm-44.098 0h30.172V58.7H78.914zm0 8.511h30.172-30.172zm60.344 0h-16.247 16.247zM82.54 58.701V47.87v10.831zm4.642 0V47.87v10.831zm4.642 0V47.87v10.831zm4.642 0V47.87v10.831zm4.641 0V47.87v10.831zm4.643 0V47.87v10.831z" />
        <g opacity=".3" fill="none" stroke="#2d3d4d" stroke-width="1.5">
          <path d="M47.967 126.784V95.838h91.292v30.946" />
          <path
            d="M64.988 101.253h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.737h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 120.595v-10.831 10.831zm4.643 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.641 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm-57.783-24.37V65.278m91.292 30.947V65.278m-74.271 5.415h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.738h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 90.035V79.204v10.831zm4.643 0V79.204v10.831zm4.642 0V79.204v10.831zm4.641 0V79.204v10.831zm4.642 0V79.204v10.831zm4.642 0V79.204v10.831zm-63.972 64.021h104.444H41.778zm6.189-27.272h91.292v27.078H47.967z" />
          <path
            d="M64.988 131.426h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm52.609-7.737h9.284v22.436h-9.284z" />
          <path
            d="M126.881 131.426h9.284v22.436h-9.284zm-47.967 0h30.172v19.341H78.914zm0 8.511h30.172-30.172zm3.626 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
        </g>
      </symbol>
      <symbol id="floor-4rd-floor-plus" viewBox="0 0 188 188">
        <g opacity=".3" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
          <path clip-rule="evenodd" d="M48 93.89h91.292V62.945H48v30.947z" />
          <path clip-rule="evenodd"
            d="M65.021 87.7h9.284V68.36H65.02V87.7zm-13.152 0h9.284V68.36h-9.284V87.7zm0-11.603h8.914-8.914zm13.152 0h8.914-8.914zM113.762 87.7h9.284V68.36h-9.284V87.7zm9.284 0h16.247V68.36h-16.247V87.7zm-44.099 0h30.173V68.36H78.947V87.7zm0-10.83h30.173-30.173zm60.345 0h-16.247 16.247zM82.574 87.701v-10.83V87.7zm4.642 0v-10.83V87.7zm4.642 0v-10.83V87.7zm4.642 0v-10.83V87.7zm4.642 0v-10.83V87.7zm4.642 0v-10.83V87.7z" />
          <path d="M48 155.784v-30.946h91.293v30.946" />
          <path clip-rule="evenodd"
            d="M65.022 149.594h9.283v-19.341h-9.284v19.341zm-13.152 0h9.283v-19.341H51.87v19.341zm0-11.604h8.913-8.914zm13.152 0h8.913-8.913zm48.74 11.604h9.284v-19.341h-9.284v19.341zm9.284 0h16.247v-19.341h-16.247v19.341zm-44.098 0h30.173v-19.341H78.948v19.341zm0-10.83h30.173-30.173zm60.344 0h-16.246 16.246zm-56.718 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
          <path d="M48 125.225V94.278m91.293 30.947V94.278" />
          <path clip-rule="evenodd"
            d="M65.022 119.035h9.283V99.693h-9.284v19.342zm-13.152 0h9.283V99.693H51.87v19.342zm0-11.604h8.913-8.914zm13.152 0h8.913-8.913zm48.74 11.604h9.284V99.693h-9.284v19.342zm9.284 0h16.247V99.693h-16.247v19.342zm-44.098 0h30.173V99.693H78.948v19.342zm0-10.831h30.173-30.173zm60.344 0h-16.246 16.246zm-56.718 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
        </g>
        <path clip-rule="evenodd" d="M48 62.947h91.292V32H48v30.947z" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" />
        <path clip-rule="evenodd"
          d="M65.021 56.757h9.284V37.415H65.02v19.342zm-13.152 0h9.284V37.415h-9.284v19.342zm0-11.605h8.914-8.914zm13.152 0h8.914-8.914zm48.741 11.605h9.284V37.415h-9.284v19.342zm9.284 0h16.247V37.415h-16.247v19.342zm-44.099 0h30.173V37.415H78.947v19.342zm0-10.831h30.173-30.173zm60.345 0h-16.247 16.247zM82.574 56.757V45.926v10.831zm4.642 0V45.926v10.831zm4.642 0V45.926v10.831zm4.642 0V45.926v10.831zm4.642 0V45.926v10.831zm4.642 0V45.926v10.831z"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="floor-basement" viewBox="0 0 188 188">
        <g opacity=".3" fill="none" stroke="#2d3d4d" stroke-width="1.5">
          <path d="M146.222 124.028H41.778h104.444zm-97.563-31.17h91.292v30.947H48.659z" />
          <path
            d="M65.68 98.273h9.284v19.342H65.68zm-13.153 0h9.284v19.342h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.74-7.738h9.284v19.342h-9.284zm9.285 0h16.247v19.342h-16.247zm-44.099 0h30.173v19.342H79.606zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zm-56.719 10.831v-10.831 10.831zm4.643 0l-.001-10.831.001 10.831zm4.642 0v-10.831 10.831zm4.641 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm-57.783-24.37V62.299h91.292v30.946" />
          <path
            d="M65.68 67.714h9.284v19.342H65.68zm-13.153 0h9.284v19.342h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm48.74-7.737h9.284v19.342h-9.284zm9.285 0h16.246v19.342h-16.246zm-44.099 0h30.173v19.342H79.606zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM83.232 87.056V76.225v10.831zm4.643 0l-.001-10.831.001 10.831zm4.642 0V76.225v10.831zm4.641 0V76.225v10.831zm4.642 0V76.225v10.831zm4.642 0V76.225v10.831zM48.659 61.911V30.965h91.292v30.946" />
          <path
            d="M65.68 36.38h9.284v19.342H65.68zm-13.153 0h9.284v19.342h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm48.74-7.737h9.284v19.342h-9.284zm9.285 0h16.246v19.342h-16.246zm-44.099 0h30.173v19.342H79.606zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM83.232 55.722V44.891v10.831z" />
          <path
            d="M87.875 55.722l-.001-10.831.001 10.831zm4.642 0V44.891v10.831zm4.641 0V44.891v10.831zm4.642 0V44.891v10.831zm4.642 0V44.891v10.831z" />
        </g>
        <path fill="none" stroke="currentColor" stroke-width="1.5" d="M47.967 124.028h91.292v28.819H47.967z" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M99.029 130.411h9.284v13.926h-9.284zm.774 7.736h8.51-8.51zm-9.72 6.19v-13.926H77.028m6.528 7.736h6.431m27.61-7.736h9.284v22.436h-9.284z" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M126.881 130.411h8.51v22.436h-8.51zm-78.914-6.383H70.5l22.195 23.791H108.7v5.028" />
      </symbol>
      <symbol id="floor-ground" viewBox="0 0 188 188">
        <g opacity=".3" fill="none" stroke="#2d3d4d" stroke-width="1.5">
          <path d="M47.967 95.838h91.292v30.946H47.967z" />
          <path
            d="M64.988 101.253h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.737h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 120.595v-10.831 10.831zm4.643 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.641 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm-57.783-24.37V65.278h91.292v30.947" />
          <path
            d="M64.988 70.693h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.738h9.283v19.341h-9.283zm9.284 0h16.247v19.341h-16.247zm-44.099 0h30.173v19.341H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 90.035V79.204v10.831zm4.643 0V79.204v10.831zm4.642 0V79.204v10.831zm4.641 0V79.204v10.831zm4.642 0V79.204v10.831zm4.642 0V79.204v10.831zM47.967 64.891V33.944h91.292v30.947" />
          <path
            d="M64.988 39.359h9.284V58.7h-9.284zm-13.153 0h9.284V58.7h-9.284zm0 7.738h8.914-8.914zm13.153 0h8.914-8.914zm48.741-7.738h9.283V58.7h-9.283zm9.284 0h16.247V58.7h-16.247zm-44.099 0h30.173V58.7H78.914zm0 8.511h30.173-30.173zm60.345 0h-16.247 16.247zM82.54 58.701V47.87v10.831zm4.643 0V47.87v10.831zm4.642 0V47.87v10.831zm4.641 0V47.87v10.831zm4.642 0V47.87v10.831zm4.642 0V47.87v10.831z" />
        </g>
        <path d="M41.778 154.056h104.444H41.778zm6.189-27.272h91.292v27.078H47.967z" fill="none" stroke="currentColor"
          stroke-width="1.5" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M64.988 131.426h9.284v19.341h-9.284zm-13.153 0h9.284v19.341h-9.284zm0 7.737h8.914-8.914zm13.153 0h8.914-8.914zm52.609-7.737h9.284v22.436h-9.284z" />
        <path fill="none" stroke="currentColor" stroke-width="1.5"
          d="M126.881 131.426h9.284v22.436h-9.284zm-47.967 0h30.172v19.341H78.914zm0 8.511h30.172-30.172zm3.626 10.831v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831zm4.642 0v-10.831 10.831z" />
      </symbol>
      <symbol id="floor-mounted" viewBox="0 0 188 188">
        <g clip-path="url(#floor-mountedclip0)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="#FB6058" d="M85.416 74h9.098v9.098h-9.098z" />
          <path
            d="M82.573 76.844c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0-.948-.948-3.412-2.275-5.686 0"
            stroke="#2388EF" />
          <path
            d="M82.573 79.805c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0-.561-.562-1.655-1.256-2.927-1.256m14.299 4.217c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0"
            stroke="#2388EF" />
          <path stroke="#FB6058" d="M101 152h9.953v14.929H101zm2.488 12.441h4.977m-4.977-2.488h4.977" />
          <path
            d="M-2.875 62.025h106.887m0 .001L67.372 19.81H-3.437M98.525 62.37v106.924M94.868 62.37v106.924m-97.18-.001H149"
            stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M47.25 109H36.33V85.342h10.92V109z" stroke="#2D3D4D" />
          <path d="M36.33 96.607h10.92" stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M36.33 109H25.41V85.342h10.92V109z" stroke="#2D3D4D" />
          <path d="M25.41 96.607h10.92M49.98 109H22.682" stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M47.25 151.658H36.33V128h10.92v23.658z" stroke="#2D3D4D" />
          <path d="M36.33 139.266h10.92" stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M36.33 151.658H25.41V128h10.92v23.658z" stroke="#2D3D4D" />
          <path d="M25.41 139.266h10.92m13.65 12.392H22.682" stroke="#2D3D4D" />
        </g>
      </symbol>
      <symbol id="fuel-electricity" viewBox="0 0 188 188">
        <path d="M137.24 41.36H95.494L58.28 94h32.992l-36.848 52.64 81.402-65.77h-34.815l36.229-39.51z" fill="none"
          stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="fuel-gas" viewBox="0 0 188 188">
        <path
          d="M113.416 87.893C100.676 61.453 95.56 43.348 94.595 37.6c0 10.059-11.897 36.297-19.546 50.293-23.165 42.39 17.374 61.07 19.546 62.507 22.116-10.884 34.748-28.739 18.821-62.507z"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M106.444 123.464c1.104 8.735-6.931 21.502-11.472 26.937-3.824-4.726-11.329-16.73-10.755-26.937.573-10.208 8.365-17.959 12.189-20.558 2.151 3.308 8.604 9.216 10.038 20.558z"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="fuel-lpg" viewBox="0 0 188 188">
        <path
          d="M113.173 79.51C100.432 57.478 95.317 42.39 94.351 37.6c.001 8.382-11.897 30.247-19.545 41.91-23.165 35.325 17.374 50.892 19.546 52.089 22.116-9.07 34.747-23.949 18.821-52.088z"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M106.045 107.459c1.1 7.829-6.903 19.271-11.425 24.141-3.809-4.235-11.283-14.993-10.712-24.141.571-9.149 8.331-16.095 12.14-18.424 2.142 2.964 8.569 8.259 9.997 18.424zM63.92 136.3l6.214 4.849a1 1 0 001.161.05l6.936-4.51a1 1 0 011.16.05l5.652 4.41a1 1 0 001.161.05l6.936-4.51a1 1 0 011.16.05l5.628 4.391a.998.998 0 001.193.027l6.241-4.426a.998.998 0 011.193.027l5.604 4.372a1 1 0 001.194.027l6.847-4.857m-58.28 8.812l6.214 4.849c.337.262.803.282 1.161.05l6.936-4.51a.998.998 0 011.16.05l5.652 4.41c.337.262.803.282 1.161.05l6.936-4.51a.998.998 0 011.16.05l5.628 4.391a1 1 0 001.193.027l6.241-4.427a1 1 0 011.193.028l5.604 4.372c.348.271.833.282 1.194.027l6.847-4.857"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="fuel-oil" viewBox="0 0 188 188">
        <path
          d="M93.06 43.24c5.138 22.059 17.593 40.664 24.308 51.45 3.452 5.205 6.706 12.549 6.712 19.892 0 16.677-13.898 30.178-31.02 30.178-17.128 0-31.02-13.495-31.02-30.178 0-7.343 3.26-14.693 6.712-19.892h-.004c6.598-10.709 19.054-28.851 24.312-51.45z"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="fusebox-0-5-ev" viewBox="0 0 170 170">
        <rect x="54.351" y="37.17" width="59.389" height="13.347" rx="1" stroke="#2D3D4D" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M83.389 114.643a1 1 0 001.414 0l6.364-6.364a1 1 0 10-1.414-1.414l-5.657 5.657-5.657-5.657a1 1 0 00-1.414 1.414l6.364 6.364zm1.414-30.35a1 1 0 00-1.414 0l-6.364 6.364a1 1 0 101.414 1.414l5.657-5.657 5.657 5.657a1 1 0 001.414-1.414l-6.364-6.364zm.293 29.643V85h-2v28.936h2z"
          fill="#FB6058" />
        <path d="M99.5 141h-6l-6 8H92l-5 7.5 12.5-10h-5l5-5.5z" stroke="#FB6058" />
        <rect x="80" y="136" width="26" height="26" rx="3" stroke="#FB6058" stroke-width="2" />
        <path d="M107 144c2.835 0 3.15 1.268 2.953 1.902v7.135c-.197.792-1.063 2.282-2.953 1.902" stroke="#FB6058"
          stroke-width="2" />
        <path
          d="M50.124 72.112c0 3.927 1.134 7.098 5.145 7.098 4.011 0 5.103-3.171 5.103-7.098 0-3.969-1.092-7.14-5.103-7.14-4.01 0-5.145 3.171-5.145 7.14zm2.94 0c0-2.331.525-4.284 2.205-4.284 1.66 0 2.163 1.953 2.163 4.284 0 2.31-.504 4.242-2.163 4.242-1.68 0-2.205-1.932-2.205-4.242zm18.501-2.919v-3.759h-2.898v3.759h-1.785v2.31h1.785V79h2.898v-7.497h2.058v-2.31h-2.058zm8.199 7.119c-1.197 0-2.205-.882-2.205-2.247 0-1.323 1.008-2.226 2.205-2.226 1.218 0 2.226.903 2.226 2.226 0 1.365-1.008 2.247-2.226 2.247zm0 2.856c2.772 0 5.103-2.016 5.103-5.103 0-3.087-2.331-5.082-5.103-5.082s-5.082 1.995-5.082 5.082c0 3.087 2.31 5.103 5.082 5.103zm19.483-5.019c0 1.113-1.134 2.121-2.73 2.121-.86 0-1.848-.588-2.394-1.428l-2.478 1.596c1.05 1.638 2.961 2.772 4.872 2.772 3.465 0 5.67-2.142 5.67-5.061 0-3.066-2.016-4.704-4.494-4.704-.903 0-1.974.231-2.52.714l.357-2.226h5.733v-2.688h-8.064l-1.134 7.329 1.617.84c.777-1.218 2.205-1.512 3.024-1.512 1.428 0 2.541.714 2.541 2.247zm16.103-2.331c.861 0 1.617.672 1.617 1.953V79h2.898v-5.565c0-2.961-1.449-4.452-3.78-4.452-.693 0-1.995.21-2.94 1.743-.546-1.134-1.596-1.722-3.15-1.722-1.05 0-2.1.588-2.562 1.617v-1.428h-2.919V79h2.919v-5.187c0-1.491.819-1.995 1.701-1.995.84 0 1.596.651 1.617 1.89V79h2.919v-5.187c0-1.428.756-1.995 1.68-1.995z"
          fill="#FB6058" />
      </symbol>
      <symbol id="fusebox-16-25-ev" viewBox="0 0 188 188">
        <rect x="58" y="23" width="65.889" height="14.972" rx="1" stroke="#2D3D4D" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M35.376 132.346a1 1 0 001.414 0l6.364-6.364a1 1 0 10-1.414-1.414l-5.657 5.657-5.657-5.657a1.001 1.001 0 00-1.414 1.414l6.364 6.364zm122.401-72.429h1a1 1 0 00-1-1v1zm-66.777 0h-1a1 1 0 001 1v-1zm.707-15.124a1 1 0 00-1.414 0l-6.364 6.364a1 1 0 001.414 1.414L91 46.914l5.657 5.657a1 1 0 001.414-1.414l-6.364-6.364zm-54.624 86.846V98.055h-2v33.584h2zm1-34.584h117.694v-2H38.083v2zm120.694-3V59.917h-2v34.138h2zm-1-35.138H91v2h66.777v-2zm-65.777 1V45.5h-2v14.417h2zm63.777 37.138a3 3 0 003-3h-2a1 1 0 01-1 1v2zm-118.694 1a1 1 0 011-1v-2a3 3 0 00-3 3h2zm27.119-25.969L59 74.564v2.539l2.312-.991v9.682h2.89V72.086zm8.628 4.996c-1.693 0-2.56 1.053-2.725 1.528-.062-1.817.496-3.902 2.477-3.902.62 0 1.198.29 1.57 1.073l2.704-1.156c-.991-2.064-2.56-2.56-4.17-2.621-5.079-.165-5.801 5.12-5.47 9.29.247 2.869 2.29 4.706 4.85 4.706 2.952 0 4.913-1.755 4.913-4.356 0-3.117-2.23-4.562-4.149-4.562zm1.156 4.397c0 .97-.805 1.775-1.775 1.775-.97 0-1.775-.805-1.775-1.775 0-.97.805-1.775 1.775-1.775.97 0 1.775.805 1.775 1.775zm13.902-5.326v-3.695h-2.849v3.695h-1.755v2.27h1.755v7.37h2.849v-7.37h2.023v-2.27h-2.023zm8.059 6.998c-1.177 0-2.167-.867-2.167-2.209 0-1.3.99-2.188 2.167-2.188 1.197 0 2.188.888 2.188 2.188 0 1.342-.99 2.21-2.188 2.21zm0 2.808c2.725 0 5.017-1.982 5.017-5.017 0-3.034-2.292-4.995-5.017-4.995-2.725 0-4.996 1.96-4.996 4.995s2.271 5.017 4.996 5.017zm20.639-.165v-2.725h-4.17l3.117-3.53c.826-.93 1.425-2.127 1.425-3.427 0-2.436-2.023-4.108-4.418-4.108-2.56 0-4.315 1.61-4.727 4.19l2.642.454c.165-1.073.888-1.878 1.92-1.878 1.073 0 1.693.62 1.693 1.342 0 .516-.31 1.053-.723 1.527l-5.553 6.586v1.569h8.794zm9.823-4.769c0 1.094-1.115 2.085-2.683 2.085-.847 0-1.817-.578-2.354-1.404l-2.436 1.57c1.032 1.61 2.911 2.724 4.79 2.724 3.406 0 5.573-2.106 5.573-4.975 0-3.014-1.981-4.624-4.417-4.624-.888 0-1.941.227-2.478.702l.351-2.188h5.636v-2.643h-7.927l-1.115 7.205 1.59.825c.764-1.197 2.167-1.486 2.972-1.486 1.404 0 2.498.702 2.498 2.209zm15.83-2.291c.846 0 1.589.66 1.589 1.92v5.14h2.849v-5.47c0-2.912-1.425-4.377-3.716-4.377-.681 0-1.961.206-2.89 1.713-.537-1.115-1.569-1.693-3.097-1.693-1.032 0-2.064.578-2.518 1.59v-1.404h-2.869v9.64h2.869v-5.098c0-1.466.805-1.961 1.672-1.961.826 0 1.569.64 1.59 1.857v5.203h2.869v-5.1c0-1.403.743-1.96 1.652-1.96z"
          fill="#FB6058" />
        <path d="M42.5 146h-6l-6 8H35l-5 7.5 12.5-10h-5l5-5.5z" stroke="#FB6058" />
        <rect x="23" y="141" width="26" height="26" rx="3" stroke="#FB6058" stroke-width="2" />
        <path d="M50 149c2.835 0 3.15 1.268 2.953 1.902v7.135c-.197.792-1.063 2.282-2.953 1.902" stroke="#FB6058"
          stroke-width="2" />
      </symbol>
      <symbol id="fusebox-25-plus-ev" viewBox="0 0 170 170">
        <g clip-path="url(#fusebox-25-plus-evclip0_8747_6730)">
          <rect x="52.542" y=".894" width="59.389" height="13.347" rx="1" stroke="#2D3D4D" stroke-width="2"
            stroke-linecap="round" stroke-linejoin="round" />
          <path
            d="M31.921 119.743a1 1 0 001.414 0l6.364-6.364a1.001 1.001 0 00-1.414-1.415l-5.657 5.657-5.657-5.657a1 1 0 00-1.414 1.415l6.364 6.364zm110.75-65.563h1a1 1 0 00-1-1v1zm-60.384 0h-1a1 1 0 001 1v-1zm.707-33.744a1 1 0 00-1.414 0L75.216 26.8a1 1 0 001.414 1.415l5.657-5.657 5.657 5.657a1 1 0 001.414-1.415l-6.364-6.364zm-49.366 98.599V88.859h-2v30.176h2zm1-31.176h106.043v-2H34.628v2zm109.043-3V54.18h-2v30.68h2zm-1-31.679H82.287v2h60.384v-2zm-59.384 1V21.144h-2V54.18h2zm57.384 33.679a3 3 0 003-3h-2a1 1 0 01-1 1v2zm-107.043 1a1 1 0 011-1v-2a3 3 0 00-3 3h2zM127.49 70.68h-3.888v2.088h3.888v3.912h2.088v-3.912h3.888V70.68h-3.888v-3.888h-2.088v3.888zm-34.538 5.9v-2.465h-3.77L92 70.923c.747-.84 1.288-1.922 1.288-3.099 0-2.202-1.83-3.714-3.995-3.714-2.314 0-3.901 1.456-4.274 3.79l2.389.41c.15-.97.803-1.699 1.736-1.699.97 0 1.53.56 1.53 1.213 0 .467-.28.953-.653 1.382L85 75.16v1.418h7.952zm8.883-4.313c0 .99-1.008 1.886-2.427 1.886-.765 0-1.643-.523-2.128-1.27l-2.203 1.419c.934 1.456 2.632 2.464 4.331 2.464 3.08 0 5.04-1.904 5.04-4.499 0-2.725-1.792-4.181-3.995-4.181-.802 0-1.754.205-2.24.635l.318-1.98h5.096v-2.388h-7.168l-1.008 6.514 1.437.747c.69-1.083 1.96-1.344 2.688-1.344 1.269 0 2.259.635 2.259 1.997zm14.314-2.072c.765 0 1.437.598 1.437 1.736v4.648h2.576v-4.946c0-2.632-1.288-3.958-3.36-3.958-.616 0-1.774.187-2.614 1.55-.485-1.008-1.418-1.531-2.8-1.531-.933 0-1.866.522-2.277 1.437v-1.27h-2.595v8.718h2.595v-4.61c0-1.326.728-1.774 1.512-1.774.747 0 1.419.579 1.437 1.68v4.704h2.595v-4.61c0-1.27.672-1.774 1.494-1.774z"
            fill="#FB6058" />
          <path d="M42.5 146h-6l-6 8H35l-5 7.5 12.5-10h-5l5-5.5z" stroke="#FB6058" />
          <rect x="23" y="141" width="26" height="26" rx="3" stroke="#FB6058" stroke-width="2" />
          <path d="M50 149c2.835 0 3.15 1.268 2.953 1.902v7.135c-.197.792-1.063 2.282-2.953 1.902" stroke="#FB6058"
            stroke-width="2" />
        </g>
      </symbol>
      <symbol id="fusebox-6-15-ev" viewBox="0 0 170 170">
        <rect x="91.902" y="37.17" width="59.389" height="13.347" rx="1" stroke="#2D3D4D" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M34.634 119.743a1 1 0 001.414 0l6.364-6.364a1.001 1.001 0 00-1.414-1.415l-5.657 5.657-5.657-5.657a1 1 0 00-1.414 1.415l6.364 6.364zm88.851-60.844a1.001 1.001 0 00-1.415 0l-6.364 6.364a1.001 1.001 0 001.415 1.414l5.656-5.657 5.657 5.657a1.001 1.001 0 001.415-1.414l-6.364-6.364zm-87.144 60.136V88.859h-2v30.176h2zm1-31.176h83.436v-2H37.341v2zm86.436-3V59.606h-2v25.253h2zm-3 3a3 3 0 003-3h-2a1 1 0 01-1 1v2zm-84.436 1a1 1 0 011-1v-2a3 3 0 00-3 3h2z"
          fill="#FB6058" />
        <path d="M45.5 146h-6l-6 8H38l-5 7.5 12.5-10h-5l5-5.5z" stroke="#FB6058" />
        <rect x="26" y="141" width="26" height="26" rx="3" stroke="#FB6058" stroke-width="2" />
        <path d="M53 149c2.835 0 3.15 1.268 2.953 1.902v7.135c-.197.792-1.063 2.282-2.953 1.902" stroke="#FB6058"
          stroke-width="2" />
        <path
          d="M39.972 70.138c-1.722 0-2.604 1.071-2.772 1.554-.063-1.848.504-3.969 2.52-3.969.63 0 1.218.294 1.596 1.092l2.751-1.176c-1.008-2.1-2.604-2.604-4.242-2.667-5.166-.168-5.901 5.208-5.565 9.45.252 2.919 2.331 4.788 4.935 4.788 3.003 0 4.998-1.785 4.998-4.431 0-3.171-2.268-4.641-4.221-4.641zm1.176 4.473a1.82 1.82 0 01-1.806 1.806 1.82 1.82 0 01-1.806-1.806 1.82 1.82 0 011.806-1.806 1.82 1.82 0 011.806 1.806zm14.142-5.418v-3.759h-2.898v3.759h-1.785v2.31h1.785V79h2.898v-7.497h2.058v-2.31H55.29zm8.198 7.119c-1.197 0-2.205-.882-2.205-2.247 0-1.323 1.008-2.226 2.205-2.226 1.218 0 2.226.903 2.226 2.226 0 1.365-1.008 2.247-2.226 2.247zm0 2.856c2.772 0 5.103-2.016 5.103-5.103 0-3.087-2.33-5.082-5.103-5.082-2.772 0-5.082 1.995-5.082 5.082 0 3.087 2.31 5.103 5.082 5.103zm17.09-14.112l-5.292 2.52v2.583l2.352-1.008V79h2.94V65.056zm10.31 9.093c0 1.113-1.134 2.121-2.73 2.121-.861 0-1.848-.588-2.394-1.428l-2.478 1.596c1.05 1.638 2.96 2.772 4.872 2.772 3.465 0 5.67-2.142 5.67-5.061 0-3.066-2.016-4.704-4.494-4.704-.903 0-1.974.231-2.52.714l.357-2.226h5.733v-2.688H84.84l-1.134 7.329 1.617.84c.777-1.218 2.205-1.512 3.024-1.512 1.428 0 2.54.714 2.54 2.247zm16.103-2.331c.861 0 1.617.672 1.617 1.953V79h2.898v-5.565c0-2.961-1.449-4.452-3.78-4.452-.693 0-1.995.21-2.94 1.743-.546-1.134-1.596-1.722-3.15-1.722-1.05 0-2.1.588-2.563 1.617v-1.428h-2.918V79h2.918v-5.187c0-1.491.82-1.995 1.702-1.995.84 0 1.596.651 1.617 1.89V79h2.919v-5.187c0-1.428.756-1.995 1.68-1.995z"
          fill="#FB6058" />
      </symbol>
      <symbol id="home-bungalow-2" viewBox="0 0 100 100">
        <path
          d="M17.601 55.302h-1.032l5.937-23.492h1.291m-6.196 23.492v18.33m0-18.33h64.798m0 0h1.032L78.01 31.81H53.485m28.914 23.492v18.33m2.323 0H15.278M28.96 31.81h17.813M22.248 70.275h1.807m17.039 0h-1.549m0 0V58.916h-15.49v11.359m15.49 0h-15.49M31.8 58.917v10.922m-7.745-5.243h15.231m19.104 5.679h1.807m17.039 0h-1.549m0 0V58.916h-15.49v11.359m15.49 0h-15.49m7.745-11.358v10.922m-7.745-5.243h15.232M23.797 26.389h5.163v9.81h-5.163zm22.718 32.528h7.228v14.457h-7.228z"
          fill="none" stroke="currentColor" />
        <path fill="none" stroke="currentColor"
          d="M48.58 60.465h3.098v4.131H48.58zm0 5.68h3.098v5.163H48.58zm-17.296-23.75l-1.033-2.066 20.136-10.068 20.137 10.068-1.033 2.066-19.104-9.81-19.103 9.81z" />
        <path
          d="M32.058 42.136v8.519h36.658v-8.519m-33.302-1.807h4.647m25.299 0h-4.905m-20.394-2.324v2.324m0 0h3.872m0-4.389v4.389m0 0h4.389m0-6.454v6.454m0 0h4.131m0-6.454v6.454m0 0h4.13m0-4.389v4.389m0 0h3.872m0-2.324v2.324"
          fill="none" stroke="currentColor" />
        <path d="M43.933 42.911v4.905m6.454-4.905v4.905m6.454-4.905v4.905m-19.62-4.905v4.905h26.59v-4.905h-26.59z"
          fill="none" stroke="currentColor" />
      </symbol>
      <symbol id="home-bungalow" viewBox="0 0 100 100">
        <path
          d="M17.601 55.122h-1.032l5.937-23.492h1.291m-6.196 23.492v18.329m0-18.329h64.798m0 0h1.032L78.01 31.63H28.96m53.439 23.492v18.329m2.323 0H15.278m6.813-3.209h1.807m17.039 0h-1.549m0 0V58.883h-15.49v11.359m15.49 0h-15.49m8.034-11.359V70.5m-7.745-5.937h15.232m18.836 5.679h1.807m17.038 0h-1.549m.001 0l-.001-11.359H60.062v11.359m15.49 0h-15.49m8.034-11.359V70.5m-7.745-5.937h15.231"
          fill="none" stroke="currentColor" />
        <path d="M23.664 26.389h5.163v9.81h-5.163v-9.81zm23.06 32.494h7.228V73.34h-7.228V58.883z" fill="none"
          stroke="currentColor" />
        <path d="M48.821 60.456h3.098v4.131h-3.098v-4.131zm0 5.765h3.098v5.163h-3.098v-5.163z" fill="none"
          stroke="currentColor" />
      </symbol>
      <symbol id="home-commercial" viewBox="0 0 180 180">
        <path
          d="M57.441 138.824v-48.63m74.103 48.63v-48.63M60.75 58.27V50h67.65v8.27m-67.652 0H128.4m-67.652 0L49.005 71.668m79.396-13.398l11.082 13.398m-90.478 0h90.478m-90.478 0v10.09c-.11 2.867 1.423 8.601 8.436 8.601s9.318-5.734 9.594-8.6m72.448-10.09v10.75c0 2.813-1.985 7.94-7.939 7.94-6.881 0-9.263-5.734-9.594-8.6m-54.915 0v-1.82m0 1.82c.33 2.866 2.68 8.6 9.428 8.6 6.749 0 8.767-5.734 8.932-8.6m0 0v-1.82m0 1.82c.22 2.866 2.382 8.6 9.263 8.6s8.822-5.734 8.932-8.6m0 0v-1.82m0 1.82c.055 2.866 1.952 8.6 9.097 8.6 7.146 0 9.153-5.734 9.263-8.6m0 0v-1.82m-68.479 58.885h81.711m-71.786-20.345v-17.368h15.383v17.368H63.396zm46.314 0v-17.368h15.383v17.368H109.71zm-24.811 20.345v-37.713h19.187v37.713H84.899zm0-20.345h4.466"
          stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="home-detached" viewBox="0 0 100 100">
        <path
          d="M27.562 26.447h51.275l3.531 13.771H51.789M22.84 26.447h-2.266l-3.531 13.771h5.797m29.12.118h29.231V76H18.809V57.474m4.108-17.138h-4.108v13.483m-3.531 22.24h69.444"
          fill="none" stroke="currentColor" />
        <path
          d="M54.178 43.75h5.179v6.944h-5.179V43.75zm5.061 3.472h-4.943m-1.412 3.59h7.768m4.149-7.062h5.179v6.944h-5.179V43.75zm5.061 3.472h-4.944m-1.412 3.59h7.769"
          fill="none" stroke="currentColor" />
        <path
          d="M69.975 43.75h5.179v6.944h-5.179V43.75zm5.061 3.472h-4.943m-1.413 3.59h7.768M26.389 40.336h21.875m-23.805 1.678V76h25.6V42.014m-25.6 13.741h25.285"
          fill="none" stroke="currentColor" />
        <path
          d="M29.99 43.397h6.238v9.063H29.99v-9.063zm6.121 4.59h-6.003m8.71-4.59h6.238v9.063h-6.238v-9.063zm6.121 4.59h-6.003M27.99 52.578h18.714M29.99 59.64h6.238v10.476H29.99V59.64zm6.121 4.943h-6.003m8.71-4.943h6.238v10.476h-6.238V59.64zm6.121 4.943h-6.003m-10.946 5.65h18.714M64.502 59.64h6.238v10.476h-6.238V59.64zm6.121 4.943h-6.002m6.12-4.943h6.238v10.476h-6.238V59.64zm6.121 4.943h-6.003m-7.651 5.65h15.095m-54.29-27.981l-1.395-1.595 14.729-12.875L51.899 40.48l-1.393 1.596-13.158-11.481-13.335 11.657zm6.096-1.916v-3.068m2.825 3.068v-5.54m2.824 5.54v-8.012m2.826 8.013l-.001-8.504m2.825 8.504v-6.032m2.825 6.031v-3.56M24.414 53.99h-5.605l-.706 3.531h6.311V53.99zm29.411 5.65h5.885v12.594h-5.885V59.64zm-2.353 14.477h10.593v1.766H51.472v-1.766z"
          fill="none" stroke="currentColor" />
        <path
          d="M55.532 65.951h2.472v4.855h-2.472v-4.855zm0-4.59h2.472v3.09h-2.472v-3.09zm-3.001 10.991h8.474v1.765h-8.474v-1.765zM23.308 22.917h4.237v6.356h-4.237v-6.356z"
          fill="none" stroke="currentColor" />
      </symbol>
      <symbol id="home-flat" viewBox="0 0 100 100">
        <path fill="none" stroke="currentColor" d="M25.514 50.978h48.56v16.461h-48.56z" />
        <path
          d="M34.568 53.858h4.938v10.288h-4.938V53.858zm-6.996 0h4.938v10.288h-4.938V53.858zm0 4.115h4.742-4.742zm6.996 0h4.742-4.742zm25.926-4.115h4.938v10.288h-4.938V53.858zm4.938 0h8.642v10.288h-8.642V53.858zm-23.456 0h16.049v10.288H41.976V53.858zm0 4.527h16.049-16.049zm32.098 0h-8.642 8.642zm-30.17 5.761v-5.761 5.761z"
          fill="none" stroke="currentColor" />
        <path
          d="M46.374 64.146l-.001-5.761v5.761h.001zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761zM25.514 34.722h48.56v16.461h-48.56V34.722z"
          fill="none" stroke="currentColor" />
        <path
          d="M34.568 37.603h4.938v10.288h-4.938V37.603zm-6.996 0h4.938v10.288h-4.938V37.603zm0 4.115h4.742-4.742zm6.996 0h4.742-4.742zm25.926-4.115h4.938v10.288h-4.938V37.603zm4.938 0h8.642v10.288h-8.642V37.603zm-23.456 0h16.049v10.288H41.976V37.603zm0 4.526h16.049-16.049zm32.098 0h-8.642 8.642zm-30.17 5.762v-5.762 5.762z"
          fill="none" stroke="currentColor" />
        <path
          d="M46.374 47.891l-.001-5.762v5.762h.001zm2.469 0v-5.762 5.762zm2.469 0v-5.762 5.762zm2.469 0v-5.762 5.762zm2.469 0v-5.762 5.762zM25.514 18.056h48.56v16.461h-48.56V18.056z"
          fill="none" stroke="currentColor" />
        <path
          d="M34.568 20.936h4.938v10.288h-4.938V20.936zm-6.996 0h4.938v10.288h-4.938V20.936zm0 4.115h4.742-4.742zm6.996 0h4.742-4.742zm25.926-4.115h4.938v10.288h-4.938V20.936zm4.938 0h8.642v10.288h-8.642V20.936zm-23.456 0h16.049v10.288H41.976V20.936zm0 4.527h16.049-16.049zm32.098 0h-8.642 8.642zm-30.17 5.761v-5.761 5.761z"
          fill="none" stroke="currentColor" />
        <path
          d="M46.374 31.224l-.001-5.761v5.761h.001zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761zM22.222 81.945h55.556-55.556zm3.292-14.506h48.56v14.403h-48.56V67.439z"
          fill="none" stroke="currentColor" />
        <path
          d="M34.568 69.908h4.938v10.288h-4.938V69.908zm-6.996 0h4.938v10.288h-4.938V69.908zm0 4.115h4.741-4.741zm6.996 0h4.741-4.741zm27.983-4.115h4.938v11.934h-4.938V69.908zm4.939 0h4.938v11.934H67.49V69.908zm-25.515 0h16.049v10.288H41.975V69.908zm0 4.526h16.05-16.05zm1.929 5.761v-5.761 5.761z"
          fill="none" stroke="currentColor" />
        <path
          d="M46.374 80.195l-.001-5.761v5.761h.001zm2.468 0v-5.761 5.761zm2.47 0v-5.761 5.761zm2.469 0v-5.761 5.761zm2.469 0v-5.761 5.761z"
          fill="none" stroke="currentColor" />
      </symbol>
      <symbol id="home-semi" viewBox="0 0 100 100">
        <path d="M10.621 41.699l2.76-10.986h32.854m3.46 42.567l-.001-30.636m0 0H34.376m15.318 0V27.083" fill="none"
          stroke="currentColor" />
        <path d="M7.639 73.33h84.708" fill="none" stroke="#2d3d4d" />
        <path
          d="M7.639 73.33H50M39.265 45.576h4.449v5.965h-4.449v-5.965zm4.348 2.983h-4.247m-1.213 3.084h6.673m-32.415-8.999h18.791m-20.449 1.441V73.28h21.991V44.085M10.753 55.889h21.721"
          fill="none" stroke="currentColor" />
        <path
          d="M15.505 45.273h5.359v7.785h-5.359v-7.785zm5.257 3.943h-5.157m7.483-3.943h5.359v7.785h-5.359v-7.785zm5.258 3.943h-5.157m-9.403 3.943h16.076m-14.357 6.067h5.359v8.999h-5.359v-8.999zm5.257 4.246h-5.157m7.483-4.246h5.359v8.999h-5.359v-8.999zm5.258 4.246h-5.157m-9.403 4.853h16.076M10.37 44.29l-1.198-1.37 12.652-11.061 12.5 10.908-1.196 1.371-11.303-9.862L10.37 44.29zm5.236-1.646v-2.636m2.427 2.636v-4.759m2.426 4.759v-6.883m2.427 6.883v-7.305m2.427 7.305v-5.182m2.426 5.182v-3.058m11.222 19.64h5.056v10.819h-5.056V59.226zM36.94 71.662h9.1v1.517h-9.1v-1.517z"
          fill="none" stroke="currentColor" />
        <path
          d="M40.428 64.647h2.123v4.171h-2.123v-4.171zm0-3.942h2.123v2.654h-2.123v-2.654zm-2.578 9.44h7.28v1.517h-7.28v-1.517z"
          fill="none" stroke="currentColor" />
        <path
          d="M89.186 41.699l-2.761-10.986h-33.45m-2.863 11.931H65.43m-9.337 2.932h4.449v5.965h-4.449v-5.965zm.101 2.983h4.246m1.213 3.084H54.98m32.416-8.999H68.605m20.449 1.441V73.28H67.063V44.085m21.991 11.804h-21.72"
          fill="none" stroke="#2d3d4d" />
        <path
          d="M78.943 45.273h5.359v7.785h-5.359v-7.785zm.102 3.943h5.156M71.36 45.273h5.359v7.785H71.36v-7.785zm.102 3.943h5.156m9.403 3.943H69.944m8.999 6.067h5.359v8.999h-5.359v-8.999zm.102 4.246h5.156M71.36 59.226h5.359v8.999H71.36v-8.999zm.102 4.246h5.156m9.403 4.853H69.944M89.437 44.29l1.198-1.37-12.652-11.061-12.5 10.908 1.196 1.371 11.303-9.862L89.437 44.29zM84.2 42.644v-2.636m-2.427 2.636v-4.759m-2.425 4.759v-6.883m-2.427 6.883v-7.305m-2.427 7.305v-5.182m-2.427 5.182v-3.058m-16.278 19.64h5.056v10.819h-5.056V59.226zm-2.021 12.436h9.1v1.517h-9.1v-1.517z"
          fill="none" stroke="#2d3d4d" />
        <path
          d="M57.256 64.647h2.123v4.171h-2.123v-4.171zm0-3.942h2.123v2.654h-2.123v-2.654zm-2.579 9.44h7.28v1.517h-7.28v-1.517z"
          fill="none" stroke="#2d3d4d" />
        <path fill="none" stroke="currentColor" d="M46.413 27.084h6.562v8.352h-6.562z" />
      </symbol>
      <symbol id="home-terrace" viewBox="0 0 100 100">
        <g fill="none">
          <path
            d="M79.724 43.339h5.545v7.79h-5.545v-7.79zm5.419 3.397H79.85m-1.512 4.394h8.318m11.297-7.791h5.545v7.79h-5.545v-7.79zm5.419 3.397h-5.293m8.875 21.706H94.835m1.732-17.312h8.318m-11.78 22.505h15.58M79.254 61.517h6.925v13.85h-6.925v-13.85zm-2.596 15.581h12.119v1.731H76.658v-1.731z"
            stroke="#2d3d4d" />
          <path
            d="M80.986 68.442h3.463v5.194h-3.463v-5.194zm0-5.193h3.463v3.462h-3.463v-3.462zm-2.597 12.118h8.656v1.731h-8.656v-1.731zm16.446-12.118h12.118v10.387H94.835V63.249z"
            stroke="#2d3d4d" />
          <path
            d="M74.494 24.296h34.192m-38.087 4.328v50.205h41.549V28.624m-41.549 6.492h41.549M-3.375 43.339H2.17v7.79h-5.545v-7.79zm5.42 3.397h-5.294M-4.76 51.13h8.318m11.297-7.791H20.4v7.79h-5.545v-7.79zm5.419 3.397H14.98m8.875 21.706H11.737m6.06 5.194V63.249M13.469 51.13h8.318M10.006 73.635h15.581M-3.844 61.517h6.925v13.85h-6.925v-13.85zM-6.44 77.098H5.679v1.731H-6.44v-1.731z"
            stroke="#2d3d4d" />
          <path
            d="M-2.112 68.442H1.35v5.194h-3.462v-5.194zm0-5.193H1.35v3.462h-3.462v-3.462zm-2.597 12.118h8.656v1.731h-8.656v-1.731zm16.446-12.118h12.118v10.387H11.737V63.249z"
            stroke="#2d3d4d" />
          <path d="M25.154 24.296H-12.5v54.533h41.549V28.624M-12.5 35.116h41.549" stroke="#2d3d4d" />
          <path stroke="#2d3d4d" d="M25.587 20.834h6.925v7.79h-6.925z" />
          <path
            d="M38.174 43.339h5.545v7.79h-5.545v-7.79zm5.42 3.397H38.3m-1.511 4.394h8.317m11.297-7.791h5.545v7.79h-5.545v-7.79zm5.419 3.397h-5.293m8.875 21.706H53.286m6.059 5.194V63.5m-4.327-12.37h8.318M51.555 73.635h15.581M37.705 61.517h6.925v13.85h-6.925v-13.85zm-2.597 15.581h12.118v1.731H35.108v-1.731z"
            stroke="currentColor" />
          <path
            d="M39.437 68.442h3.462v5.194h-3.462v-5.194zm0-5.193h3.462v3.462h-3.462v-3.462zm-2.598 12.118h8.656v1.731h-8.656v-1.731zm16.447-12.118h12.118v10.387H53.286V63.249z"
            stroke="currentColor" />
          <path
            d="M32.944 24.296h33.759m-37.654 4.328v50.205h41.549V28.624m-41.549 6.492h41.549M29.049 20.834h3.463v7.79h-3.463"
            stroke="currentColor" />
          <path stroke="#2d3d4d" d="M67.136 20.834h6.925v7.79h-6.925z" />
        </g>
      </symbol>
      <symbol id="kitchen" viewBox="0 0 100 100">
        <path fill="none" stroke="currentColor" d="M18.75 18.75h62.5v62.5h-62.5z" />
        <path fill="none" stroke="currentColor"
          d="M22.917 38.889h54.167v37.5H22.917zm18.055-15.972h18.056v2.778H40.972zM22.917 34.028h54.166M18.75 29.861h62.5" />
        <circle cx="67.361" cy="24.305" r="1.389" fill="none" stroke="currentColor" />
        <circle cx="75" cy="24.305" r="1.389" fill="none" stroke="currentColor" />
        <circle cx="25" cy="24.305" r="1.389" fill="none" stroke="currentColor" />
        <circle cx="32.639" cy="24.305" r="1.389" fill="none" stroke="currentColor" />
      </symbol>
      <symbol id="living-room" viewBox="0 0 100 100">
        <path
          d="M23.883 60.963V47.916l-6.522.001v13.046m6.522 0h23.339v17.509H17.361V60.963m6.522 0h-6.522m6.945-10.268c2.43-.232 7.291 1.458 7.291 10.069M19.444 78.472H25v2.778h-5.556zm19.445 0h5.556v2.778h-5.556zm31.25-2.083H81.25v4.861H70.139zm5.555 0V34.5c0-6.367-1.098-15.75-9.075-15.75H55.751c-2.611.335-7.834 3.284-7.834 12.399"
          fill="none" stroke="currentColor" />
        <path d="M55.556 40.972H40.278c0-5.555 2.244-8.333 3.366-9.028h9.063c1.295.695 2.849 3.82 2.849 9.028z"
          fill="none" stroke="currentColor" />
      </symbol>
      <symbol id="more-than-one-room" viewBox="0 0 100 100">
        <path
          d="M27.19 35.46v6.6l6.6-2.64V69h7.38V29.28l-13.98 6.18zm37.638 20.76h9.42V50.4h-9.42v-9.36h-5.82v9.36h-9.42v5.82h9.42v9.48h5.82v-9.48z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-0" viewBox="0 0 188 188">
        <path
          d="M65.048 89.76c0 22.32 6.36 40.44 29.16 40.44s28.8-18.12 28.8-40.44c0-22.56-6-40.68-28.8-40.68s-29.16 18.12-29.16 40.68zm14.76 0c0-14.4 3.12-26.28 14.4-26.28 11.04 0 14.16 11.88 14.16 26.28 0 14.28-3.12 26.04-14.16 26.04-11.28 0-14.4-11.76-14.4-26.04z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-1-5" viewBox="0 0 169 170">
        <g clip-path="url(#number-1-5clip0_1116_90)">
          <path
            d="M51.268 58.536l-19.152 9.12v9.348l8.512-3.648V109h10.64V58.536zm11.169 35.948h26.828v-8.36H62.437v8.36zm62.214-3.04c0 4.028-4.104 7.676-9.88 7.676-3.116 0-6.688-2.128-8.664-5.168l-8.968 5.776c3.8 5.928 10.716 10.032 17.632 10.032 12.54 0 20.52-7.752 20.52-18.316 0-11.096-7.296-17.024-16.264-17.024-3.268 0-7.144.836-9.12 2.584l1.292-8.056h20.748V59.22h-29.184l-4.104 26.524 5.852 3.04c2.812-4.408 7.98-5.472 10.944-5.472 5.168 0 9.196 2.584 9.196 8.132z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-1" viewBox="0 0 188 188">
        <path d="M77.442 61.92v13.2l13.2-5.28V129h14.76V49.56l-27.96 12.36z" fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-10-20" viewBox="0 0 170 170">
        <g clip-path="url(#number-10-20clip0_2062_26985)">
          <path
            d="M10.084 71.46v6.6l6.6-2.64V105h7.38V65.28l-13.98 6.18zm22.998 13.92c0 11.16 3.18 20.22 14.58 20.22s14.4-9.06 14.4-20.22c0-11.28-3-20.34-14.4-20.34s-14.58 9.06-14.58 20.34zm7.38 0c0-7.2 1.56-13.14 7.2-13.14 5.52 0 7.08 5.94 7.08 13.14 0 7.14-1.56 13.02-7.08 13.02-5.64 0-7.2-5.88-7.2-13.02zm28.953 8.16h21.06v-6.12h-21.06v6.12zM122.023 105v-7.02h-13.26l9.72-11.1c2.46-2.76 4.2-6.24 4.2-10.02 0-7.02-5.76-11.82-12.66-11.82-6.96 0-12.12 4.44-13.26 11.58l6.84 1.26c.48-3.36 2.94-5.7 6.12-5.7 3.36 0 5.64 2.04 5.64 4.68 0 1.8-1.02 3.54-2.4 5.16l-16.02 19.2V105h25.08zm7.681-19.62c0 11.16 3.18 20.22 14.58 20.22s14.4-9.06 14.4-20.22c0-11.28-3-20.34-14.4-20.34s-14.58 9.06-14.58 20.34zm7.38 0c0-7.2 1.56-13.14 7.2-13.14 5.52 0 7.08 5.94 7.08 13.14 0 7.14-1.56 13.02-7.08 13.02-5.64 0-7.2-5.88-7.2-13.02z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-10plus" viewBox="0 0 169 170">
        <g clip-path="url(#number-10plusclip0_1021_117)">
          <path
            d="M21.109 62.69v9.9l9.9-3.96V113h11.07V53.42l-20.97 9.27zm34.497 20.88c0 16.74 4.77 30.33 21.87 30.33s21.6-13.59 21.6-30.33c0-16.92-4.5-30.51-21.6-30.51s-21.87 13.59-21.87 30.51zm11.07 0c0-10.8 2.34-19.71 10.8-19.71 8.28 0 10.62 8.91 10.62 19.71 0 10.71-2.34 19.53-10.62 19.53-8.46 0-10.8-8.82-10.8-19.53zm65.119 10.26h14.13V85.1h-14.13V71.06h-8.73V85.1h-14.13v8.73h14.13v14.22h8.73V93.83z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-11-15" viewBox="0 0 169 170">
        <g clip-path="url(#number-11-15clip0_1116_92)">
          <path
            d="M22.62 58.536l-19.152 9.12v9.348l8.512-3.648V109h10.64V58.536zm28.648 0l-19.152 9.12v9.348l8.512-3.648V109h10.64V58.536zm11.169 35.948h26.828v-8.36H62.437v8.36zm53.55-35.948l-19.152 9.12v9.348l8.512-3.648V109h10.64V58.536zM153.3 91.444c0 4.028-4.104 7.676-9.88 7.676-3.116 0-6.688-2.128-8.664-5.168l-8.968 5.776c3.8 5.928 10.716 10.032 17.632 10.032 12.54 0 20.52-7.752 20.52-18.316 0-11.096-7.296-17.024-16.264-17.024-3.268 0-7.144.836-9.12 2.584l1.292-8.056h20.748V59.22h-29.184l-4.104 26.524 5.852 3.04c2.812-4.408 7.98-5.472 10.944-5.472 5.168 0 9.196 2.584 9.196 8.132z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-15plus" viewBox="0 0 169 170">
        <g clip-path="url(#number-15plusclip0_1116_94)">
          <path
            d="M49.97 58.536l-19.152 9.12v9.348l8.512-3.648V109h10.64V58.536zm37.312 32.908c0 4.028-4.104 7.676-9.88 7.676-3.116 0-6.688-2.128-8.664-5.168l-8.968 5.776c3.8 5.928 10.716 10.032 17.632 10.032 12.54 0 20.52-7.752 20.52-18.316 0-11.096-7.296-17.024-16.264-17.024-3.268 0-7.144.836-9.12 2.584l1.292-8.056h20.748V59.22H65.394L61.29 85.744l5.852 3.04c2.812-4.408 7.98-5.472 10.944-5.472 5.168 0 9.196 2.584 9.196 8.132zm37.377 1.596h11.78v-7.752h-11.78V73.584h-7.752v11.704h-11.704v7.752h11.704v11.78h7.752V93.04z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-1plus" viewBox="0 0 188 188">
        <path
          d="M46.797 61.92v13.2l13.2-5.28V129h14.76V49.56l-27.96 12.36zm75.277 41.52h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-2-5" viewBox="0 0 169 170">
        <g clip-path="url(#number-2-5clip0_1021_8)">
          <path
            d="M54.588 113v-10.53h-19.89l14.58-16.65c3.69-4.14 6.3-9.36 6.3-15.03 0-10.53-8.64-17.73-18.99-17.73-10.44 0-18.18 6.66-19.89 17.37l10.26 1.89c.72-5.04 4.41-8.55 9.18-8.55 5.04 0 8.46 3.06 8.46 7.02 0 2.7-1.53 5.31-3.6 7.74l-24.03 28.8V113h37.62zm11.791-17.19h31.59v-9.18h-31.59v9.18zm74.211-2.97c0 5.4-4.68 10.17-12.51 10.17-4.41 0-8.82-2.97-11.16-7.29-.81.45-8.46 5.31-9.27 5.76 4.14 7.47 12.24 12.42 20.43 12.42 14.22 0 23.49-8.64 23.49-21.06 0-12.78-8.82-19.62-19.53-19.62-3.78 0-8.46 1.08-10.89 3.15l2.07-12.06h24.66V54.14H114.4l-4.95 30.69 6.3 3.78c4.32-4.68 9.45-5.94 13.5-5.94 6.3 0 11.34 3.33 11.34 10.17z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-2" viewBox="0 0 188 188">
        <path
          d="M118.299 129v-14.04h-26.52l19.44-22.2c4.92-5.52 8.4-12.48 8.4-20.04 0-14.04-11.52-23.64-25.32-23.64-13.92 0-24.24 8.88-26.52 23.16l13.68 2.52c.96-6.72 5.88-11.4 12.24-11.4 6.72 0 11.28 4.08 11.28 9.36 0 3.6-2.04 7.08-4.8 10.32l-32.04 38.4V129h50.16z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-20-25" viewBox="0 0 170 170">
        <g clip-path="url(#number-20-25clip0_2062_26988)">
          <path
            d="M31.378 105v-7.02h-13.26l9.72-11.1c2.46-2.76 4.2-6.24 4.2-10.02 0-7.02-5.76-11.82-12.66-11.82-6.96 0-12.12 4.44-13.26 11.58l6.84 1.26c.48-3.36 2.94-5.7 6.12-5.7 3.36 0 5.64 2.04 5.64 4.68 0 1.8-1.02 3.54-2.4 5.16l-16.02 19.2V105h25.08zm7.681-19.62c0 11.16 3.18 20.22 14.58 20.22s14.4-9.06 14.4-20.22c0-11.28-3-20.34-14.4-20.34s-14.58 9.06-14.58 20.34zm7.38 0c0-7.2 1.56-13.14 7.2-13.14 5.52 0 7.08 5.94 7.08 13.14 0 7.14-1.56 13.02-7.08 13.02-5.64 0-7.2-5.88-7.2-13.02zm28.952 8.16h21.06v-6.12h-21.06v6.12zM128 105v-7.02h-13.26l9.72-11.1c2.46-2.76 4.2-6.24 4.2-10.02 0-7.02-5.76-11.82-12.66-11.82-6.96 0-12.12 4.44-13.26 11.58l6.84 1.26c.48-3.36 2.94-5.7 6.12-5.7 3.36 0 5.64 2.04 5.64 4.68 0 1.8-1.02 3.54-2.4 5.16l-16.02 19.2V105H128zm28.741-13.44c0 3.6-3.12 6.78-8.34 6.78-2.94 0-5.88-1.98-7.44-4.86-.54.3-5.64 3.54-6.18 3.84 2.76 4.98 8.16 8.28 13.62 8.28 9.48 0 15.66-5.76 15.66-14.04 0-8.52-5.88-13.08-13.02-13.08-2.52 0-5.64.72-7.26 2.1l1.38-8.04h16.44v-6.78h-22.32l-3.3 20.46 4.2 2.52c2.88-3.12 6.3-3.96 9-3.96 4.2 0 7.56 2.22 7.56 6.78z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-25plus" viewBox="0 0 170 170">
        <g clip-path="url(#number-25plusclip0_2062_26991)">
          <path
            d="M64.366 105v-7.02h-13.26l9.72-11.1c2.46-2.76 4.2-6.24 4.2-10.02 0-7.02-5.76-11.82-12.66-11.82-6.96 0-12.12 4.44-13.26 11.58l6.84 1.26c.48-3.36 2.94-5.7 6.12-5.7 3.36 0 5.64 2.04 5.64 4.68 0 1.8-1.02 3.54-2.4 5.16l-16.02 19.2V105h25.08zm28.741-13.44c0 3.6-3.12 6.78-8.34 6.78-2.94 0-5.88-1.98-7.44-4.86-.54.3-5.64 3.54-6.18 3.84 2.76 4.98 8.16 8.28 13.62 8.28 9.48 0 15.66-5.76 15.66-14.04 0-8.52-5.88-13.08-13.02-13.08-2.52 0-5.64.72-7.26 2.1l1.38-8.04h16.44v-6.78h-22.32l-3.3 20.46 4.2 2.52c2.88-3.12 6.3-3.96 9-3.96 4.2 0 7.56 2.22 7.56 6.78zm28.502.66h9.42V86.4h-9.42v-9.36h-5.82v9.36h-9.42v5.82h9.42v9.48h5.82v-9.48z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-2plus" viewBox="0 0 188 188">
        <path
          d="M88.155 129l-.001-14.04H61.635l19.44-22.2c4.92-5.52 8.4-12.48 8.4-20.04 0-14.04-11.52-23.64-25.32-23.64-13.921 0-24.241 8.88-26.52 23.16l13.679 2.52c.96-6.72 5.88-11.4 12.24-11.4 6.72 0 11.28 4.08 11.28 9.36.001 3.6-2.04 7.08-4.8 10.32l-32.04 38.4V129h50.16zm44.642-25.56h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-3" viewBox="0 0 188 188">
        <path
          d="M92.206 94.08c10.68-.36 12.24 8.04 12.24 10.08 0 6.84-5.76 11.64-13.2 11.64-5.4 0-10.44-4.08-12.72-8.76-1.08.6-11.76 5.88-12.84 6.48 4.2 9.12 13.56 15.84 24.36 16.56 15.24 1.08 28.8-8.04 30.24-22.32.84-8.4-4.8-18.24-13.08-21.36 5.64-3.24 9.72-8.88 9.72-15.36 0-11.64-9.24-21.24-22.68-21.96-10.2-.6-19.32 4.2-23.76 11.64l11.52 7.56c3.12-3.96 7.56-5.4 12.12-4.8 5.64 1.08 6.96 6 6.96 8.88 0 2.04-1.44 8.76-8.88 8.64h-8.4v13.08h8.4z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-3plus" viewBox="0 0 188 188">
        <path
          d="M62.062 94.08c10.68-.36 12.24 8.04 12.24 10.08 0 6.84-5.76 11.64-13.2 11.64-5.4 0-10.44-4.08-12.72-8.76-1.08.6-11.76 5.88-12.84 6.48 4.2 9.12 13.56 15.84 24.36 16.56 15.24 1.08 28.8-8.04 30.24-22.32.84-8.4-4.8-18.24-13.08-21.36 5.64-3.24 9.72-8.88 9.72-15.36 0-11.64-9.24-21.24-22.68-21.96-10.2-.6-19.32 4.2-23.76 11.64l11.52 7.56c3.12-3.96 7.56-5.4 12.12-4.8 5.64 1.08 6.96 6 6.96 8.88 0 2.04-1.44 8.76-8.88 8.64h-8.4v13.08h8.4zm71.027 9.36h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-4" viewBox="0 0 188 188">
        <path
          d="M82.082 99.12l17.4-27.36v27.36h-17.4zm42.48 12.84V99.12h-10.44v-48.6h-14.28l-39.12 61.44h38.76V129h14.64v-17.04h10.44z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-4plus" viewBox="0 0 188 188">
        <path
          d="M51.438 99.12l17.4-27.36-.001 27.36h-17.4zm42.481 12.84l-.001-12.84H83.479l-.001-48.6H69.199l-39.121 61.44h38.761L68.838 129h14.641l-.001-17.04h10.441zm41.835-8.52h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-5-10" viewBox="0 0 169 170">
        <g clip-path="url(#number-5-10clip0_1021_90)">
          <path
            d="M30.507 92.84c0 5.4-4.68 10.17-12.51 10.17-4.41 0-8.82-2.97-11.16-7.29-.81.45-8.46 5.31-9.27 5.76 4.14 7.47 12.24 12.42 20.43 12.42 14.22 0 23.49-8.64 23.49-21.06 0-12.78-8.82-19.62-19.53-19.62-3.78 0-8.46 1.08-10.89 3.15l2.07-12.06h24.66V54.14H4.317l-4.95 30.69 6.3 3.78c4.32-4.68 9.45-5.94 13.5-5.94 6.3 0 11.34 3.33 11.34 10.17zm21.062 2.97h31.59v-9.18H51.57v9.18zm39.588-33.12v9.9l9.9-3.96V113h11.07V53.42l-20.97 9.27zm34.498 20.88c0 16.74 4.77 30.33 21.87 30.33s21.6-13.59 21.6-30.33c0-16.92-4.5-30.51-21.6-30.51s-21.87 13.59-21.87 30.51zm11.07 0c0-10.8 2.34-19.71 10.8-19.71 8.28 0 10.62 8.91 10.62 19.71 0 10.71-2.34 19.53-10.62 19.53-8.46 0-10.8-8.82-10.8-19.53z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-5" viewBox="0 0 188 188">
        <path
          d="M108.398 102.12c0 7.2-6.24 13.56-16.68 13.56-5.88 0-11.76-3.96-14.88-9.72-1.08.6-11.28 7.08-12.36 7.68 5.52 9.96 16.32 16.56 27.24 16.56 18.96 0 31.32-11.52 31.32-28.08 0-17.04-11.76-26.16-26.04-26.16-5.04 0-11.28 1.44-14.52 4.2l2.76-16.08h32.88V50.52h-44.64l-6.6 40.92 8.4 5.04c5.76-6.24 12.6-7.92 18-7.92 8.4 0 15.12 4.44 15.12 13.56z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-5plus" viewBox="0 0 188 188">
        <path
          d="M77.754 102.12c0 7.2-6.24 13.56-16.68 13.56-5.88 0-11.76-3.96-14.88-9.72-1.08.6-11.28 7.08-12.36 7.68 5.52 9.96 16.32 16.56 27.24 16.56 18.96 0 31.32-11.52 31.32-28.08 0-17.04-11.76-26.16-26.04-26.16-5.04 0-11.28 1.44-14.52 4.2l2.76-16.08h32.88V50.52h-44.64l-6.6 40.92 8.4 5.04c5.76-6.24 12.6-7.92 18-7.92 8.4 0 15.12 4.44 15.12 13.56zm57.003 1.32h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-6-10" viewBox="0 0 169 170">
        <g clip-path="url(#number-6-10clip0_1116_5)">
          <path
            d="M31.673 76.928c-6.232 0-9.424 3.876-10.032 5.624-.228-6.688 1.824-14.364 9.12-14.364 2.28 0 4.408 1.064 5.776 3.952l9.956-4.256c-3.648-7.6-9.424-9.424-15.352-9.652-18.696-.608-21.356 18.848-20.14 34.2.912 10.564 8.436 17.328 17.86 17.328 10.868 0 18.088-6.46 18.088-16.036 0-11.476-8.208-16.796-15.276-16.796zm4.256 16.188c0 3.572-2.964 6.536-6.536 6.536-3.572 0-6.536-2.964-6.536-6.536 0-3.572 2.964-6.536 6.536-6.536 3.572 0 6.536 2.964 6.536 6.536zm19.68 1.368h26.828v-8.36H55.609v8.36zm53.55-35.948l-19.152 9.12v9.348l8.512-3.648V109h10.64V58.536zm10.94 25.536c0 14.212 4.104 25.688 18.62 25.688 14.516 0 18.468-11.476 18.468-25.688 0-14.364-3.952-25.84-18.468-25.84s-18.62 11.476-18.62 25.84zm10.64 0c0-8.436 1.9-15.504 7.98-15.504 6.004 0 7.828 7.068 7.828 15.504 0 8.36-1.824 15.352-7.828 15.352-6.08 0-7.98-6.992-7.98-15.352z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-6" viewBox="0 0 188 188">
        <path
          d="M106.765 104.16c-.12 6.72-5.4 11.88-11.999 11.76-6.481 0-11.761-5.4-11.761-12.12.12-6.6 5.64-11.88 12.24-11.76 6.6.12 11.64 5.52 11.52 12.12zm-9.12-25.8c-8.279 0-14.399 4.92-15.84 8.88-.719-12.48 3.961-24.36 15.241-24.36 4.079 0 8.039 2.04 10.559 7.2l13.56-5.76c-5.52-12-14.52-14.88-23.639-15.24-28.32-.84-32.4 29.52-30.48 53.88 1.32 16.56 12.6 27 27 27.24 16.199.12 27.599-10.44 27.839-25.2.12-16.68-11.88-26.64-24.239-26.64z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-6plus" viewBox="0 0 188 188">
        <path
          d="M76.121 104.16c-.12 6.72-5.4 11.88-12 11.76-6.48 0-11.76-5.4-11.76-12.12.12-6.6 5.64-11.88 12.24-11.76 6.6.12 11.64 5.52 11.52 12.12zm-9.12-25.8c-8.28 0-14.4 4.92-15.84 8.88-.72-12.48 3.96-24.36 15.24-24.36 4.08 0 8.04 2.04 10.56 7.2l13.56-5.76c-5.52-12-14.52-14.88-23.64-15.24-28.32-.84-32.4 29.52-30.48 53.88 1.32 16.56 12.6 27 27 27.24 16.2.12 27.6-10.44 27.84-25.2.12-16.68-11.88-26.64-24.24-26.64zm67.229 25.08h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-7" viewBox="0 0 188 188">
        <path d="M67.299 50.52v13.8h34.2L70.419 129h16.8l37.56-78.48h-57.48z" fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-7plus" viewBox="0 0 188 188">
        <path
          d="M38.764 50.52v13.8h34.2L41.884 129h16.8l37.56-78.48h-57.48zm91.423 52.92h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-8" viewBox="0 0 188 188">
        <path
          d="M85.121 70.92c0-4.2 3.48-8.16 8.88-8.16s8.76 3.96 8.76 8.16c0 4.44-3.12 8.52-8.64 8.52-5.64 0-9-3.96-9-8.52zm-5.4 32.16c0-6.6 5.64-12.96 12.96-12.96h2.16c7.32 0 13.32 6.24 13.32 12.96 0 7.2-6.48 13.2-14.16 13.2-7.8 0-14.28-6-14.28-13.2zm-8.88-32.64c0 5.4 2.28 10.08 5.76 13.44-6.96 4.08-11.52 11.04-11.52 19.68 0 14.76 12.84 26.64 28.92 26.64 15.96 0 28.8-11.88 28.8-26.64 0-8.52-4.44-15.6-11.52-19.68 3.6-3.36 5.88-7.92 5.88-13.44 0-11.76-10.32-21.36-23.16-21.36-12.96 0-23.16 9.6-23.16 21.36z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-8plus" viewBox="0 0 188 188">
        <path
          d="M54.476 70.92c0-4.2 3.48-8.16 8.88-8.16s8.76 3.96 8.76 8.16c0 4.44-3.12 8.52-8.64 8.52-5.64 0-9-3.96-9-8.52zm-5.4 32.16c0-6.6 5.64-12.96 12.96-12.96h2.16c7.32 0 13.32 6.24 13.32 12.96 0 7.2-6.48 13.2-14.16 13.2-7.8 0-14.28-6-14.28-13.2zm-8.88-32.64c0 5.4 2.28 10.08 5.76 13.44-6.96 4.08-11.52 11.04-11.52 19.68 0 14.76 12.84 26.64 28.92 26.64 15.96 0 28.8-11.88 28.8-26.64 0-8.52-4.44-15.6-11.52-19.68 3.6-3.36 5.88-7.92 5.88-13.44 0-11.76-10.32-21.36-23.16-21.36-12.96 0-23.16 9.6-23.16 21.36zm94.679 33h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-9" viewBox="0 0 188 188">
        <path
          d="M81.267 75.12c.12-6.6 5.4-11.88 12-11.76 6.48.12 11.76 5.4 11.76 12.24-.12 6.6-5.64 11.76-12.24 11.64-6.6-.12-11.76-5.52-11.52-12.12zm9 25.8c8.4 0 14.52-4.8 15.96-8.88.72 12.6-3.96 24.36-15.24 24.36-4.08 0-8.04-2.04-10.56-7.2l-13.56 5.76c5.52 12 14.52 14.88 23.64 15.24 28.32.96 32.4-29.4 30.48-53.88-1.44-16.56-12.6-26.88-27.12-27.12-16.08-.24-27.48 10.32-27.72 25.08-.12 16.68 11.88 26.64 24.12 26.64z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-9plus" viewBox="0 0 188 188">
        <path
          d="M50.622 75.12c.12-6.6 5.4-11.88 12-11.76 6.48.12 11.76 5.4 11.76 12.24-.12 6.6-5.64 11.76-12.24 11.64-6.6-.12-11.76-5.52-11.52-12.12zm9 25.8c8.4 0 14.52-4.8 15.96-8.88.72 12.6-3.96 24.36-15.24 24.36-4.08 0-8.04-2.04-10.56-7.2l-13.56 5.76c5.52 12 14.52 14.88 23.64 15.24 28.32.96 32.4-29.4 30.48-53.88-1.44-16.56-12.6-26.88-27.12-27.12-16.08-.24-27.48 10.32-27.72 25.08-.12 16.68 11.88 26.64 24.12 26.64zm74.667 2.52h18.84V91.8h-18.84V73.08h-11.64V91.8h-18.84v11.64h18.84v18.96h11.64v-18.96z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="number-up-to-10" viewBox="0 0 169 170">
        <g clip-path="url(#number-up-to-10clip0_2062_26983)">
          <path
            d="M26.663 85.38c0 11.16 3.18 20.22 14.58 20.22s14.4-9.06 14.4-20.22c0-11.28-3-20.34-14.4-20.34s-14.58 9.06-14.58 20.34zm7.38 0c0-7.2 1.56-13.14 7.2-13.14 5.52 0 7.08 5.94 7.08 13.14 0 7.14-1.56 13.02-7.08 13.02-5.64 0-7.2-5.88-7.2-13.02zm28.952 8.16h21.06v-6.12h-21.06v6.12zm26.392-22.08v6.6l6.6-2.64V105h7.38V65.28l-13.98 6.18zm22.998 13.92c0 11.16 3.18 20.22 14.58 20.22s14.4-9.06 14.4-20.22c0-11.28-3-20.34-14.4-20.34s-14.58 9.06-14.58 20.34zm7.38 0c0-7.2 1.56-13.14 7.2-13.14 5.52 0 7.08 5.94 7.08 13.14 0 7.14-1.56 13.02-7.08 13.02-5.64 0-7.2-5.88-7.2-13.02z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="number-up-to-2" viewBox="0 0 169 170">
        <g clip-path="url(#number-up-to-2clip0_1021_119)" fill="#FB6058">
          <path
            d="M127.224 114v-10.53h-19.89l14.58-16.65c3.69-4.14 6.3-9.36 6.3-15.03 0-10.53-8.64-17.73-18.99-17.73-10.44 0-18.18 6.66-19.89 17.37l10.26 1.89c.72-5.04 4.41-8.55 9.18-8.55 5.04 0 8.46 3.06 8.46 7.02 0 2.7-1.53 5.31-3.6 7.74l-24.03 28.8V114h37.62zM48.598 77.648c-2.108 0-4.046-1.598-4.046-4.012V58.574h-5.066v15.062c0 5.304 4.284 8.704 9.112 8.704 4.794 0 9.044-3.4 9.044-8.704V58.574h-4.998v15.062c0 2.414-1.972 4.012-4.046 4.012zM72.42 62.96c1.87 0 2.822 1.836 2.822 3.298 0 1.53-1.02 3.298-2.959 3.298h-4.386V62.96h4.522zM62.9 82h4.997v-8.092h4.624c5.338 0 7.719-3.774 7.719-7.65 0-3.842-2.38-7.684-7.718-7.684h-9.623V82zM37.142 97.062h6.323V116h5.032V97.062h6.325v-4.488h-17.68v4.488zm24.464 7.276c0-3.91 3.026-7.106 6.902-7.106 3.876 0 6.936 3.196 6.936 7.106 0 3.91-3.06 7.072-6.936 7.072s-6.902-3.162-6.902-7.072zm-4.964 0c0 6.596 5.236 12.002 11.866 12.002 6.664 0 11.9-5.406 11.9-12.002 0-6.664-5.236-12.036-11.9-12.036-6.63 0-11.866 5.372-11.866 12.036z" />
        </g>
      </symbol>
      <symbol id="pressure-average" viewBox="0 0 188 188">
        <path
          d="M119.278 110.321c-.599-1.78-3.474-7.35-5.211-10.321-1.263 2.502-3.979 8.069-4.737 10.321-.947 2.815 0 6.568 4.737 6.568 4.738 0 6.159-3.753 5.211-6.568zm0 21.111c-.599-1.78-3.474-7.349-5.211-10.321-1.263 2.502-3.979 8.07-4.737 10.321-.947 2.815 0 6.568 4.737 6.568 4.738 0 6.159-3.753 5.211-6.568z"
          fill="currentColor" fill-rule="nonzero" />
        <path
          d="M84.707 57.086h26.552c6.758 0 10.258 3.017 10.258 10.259v22.327c0 2.897-1.207 4.225-4.224 4.225h-10.259c-3.379 0-4.224-1.811-4.224-4.225v-8.448c0-4.345-1.81-4.224-4.224-4.224H67a1 1 0 01-1-1V58.086a1 1 0 01.999-1h17.708zm0 0V42m-15.086 0h28.965"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="pressure-fast" viewBox="0 0 188 188">
        <path
          d="M77.707 56.086h26.552c6.758 0 10.258 3.017 10.258 10.259v22.327c0 2.897-1.207 4.225-4.224 4.225h-10.259c-3.379 0-4.224-1.811-4.224-4.225v-8.448C95.81 75.879 94 76 91.586 76H60a1 1 0 01-1-1V57.086a1 1 0 01.999-1h17.708zm0 0V41m-15.086 0h28.965"
          fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="106.068" cy="107.379" r="3.621" fill="currentColor" />
        <circle cx="98.827" cy="119.449" r="3.621" fill="currentColor" />
        <circle cx="112.104" cy="119.448" r="3.621" fill="currentColor" />
        <circle cx="98.827" cy="119.449" r="3.621" fill="currentColor" />
        <circle cx="112.104" cy="119.448" r="3.621" fill="currentColor" />
        <circle cx="92.793" cy="131.517" r="3.621" fill="currentColor" />
        <circle cx="106.069" cy="131.517" r="3.621" fill="currentColor" />
        <circle cx="92.793" cy="131.517" r="3.621" fill="currentColor" />
        <circle cx="106.069" cy="131.517" r="3.621" fill="currentColor" />
        <circle cx="119.344" cy="131.517" r="3.621" fill="currentColor" />
        <circle cx="85.551" cy="143.587" r="3.621" fill="currentColor" />
        <circle cx="98.827" cy="143.586" r="3.621" fill="currentColor" />
        <circle cx="85.551" cy="143.587" r="3.621" fill="currentColor" />
        <circle cx="98.827" cy="143.586" r="3.621" fill="currentColor" />
        <circle cx="112.103" cy="143.586" r="3.621" fill="currentColor" />
        <circle cx="125.379" cy="143.586" r="3.621" fill="currentColor" />
      </symbol>
      <symbol id="pressure-slow" viewBox="0 0 188 188">
        <path
          d="M119.278 110.321c-.599-1.78-3.474-7.35-5.211-10.321-1.263 2.502-3.979 8.069-4.737 10.321-.947 2.815 0 6.568 4.737 6.568 4.738 0 6.159-3.753 5.211-6.568z"
          fill="currentColor" fill-rule="nonzero" />
        <path
          d="M84.707 57.086h26.552c6.758 0 10.258 3.017 10.258 10.259v22.327c0 2.897-1.207 4.225-4.224 4.225h-10.259c-3.379 0-4.224-1.811-4.224-4.225v-8.448c0-4.345-1.81-4.224-4.224-4.224H67a1 1 0 01-1-1V58.086a1 1 0 01.999-1h17.708zm0 0V42m-15.086 0h28.965"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="question-light" viewBox="0 0 188 188">
        <path
          d="M96.136 114.318c0-8.642 3.157-12.319 10.771-14.71 14.486-4.413 25.628-16.916 25.628-34.752C132.535 42.791 115.821 29 93.722 29c-22.1 0-38.257 14.894-38.257 38.43h22.471c0-9.745 6.871-15.997 15.786-15.997 8.914 0 16.156 5.516 16.156 13.239 0 4.045-1.3 9.561-7.056 11.952-19.872 9.561-29.529 13.79-29.529 37.694l.372 6.068h22.1l.371-6.068zm-25.814 30.524c0 8.274 6.685 14.158 14.671 14.158s14.672-5.884 14.672-14.158c0-7.907-6.686-13.975-14.672-13.975-7.986 0-14.671 6.068-14.671 13.975z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="question-thin" viewBox="0 0 188 188">
        <path
          d="M64 62.562C64.537 54.042 71.405 37 94.586 37c28.976 0 37.025 33.018 22.001 47.929-15.025 14.911-34.343 10.65-34.343 31.953V127"
          stroke="#FB6058" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="82" cy="149" r="3" fill="#FB6058" />
      </symbol>
      <symbol id="question" viewBox="0 0 188 188">
        <path
          d="M96.136 114.318c0-8.642 3.157-12.319 10.771-14.71 14.486-4.413 25.628-16.916 25.628-34.752C132.535 42.791 115.821 29 93.722 29c-22.1 0-38.257 14.894-38.257 38.43h22.471c0-9.745 6.871-15.997 15.786-15.997 8.914 0 16.156 5.516 16.156 13.239 0 4.045-1.3 9.561-7.056 11.952-19.872 9.561-29.529 13.79-29.529 37.694l.372 6.068h22.1l.371-6.068zm-25.814 30.524c0 8.274 6.685 14.158 14.671 14.158s14.672-5.884 14.672-14.158c0-7.907-6.686-13.975-14.672-13.975-7.986 0-14.671 6.068-14.671 13.975z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="room-airingcupboard" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="2"
          d="M41.778 35.25h104.444v117.5H41.778zm.653 22.847h102.486M107.708 35.25v42.431m0 75.069V97.917m37.209-20.236h-37.209m0 0v20.236m37.209 0h-37.209" />
        <path
          d="M52.222 92.25c0-5.523 4.477-10 10-10h27c5.523 0 10 4.477 10 10v60.5h-47v-60.5zm22.848-10v-6.528c0-.87 1.044-2.611 5.222-2.611"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="room-bathroom" viewBox="0 0 188 188">
        <path
          d="M154.055 104.444a5.225 5.225 0 00-5.222-5.222h-117.5a5.225 5.225 0 00-5.222 5.222 5.225 5.225 0 005.222 5.222h117.5a5.225 5.225 0 005.222-5.222zm-18.93-47h-20.889c0-3.264 1.305-11.097 11.097-11.097 7.833 0 9.792 7.398 9.792 11.097z"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M125.333 46.347c-.218-3.264 1.436-9.791 9.792-9.791 7.18 0 10.009 6.527 9.791 9.791-.217 17.843 0 52.223 0 52.223m-2.611 11.749l-18.278 30.028m0 0l6.528 11.097m-6.528-11.097H54.833m-17.625-30.028l17.625 30.028m0 0l-6.528 10.445m70.501-82.25v2.611m0 3.264v2.611m0 3.264v2.611m10.444-14.361v2.611m-5.222-5.875v2.61m0 3.918v2.61m0 3.917v2.611m0 3.916v2.612m5.222-13.055v2.611m0 3.264v2.611"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="room-bedroom" viewBox="0 0 188 188">
        <path
          d="M147.365 99.638V50.59c.163-2.067-.588-6.2-4.896-6.2H46.51c-1.795 0-5.385 1.24-5.385 6.2v49.048m-5.875 18.04v25.933h11.26l5.386-12.402h84.208l6.365 12.402h10.281v-25.933m-117.5 0v-18.04h117.5v18.04m-117.5 0h117.5"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M94 88.778H52.222v10.444H94m0-10.444v10.444m0-10.444h41.778v10.444H94" fill="none" stroke="currentColor"
          stroke-width="2" />
      </symbol>
      <symbol id="room-conservatory" viewBox="0 0 188 188">
        <path d="M152.75 37.208V152.75H35.25" fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M152.75 50.916H81.057l-36.668 42.43h6.567v58.098m-4.609-16.319h105.097m-99.874 5.222h99.874m-100.527-47h100.528"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M83.556 51.569L64.625 93.347v40.472m22.847-81.597v82.25m23.158-82.25v82.25m22.537-82.25v82.25"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="room-garage" viewBox="0 0 188 188">
        <path
          d="M152.75 39.82l-17.625 17.624h17.625v99.223H35.25m18.93-87.473v25.97m0 53.669v-42.705m7.181-28.854l-7.18 17.89m0 0v10.964m0 0L47 85.353"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M152.098 97.916c-10.445 0-15.667 2.612-29.375 13.709-4.839 3.917-25.894 4.787-31.334 5.875-3.916.783-11.097 4.047-11.097 12.403 0 5.744 3.917 6.527 6.528 6.527H94m20.889 0h37.209"
          fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="104.444" cy="135.778" r="10.444" fill="none" stroke="currentColor" stroke-width="2" />
        <ellipse cx="53.528" cy="75.722" rx="18.278" ry="35.25" fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="room-kitchen" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="2" d="M35.25 35.25h117.5v117.5H35.25z" />
        <path
          d="M43.083 73.111h101.833v70.5H43.083v-70.5zm33.945-24.805h33.944v-5.222H77.028v5.222zM43.083 63.972h101.833M35.25 56.14h117.5"
          fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="126.639" cy="45.694" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="141" cy="45.694" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="47" cy="45.694" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="61.361" cy="45.694" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="room-loft" viewBox="0 0 188 188">
        <path
          d="M63.32 84.86v10.445m0 61.361v-11.097m33.944-60.708v10.444m0 61.361v-11.097m0-50.264H63.319m33.945 0v16.32m-33.945-16.32v16.32m0 0h33.945m-33.945 0v16.972m33.945-16.972v16.972m0 0H63.319m33.945 0v16.972m-33.945-16.972v16.972m0 0h33.945m27.417-50.264h31.986l-63.32-63.972L67.89 56.791V47H48.306v29.375l-18.931 18.93h15.667"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="room-open-plan-area" viewBox="0 0 100 100">
    
        <path class="st0"
          d="M39.4 88.2h45c.6 0 1-.4 1-1V19.8c0-.6-.4-1-1-1h-8.6M27.7 88.2H17c-.6 0-1-.4-1-1V19.8c0-.6.4-1 1-1h47.2M27.8 87.6V74.1" />
        <path class="st1" d="M27.8 74.1c3.7-.4 11.1 1.9 11.1 14.1" />
        <path class="st0" d="M75.7 19.3v13.5" />
        <path class="st1" d="M75.7 32.9c-3.7.3-11.1-1.9-11.1-14.1" />
      </symbol>
      <symbol id="room-open-plan" viewBox="0 0 188 188">
        <path
          d="M74.124 165.806h85.459a1 1 0 001-1V36.25a1 1 0 00-1-1h-16.984m-90.59 130.556H31.028a1 1 0 01-1-1V36.25a1 1 0 011-1h89.589M52.222 164.701v-25.405"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M52.222 139.296c6.963-.736 20.889 3.535 20.889 26.51" fill="none" stroke="currentColor" stroke-width="2"
          stroke-dasharray="1,4" />
        <path d="M142.306 36.355V61.76" fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M142.306 61.76c-6.963.736-20.889-3.535-20.889-26.51" fill="none" stroke="currentColor" stroke-width="2"
          stroke-dasharray="1,4" />
      </symbol>
      <symbol id="room-same" viewBox="0 0 188 188">
        <path
          d="M81 110.287L84.713 114 120 78.715 116.285 75 81 110.287zm-13-13l13 13 3.713-3.715-12.997-13.001L68 97.287z"
          fill="currentColor" />
        <path fill="none" stroke="currentColor" stroke-width="2" stroke-dasharray="6,6" d="M41 41h106v106H41z" />
      </symbol>
      <symbol id="room-size-large" viewBox="0 0 180 180">
        <path
          d="M141.192 137.169l1.113-.668-4.69-10.108h-6.286l-4.69 10.108 1.113.668 3.379-5.569-.391 18.043h1.431l2.301-11.583 2.301 11.583h1.432l-.391-18.043 3.378 5.569zm-6.719-11.806a2.653 2.653 0 002.64-2.64 2.653 2.653 0 00-2.64-2.64 2.653 2.653 0 00-2.64 2.64 2.653 2.653 0 002.64 2.64z"
          fill="#a9a9a8" fill-rule="nonzero" />
        <path
          d="M48.401 163.195H25.806c-.549 0-1-.451-1-1V25.806c0-.549.451-1 1-1h136.389c.549 0 1 .451 1 1v136.389c0 .549-.451 1-1 1H71.117m-22.811-1.193v-27.446"
          fill="none" stroke="currentColor" stroke-width="3" />
        <path d="M48.306 134.555c7.398-.795 22.194 3.819 22.194 28.64" fill="none" stroke="currentColor"
          stroke-width="1.5" stroke-dasharray="1,4" />
        <path
          d="M107.055 32.333c0-.549-.451-1-1-1H81.944c-.549 0-1 .451-1 1v3.44c0 .549.451 1 1 1h24.111c.549 0 1-.451 1-1v-3.44z"
          fill="currentColor" />
      </symbol>
      <symbol id="room-size-medium" viewBox="0 0 180 180">
        <path
          d="M138.559 122.414l1.512-.907-6.366-13.72h-8.534l-6.365 13.72 1.511.907 4.587-7.559-.532 24.49h1.943l3.123-15.721 3.124 15.721h1.944l-.532-24.49 4.585 7.559zm-9.12-16.023a3.601 3.601 0 003.584-3.585 3.601 3.601 0 00-3.584-3.584h-.001a3.601 3.601 0 00-3.584 3.584 3.602 3.602 0 003.585 3.585z"
          fill="#a9a9a8" fill-rule="nonzero" />
        <path
          d="M73.764 152.75h77.986c.549 0 1-.451 1-1V36.25c0-.549-.451-1-1-1H36.25c-.549 0-1 .451-1 1v115.5c0 .549.451 1 1 1h14.014m.653-1.248l-.001-28.693"
          fill="none" stroke="currentColor" stroke-width="3" />
        <path d="M50.917 122.809c7.406-.832 22.22 3.992 22.22 29.941" fill="none" stroke="currentColor" stroke-width="1.5"
          stroke-dasharray="1,4" />
        <path
          d="M109.667 42.777c0-.549-.451-1-1-1H79.333a1.005 1.005 0 00-.999 1l-.001 4.528c.001.552.449 1 1 1h29.334c.549 0 1-.451 1-1v-4.528z"
          fill="currentColor" />
      </symbol>
      <symbol id="room-size-small" viewBox="0 0 180 180">
        <path
          d="M128.115 113.275l1.511-.908-6.365-13.72h-8.534l-6.366 13.72 1.512.908 4.587-7.559-.532 24.49h1.942l3.124-15.721 3.123 15.721h1.944l-.532-24.49 4.586 7.559zm-9.121-16.024h.001a3.601 3.601 0 003.584-3.584 3.601 3.601 0 00-3.583-3.584h-.001a3.601 3.601 0 00-3.584 3.584 3.601 3.601 0 003.583 3.584z"
          fill="#a9a9a8" fill-rule="nonzero" />
        <path
          d="M81.655 142.305h59.65c.549 0 1-.451 1-1V46.694c0-.549-.451-1-1-1H46.694c-.549 0-1 .451-1 1v94.611c0 .549.451 1 1 1h11.345m.058.058v-28.694"
          fill="none" stroke="currentColor" stroke-width="3" />
        <path d="M58.097 113.669c7.407-.831 22.221 3.993 22.221 29.942" fill="none" stroke="currentColor"
          stroke-width="1.5" stroke-dasharray="1,4" />
        <path
          d="M109.667 53.223c0-.549-.451-1-1-1H79.333a1.005 1.005 0 00-.999 1l-.001 4.527c.001.552.449 1 1 1h29.334c.549 0 1-.451 1-1v-4.527z"
          fill="currentColor" />
      </symbol>
      <symbol id="room-size-xlarge" viewBox="0 0 188 188">
        <path
          d="M28.817 182.778H6.222c-.549 0-1-.451-1-1V6.223c0-.549.451-1 1-1h175.556c.549 0 1 .451 1 1v175.555c0 .549-.451 1-1 1H51.534m-22.812-1.193v-27.446"
          fill="none" stroke="currentColor" stroke-width="3" />
        <path d="M28.722 154.138c7.398-.795 22.195 3.819 22.195 28.64" fill="none" stroke="currentColor"
          stroke-width="1.5" stroke-dasharray="1,4" />
        <path
          d="M147.72 152.835l1.113-.668-4.69-10.108h-6.287l-4.689 10.108 1.113.668 3.379-5.569-.391 18.043h1.431L141 153.727l2.301 11.582h1.432l-.392-18.043 3.379 5.569zM141 141.03h.001a2.653 2.653 0 002.64-2.64 2.653 2.653 0 00-2.64-2.64 2.653 2.653 0 00-2.641 2.64 2.653 2.653 0 002.64 2.64z"
          fill="#a9a9a8" fill-rule="nonzero" />
        <path
          d="M109.667 12.75c0-.549-.451-1-1-1H79.333a1.005 1.005 0 00-.999 1l-.001 4.528c.001.552.449 1 1 1h29.334c.549 0 1-.451 1-1V12.75z"
          fill="currentColor" />
      </symbol>
      <symbol id="room-utility" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="2" d="M41.778 35.25h104.444v112.278H41.778z" />
        <path
          d="M54.834 152.75h11.75v-5.222h-11.75v5.222zM48.306 45.694h11.75v-5.222h-11.75v5.222zm74.416 107.056h11.75v-5.222h-11.75v5.222zM43.083 49.61h101.833"
          fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="121.417" cy="43.083" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="129.25" cy="43.083" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="137.083" cy="43.083" r="2.611" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="94" cy="100.528" r="26.111" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="94" cy="100.528" r="32.639" fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M69.195 102.486l7.516-4.832a3.006 3.006 0 013.245 0l5.763 3.704a3.007 3.007 0 003.422-.123l4.284-3.214a3.008 3.008 0 013.7.078l3.612 2.956a3.007 3.007 0 003.608.144l4.85-3.357a3.005 3.005 0 013.507.066l6.104 4.578"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="somewhere-else" viewBox="0 0 100 100">
        <path
          d="M50.868 59.69c0-3.29 1.19-4.69 4.06-5.6 5.46-1.68 9.66-6.44 9.66-13.23 0-8.4-6.3-13.65-14.63-13.65-8.33 0-14.42 5.67-14.42 14.63h8.47c0-3.71 2.59-6.09 5.95-6.09 3.36 0 6.09 2.1 6.09 5.04 0 1.54-.49 3.64-2.66 4.55-7.49 3.64-11.13 5.25-11.13 14.35l.14 2.31h8.33l.14-2.31zm-9.73 11.62c0 3.15 2.52 5.39 5.53 5.39 3.01 0 5.53-2.24 5.53-5.39 0-3.01-2.52-5.32-5.53-5.32-3.01 0-5.53 2.31-5.53 5.32z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="type-back-boiler" viewBox="0 0 188 188">
        <path
          d="M95.344 159.367h37.476v-26.769h-21.415v-42.83h21.415c-6.96-6.959-8.7-14.499-8.7-17.399V42.923H95.344V54.3m0 105.067V54.3m0 105.067H65.898V54.3h29.446"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M74.597 102.483v29.446m0 0h-3.346v22.084h18.738v-22.084h-4.015m-11.377 0h6.023m0-29.446v29.446m0 0h5.354m0-29.446v29.446M61.213 32.215v34.8H80.62m-26.099-34.8v44.838H80.62M72.59 28.2v4.015m0 22.085v-5.689M87.312 28.2l.001 4.015M87.312 54.3l.001-5.689M72.59 32.215h14.722m-14.722 0v5.354m14.722-5.354l.001 5.354m-14.723 0h14.722m-14.722 0v5.354m14.722-5.354l.001 5.354m-14.723 0h14.722m-14.722 0v5.688m14.722-5.688l.001 5.688m-14.723 0h14.722m-6.691 96.702h32.792m16.062-55.544v42.16"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M114.413 129.583h2.015v2.015h-2.015v-2.015zm2.677-2.677h2.015v2.015h-2.015v-2.015zm2.677 2.677h2.015v2.015h-2.015v-2.015zm2.676-2.677h2.015v2.015h-2.015v-2.015zm2.677 2.677h2.015v2.015h-2.015v-2.015z"
          fill="#2d3d4d" stroke="currentColor" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" />
        <path
          d="M123.476 113.471c2.109-3.868-1.464-7.599-3.514-8.98.878 1.381.879 4.375 0 6.217-2.635 5.526-4.392 8.98.879 15.198-.879-4.836 0-7.599 2.635-12.435z"
          fill="currentColor" fill-rule="nonzero" stroke="currentColor" stroke-width="2" />
        <path
          d="M125.202 121.027c.843-1.934-.586-3.8-1.406-4.49.351.69.351 2.187 0 3.108-1.054 2.763-1.757 4.49.351 7.599-.351-2.418 0-3.799 1.055-6.217zm-10.194.777c-.843-1.693.586-3.325 1.406-3.929-.351.604-.351 1.914 0 2.72 1.054 2.418 1.757 3.929-.351 6.649.351-2.116 0-3.324-1.055-5.44z"
          fill="currentColor" fill-rule="nonzero" />
      </symbol>
      <symbol id="type-combi" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="2" d="M48.883 31.531h90.237v112.796H48.883z" />
        <circle cx="124.081" cy="130.791" r="6.016" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="60.165" cy="131.544" r="2.256" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="70.69" cy="131.544" r="2.256" fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M48.883 116.504h90.237" fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M96.479 94.51c.355 2.328-1.315 3.041-1.315 3.041.903-4.684-3.669-8.871-3.669-8.871.355 3.204-3.507 6.816-3.507 10.624 0 2.99 2.46 5.45 5.45 5.45 3.01 0 5.45-2.329 5.45-5.395 0-3.726-2.409-4.849-2.409-4.849z"
          fill="currentColor" />
        <path
          d="M96.479 94.51c.355 2.328-1.315 3.041-1.315 3.041.903-4.684-3.669-8.871-3.669-8.871.355 3.204-3.507 6.816-3.507 10.624 0 2.99 2.46 5.45 5.45 5.45 3.01 0 5.45-2.329 5.45-5.395 0-3.726-2.409-4.849-2.409-4.849"
          fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" />
        <path d="M106.033 144.326v12.146M81.97 144.327v12.145m12.031-12.146v12.146" fill="none" stroke="currentColor"
          stroke-width="2" />
      </symbol>
      <symbol id="type-standard" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="2" d="M29.669 75.279h62.458v78.073H29.669z" />
        <circle cx="81.719" cy="143.984" r="4.164" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="37.477" cy="144.503" r="1.561" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="44.763" cy="144.503" r="1.561" fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M29.669 134.095h62.459" fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M62.612 118.872c.246 1.611-.91 2.104-.91 2.104.625-3.241-2.54-6.14-2.54-6.14.246 2.218-2.427 4.718-2.427 7.354a3.79 3.79 0 003.772 3.772h.001c2.083 0 3.771-1.612 3.771-3.734 0-2.579-1.667-3.356-1.667-3.356z"
          fill="currentColor" />
        <path
          d="M62.612 118.872c.246 1.611-.91 2.104-.91 2.104.625-3.241-2.54-6.14-2.54-6.14.246 2.218-2.427 4.718-2.427 7.354a3.79 3.79 0 003.772 3.772h.001c2.083 0 3.771-1.612 3.771-3.734 0-2.579-1.667-3.356-1.667-3.356"
          fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" />
        <path
          d="M69.229 153.353v8.327m-16.658-8.327v8.327m8.329-8.327v8.327m49.41-9.715V82.133c0-14.573 49.792-13.966 49.792 0v69.832H110.31z"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M134.599 71.203v-7.287c0-1.012.607-3.036 3.036-3.036h5.465m3.643 85.012v15.788" fill="none"
          stroke="currentColor" stroke-width="2" />
        <circle cx="146.744" cy="144.07" r="2.643" fill="currentColor" stroke="currentColor" stroke-width="2"
          stroke-linecap="butt" stroke-linejoin="miter" />
        <path d="M123.67 145.892v15.788" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="123.669" cy="144.07" r="2.643" fill="currentColor" stroke="currentColor" stroke-width="2"
          stroke-linecap="butt" stroke-linejoin="miter" />
        <path
          d="M118.95 66.64V47.327c0-4.447 0-8.047-3.086-8.047h-5.554m-66.695-7.2h66.695m-66.695 0l3.032 28.08h60.632l3.031-28.08m-66.695 0v-5.76m66.695 5.76v-5.76"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M38.158 44.32h6.063l-.606-7.92h-5.457v7.92z" fill="currentColor" fill-rule="nonzero"
          stroke="currentColor" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" />
        <path
          d="M33.914 37.84h4.85v4.32h-4.85v-4.32zm-4.245-5.04h7.883l1.819 2.16h4.244m17.734 25.92v3.168c0 .384.46 1.152 2.304 1.152h35.136c.96-.096 2.88.057 2.88 1.44"
          fill="none" stroke="currentColor" stroke-width="2" />
      </symbol>
      <symbol id="type-system" viewBox="0 0 188 188">
        <path fill="none" stroke="currentColor" stroke-width="2" d="M30.08 54.52h67.68v84.6H30.08z" />
        <circle cx="86.48" cy="128.968" r="4.512" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="38.54" cy="129.532" r="1.692" fill="none" stroke="currentColor" stroke-width="2" />
        <circle cx="46.436" cy="129.532" r="1.692" fill="none" stroke="currentColor" stroke-width="2" />
        <path d="M30.08 118.252h67.68" fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M65.777 101.757c.266 1.746-.987 2.28-.987 2.28.678-3.513-2.752-6.653-2.752-6.653.267 2.403-2.63 5.112-2.63 7.968 0 2.243 1.845 4.088 4.088 4.088 2.258 0 4.088-1.747 4.088-4.047 0-2.794-1.807-3.636-1.807-3.636z"
          fill="currentColor" />
        <path
          d="M65.777 101.757c.266 1.746-.987 2.28-.987 2.28.678-3.513-2.752-6.653-2.752-6.653.267 2.403-2.63 5.112-2.63 7.968 0 2.243 1.845 4.088 4.088 4.088 2.258 0 4.088-1.747 4.088-4.047 0-2.794-1.807-3.636-1.807-3.636"
          fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="butt" stroke-linejoin="miter" />
        <path
          d="M72.943 139.12v9.024m-18.047-9.022v9.024m9.023-9.026v9.024m46.249-9.024V51.7c17.597-8.121 38.164-3.384 46.248 0v87.42c-18.048 7.219-38.352 3.008-46.248 0z"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M132.728 47.188V40.42c0-.94.564-2.82 2.82-2.82h5.076m9.588 81.78h12.408m-56.964 12.408h15.792m21.432 1.692v14.664"
          fill="none" stroke="currentColor" stroke-width="2" />
        <path
          d="M150.904 119.38v.049a2.395 2.395 0 01-2.383 2.384 2.395 2.395 0 01-2.384-2.384v-.049a2.394 2.394 0 012.383-2.335 2.395 2.395 0 012.384 2.335zm-5.64 12.408a2.395 2.395 0 01-2.384 2.384 2.395 2.395 0 01-2.384-2.384 2.395 2.395 0 012.384-2.384 2.395 2.395 0 012.384 2.384z"
          fill="currentColor" fill-rule="nonzero" stroke="currentColor" stroke-width="2" stroke-linecap="butt"
          stroke-linejoin="miter" />
        <circle cx="123.704" cy="131.788" r="2.384" fill="currentColor" stroke="currentColor" stroke-width="2"
          stroke-linecap="butt" stroke-linejoin="miter" />
      </symbol>
      <symbol id="wall-external" viewBox="0 0 188 188">
        <path
          d="M53.8 155H34a1 1 0 01-1-1V94m40.827 61H154a1 1 0 001-1V94m0 0V34a1 1 0 00-1-1h-36.369M155 94h-37.369M33 94V34a1 1 0 011-1h83.631M33 94h20.801m63.83-61v6.342m0 54.658V52.566m0 41.434H73.405"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M155 33H33" stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M118.273 39.588h12.426" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M130.699 39.588c.361 4.391-1.729 13.175-12.967 13.175" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1 4" />
        <path d="M54.933 154.647v-24.106" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M54.933 130.54c6.498-.698 19.493 3.354 19.493 25.155" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1 4" />
        <path d="M54.664 93.629V69.522" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M54.664 69.523c6.498-.699 19.494 3.354 19.494 25.154" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1 4" />
        <rect x="62" y="36" width="23" height="5" rx="1" fill="#FB6058" />
      </symbol>
      <symbol id="wall-internal" viewBox="0 0 188 188">
        <path
          d="M53.8 155H34a1 1 0 01-1-1V94m40.827 61H154a1 1 0 001-1V94m0 0V34a1 1 0 00-1-1h-36.369M155 94h-37.369M33 94V34a1 1 0 011-1h83.631M33 94h20.801m63.83-61v6.342m0 54.658V52.566m0 41.434H73.405"
          stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M155 94H74" stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path d="M118.273 39.588H130.7" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M130.7 39.588c.36 4.391-1.729 13.175-12.967 13.175" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1 4" />
        <path d="M54.933 154.647v-24.106" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M54.933 130.54c6.498-.698 19.493 3.354 19.493 25.155" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1 4" />
        <path d="M54.664 93.628V69.522" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M54.664 69.522c6.498-.699 19.494 3.354 19.494 25.154" stroke="#FB6058" stroke-width="1.5"
          stroke-linecap="round" stroke-linejoin="round" stroke-dasharray="1 4" />
        <rect x="88" y="97" width="23" height="5" rx="1" fill="#FB6058" />
      </symbol>
      <symbol id="wall-mounted-high" viewBox="0 0 188 188">
        <g clip-path="url(#wall-mounted-highclip0)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="#FB6058" d="M85.416 74h9.098v9.098h-9.098z" />
          <path
            d="M82.573 76.844c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0-.948-.948-3.412-2.275-5.686 0"
            stroke="#2388EF" />
          <path
            d="M82.573 79.805c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0-.561-.562-1.655-1.256-2.927-1.256m14.299 4.217c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0"
            stroke="#2388EF" />
          <path stroke="#FB6058" d="M66 94h21.205v14.509H66z" />
          <path
            d="M77.62 101.362a4.257 4.257 0 11-8.514 0 4.257 4.257 0 018.514 0zm2.42-1.132h4.603m-4.603-2.829h4.603m-4.603 4.875h4.603m-4.684 3.442h4.602"
            stroke="#FB6058" />
          <path
            d="M-2.875 62.025h106.887m0 .001L67.372 19.81H-3.437M98.525 62.37v106.924M94.868 62.37v106.924m-97.18-.001H149"
            stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M47.25 109H36.33V85.342h10.92V109z" stroke="#2D3D4D" />
          <path d="M36.33 96.607h10.92" stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M36.33 109H25.41V85.342h10.92V109z" stroke="#2D3D4D" />
          <path d="M25.41 96.607h10.92M49.98 109H22.682" stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M47.25 151.658H36.33V128h10.92v23.658z" stroke="#2D3D4D" />
          <path d="M36.33 139.266h10.92" stroke="#2D3D4D" />
          <path clip-rule="evenodd" d="M36.33 151.658H25.41V128h10.92v23.658z" stroke="#2D3D4D" />
          <path d="M25.41 139.266h10.92m13.65 12.392H22.682" stroke="#2D3D4D" />
        </g>
      </symbol>
      <symbol id="wall-mounted-low" viewBox="0 0 188 188">
        <g clip-path="url(#wall-mounted-lowclip0)">
          <path
            d="M-2.875 62.025h106.887m0 .001L67.372 19.81H-3.437M98.525 62.37v106.924M94.868 62.37v106.924m-97.18-.001H149"
            stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
          <path clip-rule="evenodd" d="M47.25 109H36.33V85.342h10.92V109z" stroke="#2D3D4D" stroke-width="1.5"
            stroke-linecap="round" stroke-linejoin="round" />
          <path d="M36.33 96.607h10.92" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
            stroke-linejoin="round" />
          <path clip-rule="evenodd" d="M36.33 109H25.41V85.342h10.92V109z" stroke="#2D3D4D" stroke-width="1.5"
            stroke-linecap="round" stroke-linejoin="round" />
          <path d="M25.41 96.607h10.92M49.98 109H22.682" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
            stroke-linejoin="round" />
          <path clip-rule="evenodd" d="M47.25 151.658H36.33V128h10.92v23.658z" stroke="#2D3D4D" stroke-width="1.5"
            stroke-linecap="round" stroke-linejoin="round" />
          <path d="M36.33 139.266h10.92" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
            stroke-linejoin="round" />
          <path clip-rule="evenodd" d="M36.33 151.658H25.41V128h10.92v23.658z" stroke="#2D3D4D" stroke-width="1.5"
            stroke-linecap="round" stroke-linejoin="round" />
          <path d="M25.41 139.266h10.92m13.65 12.392H22.682" stroke="#2D3D4D" stroke-width="1.5" stroke-linecap="round"
            stroke-linejoin="round" />
          <path stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
            d="M85.416 74h9.098v9.098h-9.098z" />
          <path
            d="M82.573 76.844c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0-.948-.948-3.412-2.275-5.686 0"
            stroke="#2388EF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
          <path
            d="M82.573 79.805c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0-.561-.562-1.655-1.256-2.927-1.256m14.299 4.217c-.758-.948-2.843-2.275-5.117 0-2.275 2.274-5.118.947-6.255 0"
            stroke="#2388EF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
          <path stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
            d="M62.269 137.491h21.205V152H62.269z" />
          <path
            d="M73.888 144.853a4.257 4.257 0 11-8.514 0 4.257 4.257 0 018.514 0zm2.422-1.131h4.602m-4.602-2.829h4.602m-4.602 4.875h4.602m-4.685 3.441h4.603"
            stroke="#FB6058" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
          <path
            d="M69.47 132.53a.75.75 0 001.06 0l4.773-4.773a.75.75 0 10-1.06-1.06L70 130.939l-4.243-4.242a.75.75 0 00-1.06 1.06l4.773 4.773zm16.06-43.06a.75.75 0 00-1.06 0l-4.773 4.773a.75.75 0 001.06 1.06L85 91.061l4.243 4.242a.75.75 0 001.06-1.06L85.53 89.47zM70.75 132v-13.887h-1.5V132h1.5zM75 113.863h5v-1.5h-5v1.5zm10.75-5.75V90h-1.5v18.113h1.5zm-5.75 5.75a5.75 5.75 0 005.75-5.75h-1.5a4.25 4.25 0 01-4.25 4.25v1.5zm-9.25 4.25a4.25 4.25 0 014.25-4.25v-1.5a5.75 5.75 0 00-5.75 5.75h1.5z"
            fill="#FB6058" />
        </g>
      </symbol>
      <symbol id="where-car-port" viewBox="0 0 188 188">
        <path d="M35 157h117V57.19H68V48h84" stroke="#FB6058" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
        <path
          d="M99.544 147.997H93.06c-2.358 0-7.061.285-7.061-4.921 0-7.572.076-16.961 3.612-17.671 4.912-.986 19.219.567 23.588-2.983C125.577 112.366 130.292 110 139.723 110H152m-33.596 37.997H152"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="109" cy="148" r="9" stroke="#FB6058" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="where-driveway" viewBox="0 0 188 188">
        <path d="M152.75 39.82l-17.625 17.624h17.625v99.223H35.25" stroke="#FB6058" stroke-width="2"
          stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M98.544 146.997H92.06c-2.358 0-7.061.285-7.061-4.921 0-7.572.076-16.961 3.612-17.671 4.912-.986 19.219.567 23.588-2.983C124.577 111.366 129.292 109 138.723 109H152m-34.596 37.997H152"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="108" cy="147" r="9" stroke="#FB6058" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
        <path d="M54.18 69.195v25.969m0 53.669v-42.705m7.181-28.854l-7.18 17.89m0 0v10.964m0 0L47 85.353" stroke="#FB6058"
          stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <ellipse cx="53.528" cy="75.722" rx="18.278" ry="35.25" stroke="#FB6058" stroke-width="2" stroke-linecap="round"
          stroke-linejoin="round" />
      </symbol>
      <symbol id="where-garage" viewBox="0 0 188 188">
        <path d="M157.292 91.972h-15.986V153.5h-95.64V91.972H30l18.93-18.93V43.667h19.584v9.791L93.972 28l63.32 63.972z"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <path
          d="M87.544 143.997H81.06c-2.358 0-7.061.285-7.061-4.921 0-7.572.076-16.961 3.612-17.671 4.912-.986 19.22.567 23.588-2.983C113.577 108.366 118.292 106 127.723 106H140m-33.596 37.997H140"
          stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <circle cx="97" cy="144" r="9" stroke="#FB6058" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </symbol>
      <symbol id="yn-no" viewBox="0 0 188 188">
        <path d="M150.4 37.6L37.6 150.4m0-112.8l112.8 112.8" fill="none" stroke="currentColor" stroke-width="3" />
      </symbol>
      <symbol id="yn-yes" viewBox="0 0 188 188">
        <path d="M156.04 41.36l-85.982 88.36L33.84 99.977" fill="none" stroke="currentColor" stroke-width="3" />
      </symbol>
    </defs>
    <view id="view-aircon" viewBox="0 0 50 50" />
    <view id="view-airing-cupboard" viewBox="50 0 50 50" />
    <view id="view-answer-house" viewBox="100 0 50 50" />
    <view id="view-arrow" viewBox="150 0 50 50" />
    <view id="view-back" viewBox="200 0 50 50" />
    <view id="view-basket" viewBox="250 0 50 50" />
    <view id="view-bathroom" viewBox="300 0 50 50" />
    <view id="view-baths" viewBox="350 0 80.77 50" />
    <view id="view-bedrooms" viewBox="450 0 119.05 50" />
    <view id="view-boiler-white" viewBox="600 0 50 50" />
    <view id="view-boxt-buy" viewBox="650 0 50 50" />
    <view id="view-boxt-logo-white" viewBox="700 0 206.25 50" />
    <view id="view-bubbles-1" viewBox="0 50 78.18 50" />
    <view id="view-bubbles-2" viewBox="100 50 36.13 50" />
    <view id="view-bubbles-3" viewBox="150 50 38.03 50" />
    <view id="view-burst" viewBox="200 50 50 50" />
    <view id="view-calculator-navy" viewBox="250 50 37.5 50" />
    <view id="view-calculator-white" viewBox="300 50 37.5 50" />
    <view id="view-calendar-check" viewBox="350 50 46.88 50" />
    <view id="view-call-icon" viewBox="400 50 50 50" />
    <view id="view-call-transparent" viewBox="450 50 50 50" />
    <view id="view-cart" viewBox="500 50 63.46 50" />
    <view id="view-chat-icon" viewBox="600 50 50 50" />
    <view id="view-chevron-down-v2" viewBox="650 50 91.67 50" />
    <view id="view-chevron-down" viewBox="750 50 50 50" />
    <view id="view-circle-inactive" viewBox="800 50 50 50" />
    <view id="view-clock" viewBox="850 50 49.32 50" />
    <view id="view-coockie-icon" viewBox="900 50 50 50" />
    <view id="view-cross" viewBox="950 50 50 50" />
    <view id="view-discount" viewBox="0 100 52.38 50" />
    <view id="view-double-chevron-up" viewBox="100 100 45.83 50" />
    <view id="view-dropdown-chevron" viewBox="150 100 85.71 50" />
    <view id="view-engineer" viewBox="250 100 38.46 50" />
    <view id="view-epa-heat-a" viewBox="300 100 160.64 50" />
    <view id="view-epa-heat-b" viewBox="500 100 160.64 50" />
    <view id="view-epa-heat" viewBox="700 100 157.29 50" />
    <view id="view-epa-water-a" viewBox="0 150 160.64 50" />
    <view id="view-epa-water-b" viewBox="200 150 160.64 50" />
    <view id="view-epa-water" viewBox="400 150 160.64 50" />
    <view id="view-ev-car" viewBox="600 150 50 50" />
    <view id="view-ev" viewBox="650 150 50 50" />
    <view id="view-external" viewBox="700 150 50 50" />
    <view id="view-filter" viewBox="750 150 50 50" />
    <view id="view-flue-roof" viewBox="800 150 46.3 50" />
    <view id="view-flue-wall" viewBox="850 150 22.22 50" />
    <view id="view-form-icon" viewBox="900 150 38.46 50" />
    <view id="view-forward" viewBox="950 150 29.27 50" />
    <view id="view-garage" viewBox="0 200 50 50" />
    <view id="view-green-tick" viewBox="50 200 50 50" />
    <view id="view-halfords-logo" viewBox="100 200 241.67 50" />
    <view id="view-hbw-flow-answer-lg" viewBox="350 200 34.29 50" />
    <view id="view-hbw-flow-answer-sm" viewBox="400 200 60.74 50" />
    <view id="view-hbw-flow-bottom-line-lg" viewBox="500 200 131.82 50" />
    <view id="view-hbw-flow-bottom-line-md" viewBox="650 200 90.91 50" />
    <view id="view-hbw-flow-bottom-line-sm" viewBox="750 200 16.67 50" />
    <view id="view-hbw-flow-choose-lg" viewBox="800 200 34.29 50" />
    <view id="view-hbw-flow-choose-sm" viewBox="850 200 60.74 50" />
    <view id="view-hbw-flow-install-lg" viewBox="0 250 34.29 50" />
    <view id="view-hbw-flow-install-sm" viewBox="50 250 60.74 50" />
    <view id="view-hbw-flow-pay-lg" viewBox="150 250 34.29 50" />
    <view id="view-hbw-flow-pay-sm" viewBox="200 250 60.74 50" />
    <view id="view-hbw-flow-receive-lg" viewBox="300 250 34.29 50" />
    <view id="view-hbw-flow-receive-sm" viewBox="350 250 60.74 50" />
    <view id="view-hbw-flow-top-line-lg" viewBox="450 250 95.91 50" />
    <view id="view-hbw-flow-top-line-md" viewBox="550 250 69.81 50" />
    <view id="view-hbw-flow-top-line-sm" viewBox="650 250 23.89 50" />
    <view id="view-heat" viewBox="700 250 50.11 50" />
    <view id="view-heating" viewBox="800 250 50 50" />
    <view id="view-heating_and_hot_water" viewBox="850 250 50 50" />
    <view id="view-hiw-answer-ac" viewBox="900 250 50 50" />
    <view id="view-hiw-answer-ev" viewBox="950 250 50 50" />
    <view id="view-hiw-answer-halfords-ev" viewBox="0 300 49.71 50" />
    <view id="view-hiw-answer" viewBox="50 300 50 50" />
    <view id="view-hiw-choose-halfords-ev" viewBox="100 300 49.71 50" />
    <view id="view-hiw-choose" viewBox="150 300 50 50" />
    <view id="view-hiw-cover-pay" viewBox="200 300 50 50" />
    <view id="view-hiw-installer-halfords-ev" viewBox="250 300 49.71 50" />
    <view id="view-hiw-installer" viewBox="300 300 50 50" />
    <view id="view-hot_water" viewBox="350 300 50 50" />
    <view id="view-house-white" viewBox="400 300 50.29 50" />
    <view id="view-howitworks-1" viewBox="500 300 50 50" />
    <view id="view-howitworks-2" viewBox="550 300 50 50" />
    <view id="view-howitworks-3" viewBox="600 300 50 50" />
    <view id="view-info-grey" viewBox="650 300 50 50" />
    <view id="view-info" viewBox="700 300 23.08 50" />
    <view id="view-installation-included" viewBox="750 300 51.47 50" />
    <view id="view-installation" viewBox="850 300 48.98 50" />
    <view id="view-installer-v2" viewBox="900 300 50 50" />
    <view id="view-installer" viewBox="950 300 29.65 50" />
    <view id="view-landlord-protect-plus" viewBox="0 350 50 50" />
    <view id="view-landlord-service-plan" viewBox="50 350 50 50" />
    <view id="view-laptop" viewBox="100 350 66.86 50" />
    <view id="view-loader" viewBox="200 350 50 50" />
    <view id="view-loading" viewBox="250 350 50 50" />
    <view id="view-loft" viewBox="300 350 50 50" />
    <view id="view-logo-baxi" viewBox="350 350 163.64 50" />
    <view id="view-logo-gassafe-black" viewBox="550 350 46.1 50" />
    <view id="view-logo-gassafe-reg" viewBox="600 350 36.84 50" />
    <view id="view-logo-gassafe" viewBox="650 350 48.17 50" />
    <view id="view-logo-mark-text" viewBox="700 350 41.8 50" />
    <view id="view-logo-mark" viewBox="750 350 42.57 50" />
    <view id="view-logo-refcom" viewBox="800 350 50.45 50" />
    <view id="view-logo-text" viewBox="900 350 189.66 50" />
    <view id="view-logo-trustpilot" viewBox="0 400 443.48 50" />
    <view id="view-logo-vokera" viewBox="450 400 190.71 50" />
    <view id="view-logo-worcesterbosch" viewBox="650 400 181.4 50" />
    <view id="view-logo" viewBox="0 450 42.06 50" />
    <view id="view-math-equals" viewBox="50 450 61.54 50" />
    <view id="view-math-plus" viewBox="150 450 50 50" />
    <view id="view-menu" viewBox="200 450 50 50" />
    <view id="view-nav-aircons" viewBox="250 450 50 50" />
    <view id="view-nav-boilers" viewBox="300 450 50 50" />
    <view id="view-nav-ev" viewBox="350 450 50 50" />
    <view id="view-nav-home-cover" viewBox="400 450 50 50" />
    <view id="view-nav-home" viewBox="450 450 50 50" />
    <view id="view-next" viewBox="500 450 29.27 50" />
    <view id="view-no-installer-day" viewBox="550 450 29.79 50" />
    <view id="view-open-plan-area" viewBox="600 450 50 50" />
    <view id="view-outdoor-unit-floor" viewBox="650 450 50 50" />
    <view id="view-outdoor-unit-high" viewBox="700 450 50 50" />
    <view id="view-outdoor-unit-low" viewBox="750 450 50 50" />
    <view id="view-padlock-white" viewBox="800 450 31.25 50" />
    <view id="view-page-loader" viewBox="850 450 50 50" />
    <view id="view-pdf" viewBox="900 450 44.26 50" />
    <view id="view-phonebox" viewBox="950 450 39.83 50" />
    <view id="view-phoneus" viewBox="0 500 44.66 50" />
    <view id="view-pickdate" viewBox="50 500 38.89 50" />
    <view id="view-play-icon" viewBox="100 500 39.29 50" />
    <view id="view-popular-choice" viewBox="150 500 53.33 50" />
    <view id="view-product-central-heating-output" viewBox="250 500 50 50" />
    <view id="view-product-cooling-power" viewBox="300 500 50 50" />
    <view id="view-product-flow-rate" viewBox="350 500 50 50" />
    <view id="view-product-heating-power" viewBox="400 500 50 50" />
    <view id="view-product-power-output" viewBox="450 500 38.33 50" />
    <view id="view-product-warranty" viewBox="500 500 50 50" />
    <view id="view-protect-plus" viewBox="550 500 50 50" />
    <view id="view-question_mark" viewBox="600 500 50 50" />
    <view id="view-radiator" viewBox="650 500 50 50" />
    <view id="view-right-arrow" viewBox="700 500 50 50" />
    <view id="view-rotate" viewBox="750 500 50 50" />
    <view id="view-screener-summary-menu" viewBox="800 500 50 50" />
    <view id="view-screening-summary-baths" viewBox="850 500 50 50" />
    <view id="view-screening-summary-bed" viewBox="900 500 50 50" />
    <view id="view-screening-summary-showers" viewBox="950 500 50 50" />
    <view id="view-search-teal" viewBox="0 550 50 50" />
    <view id="view-search" viewBox="50 550 50 50" />
    <view id="view-service-plan" viewBox="100 550 50 50" />
    <view id="view-showers" viewBox="150 550 59.26 50" />
    <view id="view-smart-home" viewBox="250 550 50 50" />
    <view id="view-snowflake" viewBox="300 550 48.86 50" />
    <view id="view-social-facebook" viewBox="350 550 50 50" />
    <view id="view-social-instagram" viewBox="400 550 50 50" />
    <view id="view-social-linkedin" viewBox="450 550 50 50" />
    <view id="view-social-pinterest" viewBox="500 550 50 50" />
    <view id="view-social-twitter" viewBox="550 550 50 50" />
    <view id="view-social-youtube" viewBox="600 550 71.07 50" />
    <view id="view-star" viewBox="700 550 52.94 50" />
    <view id="view-success" viewBox="800 550 50 50" />
    <view id="view-sunlight-lots" viewBox="850 550 50 50" />
    <view id="view-sunlight-not-much" viewBox="900 550 50 50" />
    <view id="view-sunlight-some" viewBox="950 550 50 50" />
    <view id="view-tea-cup-icon" viewBox="0 600 46.15 50" />
    <view id="view-tick-circle" viewBox="50 600 50 50" />
    <view id="view-tick-inactive" viewBox="100 600 50 50" />
    <view id="view-tick-v2" viewBox="150 600 50 50" />
    <view id="view-tick-white" viewBox="200 600 71.05 50" />
    <view id="view-tree" viewBox="300 600 35.64 50" />
    <view id="view-trustpilot-logo-lightbg" viewBox="350 600 443.48 50" />
    <view id="view-trustpilot-logo" viewBox="0 650 203.59 50" />
    <view id="view-trustpilot-rating-1" viewBox="250 650 266.67 50" />
    <view id="view-trustpilot-rating-2" viewBox="550 650 266.67 50" />
    <view id="view-trustpilot-rating-3" viewBox="0 700 266.67 50" />
    <view id="view-trustpilot-rating-4" viewBox="300 700 266.67 50" />
    <view id="view-trustpilot-rating-5" viewBox="600 700 266.67 50" />
    <view id="view-ui-booking" viewBox="0 750 50 50" />
    <view id="view-ui-bullet" viewBox="50 750 50 50" />
    <view id="view-ui-calendar" viewBox="100 750 50 50" />
    <view id="view-ui-caution" viewBox="150 750 50 50" />
    <view id="view-ui-clock" viewBox="200 750 50 50" />
    <view id="view-ui-cross-round" viewBox="250 750 50 50" />
    <view id="view-ui-edit" viewBox="300 750 50 50" />
    <view id="view-ui-email" viewBox="350 750 50 50" />
    <view id="view-ui-filters" viewBox="400 750 50 50" />
    <view id="view-ui-info" viewBox="450 750 50 50" />
    <view id="view-ui-installer" viewBox="500 750 50 50" />
    <view id="view-ui-lock" viewBox="550 750 50 50" />
    <view id="view-ui-minus-outline" viewBox="600 750 50 50" />
    <view id="view-ui-minus" viewBox="650 750 50 50" />
    <view id="view-ui-phone" viewBox="700 750 50 50" />
    <view id="view-ui-plus-outline" viewBox="750 750 50 50" />
    <view id="view-ui-plus" viewBox="800 750 50 50" />
    <view id="view-ui-refresh" viewBox="850 750 50 50" />
    <view id="view-ui-repair" viewBox="900 750 50 50" />
    <view id="view-ui-reset" viewBox="950 750 50 50" />
    <view id="view-ui-settings" viewBox="0 800 50 50" />
    <view id="view-ui-tick-outline" viewBox="50 800 50 50" />
    <view id="view-ui-tick-round" viewBox="100 800 50 50" />
    <view id="view-ui-tick" viewBox="150 800 50 50" />
    <view id="view-up" viewBox="200 800 77.27 50" />
    <view id="view-utility-room" viewBox="300 800 50 50" />
    <view id="view-warning" viewBox="350 800 50 50" />
    <view id="view-airingcupboard-middle" viewBox="400 800 50 50" />
    <view id="view-airingcupboard-outsidewall" viewBox="450 800 50 50" />
    <view id="view-bedroom" viewBox="500 800 50 50" />
    <view id="view-boundary-less-than" viewBox="550 800 50 50" />
    <view id="view-boundary-more-than" viewBox="600 800 50 50" />
    <view id="view-car" viewBox="650 800 50 50" />
    <view id="view-conservatory" viewBox="700 800 50 50" />
    <view id="view-day" viewBox="750 800 50 50" />
    <view id="view-distance-16-25m" viewBox="800 800 50 50" />
    <view id="view-distance-3-to-5m" viewBox="850 800 50 50" />
    <view id="view-distance-6-to-15m" viewBox="900 800 50 50" />
    <view id="view-fitted-back-to-back" viewBox="950 800 50 50" />
    <view id="view-fitted-somewhere-else" viewBox="0 850 50 50" />
    <view id="view-floor-1st-floor" viewBox="50 850 50 50" />
    <view id="view-floor-2nd-3rd-floor" viewBox="100 850 50 50" />
    <view id="view-floor-2nd-floor" viewBox="150 850 50 50" />
    <view id="view-floor-3rd-floor-plus" viewBox="200 850 50 50" />
    <view id="view-floor-4rd-floor-plus" viewBox="250 850 50 50" />
    <view id="view-floor-basement" viewBox="300 850 50 50" />
    <view id="view-floor-ground" viewBox="350 850 50 50" />
    <view id="view-floor-mounted" viewBox="400 850 50 50" />
    <view id="view-fuel-electricity" viewBox="450 850 50 50" />
    <view id="view-fuel-gas" viewBox="500 850 50 50" />
    <view id="view-fuel-lpg" viewBox="550 850 50 50" />
    <view id="view-fuel-oil" viewBox="600 850 50 50" />
    <view id="view-fusebox-0-5-ev" viewBox="650 850 50 50" />
    <view id="view-fusebox-16-25-ev" viewBox="700 850 50 50" />
    <view id="view-fusebox-25-plus-ev" viewBox="750 850 50 50" />
    <view id="view-fusebox-6-15-ev" viewBox="800 850 50 50" />
    <view id="view-home-bungalow-2" viewBox="850 850 50 50" />
    <view id="view-home-bungalow" viewBox="900 850 50 50" />
    <view id="view-home-commercial" viewBox="950 850 50 50" />
    <view id="view-home-detached" viewBox="0 900 50 50" />
    <view id="view-home-flat" viewBox="50 900 50 50" />
    <view id="view-home-semi" viewBox="100 900 50 50" />
    <view id="view-home-terrace" viewBox="150 900 50 50" />
    <view id="view-kitchen" viewBox="200 900 50 50" />
    <view id="view-living-room" viewBox="250 900 50 50" />
    <view id="view-more-than-one-room" viewBox="300 900 50 50" />
    <view id="view-number-0" viewBox="350 900 50 50" />
    <view id="view-number-1-5" viewBox="400 900 49.71 50" />
    <view id="view-number-1" viewBox="450 900 50 50" />
    <view id="view-number-10-20" viewBox="500 900 50 50" />
    <view id="view-number-10plus" viewBox="550 900 49.71 50" />
    <view id="view-number-11-15" viewBox="600 900 49.71 50" />
    <view id="view-number-15plus" viewBox="650 900 49.71 50" />
    <view id="view-number-1plus" viewBox="700 900 50 50" />
    <view id="view-number-2-5" viewBox="750 900 49.71 50" />
    <view id="view-number-2" viewBox="800 900 50 50" />
    <view id="view-number-20-25" viewBox="850 900 50 50" />
    <view id="view-number-25plus" viewBox="900 900 50 50" />
    <view id="view-number-2plus" viewBox="950 900 50 50" />
    <view id="view-number-3" viewBox="0 950 50 50" />
    <view id="view-number-3plus" viewBox="50 950 50 50" />
    <view id="view-number-4" viewBox="100 950 50 50" />
    <view id="view-number-4plus" viewBox="150 950 50 50" />
    <view id="view-number-5-10" viewBox="200 950 49.71 50" />
    <view id="view-number-5" viewBox="250 950 50 50" />
    <view id="view-number-5plus" viewBox="300 950 50 50" />
    <view id="view-number-6-10" viewBox="350 950 49.71 50" />
    <view id="view-number-6" viewBox="400 950 50 50" />
    <view id="view-number-6plus" viewBox="450 950 50 50" />
    <view id="view-number-7" viewBox="500 950 50 50" />
    <view id="view-number-7plus" viewBox="550 950 50 50" />
    <view id="view-number-8" viewBox="600 950 50 50" />
    <view id="view-number-8plus" viewBox="650 950 50 50" />
    <view id="view-number-9" viewBox="700 950 50 50" />
    <view id="view-number-9plus" viewBox="750 950 50 50" />
    <view id="view-number-up-to-10" viewBox="800 950 49.71 50" />
    <view id="view-number-up-to-2" viewBox="850 950 49.71 50" />
    <view id="view-pressure-average" viewBox="900 950 50 50" />
    <view id="view-pressure-fast" viewBox="950 950 50 50" />
    <view id="view-pressure-slow" viewBox="0 1000 50 50" />
    <view id="view-question-light" viewBox="50 1000 50 50" />
    <view id="view-question-thin" viewBox="100 1000 50 50" />
    <view id="view-question" viewBox="150 1000 50 50" />
    <view id="view-room-airingcupboard" viewBox="200 1000 50 50" />
    <view id="view-room-bathroom" viewBox="250 1000 50 50" />
    <view id="view-room-bedroom" viewBox="300 1000 50 50" />
    <view id="view-room-conservatory" viewBox="350 1000 50 50" />
    <view id="view-room-garage" viewBox="400 1000 50 50" />
    <view id="view-room-kitchen" viewBox="450 1000 50 50" />
    <view id="view-room-loft" viewBox="500 1000 50 50" />
    <view id="view-room-open-plan-area" viewBox="550 1000 50 50" />
    <view id="view-room-open-plan" viewBox="600 1000 50 50" />
    <view id="view-room-same" viewBox="650 1000 50 50" />
    <view id="view-room-size-large" viewBox="700 1000 50 50" />
    <view id="view-room-size-medium" viewBox="750 1000 50 50" />
    <view id="view-room-size-small" viewBox="800 1000 50 50" />
    <view id="view-room-size-xlarge" viewBox="850 1000 50 50" />
    <view id="view-room-utility" viewBox="900 1000 50 50" />
    <view id="view-somewhere-else" viewBox="950 1000 50 50" />
    <view id="view-type-back-boiler" viewBox="0 1050 50 50" />
    <view id="view-type-combi" viewBox="50 1050 50 50" />
    <view id="view-type-standard" viewBox="100 1050 50 50" />
    <view id="view-type-system" viewBox="150 1050 50 50" />
    <view id="view-wall-external" viewBox="200 1050 50 50" />
    <view id="view-wall-internal" viewBox="250 1050 50 50" />
    <view id="view-wall-mounted-high" viewBox="300 1050 50 50" />
    <view id="view-wall-mounted-low" viewBox="350 1050 50 50" />
    <view id="view-where-car-port" viewBox="400 1050 50 50" />
    <view id="view-where-driveway" viewBox="450 1050 50 50" />
    <view id="view-where-garage" viewBox="500 1050 50 50" />
    <view id="view-yn-no" viewBox="550 1050 50 50" />
    <view id="view-yn-yes" viewBox="600 1050 50 50" />
    {children}
  </svg>
  )
}

export default Svg