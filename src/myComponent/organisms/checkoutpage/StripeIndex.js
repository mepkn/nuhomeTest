import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import React from "react";
import CheckoutForm from "./CheckoutForm";
const PUBLIC_KEY =
  "pk_test_51MwdqiSD1XaStwlPjHQwwkSWQdaWSJN9Q0UwjOhEIVPNIYL64i2gVoF7UGJWgHuDPRA0RtgXVE7VgGGj1JcQjTZ500Cr4eFXdp";

const stripeTestPromise = loadStripe(PUBLIC_KEY);
const StripeIndex = () => {
  return (
    <Elements stripe={stripeTestPromise}>
      <CheckoutForm />
    </Elements>
  );
};

export default StripeIndex;
