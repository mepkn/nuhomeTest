import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_FORM_DETAILS } from "./redux/formActions";
import { useForm } from "react-hook-form";
import { storeSelectedAddress } from "./redux/formReducer";
import { setRoute } from "../../../scenes/router/redux/reducer";
import { ROUTES } from "../../../utils/constants";
import axios from "axios";

const Form = ({ handelNext }) => {
  const dispatch = useDispatch();
  const [postCode, setPostCode] = useState("");
  const [address, setAddress] = useState("");
  const [selectedAddress, setSelectedAddress] = useState("");
  const key = "AN23-DGB0-3E09-SCBI";
  const user = "DaniBrown379802";
  const API_KEY = "AIzaSyAYY2jlGhu3mzukE1ghn0Z0LIt6kkR07zk"; // Replace with your actual API key

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    // setAddress(data.postalcode);

    axios
      .get("https://maps.googleapis.com/maps/api/geocode/json", {
        params: {
          components: `postal_code:${address}|country:UK`,
          key: API_KEY,
        },
      })
      .then((response) => {
        setPostCode(response.data.results);

        dispatch({
          type: GET_FORM_DETAILS,
          payload: { postCode: address, key: key, user: user },
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleInputChange = (event) => {
    setAddress(event.target.value);
  };

  const handleSelectChange = (event) => {
    setSelectedAddress(event.target.value);
  };

  const handelAddress = () => {
    if (address) {
      dispatch(
        storeSelectedAddress({
          payload: { postCode: address, address: selectedAddress },
        })
      );
      handelNext();
      dispatch(setRoute(ROUTES.infoForm));
    }
  };

  const { getFormDetailsData } = useSelector(
    ({ getFormDetails }) => getFormDetails
  );
  const { options } = useSelector(({ getFormDetails }) => getFormDetails);

  return (
    <section className="section has-text-centered" id="Section25">
      <div className="is-max-smalldesktop">
        <div className="step-title">
          <h1 className="title is-4">
            Finally, enter the post code of the property where we will perform
            the install
          </h1>
        </div>
        <div className="box">
          <div className="card-content">
            <div className="content">
              We use this to assign you a local installer make sure you enter
              the right one, or you'll have to go back and change it later
            </div>
          </div>

          <div></div>
          <form onSubmit={handleSubmit(onSubmit)} className="control">
            <input
              className="input is-large mb-3"
              type="text"
              placeholder="Eg AA9"
              {...register("postalcode", { required: true })}
              onChange={handleInputChange}
            />
            <button type="submit" className="searchButton">
              Search
            </button>
            {postCode &&
              postCode.map((item, index) => {
                return (
                  <select
                    className="selectors"
                    onChange={handleSelectChange}
                    key={index}
                  >
                    <option>Choose Your Address</option>
                    {item &&
                      item.address_components.map((value, i) => {
                        return (
                          <option
                            key={i}
                            value={`${value.long_name} , ${value.types[0]}`}
                          >
                            {`${value.long_name} , ${value.types[0]} `}
                          </option>
                        );
                      })}
                  </select>
                );
              })}
          </form>
          <div className="field">
            <div className="control">
              <button
                onClick={handelAddress}
                disabled={!selectedAddress}
                className="button is-warning is-large is-fullwidth"
              >
                Next
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Form;
