// import React, { useEffect, useRef } from "react";

// function AddressForm() {
//   const address1Field = useRef(null);
//   const address2Field = useRef(null);
//   const postalField = useRef(null);
//   let autocomplete;

//   useEffect(() => {
//     initAutocomplete();
//   }, []);

//   function initAutocomplete() {
//     // Create the autocomplete object, restricting the search predictions to
//     // addresses in the US and Canada.
//     autocomplete = new window.google.maps.places.Autocomplete(
//       address1Field.current,
//       {
//         componentRestrictions: { country: ["us", "ca"] },
//         fields: ["address_components", "geometry"],
//         types: ["address"],
//       }
//     );

//     address1Field.current.focus();

//     // When the user selects an address from the drop-down, populate the
//     // address fields in the form.
//     autocomplete.addListener("place_changed", fillInAddress);
//   }

//   function fillInAddress() {
//     // Get the place details from the autocomplete object.
//     const place = autocomplete.getPlace();
//     let address1 = "";
//     let postcode = "";

//     // Get each component of the address from the place details,
//     // and then fill-in the corresponding field on the form.
//     // place.address_components are google.maps.GeocoderAddressComponent objects
//     // which are documented at http://goo.gle/3l5i5Mr
//     for (const component of place.address_components) {
//       const componentType = component.types[0];

//       switch (componentType) {
//         case "street_number": {
//           address1 = `${component.long_name} ${address1}`;
//           break;
//         }
//         case "route": {
//           address1 += component.short_name;
//           break;
//         }
//         case "postal_code": {
//           postcode = `${component.long_name}${postcode}`;
//           break;
//         }
//         case "postal_code_suffix": {
//           postcode = `${postcode}-${component.long_name}`;
//           break;
//         }
//         case "locality": {
//           document.querySelector("#locality").value = component.long_name;
//           break;
//         }
//         case "administrative_area_level_1": {
//           document.querySelector("#state").value = component.short_name;
//           break;
//         }
//         case "country": {
//           document.querySelector("#country").value = component.long_name;
//           break;
//         }
//         default:
//           break;
//       }
//     }

//     address1Field.current.value = address1;
//     postalField.current.value = postcode;
//     address2Field.current.focus();
//   }

//   return (
//     <div>
//       <form id="address-form" action="" method="get" autoComplete="off">
//         <p className="title">Sample address form for North America</p>
//         <p className="note">
//           <em>* = required field</em>
//         </p>
//         <label className="full-field">
//           <span className="form-label">Deliver to*</span>
//           <input
//             ref={address1Field}
//             id="ship-address"
//             name="ship-address"
//             required
//             autoComplete="off"
//           />
//         </label>
//         <label className="full-field">
//           <span className="form-label">Apartment, unit, suite, or floor #</span>
//           <input ref={address2Field} id="address2" name="address2" />
//         </label>
//         <label className="full-field">
//           <span className="form-label">City*</span>
//           <input id="locality" name="locality" required />
//         </label>
//         <label className="slim-field-left">
//           <span className="form-label">State/Province*</span>
//           <input id="state" name="state" required />
//         </label>
//         <label className="slim-field-right" htmlFor="postal_code">
//           <span className="form-label">Postal code*</span>
//           <input ref={postalField} id="postcode" name="postcode" required />
//         </label>
//         <label className="full-field">
//           <span className="form-label">Country/Region*</span>
//           <input id="country" name="country" required />
//         </label>
//         <button type="button" className="my-button">
//           Save address
//         </button>
//         <input type="reset" value="Clear form" />
//       </form>
//       <img
//         className="powered-by-google"
//         src="https://storage.googleapis.com/geo-devrel-public-buckets/powered_by_google_on_white.png"
//         alt="Powered by Google"
//       />
//       <script
//         src={`https://maps.googleapis.com/maps/api/js?key=AIzaSyAYY2jlGhu3mzukE1ghn0Z0LIt6kkR07zk&callback=initAutocomplete&libraries=places&v=weekly`}
//         defer
//       ></script>
//     </div>
//   );
// }

// export default AddressForm;
