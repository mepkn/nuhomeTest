import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  route: "getQuotes",
};

const routeSlice = createSlice({
  name: "router",
  initialState,
  reducers: {
    setRoute(state, { payload }) {
      state.route = payload;
    },
  },
});

const { actions, reducer } = routeSlice;

export const { setRoute } = actions;
export default reducer;
