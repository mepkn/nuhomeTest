import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setRoute } from "../router/redux/reducer";
import { ROUTES } from "../../utils/constants";
import "./paymentSuccessful.css";
import { Image, Shimmer } from "react-shimmer";
import { SHOW_PAYMENT_DATA } from "./redux/PaymentSuccessAction";

const PaymentSuccessful = () => {
  const dispatch = useDispatch();

  const { PaymentDetailsData } = useSelector(
    ({ getPaymentDetailsData }) => getPaymentDetailsData
  );
  // useEffect(() => {
  //   const div = document.getElementById("animatedDiv");
  //   div.style.opacity = "1"; // Update the opacity to 1 to trigger the animation
  // }, []);

  const handleToProductScreen = () => {
    dispatch(setRoute(ROUTES.getQuotes));
  };
  return (
    <>
      <div className="content">
        <div className="wrapper-1">
          <div className="wrapper-2">
            <Image
              className="thanku_topper"
              src="./image/thank-you.jpg"
              fallback={<Shimmer width={200} height={200} />}
            />
            <h1 className="thank_heading has-text-success">Thank you !</h1>
            <p className="subtitle is-size-4">Continue Shopping with us....</p>
            <a href="#" className="go-home" onClick={handleToProductScreen}>
              go home
            </a>
          </div>
        </div>
      </div>

      {/* <section className="section less-pad-mob">
        <div className="container is-fullhd">
          <div className="columns is-centered is-flex is-flex-direction-column">
            <div className="column">
              <h
                className="title is-size-2"
                style={{
                  animationDuration: "4s",
                  animationDelay: "0.5s",
                  animationFillMode: "forwards",
                  animationTimingFunction: "ease-in-out",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                }}
              >
                Thank you For Shopping with us
              </h>
            </div>
            <div className="column my-5">
              <div
                id="animatedDiv"
                className="is-flex is-justify-content-center is-align-content-center is-flex-direction-column button is-success"
                style={{
                  width: "200px",
                  height: "200px",
                  margin: "auto",
                  borderRadius: "50%",
                }}
              >
                <span></span>
                <span style={{ color: "white" }}>
                  <i class="fas fa-check" style={{ scale: "5.9" }}></i>
                </span>
              </div>
            </div>
            <div className="column my-5">
              <p className="title is-size-5 has-text-danger viewtextinstallation">
                Continue.....
              </p>
            </div>
          </div>
        </div>
      </section> */}
    </>
  );
};

export default PaymentSuccessful;
