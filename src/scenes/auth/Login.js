import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FETCH_CLIENT } from "./redux/LoginAuthAction";
import { type } from "@testing-library/user-event/dist/type";

const Login = () => {
	const dispatch = useDispatch();
	let url = window.location.search;
	const newUrl = url.split("?")[1];
	const param1 = newUrl.split("&&");
	let p1 = param1[0];
	let p2 = param1[1];

	useEffect(() => {
		const payload = {
			client_id: Number(p1),
			end_user_is: p2,
		};
		dispatch({ type: FETCH_CLIENT, payload: payload });
	}, []);
	return (
		<div>
			<div className="loaderContainer">
				<div className="mainLoader"></div>
			</div>
		</div>
	);
};

export default Login;
