<div className="column ">
  <div className="columns ">
    <div className="column  is-half ">
      <div className="columns is-flex is-flex-direction-column is-justify-content-center">
        <div className="column">
          <figure className="imagebox">
            <img
              //   src={item.image_path}
              alt="Placeholder image"
              style={{ height: 200 }}
            />
          </figure>
        </div>

        <div className="column">
          <span className="icon has-text-left">
            <i class="fas fa-check"></i>
          </span>
          <span>Comes with 15 years warranty</span>
        </div>
        <div className="column">
          <span className="icon has-color-danger">
            <i class="fas fa-check"></i>
          </span>
          <span>TRV and lockshield included with every radiator</span>
        </div>
      </div>
    </div>
    <div className="column is-half">
      <div className="columns">
        <div className="column is-flex is-flex-direction-row is-justify-content-space-between p-3">
          <span>Type</span>
          <span
            className="helpchooseSpanText has-text-danger"
            id="helpchooseSpanText"
          >
            Help me Choose
          </span>
        </div>
        <div className="columns px-4">
          <div className="column">
            <select className="input is-medium is-hovered">
              {" "}
              <option value="LowestPrice" disabled>
                Choose type
              </option>
              <option value="HighestPrice">Double Converter K2</option>
              <option value="HotWaterFlowRate">Single Converter K1</option>
            </select>
          </div>
        </div>
        <div className="columns p-3 ">
          <span>
            <i class="fas fa-circle-info"></i>
          </span>
          <span className="subtitle is-7 has-text-weight-light">
            If we don’t offer the exact size you need please choose the nearest
            smaller size to your current radiator.
          </span>
        </div>

        <div className="columns is-flex is-flex-direction-row is-justify-content-space-between px-3 py-1">
          <span className="is-5">Height(mm)</span>
          <span className="has-text-danger is-5" id="helpchooseSpanText">
            How to measure
          </span>
        </div>

        <div className="columns px-4">
          <div className="column">
            <select className="input is-medium is-hovered">
              <option value="LowestPrice" disabled>
                Select Height
              </option>
              <option value="HighestPrice">300</option>
              <option value="HotWaterFlowRate">450</option>
              <option value="HighestPrice">600</option>
              <option value="HotWaterFlowRate">750</option>
            </select>
          </div>
        </div>
        <div className="columns is-flex is-flex-direction-row is-justify-content-space-between px-3 py-1">
          <span className="is-5">Length</span>
        </div>

        <div className="columns px-4">
          <div className="column">
            <select className="input is-medium is-hovered">
              <option value="LowestPrice" disabled>
                Select length
              </option>
              <option value="300">300</option>
              <option value="450">450</option>
              <option value="600">600</option>
              <option value="750">750</option>
            </select>
          </div>
        </div>
        <div className="columns is-flex is-flex-direction-row is-justify-content-space-between p-3">
          <span>Total BTU for this radiator is :</span>
          <span>1233</span>
        </div>

        <div className="columns">
          <div className="column">
            <p className="is-size-3 has-text-success has-text-left has-text-weight-bold">
              £{price}
            </p>
            <p className="has-text-left is-size-7 has-text-success">
              incl VAT & installation
            </p>
          </div>
          <div className="column">
            <p>Quantity</p>
            <div>
              <button onClick={handleSub}>-</button>
              <button>{quantity}</button>
              <button onClick={handleAdd}>+</button>
            </div>
          </div>
        </div>

        <div className="columns is-flex is-flex-direction-row is-justify-content-centered px-5 py-1">
          <span className="is-size-7 has-text-centered ">
            Add up to 10 radiators to your order.
          </span>
        </div>

        <div className="column">
          <button
            className="button is-success is-fullwidth"
            onClick={handleToChooseControl}
          >
            Add to basket{" "}
            <span className="px-3">
              <i class="fas fa-plus"></i>
            </span>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>;
