# TrueQuote-ReactJS

> Making homes a better place to live in by helping customers upgrade their home heating systems hassle-free.

## Built With

```
1. ReactJS
2. Bulma CSS
3. Redux with Saga
```

## Staging Environments

https://cheerful-starburst-cbaab4.netlify.app/

## Install Dependencies

```
yarn install
```

## Run App

```
yarn start
```
